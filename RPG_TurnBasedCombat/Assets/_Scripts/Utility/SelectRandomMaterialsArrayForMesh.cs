using UnityEngine;

//Last Reviewed: 10/30/19

//On Start() chooses a random array of materials for a model
public class SelectRandomMaterialsArrayForMesh : MonoBehaviour
{
    [System.Serializable]
    public class ArrayOfMats
    {
        public Material[] mats = null;
    }

    [SerializeField] ArrayOfMats[] matArrays = null;
    Renderer _renderer;


    void Start()
    {
        _renderer = GetComponent<Renderer>();
        ChooseRandomMat();
    }

    void ChooseRandomMat()
    {
        //create a new array of materials
        Material[] chosenMat = new Material[0];
        //get a material array at random from the list of options and assign it to chosenMat
        chosenMat = matArrays[Random.Range(0, matArrays.Length)].mats;
        //replace the Renderer's materials array with chosenMat[]
        _renderer.materials = chosenMat;
    }
}
