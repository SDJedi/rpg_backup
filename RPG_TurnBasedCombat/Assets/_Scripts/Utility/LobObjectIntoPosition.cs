﻿using System;
using System.Collections;
using UnityEngine;

public class LobObjectIntoPosition : MonoBehaviour
{
    [SerializeField] Vector3 targetPos = Vector3.zero;
    [SerializeField] float firingAngle = 65.0f; // default == 65.0f
    [SerializeField] float gravity = 12.5f; //default == 12.5f

    public Action LobComplete;


    void Start()
    {
        StartCoroutine(Lob(targetPos));
    }

    //move the object in an arc over time from its origin to the target position.
    IEnumerator Lob(Vector3 targetLocation)
    {
        Quaternion startingRotation = transform.rotation;

        // Calculate distance to target
        float target_Distance = Vector3.Distance(transform.position, targetLocation);

        // Calculate the velocity needed to throw the object to the target at specified angle.
        float projectile_Velocity = target_Distance / (Mathf.Sin(2 * firingAngle * Mathf.Deg2Rad) / gravity);

        // Extract the X  Y componenent of the velocity
        float Vx = Mathf.Sqrt(projectile_Velocity) * Mathf.Cos(firingAngle * Mathf.Deg2Rad);
        float Vy = Mathf.Sqrt(projectile_Velocity) * Mathf.Sin(firingAngle * Mathf.Deg2Rad);

        // Calculate flight time.
        float flightDuration = target_Distance / Vx;

        // Rotate projectile to face the target.
        transform.rotation = Quaternion.LookRotation(targetLocation - transform.position);

        float elapsed_time = 0;

        //move the object
        while (elapsed_time < flightDuration)
        {
            transform.Translate(0, (Vy - (gravity * elapsed_time)) * Time.deltaTime, Vx * Time.deltaTime);

            elapsed_time += Time.deltaTime;

            yield return null;
        }

        //object may not land in exactly the right location, so we snap it into place
        transform.position = targetLocation;
        transform.rotation = startingRotation;

        //broadcast that the lob is complete to interested classes
        if (LobComplete != null)
        {
            LobComplete();
        }
    }
}
