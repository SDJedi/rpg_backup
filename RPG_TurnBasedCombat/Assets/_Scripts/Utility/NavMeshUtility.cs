﻿using UnityEngine;
using UnityEngine.AI;

namespace RPG.Utility
{
    public static class NavMeshUtility
    {
        //returns true if we have a complete path that is not too long
        public static bool NavPathIsValid(NavMeshAgent nav, Vector3 desiredPos, float maxNavPathLength)
        {
            if (!nav.isActiveAndEnabled)
            {
                return false;
            }

            NavMeshHit navHit;
            if (!NavMesh.SamplePosition(desiredPos, out navHit, 3f, NavMesh.AllAreas))
            {
                //Debug.Log("Sample Pos: " + NavMesh.SamplePosition(desiredPos, out navHit, 3f, NavMesh.AllAreas));
                return false;
            }

            NavMeshPath path = new NavMeshPath();
            NavMesh.CalculatePath(nav.transform.position, navHit.position, NavMesh.AllAreas, path);
            //no complete path to the NavMesh we hit; return false
            if (path.status != NavMeshPathStatus.PathComplete)
            {
                //Debug.Log("path incomplete");
                nav.SetDestination(nav.transform.position);
                return false;
            }

            //path is complete, but too long; return false
            if (GetPathLength(path) > maxNavPathLength)
            {
                //Debug.Log("path too long");
                return false;
            }

            //path is valid; return true
            return true;
        }

        static float GetPathLength(NavMeshPath path)
        {
            float pathLength = 0;
            if (path.corners.Length < 2) return pathLength;

            for (int i = 0; i < path.corners.Length - 1; i++)
            {
                pathLength += Vector3.Distance(path.corners[i], path.corners[i + 1]);
            }
            //Debug.Log("pathLenth: " + pathLength);
            return pathLength;
        }

        public static Ray GetMouseRay(Camera cam)
        {
            if (cam == null)
            {
                return new Ray();
            }
            return cam.ScreenPointToRay(Input.mousePosition);
        }
    }
}