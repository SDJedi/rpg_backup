using UnityEngine;


public class PlayEndOfAnimationOnEnable : MonoBehaviour
{
    [SerializeField] string animationName = "Die";
    [SerializeField] Animator anim = null;


    void OnEnable()
    {
        if (anim == null)
        {
            anim = GetComponentInChildren<Animator>();
        }

        anim.Play(animationName, 0, 1);
    }
}
