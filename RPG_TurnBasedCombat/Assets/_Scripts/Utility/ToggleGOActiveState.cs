using UnityEngine;

public class ToggleGOActiveState : MonoBehaviour
{
    [SerializeField] GameObject objToToggle = null;
    [SerializeField] KeyCode btnToHold = KeyCode.LeftControl;
    [SerializeField] KeyCode btnToPress = KeyCode.D;


    void Update()
    {
        if (objToToggle == null)
        {
            return;
        }

        if (Input.GetKey(btnToHold) && Input.GetKeyDown(btnToPress))
        {
            objToToggle.SetActive(!objToToggle.activeSelf);
        }
    }
}
