using UnityEngine;

public class TriggerAnimationOnEnable : MonoBehaviour
{
    [SerializeField] Animator anim = null;
    [SerializeField] string trigger = "";


    void OnEnable()
    {
        anim.SetTrigger(trigger);
    }
}
