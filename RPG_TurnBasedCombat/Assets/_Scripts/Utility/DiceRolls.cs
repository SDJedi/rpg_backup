using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DiceRolls
{
    public static int D20()
    {
        int result = Random.Range(1, 21);
        //Debug.Log("D20: " + result);
        return result;
    }
}
