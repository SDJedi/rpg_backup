﻿using UnityEngine;

public class ToggleCursorOnEnable_Disable : MonoBehaviour
{
    void OnEnable()
    {
        Cursor.visible = false;
    }

    void OnDisable()
    {
        Cursor.visible = true;
    }
}
