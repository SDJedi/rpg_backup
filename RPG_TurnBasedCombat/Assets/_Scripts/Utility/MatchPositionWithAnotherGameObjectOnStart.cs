using UnityEngine;

public class MatchPositionWithAnotherGameObjectOnStart : MonoBehaviour
{
    [SerializeField] GameObject otherGO = null;


    void Start()
    {
        gameObject.transform.position = otherGO.transform.position;
    }
}
