using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapMaterialOnTriggerEnter : MonoBehaviour
{
    [SerializeField] MeshRenderer _renderer;
    [SerializeField] Material newMaterial;
    [SerializeField] string tagThatTriggers = "OverWorldPC";


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(tagThatTriggers))
        {
            _renderer.material = newMaterial;
        }
    }
}
