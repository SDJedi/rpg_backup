﻿using System.Collections.Generic;
using UnityEngine;

public class ActivateDeactivateOnTriggerEnter : MonoBehaviour
{
    public enum CollideWith { Any, SpecificLayer, SpecificTag }

    [SerializeField] CollideWith collideWith = CollideWith.Any;
    [SerializeField] bool activateAudioSource = true;
    [SerializeField] string tagToHit = "";
    [SerializeField] int layerToHit = 0;
    [SerializeField] string tagToIgnore = "";

    [SerializeField] List<GameObject> objectsToActivate = null;
    [SerializeField] List<GameObject> objectsToDeactivate = null;
    [SerializeField] List<MonoBehaviour> componentsToActivate = null;
    [SerializeField] List<MonoBehaviour> componentsToDeactivate = null;


    void OnTriggerEnter(Collider other)
    {
        int layer = other.gameObject.layer;
        string tag = other.gameObject.tag;

        switch (collideWith)
        {
            case CollideWith.SpecificLayer:
                if (layerToHit == layer)
                {
                    ActivateDeactivateObjects();
                }
                break;
            case CollideWith.SpecificTag:
                if (tagToHit == tag)
                {
                    ActivateDeactivateObjects();
                }
                break;
            default:
                if (tag != tagToIgnore)
                {
                    ActivateDeactivateObjects();
                }
                break;
        }
    }

    void ActivateDeactivateObjects()
    {
        AudioSource src = GetComponent<AudioSource>();
        if (activateAudioSource && src != null)
        {
            src.enabled = true;
        }

        foreach (GameObject obj in objectsToActivate)
        {
            obj.SetActive(true);
        }

        foreach (GameObject obj in objectsToDeactivate)
        {
            obj.SetActive(false);
        }

        foreach (MonoBehaviour comp in componentsToDeactivate)
        {
            comp.enabled = false;
        }

        foreach (MonoBehaviour comp in componentsToActivate)
        {
            comp.enabled = true;
        }
    }
}
