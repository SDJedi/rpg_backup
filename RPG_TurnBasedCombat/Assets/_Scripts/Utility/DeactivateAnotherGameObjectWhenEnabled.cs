using UnityEngine;

//Last Reviewed: 9/24/19

public class DeactivateAnotherGameObjectWhenEnabled : MonoBehaviour
{
    [SerializeField] GameObject objToDeactivate = null;

    void OnEnable()
    {
        objToDeactivate.SetActive(false);
    }
}
