﻿using UnityEngine;

public class PlayAudioClip : MonoBehaviour
{
    [SerializeField] AudioSource audioSource = null;
    [SerializeField] AudioClip clipToPlay = null;


    //can be called by an animation event ... see Crocasaur_Special_01 animation 
    public void PlayClip()
    {
        audioSource.PlayOneShot(clipToPlay);
    }
}
