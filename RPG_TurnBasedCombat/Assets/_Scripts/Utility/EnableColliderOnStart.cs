using System.Collections;
using UnityEngine;

public class EnableColliderOnStart : MonoBehaviour
{
    [SerializeField] Collider col = null;
    [SerializeField] float delay = 0;


    void Start()
    {
        StartCoroutine(EnableCol());
    }

    IEnumerator EnableCol()
    {
        yield return new WaitForSeconds(delay);
        //col.enabled = false; //turn off first in case we need to trigger an OnTriggerEnter
        col.enabled = true;
    }
}
