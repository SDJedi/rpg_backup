using RPG.InteractableSystem;
using RPG.SceneControl;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ResetAllGameSwitchesOnEnable : MonoBehaviour
{
    void OnEnable()
    {
        SceneController.Instance.AfterSceneLoad += ResetSwitches;
    }

    void OnDisable()
    {
        if (SceneController.InstanceExists)
        {
            SceneController.Instance.AfterSceneLoad -= ResetSwitches;
        }
    }

    void ResetSwitches()
    {
        if (SceneManager.GetActiveScene().name == "MainMenu")
        {
            AllGameSwitches AGS = AllGameSwitches.Instance;
            AGS.Reset();
        }
    }
}
