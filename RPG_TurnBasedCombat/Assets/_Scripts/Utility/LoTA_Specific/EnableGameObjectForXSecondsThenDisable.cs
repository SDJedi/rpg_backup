using System.Collections;
using UnityEngine;


public class EnableGameObjectForXSecondsThenDisable : MonoBehaviour
{
    [SerializeField] GameObject objectToActivate = null;
    [SerializeField] float timeToStayActive = 2;


    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "OverWorldPC")
        {
            objectToActivate.SetActive(true);
            StartCoroutine(DeactivateAfterXSeconds());
        }
    }

    IEnumerator DeactivateAfterXSeconds()
    {
        float timeRemaining = timeToStayActive;

        while (timeRemaining > 0)
        {
            timeRemaining -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        objectToActivate.SetActive(false);
    }
}
