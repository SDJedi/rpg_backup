using RPG.InteractableSystem;
using UnityEngine;

public class SetGameSwitchOnEnable : MonoBehaviour
{
    [SerializeField] Condition switchToSet = null;
    [SerializeField] bool value = true;


    void OnEnable()
    {
        AllGameSwitches.TrySetCondition(switchToSet, value);
    }
}
