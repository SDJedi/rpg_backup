using RPG.ProgressionSystem;
using RPG.QuestSystem;
using UnityEngine;

//Last Reviewed: 1/12/22

//show the "red dot" in the top-right corner of a button when the player has unspent resources
public class UnspentResourcesNotificationHandler : MonoBehaviour
{
    [SerializeField] GameObject unspentCPNotification;
    [SerializeField] GameObject unspentQuestNotification;


    void OnEnable()
    {
        InvokeRepeating("UpdateNotifications", 0, 1);
    }

    void OnDisable()
    {
        CancelInvoke();
    }

    void UpdateNotifications()
    {
        unspentCPNotification.SetActive(CharacterPoints_Tracker.Instance.CurrentCP > 0);
        unspentQuestNotification.SetActive(QuestManager.Instance.PlayerHasUnclaimedRewards);
    }
}