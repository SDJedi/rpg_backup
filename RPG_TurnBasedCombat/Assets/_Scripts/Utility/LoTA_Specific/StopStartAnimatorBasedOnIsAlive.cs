using RPG.CombatSystem;
using UnityEngine;
//Last Reviewed: 9/10/19

//used to stop Lisa's wings from flapping when she's dead
public class StopStartAnimatorBasedOnIsAlive : MonoBehaviour
{
    Character CScript;
    Animator anim;


    void Start()
    {
        CScript = GetComponentInParent<Character>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (CScript.IsAlive)
        {
            anim.enabled = true;
        }
        else
        {
            anim.enabled = false;
        }
    }
}
