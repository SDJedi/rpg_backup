using RPG.InteractableSystem;
using UnityEngine;

public class ActivateOrDeactivateGameObjectsBasedOnSwitches : MonoBehaviour
{
    [System.Serializable]
    public class ConditionValuePairs
    {
        public Condition condition;
        public bool value;
    }

    [SerializeField] ConditionValuePairs[] conditionsToCheck = null;

    [Header("Activate these objects if all conditions met")]
    [SerializeField] GameObject[] objectsToActivate = null;

    [Header("Deactivate these objects if all conditions met")]
    [SerializeField] GameObject[] objectsToDeactivate = null;


    void Start()
    {
        foreach (ConditionValuePairs pair in conditionsToCheck)
        {
            if (pair.condition.Satisfied != pair.value)
                return;
        }

        foreach (GameObject obj in objectsToActivate)
        {
            obj.SetActive(true);
        }

        foreach (GameObject obj in objectsToDeactivate)
        {
            obj.SetActive(false);
        }
    }
}
