using UnityEngine;

//Last Reviewed: 11/18/19

public class PlayAnimationOnTriggerEnter : MonoBehaviour
{
    [SerializeField] Animator anim = null;
    [SerializeField] string animStateName = "Special1";


    void OnTriggerEnter(Collider other)
    {
        if (anim == null)
        {
            print("No animator to trigger animation from");
            return;
        }

        if (other.tag == "OverWorldPC")
        {
            anim.Play(animStateName);
        }
    }
}
