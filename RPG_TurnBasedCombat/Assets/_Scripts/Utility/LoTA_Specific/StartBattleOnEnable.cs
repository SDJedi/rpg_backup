using System.Collections.Generic;
using RPG.CombatSystem;
using UnityEngine;

public class StartBattleOnEnable : MonoBehaviour
{
    [SerializeField] EnemyEncounter encounter = null;
    [SerializeField] bool playStartBattleSFX = true;

    void OnEnable()
    {
        List<EncounterAndSpawnChance> eList = new List<EncounterAndSpawnChance>();
        eList.Add(new EncounterAndSpawnChance());
        eList[0].Encounter = encounter;
        
        RandomEncounterManager.Instance.SetPossibleEncountersList(eList);
        RandomEncounterManager.Instance.LoadCombatScene(playStartBattleSFX);
    }
}
