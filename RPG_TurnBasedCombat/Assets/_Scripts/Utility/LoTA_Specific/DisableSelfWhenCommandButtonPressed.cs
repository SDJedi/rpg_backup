using RPG.CombatSystem;
using UnityEngine;

//currently used to hide the in-combat item info panel when a command is pressed
public class DisableSelfWhenCommandButtonPressed : MonoBehaviour
{
    void Start()
    {
        ButtonCommandManager.CommandButtonPressed += DisableSelf;
    }

    void OnDestroy()
    {
        ButtonCommandManager.CommandButtonPressed -= DisableSelf;
    }

    public void DisableSelf(BaseCommand cmnd)
    {
        gameObject.SetActive(false);
    }
}
