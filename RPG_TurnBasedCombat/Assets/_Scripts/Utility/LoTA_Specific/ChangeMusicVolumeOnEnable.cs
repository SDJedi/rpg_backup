using UnityEngine;

public class ChangeMusicVolumeOnEnable : MonoBehaviour
{
    [Range(0, 1)]
    [SerializeField] float volume = 0.1f;


    void OnEnable()
    {
        SoundManager.Instance.AdjustMusicVolume(volume);
    }
}
