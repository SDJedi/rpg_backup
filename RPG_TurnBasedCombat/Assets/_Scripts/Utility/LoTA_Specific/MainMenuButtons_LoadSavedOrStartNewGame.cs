using RPG.Saving;
using UnityEngine;

//Last Reviewed: 9/13/19

//used by MainMenu buttons
public class MainMenuButtons_LoadSavedOrStartNewGame : MonoBehaviour
{
    [SerializeField] int index = -1; //0 == autosave / 1 == save1 / 2 == save2


    public void TryLoadGame()
    {
        SavingWrapper sw = GameObject.FindObjectOfType<SavingWrapper>();
        sw.TryLoadSavedGame(index);
    }

    public void _StartNewGame()
    {
        GameObject.FindObjectOfType<SavingWrapper>().StartNewGame();
    }
}