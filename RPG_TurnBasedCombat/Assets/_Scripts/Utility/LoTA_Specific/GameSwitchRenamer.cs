using RPG.InteractableSystem;
using UnityEditor;
using UnityEngine;

#if (UNITY_EDITOR)
public class GameSwitchRenamer : ResettableScriptableObject
{
    const string loadPath = "GameSwitchRenamer";
    static GameSwitchRenamer instance;

    [SerializeField] int indexInAllGameSwitches = 0;
    [SerializeField] string newDescription = "";

    public static GameSwitchRenamer Instance
    {
        get
        {
            if (!instance)
                instance = FindObjectOfType<GameSwitchRenamer>();
            if (!instance)
                instance = Resources.Load<GameSwitchRenamer>(loadPath);
            if (!instance)
                Debug.LogError("A \"GameSwitchRenamer\" asset has not been created yet. Go to Assets > Create > GameSwitchRenamer. (Asset will be placed in Resources folder automatically).");
            return instance;
        }
        set
        {
            if (instance == null)
            {
                instance = value;
            }
        }
    }

    public void RenameSwitch()
    {
        if (newDescription != "" && indexInAllGameSwitches >= 0 && AllGameSwitches.Instance != null && indexInAllGameSwitches < AllGameSwitches.Instance.conditions.Length)
        {
            foreach (Condition c in AllGameSwitches.Instance.conditions)
            {
                if (newDescription == c.description)
                {
                    Debug.Log("description already exists");
                    return;
                }
            }
            string oldDescription = AllGameSwitches.Instance.conditions[indexInAllGameSwitches].description;
            AllGameSwitches.Instance.conditions[indexInAllGameSwitches].description = newDescription;
            Debug.Log("The switch at index [" + indexInAllGameSwitches + "], " + oldDescription + ", was renamed to " + newDescription);
            Reset();
            MatchAllNames();
            AllGameSwitches.SortConditionsAlphabetically();
            return;
        }
        Debug.LogWarning("Rename failed");
        Reset();
    }

    public override void Reset()
    {
        indexInAllGameSwitches = 0;
        newDescription = "";
    }

    //makes the names of the assets in AllGameSwitches match the description
    public void MatchAllNames()
    {
        Debug.Log("Matching names");
        foreach (Condition c in AllGameSwitches.Instance.conditions)
        {
            c.name = c.description;
            AssetDatabase.SaveAssets();
        }
    }
}
#endif
