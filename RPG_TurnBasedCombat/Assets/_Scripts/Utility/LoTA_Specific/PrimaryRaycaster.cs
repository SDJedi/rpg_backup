using System;
using RPG.CombatSystem;
using RPG.InteractableSystem;
using RPG.Utility;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

//Last Reviewed: 12/30/19

public enum CursorType { None, Movement, Dialogue, UI, Interactable, Enemy, PC }


public class PrimaryRaycaster : Singleton<PrimaryRaycaster>
{
    [System.Serializable]
    public struct CursorMapping
    {
        public CursorType cType;
        public Texture2D cTexture;
        public Vector2 cHotSpot;
    }

    [SerializeField] CursorMapping[] cursorMaps = null;

    StandAloneInputModuleCustom customIM; //necessary to get the object that the mouse is hovering over
    GameObject overWorldPC;
    NavMeshAgent nav;
    Interactable interactableMouseIsOver;
    float maxNavPath;

    public CursorMapping[] CursorMaps { get { return cursorMaps; } }
    public Interactable InteractableMouseIsOver { get { return interactableMouseIsOver; } }
    public bool MouseOverInteractable { get { return HandleInteractable(); } }
    public bool MouseOverReachableTerrain { get { return HandleUI() == false && HandleInteractable() == false && HandleMovement(); } }


    void Start()
    {
        customIM = EventSystem.current.GetComponent<StandAloneInputModuleCustom>();
    }

    //if we're in a scene with the OverWorldPC, we need to deal with the movement cursor
    public void SetOverWorldPC(GameObject PC)
    {
        overWorldPC = PC;
        if (overWorldPC != null)
        {
            maxNavPath = overWorldPC.GetComponent<PC_MoveOrStartInteraction>().MaxNavPathLength;
            nav = overWorldPC.GetComponent<NavMeshAgent>();
        }
    }

    void Update()
    {
        //If mouse is over UI...(All Scenes)
        if (HandleUI())
        {
            return;
        }

        //If mouse is over a PC or an enemy...(CombatScene)
        if (HandleCharacter())
        {
            return;
        }

        //If mouse is over an Interactable...(OverWorld)
        if (HandleInteractable())
        {
            return;
        }

        //If mouse is over terrain we can walk to...(OverWorld)
        if (HandleMovement())
        {
            return;
        }

        //don't use "none" cursor in combat and main menu screens
        if (HandleDefaultForCombatSceneAndMainMenu())
        {
            return;
        }

        SetCursor(CursorType.None);
    }

    bool HandleUI()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            return false;
        }

        foreach (GameObject obj in customIM.GetLastPointerEventDataPublic().hovered)
        {
            if (obj.layer == LayerMask.NameToLayer("UI"))
            {
                SetCursor(CursorType.UI);
                return true;
            }
        }

        return false;
    }

    bool HandleCharacter()
    {
        if (SceneManager.GetActiveScene().name != "CombatScene")
        {
            return false;
        }

        RaycastHit[] hits = RaycastAll_Sorted();

        foreach (RaycastHit hit in hits)
        {
            EnemyCharacter[] enemies = hit.transform.GetComponentsInChildren<EnemyCharacter>();
            foreach (EnemyCharacter enemy in enemies)
            {
                SetCursor(CursorType.Enemy);
                return true;
            }
        }

        foreach (RaycastHit hit in hits)
        {
            PlayerCharacter[] PCs = hit.transform.GetComponentsInParent<PlayerCharacter>();
            foreach (PlayerCharacter PC in PCs)
            {
                SetCursor(CursorType.PC);
                return true;
            }
        }

        return false;
    }

    bool HandleInteractable()
    {
        RaycastHit[] hits = RaycastAll_Sorted();
        foreach (RaycastHit hit in hits)
        {
            //Debug.Log(hit.transform.gameObject);
            Interactable[] interactables = hit.transform.GetComponentsInChildren<Interactable>();
            foreach (Interactable interactable in interactables)
            {
                if (interactable.cursorInfo.cTexture != null)
                {
                    SetCursor(interactable.cursorInfo.cTexture, interactable.cursorInfo.cHotSpot);
                }
                else
                {
                    SetCursor(CursorType.Interactable);
                }
                interactableMouseIsOver = interactable;
                return true;
            }
        }
        interactableMouseIsOver = null;
        return false;
    }

    bool HandleMovement()
    {
        if (overWorldPC == null || !overWorldPC.activeSelf)
        {
            return false;
        }

        RaycastHit[] hits = RaycastAll_Sorted();
        bool hasHit = hits.Length > 0;


        if (!hasHit || !NavMeshUtility.NavPathIsValid(nav, hits[0].point, maxNavPath))
        {
            if (hasHit)
            {
                //Debug.Log(hits[0].transform.gameObject.name);
                //Debug.Log("NavPathIsValid == " + NavMeshUtility.NavPathIsValid(nav, hits[0].point, maxNavPath));
            }
            else
            {
                //Debug.Log("????");
            }
            return false;
        }

        SetCursor(CursorType.Movement);

        return true;
    }

    bool HandleDefaultForCombatSceneAndMainMenu()
    {
        string activeScene = SceneManager.GetActiveScene().name;

        if (activeScene == "CombatScene" || activeScene == "MainMenu" || activeScene == "CharacterSelectScene" || activeScene == "CharacterPointsScene" || activeScene == "MiniGame_ArmWrestle")
        {
            SetCursor(CursorType.UI);
            return true;
        }
        return false;
    }

    RaycastHit[] RaycastAll_Sorted()
    {
        RaycastHit[] hits = Physics.RaycastAll(NavMeshUtility.GetMouseRay(Camera.main));
        float[] distances = new float[hits.Length];
        for (int i = 0; i < hits.Length; i++)
        {
            distances[i] = hits[i].distance;
        }
        Array.Sort(distances, hits);
        return hits;
    }

    void SetCursor(CursorType type)
    {
        CursorMapping mapping = GetCursorMapping(type);
        Cursor.SetCursor(mapping.cTexture, mapping.cHotSpot, CursorMode.Auto);
    }

    void SetCursor(Texture2D texture, Vector2 hotSpot)
    {
        Cursor.SetCursor(texture, hotSpot, CursorMode.Auto);
    }

    CursorMapping GetCursorMapping(CursorType type)
    {
        foreach (CursorMapping cMap in cursorMaps)
        {
            if (cMap.cType == type)
            {
                return cMap;
            }
        }
        return cursorMaps[0];
    }
}
