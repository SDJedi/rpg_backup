using UnityEngine;

public class SelfDestruct : MonoBehaviour
{
    public float Timer;
    void Start()
    {
        Destroy(gameObject, Timer);
    }
}
