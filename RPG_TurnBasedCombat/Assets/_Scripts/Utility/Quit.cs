using UnityEngine;


public class Quit : MonoBehaviour
{
    public void QuitGame()
    {
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #elif UNITY_WEBGL
        //do nothing
        #else
        Application.Quit();
        #endif
    }
}
