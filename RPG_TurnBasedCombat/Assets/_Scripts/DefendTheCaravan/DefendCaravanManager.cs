﻿using System.Collections;
using RPG.InteractableSystem;
using RPG.Messaging;
using RPG.Saving;
using UnityEngine;

//Last Reviewed: 11/25/20
public class DefendCaravanManager : Singleton<DefendCaravanManager>, ISaveable
{
    [SerializeField] TextManager txtManager; //used to display messages as tasks are completed
    [SerializeField] GameObject taskPanel; //shows the tasks
    [SerializeField] GameObject[] tasks = null; //UI objects that contain the checkboxes and text for each of the tasks

    //interactables that get switched on/off during event...
    //NOTE: seperate from the objects that are being rendered in scene so that the objects stay visible
    //though interaction is disabled
    [SerializeField] GameObject tent_0 = null;
    [SerializeField] GameObject tent_1 = null;
    [SerializeField] GameObject tent_2 = null;
    [SerializeField] GameObject fire = null;
    [SerializeField] GameObject horses = null;
    [SerializeField] GameObject supplies = null;
    [SerializeField] GameObject cinematicTrigger = null;

    //bools used to track event progress. 
    //Get tracked by save system in case player leaves the scene (i.e. goes to character select or progression)
    bool checkedSkelli_0 = false;
    bool checkedSkelli_1 = false;
    bool checkedSkelli_2 = false;
    bool checkedFire = false;
    bool checkedHorses = false;


    void OnEnable()
    {
        ReactionCollection.reactionTriggered += ReactionTriggered;
    }

    void OnDisable()
    {
        ReactionCollection.reactionTriggered -= ReactionTriggered;
    }

    //called whenever a reaction is triggered ... i.e. click on tents, fire, horses, supplies
    void ReactionTriggered(GameObject callingObj)
    {
        //when tents are clicked...
        if (callingObj.GetInstanceID() == tent_0.GetInstanceID())
        {
            CheckOnSkelli(0);
        }
        else if (callingObj.GetInstanceID() == tent_1.GetInstanceID())
        {
            CheckOnSkelli(1);
        }
        else if (callingObj.GetInstanceID() == tent_2.GetInstanceID())
        {
            CheckOnSkelli(2);
        }
        //fire clicked...
        else if (callingObj.GetInstanceID() == fire.GetInstanceID())
        {
            checkedFire = true;
            txtManager.DisplayMessage("The fire has plenty of wood.", Color.green);
            fire.SetActive(false);
            horses.SetActive(true);
            CheckOffTask(1);
            ShowTask(2);
        }
        //horses clicked
        else if (callingObj.GetInstanceID() == horses.GetInstanceID())
        {
            checkedHorses = true;
            txtManager.DisplayMessage("The horses look fine.", Color.green);
            horses.SetActive(false);
            supplies.SetActive(true);
            cinematicTrigger.SetActive(true);
            CheckOffTask(2);
            ShowTask(3);
        }
    }

    IEnumerator ShowSkelliMssgAndActivateFireIfReady()
    {
        if (AllSkellisChecked())
        {
            CheckOffTask(0);
            yield return new WaitForSeconds(1.25f);
            txtManager.DisplayMessage("Everyone is fast asleep.", Color.green, 0, 0, true);
            if (!fire.activeSelf && !tasks[1].activeSelf && !checkedFire)
            {
                fire.SetActive(true);
                ShowTask(1);
            }
        }
    }

    //add check mark UI for a task
    void CheckOffTask(int index)
    {
        tasks[index].transform.Find("Checked").gameObject.SetActive(true);
    }

    //when a task is completed, show the next task on the list.
    void ShowTask(int index)
    {
        tasks[index].SetActive(true);
    }

    //called when tents are clicked on
    void CheckOnSkelli(int num)
    {
        switch (num)
        {
            case 0:
                checkedSkelli_0 = true;
                tent_0.SetActive(false);
                break;
            case 1:
                checkedSkelli_1 = true;
                tent_1.SetActive(false);
                break;
            case 2:
                checkedSkelli_2 = true;
                tent_2.SetActive(false);
                break;
        }
        StartCoroutine(ShowSkelliMssgAndActivateFireIfReady());
    }

    //returns true if all 3 tents have been checked
    bool AllSkellisChecked()
    {
        return checkedSkelli_0 && checkedSkelli_1 && checkedSkelli_2;
    }

    [System.Serializable]
    class CaravanSaveData
    {
        public bool checked_S_0;
        public bool checked_S_1;
        public bool checked_S_2;
        public bool checkedFire;
        public bool checkedHorses;
    }

    public object CaptureState()
    {
        CaravanSaveData saveData = new CaravanSaveData();
        saveData.checked_S_0 = checkedSkelli_0;
        saveData.checked_S_1 = checkedSkelli_1;
        saveData.checked_S_2 = checkedSkelli_2;
        saveData.checkedFire = checkedFire;
        saveData.checkedHorses = checkedHorses;

        return saveData;
    }

    public void RestoreState(object state)
    {
        CaravanSaveData saveData = (CaravanSaveData)state;

        checkedSkelli_0 = saveData.checked_S_0;
        checkedSkelli_1 = saveData.checked_S_1;
        checkedSkelli_2 = saveData.checked_S_2;
        checkedFire = saveData.checkedFire;
        checkedHorses = saveData.checkedHorses;
    }
}
