using UnityEngine;

//Last Reviewed: 1/20/20
namespace RPG.DialogueSystem
{
    public class TestCombatDialogue : MonoBehaviour
    {
        public CombatDialogueManager manager;
        public Sprite portrait;
        public string speakerName;
        [TextArea]
        public string dialogue;

        public void Talk()
        {
            manager.SaySomethingInCombat(portrait, speakerName, dialogue, 0);
        }
    }
}
