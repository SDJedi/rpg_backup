using RPG.Messaging;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

//Last Reviewed: 1/20/20
namespace RPG.DialogueSystem
{
    //allows for characters to talk trash during combat.
    public class CombatDialogueManager : Singleton<CombatDialogueManager>
    {
        [SerializeField] GameObject mainCombatDialoguePanel = null;
        [SerializeField] Image speakerPortrait = null;
        [SerializeField] TextMeshProUGUI speakerName = null;

        TextManager combatDialogueTextManager;
        Color txtColor;


        protected override void Awake()
        {
            base.Awake();
            combatDialogueTextManager = GetComponentInChildren<TextManager>();
            txtColor = combatDialogueTextManager.text.color;
        }

        void Update()
        {
            if (combatDialogueTextManager.IsDisplayingText)
            {
                mainCombatDialoguePanel.SetActive(true);
            }
            else
            {
                mainCombatDialoguePanel.SetActive(false);
            }
        }

        public void SaySomethingInCombat(Sprite portrait, string name, string dialogue, float minDisplayTime = 0)
        {
            speakerPortrait.sprite = portrait;
            speakerName.text = name + ":";
            combatDialogueTextManager.DisplayMessage(dialogue, txtColor, 0, minDisplayTime);
        }
    }
}
