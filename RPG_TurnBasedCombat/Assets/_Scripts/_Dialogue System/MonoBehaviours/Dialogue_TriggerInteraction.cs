﻿using RPG.InteractableSystem;
using UnityEngine;

//Last Reviewed: 11/18/2020
namespace DialogueSystem
{
    //cause a specific dialogue node to trigger an interaction
    [RequireComponent(typeof(Interactable))]
    public class Dialogue_TriggerInteraction : MonoBehaviour
    {
        [SerializeField] Dialogue dialogue = null;
        [SerializeField] string nodeTag = ""; //node that will cause an interaction
        Interactable interactable;


        void Start() //changed from OnEnable on 12/11/20 ... check for issues
        {
            //get notified when continue/end or a choice is made in dialogue
            if (dialogue == null)
            {
                //try to find a dialogue if one isn't specified
                Debug.LogWarning("please assign a specific dialogue to " + gameObject.name + " in inspector.");
                dialogue = GetComponentInParent<DialogueComponent>().GetDialogue();
                if (dialogue != null)
                {
                    DialogueManager.Instance.nodeSelected += Interact;
                    interactable = GetComponent<Interactable>();
                }
            }
            else
            {
                DialogueManager.Instance.nodeSelected += Interact;
                interactable = GetComponent<Interactable>();
            }
        }

        void OnDestroy() //changed from OnDisable on 12/11/20 ... check for issues
        {
            if (DialogueSystem.DialogueManager.InstanceExists)
            {
                DialogueManager.Instance.nodeSelected -= Interact;
            }
        }

        //trigger Interact (can happen when selection is pressed, or continue/end depending on node)
        void Interact(DialogueNode node)
        {
            if (dialogue != null && dialogue.GetAllNodes().Contains(node))
            {
                string tag = node.GetTag();
                if (tag != "" && tag == nodeTag && interactable != null)
                {
                    //Debug.Log(node.GetTag() + "   ::    " + node.GetID());
                    interactable.Interact();
                }
            }
        }
    }
}
