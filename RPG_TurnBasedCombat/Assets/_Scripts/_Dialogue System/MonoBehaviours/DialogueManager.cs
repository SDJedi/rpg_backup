﻿using System.Collections.Generic;
using System.Linq;
using RPG.QuestSystem;
using RPG.SceneControl;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DialogueSystem
{
    public enum ContinueOrEnd { Default, Continue, End }

    public class DialogueManager : Singleton<DialogueManager>
    {
        Dialogue currentDialogue;
        DialogueNode currentNode;

        //hook up UI in inspector / see MainDialoguePanel prefab
        [SerializeField] GameObject mainDialoguePanel = null;
        [SerializeField] TextMeshProUGUI speakerName_Txt = null;
        [SerializeField] TextMeshProUGUI dialogue_Txt = null;
        [SerializeField] GameObject choicePanel = null;
        [SerializeField] GameObject continueOrEnd = null;
        [SerializeField] Button X_Button = null;

        [SerializeField] GameObject choiceBtnPrefab = null; //seperate prefab

        bool movedCamForConvo;

        public bool InConversation { get; private set; }
        public bool playerIsChoosing { get; private set; }
        public bool allowPlayerToEndConversationEarly { get; private set; } = true;

        public delegate void NodeSelected(DialogueNode node);
        public event NodeSelected nodeSelected;

        // public delegate void ConversationStarted();
        // public event ConversationStarted conversationStareted;

        // public delegate void ConversationEnded();
        // public event ConversationEnded conversationEnded;


        void Start()
        {
            continueOrEnd.GetComponentInChildren<Button>().onClick.AddListener(_ContinueOrEnd);
            X_Button.onClick.AddListener(EndConversation);
            //whenever a scene unloads, end any active conversation 
            SceneController.Instance.BeforeSceneUnload += OnSceneChange;
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape) && InConversation && allowPlayerToEndConversationEarly)
            {
                EndConversation();
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (SceneController.InstanceExists)
            {
                SceneController.Instance.BeforeSceneUnload -= OnSceneChange;
            }
        }

        void OnSceneChange()
        {
            if (mainDialoguePanel.activeInHierarchy)
            {
                EndConversation();
            }
        }

        //Can be called via an Interactable using a StartConversationReaction, nodeTag is the node where the conversation will start
        public void StartConversation(GameObject npc, string nodeTag = "", bool endEarly = true, bool useConvoCam = true)
        {
            if (OkToStartAConversation(npc)) //error checking: ensures we have an npc with a dialogue component
            {
                HandleCamera(useConvoCam);

                SetContinueOrEndButtonText(DialogueSystem.ContinueOrEnd.Continue); //reset the text of Continue/End btn
                SetCurrentDialogue(npc.GetComponent<DialogueComponent>());

                allowPlayerToEndConversationEarly = endEarly;
                X_Button.gameObject.SetActive(endEarly);

                // if (conversationStareted != null)
                // {
                //     conversationStareted();
                // }

                InConversation = true; //flag is used to prevent certain actions, such as moving or opening inv, quest journal, or character selects scene
                mainDialoguePanel.SetActive(true); //show the panel

                //grab the root node by default
                currentNode = currentDialogue.GetRootNode();
                //use a node with a specific tag to start the conversation instead of the root node
                if (nodeTag != "")
                {
                    currentNode = currentDialogue.GetNodeByTag(nodeTag);
                    if (currentNode == null)
                    {
                        Debug.LogWarning("Could not find a DialogueNode with the tag: " + nodeTag + ". Loading root node.");
                        currentNode = currentDialogue.GetRootNode();
                    }
                }

                UpdateUI();
            }
        }

        void HandleCamera(bool useConvoCam)
        {
            if (!InConversation && useConvoCam)
            {
                movedCamForConvo = true;
                GameObject.FindObjectOfType<CameraZoomControls>().MoveCamToConversationPosition();
            }
            else if (!InConversation)
            {
                movedCamForConvo = false;
            }
        }

        //called on button press / End or Continue based on the text of the button. 
        //also called on choice selection
        void _ContinueOrEnd()
        {
            //call any subscribers to nodeSelected (see Dialogue_TriggerInteraction.cs)
            if (nodeSelected != null)
            {
                nodeSelected(currentNode);
            }

            //End
            if (continueOrEnd.GetComponentInChildren<TextMeshProUGUI>().text == "End" || currentNode.GetChildIDs().Count < 1)
            {
                EndConversation();
                return;
            }

            //Continue
            List<DialogueNode> children = currentDialogue.GetAllChildren(currentNode).ToList();

            if (children.Count > 0)
            {
                currentNode = children[UnityEngine.Random.Range(0, children.Count)];
            }

            UpdateUI();
        }

        void SelectChoice(DialogueNode node)
        {
            currentNode = node;
            _ContinueOrEnd();
        }

        void UpdateUI()
        {
            Set_SpeakerNameTxt(currentNode.GetSpeakerName()); //set the "SpeakerName" text
            Set_DialogueText(currentNode.GetText()); //set the "dialogue" text
            DisableDialogueChoices(); //destroy any choice buttons that currently exist; these get rebuilt further down if needed
            playerIsChoosing = false;

            //if currentNode has children, there is more dialogue...
            if (NodeHasChildren(currentNode))
            {
                //is it time for player choice, or does npc have more to say first?
                if (NodeHasChoices(currentNode))
                {
                    //...time for player choice: show the choices / hide the End/Continue Button
                    BuildChoiceList(currentNode);
                    Show_Continue_Or_End_Btn(false);
                    playerIsChoosing = true;
                }
                else
                {
                    //...npc has more to say: show Continue button unless otherwise specified
                    if (currentNode.GetContinueOrEnd() != ContinueOrEnd.End)
                    {
                        SetContinueOrEndButtonText(DialogueSystem.ContinueOrEnd.Continue);
                    }
                    else
                    {
                        SetContinueOrEndButtonText(DialogueSystem.ContinueOrEnd.End);
                    }
                    Show_Continue_Or_End_Btn(true);
                }
            }
            //currentNode has no children; show End button unless otherwise specified
            else
            {
                if (currentNode.GetContinueOrEnd() != ContinueOrEnd.Continue)
                {
                    SetContinueOrEndButtonText(DialogueSystem.ContinueOrEnd.End);
                }
                else
                {
                    SetContinueOrEndButtonText(DialogueSystem.ContinueOrEnd.Continue);
                }
                Show_Continue_Or_End_Btn(true);
            }
            //Refresh the UI layout
            LayoutRebuilder.ForceRebuildLayoutImmediate(mainDialoguePanel.GetComponent<RectTransform>());
        }

        //Note: when called by X_Btn, ignores any reactions associated with the current node
        //must press Continue _End or a choice selection to get reactions to fire
        public void EndConversation()
        {
            // if (conversationEnded != null)
            // {
            //     conversationEnded();
            // }

            currentDialogue = null;
            currentNode = null;
            DisableDialogueChoices();
            mainDialoguePanel.SetActive(false);
            InConversation = false;
            allowPlayerToEndConversationEarly = true; //reset this flag to default of true whenever a conversation ends

            var zoomCntrl = GameObject.FindObjectOfType<CameraZoomControls>();

            if (zoomCntrl != null && movedCamForConvo)
            {
                zoomCntrl.RestoreCamToPreviousPosition();
            }
            movedCamForConvo = false;
        }

        #region Helper methods
        void Set_SpeakerNameTxt(string name)
        {
            speakerName_Txt.text = name;
        }

        void Set_DialogueText(string dialogue)
        {
            dialogue_Txt.text = dialogue;
        }

        void SetCurrentDialogue(DialogueComponent dc)
        {
            currentDialogue = dc.GetDialogue();
        }

        void SetContinueOrEndButtonText(ContinueOrEnd value)
        {
            if (value == DialogueSystem.ContinueOrEnd.End)
            {
                continueOrEnd.GetComponentInChildren<TextMeshProUGUI>().text = "End";
                return;
            }
            continueOrEnd.GetComponentInChildren<TextMeshProUGUI>().text = "Continue";
        }

        void Show_Continue_Or_End_Btn(bool show)
        {
            continueOrEnd.gameObject.SetActive(show);
        }

        void DisableDialogueChoices()
        {
            foreach (Transform child in choicePanel.transform)
            {
                Destroy(child.gameObject);
            }
            choicePanel.SetActive(false);
        }

        bool NodeHasChildren(DialogueNode node)
        {
            return node.GetChildIDs().Count > 0;
        }

        bool NodeHasChoices(DialogueNode node)
        {
            if (!NodeHasChildren(node))
            {
                return false;
            }

            foreach (DialogueNode child in currentDialogue.GetAllChildren(node))
            {
                if (!child.GetNodeIsPlayerChoice())
                {
                    return false;
                }
            }
            return true;
        }

        void BuildChoiceList(DialogueNode parentNode)
        {
            if (!NodeHasChoices(parentNode))
            {
                return;
            }

            foreach (Transform child in choicePanel.transform)
            {
                Destroy(child.gameObject);
            }

            foreach (DialogueNode child in currentDialogue.GetAllChildren(parentNode))
            {
                GameObject newChoice = Instantiate(choiceBtnPrefab, choicePanel.transform);
                newChoice.GetComponentInChildren<Button>().onClick.AddListener(() => SelectChoice(child));
                newChoice.GetComponentInChildren<TextMeshProUGUI>().text = child.GetText();
            }
            choicePanel.SetActive(true);
        }

        //Error checking
        bool OkToStartAConversation(GameObject npc)
        {
            currentDialogue = null;
            if (npc == null)
            {
                Debug.LogWarning("npc was null");
                return false;
            }

            DialogueComponent dc = npc.GetComponent<DialogueComponent>();
            if (dc == null || dc.GetDialogue() == null)
            {
                Debug.LogWarning(npc + " has no dialogue");
                return false;
            }

            if (InConversation)
            {
                Debug.LogWarning("already in a conversation");
                return false;
            }

            if (RPG.Inventory.Inventory_UI.Instance.InvWindowIsOpen())
            {
                Debug.LogWarning("Can't start a conversation with inv window open");
                return false;
            }

            if (SceneController.Instance.sceneChangeInProgress)
            {
                Debug.LogWarning("Can't start a conversation during scene change");
                return false;
            }

            if (QuestJournal.Instance.QuestJournalIsOpen())
            {
                Debug.LogWarning("Can't start a conversation with quest journal open");
                return false;
            }

            return true;
        }
        #endregion
    }
}
