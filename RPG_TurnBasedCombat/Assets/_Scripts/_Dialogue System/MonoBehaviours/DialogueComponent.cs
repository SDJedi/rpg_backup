﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DialogueSystem
{
    public class DialogueComponent : MonoBehaviour
    {
        [SerializeField] Dialogue dialogue = null;


        public Dialogue GetDialogue()
        {
            return dialogue;
        }
    }
}
