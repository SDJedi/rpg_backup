﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

//Last Reviewed: 11/18/2020
namespace DialogueSystem
{
    public class DialogueNode : ScriptableObject
    {
        public static float minWidth = 250;
        public static float minHeight = 200;

        [SerializeField] string speakerName = "";
        [SerializeField] bool isPlayerChoice = false;
        [SerializeField] string tag;
        [SerializeField] List<string> childNodeIDs = new List<string>();
        [SerializeField] [TextAreaAttribute(10, 20)] string text = ""; //dialogue text
        [SerializeField] Rect rect = new Rect(0, 0, minWidth, minHeight);
        [SerializeField] ContinueOrEnd continueOrEnd = ContinueOrEnd.Default;


        #if UNITY_EDITOR
        public void SetRectPos(Vector2 newPos)
        {
            Undo.RecordObject(this, "reposition dialogue node");
            rect.position = newPos;
            EditorUtility.SetDirty(this);
        }

        public void SetSpeakerName(string text)
        {
            if (speakerName != text)
            {
                Undo.RecordObject(this, "change speaker name");
                speakerName = text;
                EditorUtility.SetDirty(this);
            }
        }

        public void SetText(string text)
        {
            if (text != this.text)
            {
                Undo.RecordObject(this, "change to dialogue text");
                this.text = text;
                EditorUtility.SetDirty(this);
            }
        }

        public void SetTag(string newTag)
        {
            tag = newTag;
            EditorUtility.SetDirty(this);
        }

        public void SetID(string ID)
        {
            this.name = ID;
            EditorUtility.SetDirty(this);
        }

        public void SetRectSize(float width, float height)
        {
            rect.width = width;
            rect.height = height;
            EditorUtility.SetDirty(this);
        }

        public void AddChildToList(string ID)
        {
            if (!childNodeIDs.Contains(ID))
            {
                Undo.RecordObject(this, "add child node");
                childNodeIDs.Add(ID);
                EditorUtility.SetDirty(this);
            }
        }

        public void RemoveChildFromList(string ID)
        {
            if (childNodeIDs.Contains(ID))
            {
                Undo.RecordObject(this, "remove child node");
                childNodeIDs.Remove(ID);
                EditorUtility.SetDirty(this);
            }
        }

        public void SetNodeIsPlayerChoice(bool isChoice)
        {
            isPlayerChoice = isChoice;
            EditorUtility.SetDirty(this);
        }
        #endif

        public string GetText()
        {
            return text;
        }

        public string GetID()
        {
            return this.name;
        }

        public Rect GetRect()
        {
            return rect;
        }

        public bool GetNodeIsPlayerChoice()
        {
            return isPlayerChoice;
        }

        public List<string> GetChildIDs()
        {
            return childNodeIDs;
        }

        public string GetSpeakerName()
        {
            return speakerName;
        }

        public string GetTag()
        {
            return tag;
        }

        public ContinueOrEnd GetContinueOrEnd()
        {
            return continueOrEnd;
        }
    }
}
