﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


namespace DialogueSystem
{
    [CreateAssetMenu(fileName = "New Dialogue", menuName = "Dialogue")]
    public class Dialogue : ScriptableObject, ISerializationCallbackReceiver
    {
        Vector2 newNodePosOffset = new Vector2(280, 90);

        [SerializeField] List<DialogueNode> nodes = new List<DialogueNode>();
        [NonSerialized] Dictionary<string, DialogueNode> nodeLookup = new Dictionary<string, DialogueNode>();

        //Note: need this for dialogue to work in game build vs just in editor
        void OnEnable()
        {
            RepopulateNodeLookup();
        }

        //for in-Editor
        void OnValidate()
        {
            RepopulateNodeLookup();
        }

        void RepopulateNodeLookup()
        {
            nodeLookup.Clear();
            foreach (DialogueNode node in nodes)
            {
                nodeLookup[node.GetID()] = node;
            }
        }

        public List<DialogueNode> GetAllNodes()
        {
            return nodes;
        }

        public DialogueNode GetRootNode()
        {
            if (nodes.Count >= 1)
            {
                return nodes[0];
            }
            return null;
        }

        public DialogueNode GetNodeByTag(string tag)
        {
            foreach (DialogueNode node in nodes)
            {
                if (node.GetTag() == tag)
                {
                    return node;
                }
            }
            return null;
        }

        public IEnumerable<DialogueNode> GetAllChildren(DialogueNode parentNode)
        {
            foreach (string childID in parentNode.GetChildIDs())
            {
                if (nodeLookup.ContainsKey(childID))
                {
                    yield return nodeLookup[childID];
                }
            }
        }

        public string GetRootNodeSpeaker()
        {
            return GetRootNode().GetSpeakerName();
        }

        public void OnBeforeSerialize()
        {
#if UNITY_EDITOR
            //add a root node if none exists
            if (nodes.Count < 1)
            {
                DialogueNode root = BuildNode(null);
                AddNodeToList(root);
                EditorApplication.ExecuteMenuItem("File/Save");
            }

            if (AssetDatabase.GetAssetPath(this) != "")
            {
                foreach (DialogueNode node in GetAllNodes())
                {
                    if (AssetDatabase.GetAssetPath(node) == "")
                    {
                        AssetDatabase.AddObjectToAsset(node, this);
                    }
                }
            }
#endif
        }

        public void OnAfterDeserialize()
        {
            //required for interface implementation
        }

#if UNITY_EDITOR
        public void CreateNode(DialogueNode parentNode)
        {
            DialogueNode newNode = BuildNode(parentNode);
            Undo.RegisterCreatedObjectUndo(newNode, "created new node");
            Undo.RecordObject(this, "created new node");
            AddNodeToList(newNode);
        }

        DialogueNode BuildNode(DialogueNode parentNode)
        {
            DialogueNode newNode = CreateInstance<DialogueNode>();

            newNode.SetID(Guid.NewGuid().ToString());
            if (parentNode != null)
            {
                parentNode.AddChildToList(newNode.GetID());
                newNode.SetNodeIsPlayerChoice(!parentNode.GetNodeIsPlayerChoice());
                newNode.SetRectPos(new Vector2(parentNode.GetRect().position.x + newNodePosOffset.x, parentNode.GetRect().position.y + newNodePosOffset.y));
                if (!newNode.GetNodeIsPlayerChoice())
                {
                    newNode.SetSpeakerName(GetRootNodeSpeaker());
                }
            }

            return newNode;
        }

        void AddNodeToList(DialogueNode newNode)
        {
            nodes.Add(newNode);
            OnValidate();
            AssetDatabase.SaveAssets();
            EditorApplication.ExecuteMenuItem("File/Save");
        }

        public void DeleteNode(DialogueNode nodeToDelete)
        {
            if (nodes.Count > 1)
            {
                Undo.RecordObject(this, "deleted node");
            }

            nodes.Remove(nodeToDelete);
            OnValidate();
            foreach (DialogueNode node in GetAllNodes())
            {
                node.RemoveChildFromList(nodeToDelete.GetID());
            }

            if (nodes.Count > 0)
            {
                Undo.DestroyObjectImmediate(nodeToDelete);
            }
            else
            {
                UnityEngine.Object.DestroyImmediate(nodeToDelete, true);
            }
            EditorApplication.ExecuteMenuItem("File/Save");
        }
#endif
    }
}
