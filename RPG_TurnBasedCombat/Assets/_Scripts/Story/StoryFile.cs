/*

Character Roles:  Saul, [Sir John], Jimbo -- powerful support abilities (buffs / debuffs) -- high durability

                  Babz, [Ed], Lance -- powerful, single-target, melee DPS (mostly physical) -- healing abilities -- medium durability

                  Karl, [Lisa], Billy -- med to high, ranged dps (AoE and single, mostly non-physical) -- rez abilities -- low durability

Order of appearance: Saul
                     Karl
                     Babz
                     Lance
                     Billy
                     Jimbo
                     [Sir John, Ed, Lisa] ... optional characters


                                                    **********THE CHARACTERS**********

1) Saul:      Main character. Has been dead for quite some time (100 years?). Was a king in life (kind of king tbd by gameplay?). Recently 
          brought back to help Karl get revenge. Is bound to serve until Karl frees him or Karl dies. The story is told through his eyes.

          Role: Fighter / DPS / Melee

          Special Move(s) : Death Touch

          Traits: Undying (rez after combat with 1 HP) / Counter Attack

          Abilities:

          * Murder Spree - attack all with melee
          * Frenzied Strikes - attack one with melee
          * Deadly Swipe - Single target melee attack
          * Ball of Rot - poison ranged attack
          * Contamination - AoE poison
          * Poison Aura - buff all allies with poison damage shield
          * Burning Rage - buff self
          * Strategic initiative - manipulate turn order
          * Blessing of the damned - buff ally (add poison proc)?


2) Karl:      Saul's first companion. Died more recently (20 years?). Broght Saul back to avenge his death

          Role: Mage / DPS / Ranged

          Special Move(s) : ???

3) Babz:

4) Billy:
5) Lance:
6) Jimbo:
7) Sir John:
8) Ed:
9) Lisa: 

                                                    **********THE STORY**********

* [Karl] raises [Saul] to help him avenge his death / Saul is bound to serve (or so he believes)

* Karl was double-crossed by Gromm, a young orc chieftan ... [Babz], the orc hunter can find this orc

* Babz will help if we agree to rescue her brother, [Lance] from the orc stronghold

* Lance says the orc chieftan is in search of a world-ending relic and must be stopped

* Treasure hunter [Jimbo] might know where the relic is

* Jimbo's partner, [Billy], is in jail. Jimbo will help find the relic if we free Billy.

* The group of six travel to a volcanic cave where the orc and the relic are waiting

* The group loses a major battle / orc gets away with the relic

* Time is short, group must find a way to stop orc

* They journey to find an answer (powerful item / character(s)) to the threat

* They face the even more powerful orc, but this time they are victorious! The world is safe, and Karl's death is avenged!
 */
