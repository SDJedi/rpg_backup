using System;
using System.Collections.Generic;
using RPG.CombatSystem;
using RPG.PartySelectSystem;
using RPG.Saving;
using RPG.SceneControl;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

//Last Reviewed: 1/20/20
namespace RPG.ProgressionSystem
{
    //attached to CharacterPointsManager in CharacterPointsScene.
    //handles UI related to purchasing abilities with character points.
    //grabs info from a PC_SkillTree (SkillTreeManager.cs) and assigns the correct
    //PC_Purchasable to each button
    public class CharacterPointsManager : Singleton<CharacterPointsManager>, ISaveable
    {
        //each list contains 9 buttons / assigned in inspector
        [SerializeField] List<Button> Level_1_Buttons = null;
        [SerializeField] List<Button> Level_2_Buttons = null;
        [SerializeField] List<Button> Level_3_Buttons = null;
        [SerializeField] List<Button> Level_4_Buttons = null;
        [SerializeField] List<Button> Level_5_Buttons = null;
        [SerializeField] List<Button> Level_6_Buttons = null;
        [SerializeField] List<Button> Level_7_Buttons = null;
        [SerializeField] List<Button> Level_8_Buttons = null;
        [SerializeField] List<Button> Level_9_Buttons = null;
        [SerializeField] List<Button> Level_10_Buttons = null;
        [SerializeField] List<Button> Level_11_Buttons = null;
        [SerializeField] List<Button> Level_12_Buttons = null;
        [SerializeField] List<Button> Level_13_Buttons = null;
        [SerializeField] List<Button> Level_14_Buttons = null;
        [SerializeField] List<Button> Level_15_Buttons = null;
        [SerializeField] List<Button> Level_16_Buttons = null;
        [SerializeField] List<Button> Level_17_Buttons = null;
        [SerializeField] List<Button> Level_18_Buttons = null;
        [SerializeField] List<Button> Level_19_Buttons = null;
        [SerializeField] List<Button> Level_20_Buttons = null;

        //UI elements used for every PC / assigned in inspector
        [SerializeField] RawImage PC_Portrait_500x500 = null;
        [SerializeField] Image DeadIcon = null;
        [SerializeField] TextMeshProUGUI PC_Name = null;
        [SerializeField] TextMeshProUGUI CharPoints_Txt = null;
        [SerializeField] TextMeshProUGUI XPToNextPoint_Txt = null;
        [SerializeField] List<TextMeshProUGUI> LevelIndicators = null;
        [SerializeField] Color32 BtnColor_Purchased = Color.green;
        [SerializeField] Color32 BtnColor_Unpurchased = Color.white;

        //confirmation window / assigned in inspector
        [SerializeField] GameObject confirmation_Panel = null;
        [SerializeField] TextMeshProUGUI confirmation_Txt = null;
        [SerializeField] Button yes_Btn = null;
        [SerializeField] Sprite defaultIcon = null;

        //the name gets changed by SetDisplayedPC(), and this data is saved when leaving the scene
        string DispCharName = "Saul";
        //the current PC on display
        PC_Stats_Config DisplayedCharacter;
        //a List containing all the Level specific button lists
        List<List<Button>> masterButtonList = new List<List<Button>>();


        protected override void Awake()
        {
            base.Awake();
            //populate masterButtonList
            CreateListOfButtonLists();
        }

        void Start()
        {
            PC_Stats_Config dispCharStats = null;

            //find the PC that should be displayed based on the DispCharName that 
            //was most recently saved (default is Saul)
            foreach (PC_Stats_Config stats in GetListOfAvailablePCs())
            {
                if (stats.CharName == DispCharName)
                {
                    dispCharStats = stats;
                    break;
                }
            }
            HideConfirmationPanel();
            SetDisplayedPC(dispCharStats);
            UpdateUI();
        }

        void Update()
        {
            //leave scene when "Done" button is pressed || "C" is pressed
            if (Input.GetKeyDown(KeyCode.C))
            {
                HideConfirmationPanel();
                ReturnToLastScene();
            }
        }

        //create a List that contains all the Lists of buttons
        void CreateListOfButtonLists()
        {
            masterButtonList.Add(Level_1_Buttons);
            masterButtonList.Add(Level_2_Buttons);
            masterButtonList.Add(Level_3_Buttons);
            masterButtonList.Add(Level_4_Buttons);
            masterButtonList.Add(Level_5_Buttons);
            masterButtonList.Add(Level_6_Buttons);
            masterButtonList.Add(Level_7_Buttons);
            masterButtonList.Add(Level_8_Buttons);
            masterButtonList.Add(Level_9_Buttons);
            masterButtonList.Add(Level_10_Buttons);
            masterButtonList.Add(Level_11_Buttons);
            masterButtonList.Add(Level_12_Buttons);
            masterButtonList.Add(Level_13_Buttons);
            masterButtonList.Add(Level_14_Buttons);
            masterButtonList.Add(Level_15_Buttons);
            masterButtonList.Add(Level_16_Buttons);
            masterButtonList.Add(Level_17_Buttons);
            masterButtonList.Add(Level_18_Buttons);
            masterButtonList.Add(Level_19_Buttons);
            masterButtonList.Add(Level_20_Buttons);
        }

        //main point of entry for displaying a PC and thier skill tree in CharacterPointsScene
        void SetDisplayedPC(PC_Stats_Config stats)
        {
            DisplayedCharacter = stats;
            DispCharName = stats.CharName;
            LoadSkillTreeForDisplayedCharacter();
            UpdateUI();
        }

        //each PC has their own unique skill tree
        void LoadSkillTreeForDisplayedCharacter()
        {
            //start by clearing out all buttons
            ClearAllButtons();
            //get the correct skill tree
            PC_SkillTree currentTree = SkillTreeManager.Instance.GetSkillTree(DisplayedCharacter.CharName);

            //set up all buttons for all levels based on the PCs skill tree
            if (currentTree != null)
            {
                SetUpButtons(Level_1_Buttons, currentTree.Level_1_Purchasables, 1);
                SetUpButtons(Level_2_Buttons, currentTree.Level_2_Purchasables, 2);
                SetUpButtons(Level_3_Buttons, currentTree.Level_3_Purchasables, 3);
                SetUpButtons(Level_4_Buttons, currentTree.Level_4_Purchasables, 4);
                SetUpButtons(Level_5_Buttons, currentTree.Level_5_Purchasables, 5);
                SetUpButtons(Level_6_Buttons, currentTree.Level_6_Purchasables, 6);
                SetUpButtons(Level_7_Buttons, currentTree.Level_7_Purchasables, 7);
                SetUpButtons(Level_8_Buttons, currentTree.Level_8_Purchasables, 8);
                SetUpButtons(Level_9_Buttons, currentTree.Level_9_Purchasables, 9);
                SetUpButtons(Level_10_Buttons, currentTree.Level_10_Purchasables, 10);
                SetUpButtons(Level_11_Buttons, currentTree.Level_11_Purchasables, 11);
                SetUpButtons(Level_12_Buttons, currentTree.Level_12_Purchasables, 12);
                SetUpButtons(Level_13_Buttons, currentTree.Level_13_Purchasables, 13);
                SetUpButtons(Level_14_Buttons, currentTree.Level_14_Purchasables, 14);
                SetUpButtons(Level_15_Buttons, currentTree.Level_15_Purchasables, 15);
                SetUpButtons(Level_16_Buttons, currentTree.Level_16_Purchasables, 16);
                SetUpButtons(Level_17_Buttons, currentTree.Level_17_Purchasables, 17);
                SetUpButtons(Level_18_Buttons, currentTree.Level_18_Purchasables, 18);
                SetUpButtons(Level_19_Buttons, currentTree.Level_19_Purchasables, 19);
                SetUpButtons(Level_20_Buttons, currentTree.Level_20_Purchasables, 20);
            }
            else
            {
                Debug.LogWarning("No skill tree for " + DisplayedCharacter.CharName);
            }
        }

        //go through each button in "level_X_Buttons" ... assign a purchasable if necessary, based on
        //the corresponding "Level_X_Purchasables" from the current skill tree
        void SetUpButtons(List<Button> btnList, List<CP_Purchasable> lvlList, int minLvl)
        {
            for (int i = 0; i < btnList.Count; i++)
            {
                if (i < lvlList.Count && lvlList[i] != null)
                {
                    var purchasable = lvlList[i];
                    btnList[i].GetComponent<CharacterPoints_ButtonScript>().AttachPurchasableToButton(purchasable);
                    purchasable.RequiredLevel = minLvl;
                    btnList[i].interactable = !purchasable.IsPurchased;
                    foreach (Image img in btnList[i].GetComponentsInChildren<Image>())
                    {
                        img.enabled = true;
                    }
                    SetPurchasableIcon(btnList[i], purchasable.Icon);
                    btnList[i].GetComponentInChildren<TextMeshProUGUI>().text = purchasable.NameOfPurchasable;

                    if (!purchasable.IsPurchased)
                    {
                        btnList[i].onClick.AddListener(delegate { purchasable.TryPurchasePurchasable(); });
                    }
                }
            }
            UpdateUI();
        }

        void SetPurchasableIcon(Button button, Sprite icon)
        {
            button.transform.Find("Icon_Img").GetComponent<Image>().sprite = defaultIcon;
            if (icon != null)
            {
                button.transform.Find("Icon_Img").GetComponent<Image>().sprite = icon;
            }
        }

        void ClearAllButtons()
        {
            foreach (List<Button> btnList in masterButtonList)
            {
                ClearButtons(btnList);
            }
        }

        void ClearButtons(List<Button> btnList)
        {
            foreach (Button btn in btnList)
            {
                btn.onClick.RemoveAllListeners();
                btn.GetComponent<CharacterPoints_ButtonScript>().AttachPurchasableToButton(null);
                btn.interactable = false;
                foreach (Image img in btn.GetComponentsInChildren<Image>())
                {
                    img.enabled = false;
                }
                btn.GetComponentInChildren<TextMeshProUGUI>().text = "";
                //btn.GetComponent<Image>().color = new Color32(255, 255, 255, 55);
            }
        }

        void UpdateUI()
        {
            if (DisplayedCharacter.IsAlive())
            {
                DeadIcon.enabled = false;
            }
            else
            {
                DeadIcon.enabled = true;
            }
            CharPoints_Txt.text = CharacterPoints_Tracker.Instance.CurrentCP.ToString();
            XPToNextPoint_Txt.text = (CharacterPoints_Tracker.Instance.XPRequiredPerCP - CharacterPoints_Tracker.Instance.XPTowardsNextPoint).ToString();
            PC_Portrait_500x500.texture = DisplayedCharacter.PC_Portrait;
            PC_Name.text = DisplayedCharacter.CharName;
            LightUpLevelNumbersToDisplayedCharacterCurrentLevel();
            UpdateTintOfAbilityButtonsBasedOnPurchasedStatus();
        }

        void UpdateTintOfAbilityButtonsBasedOnPurchasedStatus()
        {
            PC_SkillTree currentTree = SkillTreeManager.Instance.GetSkillTree(DisplayedCharacter.CharName);

            if (currentTree != null)
            {
                UpdateBtnTints(Level_1_Buttons, currentTree.Level_1_Purchasables);
                UpdateBtnTints(Level_2_Buttons, currentTree.Level_2_Purchasables);
                UpdateBtnTints(Level_3_Buttons, currentTree.Level_3_Purchasables);
                UpdateBtnTints(Level_4_Buttons, currentTree.Level_4_Purchasables);
                UpdateBtnTints(Level_5_Buttons, currentTree.Level_5_Purchasables);
                UpdateBtnTints(Level_6_Buttons, currentTree.Level_6_Purchasables);
                UpdateBtnTints(Level_7_Buttons, currentTree.Level_7_Purchasables);
                UpdateBtnTints(Level_8_Buttons, currentTree.Level_8_Purchasables);
                UpdateBtnTints(Level_9_Buttons, currentTree.Level_9_Purchasables);
                UpdateBtnTints(Level_10_Buttons, currentTree.Level_10_Purchasables);
                UpdateBtnTints(Level_11_Buttons, currentTree.Level_11_Purchasables);
                UpdateBtnTints(Level_12_Buttons, currentTree.Level_12_Purchasables);
                UpdateBtnTints(Level_13_Buttons, currentTree.Level_13_Purchasables);
                UpdateBtnTints(Level_14_Buttons, currentTree.Level_14_Purchasables);
                UpdateBtnTints(Level_15_Buttons, currentTree.Level_15_Purchasables);
                UpdateBtnTints(Level_16_Buttons, currentTree.Level_16_Purchasables);
                UpdateBtnTints(Level_17_Buttons, currentTree.Level_17_Purchasables);
                UpdateBtnTints(Level_18_Buttons, currentTree.Level_18_Purchasables);
                UpdateBtnTints(Level_19_Buttons, currentTree.Level_19_Purchasables);
                UpdateBtnTints(Level_20_Buttons, currentTree.Level_20_Purchasables);
            }
            else
            {
                Debug.LogWarning("No skill tree for " + DisplayedCharacter.CharName);
            }
        }

        void UpdateBtnTints(List<Button> btnList, List<CP_Purchasable> lvlList)
        {
            for (int i = 0; i < btnList.Count; i++)
            {
                if (i < lvlList.Count && lvlList[i] != null)
                {
                    var purchasable = lvlList[i];
                    if (purchasable.IsPurchased)
                    {
                        btnList[i].interactable = false;
                        btnList[i].GetComponent<Image>().color = BtnColor_Purchased;
                    }
                    else
                    {
                        btnList[i].interactable = true;
                        btnList[i].GetComponent<Image>().color = BtnColor_Unpurchased;
                    }
                    //Note: need to toggle button off and on to update UI button tint
                    btnList[i].enabled = false;
                    btnList[i].enabled = true;
                    /////////////////////////////////////////////////////////////////
                }
                // else
                // {
                //     btnList[i].GetComponent<Image>().color = new Color32(255, 255, 255, 55);
                // }
            }
        }

        //called by "Done" button
        public void ReturnToLastScene()
        {
            SceneController.Instance.FadeAndLoadScene(SceneController.Instance.nameOfMostRecentlyUnloadedScene);
        }

        //assigned as an OnClick() event on the ScrollRight_Btn
        public void ScrollRight()
        {
            HideConfirmationPanel();
            //loop through all PC stat configs
            for (int i = 0; i < PCManager.Instance.PC_StatConfigs.Count; i++)
            {
                //find the currently displayed PC
                if (DisplayedCharacter == PCManager.Instance.PC_StatConfigs[i])
                {
                    //if we are NOT at the end of the PC_StatConfigs List...
                    if (i + 1 < PCManager.Instance.PC_StatConfigs.Count)
                    {
                        //start at the next index (i + 1) ... loop through the rest of the 
                        //PC_StatConfigs looking for an "available" PC (see PCManager.cs ... acquired team members)
                        for (int j = i + 1; j < PCManager.Instance.PC_StatConfigs.Count; j++)
                        {
                            if (GetListOfAvailablePCs().Contains(PCManager.Instance.PC_StatConfigs[j]))
                            {
                                //we found an available PC farther down the List than the currently displayed PC,
                                //set it as the new displayed PC and we're done
                                SetDisplayedPC(PCManager.Instance.PC_StatConfigs[j]);
                                return;
                            }
                        }

                        //we got to the end of PC_StatConfigs and didn't find a new, available PC.
                        //Start back at 0; set the first available PC we find to the displayed PC.
                        for (int j = 0; j < PCManager.Instance.PC_StatConfigs.Count; j++)
                        {
                            if (GetListOfAvailablePCs().Contains(PCManager.Instance.PC_StatConfigs[j]))
                            {
                                SetDisplayedPC(PCManager.Instance.PC_StatConfigs[j]);
                                return;
                            }
                        }
                    }
                    //if we are here, the currently displayed PC is the PC at the very end of the PC_StatsConfigs List
                    //start at index 0 and find the first available PC to display
                    else
                    {
                        for (int j = 0; j < PCManager.Instance.PC_StatConfigs.Count; j++)
                        {
                            if (GetListOfAvailablePCs().Contains(PCManager.Instance.PC_StatConfigs[j]))
                            {
                                SetDisplayedPC(PCManager.Instance.PC_StatConfigs[j]);
                                return;
                            }
                        }
                    }
                }
            }
        }

        //assigned as an OnClick() event on the ScrollLeft_Btn
        public void ScrollLeft()
        {
            HideConfirmationPanel();
            //loop through all PC stat configs
            for (int i = 0; i < PCManager.Instance.PC_StatConfigs.Count; i++)
            {
                //find the currently displayed PC
                if (DisplayedCharacter == PCManager.Instance.PC_StatConfigs[i])
                {
                    //if we are NOT at the start of the PC_StatConfigs List...
                    if (i > 0)
                    {
                        //start at the next index (i - 1) ... loop through the rest of the 
                        //PC_StatConfigs looking for an "available" PC (see PCManager.cs ... acquired team members + Saul)
                        for (int j = i - 1; j >= 0; j--)
                        {
                            if (GetListOfAvailablePCs().Contains(PCManager.Instance.PC_StatConfigs[j]))
                            {
                                //we found an available PC farther down the List than the currently displayed PC,
                                //set it as the new displayed PC and we're done
                                SetDisplayedPC(PCManager.Instance.PC_StatConfigs[j]);
                                return;
                            }
                        }

                        //we got to the start of PC_StatConfigs and didn't find a new, available PC.
                        //Start back at the last index; set the first available PC we find to the displayed PC.
                        for (int j = PCManager.Instance.PC_StatConfigs.Count - 1; j >= 0; j--)
                        {
                            if (GetListOfAvailablePCs().Contains(PCManager.Instance.PC_StatConfigs[j]))
                            {
                                SetDisplayedPC(PCManager.Instance.PC_StatConfigs[j]);
                                return;
                            }
                        }
                    }
                    //if we are here, the currently displayed PC is the PC at index 0 of the PC_StatsConfigs List
                    //start at the last index and loop back to the first available PC to display
                    else
                    {
                        for (int j = PCManager.Instance.PC_StatConfigs.Count - 1; j >= 0; j--)
                        {
                            if (GetListOfAvailablePCs().Contains(PCManager.Instance.PC_StatConfigs[j]))
                            {
                                SetDisplayedPC(PCManager.Instance.PC_StatConfigs[j]);
                                return;
                            }
                        }
                    }
                }
            }
        }

        //returns a list of all "available" PCs (i.e. all PCs that would show up in the CharacterSelectScene)
        List<PC_Stats_Config> GetListOfAvailablePCs()
        {
            //make a new list to hold all available PCs
            List<PC_Stats_Config> allAvilablePCs = new List<PC_Stats_Config>();

            //loop through AcquiredTeamMembers and add any marked as available
            foreach (SelectablePC PC in PCManager.Instance.AcquiredTeamMembers)
            {
                if (PC.IsAvailable)
                {
                    allAvilablePCs.Add(PCManager.Instance.GetStatsForPC(PC.PC_Name));
                }
            }

            return allAvilablePCs;
        }

        void LightUpLevelNumbersToDisplayedCharacterCurrentLevel()
        {
            if (DisplayedCharacter != null)
            {
                for (int i = 0; i < LevelIndicators.Count; i++)
                {
                    if (DisplayedCharacter.CurLvl - 1 >= i)
                    {
                        LevelIndicators[i].color = Color.green;
                    }
                    else
                    {
                        LevelIndicators[i].color = Color.white;
                    }
                }
            }
        }

        //called by the CP_Purchasable attached to the button after a check is made confirming
        //the purchase is valid
        public void ShowConfirmationPanel(CP_Purchasable p)
        {
            yes_Btn.onClick.RemoveAllListeners();
            yes_Btn.onClick.AddListener(() => p.CompletePurchase());
            yes_Btn.onClick.AddListener(() => UpdateUI());
            if (p.CP_Cost > 1 || p.CP_Cost == 0)
            {
                confirmation_Txt.text = "Purchase [" + p.NameOfPurchasable + "] for (" + p.CP_Cost.ToString() + ") Character Points?";
            }
            else
            {
                confirmation_Txt.text = "Purchase [" + p.NameOfPurchasable + "] for (1) Character Point?";
            }

            confirmation_Panel.SetActive(true);
        }

        //called by "Done" button
        public void HideConfirmationPanel()
        {
            confirmation_Panel.SetActive(false);
        }

        #region SaveData

        public object CaptureState()
        {
            return DispCharName;
        }

        public void RestoreState(object state)
        {
            DispCharName = (string)state;
        }
        #endregion
    }
}
