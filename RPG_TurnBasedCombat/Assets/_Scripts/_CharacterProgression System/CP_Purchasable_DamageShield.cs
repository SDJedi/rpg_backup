using System.Collections.Generic;
using RPG.CombatSystem;
using RPG.Messaging;
using UnityEngine;

//Last Reviewed: 1/20/20
namespace RPG.ProgressionSystem
{
    //allows for the creation of purchasable damage shields that can be added to a PC_SkillTree.
    //once purchased, the damage shield is always active for the PC
    [CreateAssetMenu(menuName = "CP_Purchasable / Purchasable_DamageShield")]
    public class CP_Purchasable_DamageShield : CP_Purchasable
    {
        [SerializeField] DamageShield damageShieldToAdd = null;
        [SerializeField] List<BaseCommand_Config> prerequisitAbilities = null;
        [TextArea]
        [SerializeField] string description = "";

        public DamageShield DamageShieldToAdd { get { return damageShieldToAdd; } }
        public List<BaseCommand_Config> PrerequisitAbilities { get { return prerequisitAbilities; } }
        public string Description { get { return description; } }


        protected override bool AllPrerequisitsMet()
        {
            if (PCManager.Instance.GetStatsForPC(CharName) == null)
            {
                Debug.Log("Stats for " + CharName + " came back null");
                return false;
            }

            if (PCManager.Instance.GetStatsForPC(CharName).CurLvl < RequiredLevel)
            {
                NotificationWindowManager.Instance.DisplayErrorMessage(CharName + " must be at least level " + RequiredLevel + " to purchase " + DamageShieldToAdd.nameOfDmgShield + ".", notificationWindowPosition);
                return false;
            }

            foreach (BaseCommand_Config command in prerequisitAbilities)
            {
                if (!PCManager.Instance.GetStatsForPC(CharName).KnownAbilities.Contains(command))
                {
                    NotificationWindowManager.Instance.DisplayErrorMessage("Missing prerequisite: " + command.CmndName, notificationWindowPosition);
                    return false;
                }
            }

            return true;
        }

        public override void CompletePurchase(bool activateForFree = false)
        {
            base.CompletePurchase(activateForFree);
            PCManager.Instance.GetStatsForPC(CharName).AddNewDamageShield(DamageShieldToAdd);
        }

        public override void ResetCP_Purchasable()
        {
            IsPurchased = false;
        }

        public override void InitializePurchasable()
        {
            nameOfPurchasable = DamageShieldToAdd.nameOfDmgShield;
            icon = DamageShieldToAdd.icon;
        }
    }
}