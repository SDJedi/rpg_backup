using System;
using RPG.Messaging;
using RPG.Saving;
using UnityEngine;

//Last Reviewed: 1/16/20
namespace RPG.ProgressionSystem
{
    //attached to PCManager in BootScene
    public class CharacterPoints_Tracker : Singleton<CharacterPoints_Tracker>, ISaveable
    {
        public readonly int startingCPAmnt = 3;
        public event Action<CP_Purchasable> OnPurchasablePurchased;
        public event Action CPEarned;

        [SerializeField] int xPRequiredPerCP = 500; //500 == original value
        [SerializeField] int currentCharacterPoints;

        public int XPTowardsNextPoint { get; set; } = 0;
        public int CurrentCP { get { return currentCharacterPoints; } }
        public int XPRequiredPerCP { get { return xPRequiredPerCP; } }


        void Start()
        {
            SavingWrapper.Instance.StartingNewGame += ResetCharacterPoints;
        }

        void OnDisable()
        {
            if (SavingWrapper.InstanceExists)
            {
                SavingWrapper.Instance.StartingNewGame -= ResetCharacterPoints;
            }
        }

        //Add or subtract CP
        public void ModifyCharacterPoints(int amount)
        {
            currentCharacterPoints += amount;
            if (amount > 0)
            {
                if (CPEarned != null)
                {
                    CPEarned();
                }
                NotificationWindowManager.Instance.DisplayNotification("Character Point earned!", Color.yellow, new Vector2(0, -400));
            }
        }

        //called at the moment "p" is purchased by CP_Purchasable.cs --> CompletePurchase()
        public void PurchasablePurchased(CP_Purchasable p)
        {
            if (OnPurchasablePurchased != null)
            {
                OnPurchasablePurchased(p);
            }
        }

        void ResetCharacterPoints()
        {
            currentCharacterPoints = startingCPAmnt;
            //print("CP reset to starting amount");
        }

        #region SaveData
        [System.Serializable]
        class CPTrackerSaveData
        {
            public int XP_TowardsNextPoint;
            public int currentCP;
        }

        public object CaptureState()
        {
            var saveData = new CPTrackerSaveData();

            saveData.XP_TowardsNextPoint = XPTowardsNextPoint;
            saveData.currentCP = CurrentCP;

            return saveData;
        }

        public void RestoreState(object state)
        {
            var saveData = (CPTrackerSaveData)state;

            currentCharacterPoints = saveData.currentCP;
            XPTowardsNextPoint = saveData.XP_TowardsNextPoint;
        }
        #endregion
    }
}