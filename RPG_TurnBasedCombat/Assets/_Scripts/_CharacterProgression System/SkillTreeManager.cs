using UnityEngine;

//Last Reviewed: 1/20/20
namespace RPG.ProgressionSystem
{
    //See: BootScene > PCManager > PCSkillTrees.
    //holds a PC_SkillTree for each PC and a public method to access them. 
    public class SkillTreeManager : Singleton<SkillTreeManager>
    {
        [SerializeField] PC_SkillTree SaulsSkillTree = null;
        [SerializeField] PC_SkillTree KarlsSkillTree = null;
        [SerializeField] PC_SkillTree BillysSkillTree = null;
        [SerializeField] PC_SkillTree EdsSkillTree = null;
        [SerializeField] PC_SkillTree BabzsSkillTree = null;
        [SerializeField] PC_SkillTree JimbosSkillTree = null;
        [SerializeField] PC_SkillTree SirJohnsSkillTree = null;
        [SerializeField] PC_SkillTree LancesSkillTree = null;
        [SerializeField] PC_SkillTree LisasSkillTree = null;


        public PC_SkillTree GetSkillTree(string PC_Name)
        {
            switch (PC_Name)
            {
                case "Saul":
                    return SaulsSkillTree;
                case "Karl":
                    return KarlsSkillTree;
                case "Billy":
                    return BillysSkillTree;
                case "Ed":
                    return EdsSkillTree;
                case "Babz":
                    return BabzsSkillTree;
                case "Jimbo":
                    return JimbosSkillTree;
                case "Sir John":
                    return SirJohnsSkillTree;
                case "Lance":
                    return LancesSkillTree;
                case "Lisa":
                    return LisasSkillTree;
                default:
                    Debug.LogWarning("Error: No skill tree exists for " + PC_Name);
                    return null;
            }
        }
    }
}
