using UnityEngine;

//Last Reviewed: 1/15/20
namespace RPG.ProgressionSystem
{
    //used to activate game objects at the moment of purchase
    //Note: there is a similar but different script that activates a gameObject
    //when there is a scene change and an ability has already been purchased
    public class ActivateGameObjectsOnPurchase : MonoBehaviour
    {
        [SerializeField] GameObject[] objectsToActivate = null;
        [SerializeField] CP_Purchasable purchasableTrigger = null;


        void Start()
        {
            if (CharacterPoints_Tracker.InstanceExists)
            {
                CharacterPoints_Tracker.Instance.OnPurchasablePurchased += ActivateGameObjects;
            }
        }

        void OnDestroy()
        {
            if (CharacterPoints_Tracker.InstanceExists)
            {
                CharacterPoints_Tracker.Instance.OnPurchasablePurchased -= ActivateGameObjects;
            }
        }

        void ActivateGameObjects(CP_Purchasable p)
        {
            if (p.NameOfPurchasable == purchasableTrigger.NameOfPurchasable)
            {
                foreach (GameObject obj in objectsToActivate)
                {
                    if (obj != null)
                    {
                        obj.SetActive(true);
                    }
                }
            }
        }
    }
}
