using System.Collections.Generic;
using RPG.CombatSystem;
using RPG.Messaging;
using UnityEngine;

//Last Reviewed: 1/16/20
namespace RPG.ProgressionSystem
{
    //allows for the creation of purchasable abilities that can be added to a PC_SkillTree
    [CreateAssetMenu(menuName = "CP_Purchasable / Purchasable_Ability")]
    public class CP_Purchasable_Ability : CP_Purchasable
    {
        [SerializeField] BaseCommand_Config abilityToAdd = null;
        [SerializeField] List<BaseCommand_Config> prerequisitAbilities = null;
        [Header("Optional: The new ability replaces an old one")]
        [SerializeField] BaseCommand_Config abilityToRemove = null;

        public BaseCommand_Config AbilityToAdd { get { return abilityToAdd; } }
        public BaseCommand_Config AbilityToRemove { get { return abilityToRemove; } }
        public List<BaseCommand_Config> PrerequisitAbilities { get { return prerequisitAbilities; } }


        protected override bool AllPrerequisitsMet()
        {
            if (PCManager.Instance.GetStatsForPC(CharName) == null)
            {
                Debug.Log("Stats for " + CharName + " came back null");
                return false;
            }

            if (PCManager.Instance.GetStatsForPC(CharName).CurLvl < RequiredLevel)
            {
                NotificationWindowManager.Instance.DisplayErrorMessage(CharName + " must be at least level " + RequiredLevel + " to purchase " + AbilityToAdd.CmndName + ".", notificationWindowPosition);
                return false;
            }

            foreach (BaseCommand_Config command in prerequisitAbilities)
            {
                if (!PCManager.Instance.GetStatsForPC(CharName).KnownAbilities.Contains(command))
                {
                    NotificationWindowManager.Instance.DisplayErrorMessage("Missing prerequisite: " + command.CmndName, notificationWindowPosition);
                    return false;
                }
            }

            return true;
        }

        public override void CompletePurchase(bool activateForFree = false)
        {
            base.CompletePurchase(activateForFree);
            PCManager.Instance.GetStatsForPC(CharName).AddAbility(AbilityToRemove, AbilityToAdd);
        }

        public override void ResetCP_Purchasable()
        {
            IsPurchased = false;
        }

        public override void InitializePurchasable()
        {
            nameOfPurchasable = AbilityToAdd.CmndName;
            icon = AbilityToAdd.Icon;
        }
    }
}