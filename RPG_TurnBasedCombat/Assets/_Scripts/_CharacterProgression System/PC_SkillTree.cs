using System.Collections.Generic;
using UnityEngine;

//Last Reviewed: 1/20/20
namespace RPG.ProgressionSystem
{
    //contains one list of CP_Purchasables for each PC level.
    //note: UI is set up to handle up to 9 abilities per level and 20 levels.
    public class PC_SkillTree : MonoBehaviour
    {
        public List<CP_Purchasable> Level_1_Purchasables = null;
        public List<CP_Purchasable> Level_2_Purchasables = null;
        public List<CP_Purchasable> Level_3_Purchasables = null;
        public List<CP_Purchasable> Level_4_Purchasables = null;
        public List<CP_Purchasable> Level_5_Purchasables = null;
        public List<CP_Purchasable> Level_6_Purchasables = null;
        public List<CP_Purchasable> Level_7_Purchasables = null;
        public List<CP_Purchasable> Level_8_Purchasables = null;
        public List<CP_Purchasable> Level_9_Purchasables = null;
        public List<CP_Purchasable> Level_10_Purchasables = null;
        public List<CP_Purchasable> Level_11_Purchasables = null;
        public List<CP_Purchasable> Level_12_Purchasables = null;
        public List<CP_Purchasable> Level_13_Purchasables = null;
        public List<CP_Purchasable> Level_14_Purchasables = null;
        public List<CP_Purchasable> Level_15_Purchasables = null;
        public List<CP_Purchasable> Level_16_Purchasables = null;
        public List<CP_Purchasable> Level_17_Purchasables = null;
        public List<CP_Purchasable> Level_18_Purchasables = null;
        public List<CP_Purchasable> Level_19_Purchasables = null;
        public List<CP_Purchasable> Level_20_Purchasables = null;
    }
}