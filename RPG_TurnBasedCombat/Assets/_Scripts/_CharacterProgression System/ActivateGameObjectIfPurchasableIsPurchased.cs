using RPG.CombatSystem;
using RPG.SceneControl;
using UnityEngine;

//Last Reviewed: 1/15/20
namespace RPG.ProgressionSystem
{
    //attached to a PC gameObject.
    //used to show particle effect auras after an ability has been purchased.
    //hides the aura if PC is dead on scene load, shows aura again if PC is rezed.
    //a seperate script, Destroyed_DeactivateGameObjects.cs is used to disable the effect on PC death
    public class ActivateGameObjectIfPurchasableIsPurchased : MonoBehaviour
    {
        [SerializeField] GameObject gameObjectToActivate = null;
        [SerializeField] CP_Purchasable purchasableToCheck = null;

        PlayerCharacter PCScript;


        void Start()
        {
            PCScript = GetComponent<PlayerCharacter>();

            //subscribe to event
            SceneController.Instance.AfterSceneLoad += AfterSceneLoads;

            if (PCScript != null)
            {
                ((PC_Stats_Config)PCScript.Stats_Config).PCRezed += OnPCRezed;
            }

            //need to sub in Start() to ensure SceneController exists;
            //need to call AfterSceneLoads() in case we subbed AFTER scene already loaded
            AfterSceneLoads();
        }

        void OnDestroy()
        {
            //unsubscribe from events
            if (SceneController.InstanceExists)
            {
                SceneController.Instance.AfterSceneLoad -= AfterSceneLoads;
            }
            if (PCScript != null)
            {
                ((PC_Stats_Config)PCScript.Stats_Config).PCRezed -= OnPCRezed;
            }
        }

        void AfterSceneLoads()
        {
            //if the PC is alive && has purchased the specified ability: activate the (particle effect) gameObject(s)
            if (GetComponent<Character>() != null)
            {
                if (GetComponent<Character>().IsAlive && purchasableToCheck.IsPurchased)
                {
                    gameObjectToActivate.SetActive(true);
                }
                else
                {
                    gameObjectToActivate.SetActive(false);
                }
                return;
            }

            //case where we are not dealing with a PC,
            //we just want the objects active if the ability has been purchased (RenderTexture dummies)
            if (purchasableToCheck.IsPurchased)
            {
                gameObjectToActivate.SetActive(true);
            }
            else
            {
                gameObjectToActivate.SetActive(false);
            }
        }

        //activate these objects after a PC is rezed
        void OnPCRezed(PlayerCharacter pc)
        {
            if (PCScript.CharName == pc.CharName && purchasableToCheck.IsPurchased)
            {
                gameObjectToActivate.SetActive(true);
            }
        }
    }
}
