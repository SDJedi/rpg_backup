using System.Collections.Generic;
using RPG.Saving;
using UnityEngine;

//Last Reviewed: 1/19/22
namespace RPG.ProgressionSystem
{
    //Attached to the PCManager in the BootScene
    //when the BootScene is loaded, resets and initializes all the CP_Purchasable scriptable objects.
    //also responsible for saving/restoring the purchased state of each CP_Purchasable during gameplay.
    public class InitializeAllPurchasables : MonoBehaviour, ISaveable
    {
        CP_Purchasable[] AllPs = new CP_Purchasable[0];


        void Awake()
        {
            SavingWrapper.Instance.StartingNewGame += ResetAndInitializePurchasables;
            //Load all CP_Purchasables into the array. Note: for this to work, they need to be in the Resources folder 
            AllPs = Resources.LoadAll<CP_Purchasable>("");
            ResetAndInitializePurchasables();
        }

        void OnDisable()
        {
            if (SavingWrapper.InstanceExists)
            {
                SavingWrapper.Instance.StartingNewGame -= ResetAndInitializePurchasables;
            }
        }

        public void ResetAndInitializePurchasables()
        {
            ResetPurchasables();
            InitializePurchasables();
            //print("all purchasables reset and initialized");
        }

        void ResetPurchasables()
        {
            for (int i = 0; i < AllPs.Length; i++)
            {
                if (AllPs[i] == null)
                {
                    Debug.LogWarning("Error: Null value in AllPs[]");
                    continue;
                }
                AllPs[i].ResetCP_Purchasable();
            }
        }

        void InitializePurchasables()
        {
            for (int i = 0; i < AllPs.Length; i++)
            {
                if (AllPs[i] == null)
                {
                    break;
                }
                AllPs[i].InitializePurchasable(); //e.g. sets name and icon
            }
        }

        #region SaveData

        [System.Serializable]
        class PurchasableSaveData
        {
            public List<bool> purchasedState = new List<bool>();
        }

        public object CaptureState()
        {
            PurchasableSaveData saveData = new PurchasableSaveData();

            for (int i = 0; i < AllPs.Length; i++)
            {
                saveData.purchasedState.Add(AllPs[i].IsPurchased);
            }

            return saveData;
        }

        public void RestoreState(object state)
        {
            PurchasableSaveData saveData = (PurchasableSaveData)state;

            for (int i = 0; i < AllPs.Length; i++)
            {
                if (i <= saveData.purchasedState.Count - 1)
                {
                    AllPs[i].IsPurchased = saveData.purchasedState[i];
                }
                else
                {
                    AllPs[i].IsPurchased = false;
                }
            }
        }
        #endregion
    }
}