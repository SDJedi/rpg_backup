using RPG.Messaging;
using UnityEngine;

//Last Reviewed: 1/16/20
namespace RPG.ProgressionSystem
{
    //base class for perks purchased with character points
    //created via menu as ScriptableObject assets inside Unity
    public abstract class CP_Purchasable : ScriptableObject
    {
        [SerializeField] protected string CharName = "";
        [SerializeField] protected int cP_Cost = 0;

        protected Sprite icon;//set in InitializePurchasable()
        protected Vector2 notificationWindowPosition = new Vector2(381, -783);
        protected string nameOfPurchasable = ""; //set in InitializePurchasable()

        public bool IsPurchased; // { get; set; } = false;
        public int RequiredLevel { get; set; } //Note: set by CharacterPointManager.cs / SetUpButtons()
        public string NameOfPurchasable { get { return nameOfPurchasable; } }
        public int CP_Cost { get { return cP_Cost; } }
        public Sprite Icon { get { return icon; } }


        //see: SetUpButtons() in CharacterPointsManager.cs
        public void TryPurchasePurchasable()
        {
            if (AbleToPurchase())
            {
                CharacterPointsManager.Instance.ShowConfirmationPanel(this);
            }
        }

        //returns true if prerequisites are met, the player has enough CP to cover the cost, and it
        //hasn't already been purchased. 
        bool AbleToPurchase()
        {
            return (AllPrerequisitsMet() && CanAffordCP_Purchasable() && !IsPurchased);
        }

        bool CanAffordCP_Purchasable()
        {
            if (CharacterPoints_Tracker.Instance.CurrentCP < cP_Cost)
            {
                NotificationWindowManager.Instance.DisplayErrorMessage("Not enough Character Points", notificationWindowPosition);
            }
            return CharacterPoints_Tracker.Instance.CurrentCP >= cP_Cost;
        }

        //overloaded by the different purchasable types
        public virtual void CompletePurchase(bool activateForFree = false) //free is used in CombatTestEnvironment
        {
            IsPurchased = true;
            CharacterPoints_Tracker.Instance.PurchasablePurchased(this);
            if (!activateForFree)
            {
                CharacterPoints_Tracker.Instance.ModifyCharacterPoints(-CP_Cost);
                CharacterPointsManager.Instance.HideConfirmationPanel();
            }
        }

        protected abstract bool AllPrerequisitsMet();
        public abstract void InitializePurchasable();
        public abstract void ResetCP_Purchasable();
    }
}