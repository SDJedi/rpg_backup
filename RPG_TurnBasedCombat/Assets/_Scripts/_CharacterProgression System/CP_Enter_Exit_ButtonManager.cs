using System;
using RPG.CombatSystem;
using TMPro;
using UnityEngine;

//Last Reviewed: 1/16/20
namespace RPG.ProgressionSystem
{
    //attached to CharacterPointsManager in CharacterPointsScene.
    //manages the UI when mouse is over a purchasable button in CharacterPointsScene
    public class CP_Enter_Exit_ButtonManager : Singleton<CP_Enter_Exit_ButtonManager>
    {
        //stuff that applies to all types of purchasables
        [SerializeField] GameObject purchasableInfo_Panel = null;
        [SerializeField] TextMeshProUGUI purchasableName_Txt = null;
        [SerializeField] TextMeshProUGUI cP_Cost_Txt = null;
        [SerializeField] TextMeshProUGUI prerequistList_Txt = null;

        //CP_Purchasable_Ability specific
        [SerializeField] GameObject abilityInfoPanel = null;
        [SerializeField] GameObject durationPanel = null;
        [SerializeField] TextMeshProUGUI commandDescription_Txt = null;
        [SerializeField] TextMeshProUGUI hPCost_Txt = null;
        [SerializeField] TextMeshProUGUI mPCost_Txt = null;
        [SerializeField] TextMeshProUGUI tPCost_Txt = null;
        [SerializeField] TextMeshProUGUI targetType_Txt = null;
        [SerializeField] TextMeshProUGUI reapplicationType_Txt = null;
        [SerializeField] TextMeshProUGUI duration_Txt = null;

        //a generic panel for descriptions
        [SerializeField] GameObject descriptionPanel = null;
        [SerializeField] TextMeshProUGUI description_Txt = null;


        void Start()
        {
            //hide the master panel on Start()
            HidePurchasableInfoPanel();
        }

        //called by CP_Purchasable_ButtonScript.cs (attached to all buttons) OnPointerEnter()
        public void ShowPurchasableInfoPanel(CP_Purchasable purchasable)
        {
            SetNameAndCostOfPurchasable(purchasable);
            purchasableInfo_Panel.SetActive(true);

            switch (purchasable.GetType().Name)
            {
                case nameof(CP_Purchasable_Ability):
                    ShowAbilityInfoPanel((CP_Purchasable_Ability)purchasable);
                    break;
                case nameof(CP_Purchasable_DamageShield):
                case nameof(CP_Purchasable_Proc):
                    ShowDescriptionPanel(purchasable);
                    break;
                default:
                    Debug.LogWarning($"No purchasable of type {purchasable.GetType().Name} exists");
                    break;
            }
        }

        //hide the master panel
        public void HidePurchasableInfoPanel()
        {
            purchasableInfo_Panel.SetActive(false);
        }

        //set the UI text fields for name and cost
        void SetNameAndCostOfPurchasable(CP_Purchasable purchasable)
        {
            purchasableName_Txt.text = purchasable.NameOfPurchasable;
            cP_Cost_Txt.text = purchasable.CP_Cost.ToString();
        }

#region CP_Purchasable_Ability specific

        //show the sub-panel that has UI specifically for abilities
        void ShowAbilityInfoPanel(CP_Purchasable_Ability purchasableAbility)
        {
            HideDescriptionPanel();

            BaseCommand_Config abilityConfig = purchasableAbility.AbilityToAdd;
            commandDescription_Txt.text = abilityConfig.Description;
            hPCost_Txt.text = abilityConfig.HPCost.ToString();
            mPCost_Txt.text = abilityConfig.MPCost.ToString();
            tPCost_Txt.text = abilityConfig.TPCost.ToString();
            targetType_Txt.text = abilityConfig._TargetType.ToString();

            prerequistList_Txt.text = "";

            for (int i = 0; i < purchasableAbility.PrerequisitAbilities.Count; i++)
            {
                if (i < purchasableAbility.PrerequisitAbilities.Count - 1)
                {
                    prerequistList_Txt.text += purchasableAbility.PrerequisitAbilities[i].CmndName + " : ";
                }
                else
                {
                    prerequistList_Txt.text += purchasableAbility.PrerequisitAbilities[i].CmndName;
                }
            }

            if (IsSameOrSubclassOf(typeof(Command_SpellEffect), abilityConfig.CommandType) && ((BaseSpell_Config)abilityConfig).OnReApply != ReapplicationType.NONE)
            {
                reapplicationType_Txt.text = ((BaseSpell_Config)abilityConfig).OnReApply.ToString();
            }
            else
            {
                reapplicationType_Txt.text = "NA";
            }
            if (IsSameOrSubclassOf(typeof(Command_SpellEffect), abilityConfig.CommandType) && ((BaseSpell_Config)abilityConfig).TurnDuration > 0)
            {
                durationPanel.SetActive(true);
                duration_Txt.text = ((BaseSpell_Config)abilityConfig).TurnDuration.ToString();
            }
            else
            {
                durationPanel.SetActive(false);
            }

            abilityInfoPanel.SetActive(true);
        }

        //hide the ability sub-panel
        void HideAbilityInfoPanel()
        {
            abilityInfoPanel.SetActive(false);
        }

        bool IsSameOrSubclassOf(Type potentialBase, Type potentialDescendant)
        {
            return potentialDescendant.IsSubclassOf(potentialBase) ||
                potentialDescendant == potentialBase;
        }

#endregion

        //show the sub-panel that has just a description
        void ShowDescriptionPanel(CP_Purchasable purchasable)
        {
            HideAbilityInfoPanel();

            prerequistList_Txt.text = "";

            if (purchasable.GetType() == typeof(CP_Purchasable_DamageShield))
            {
                var dsPurchasable = (CP_Purchasable_DamageShield)purchasable;

                description_Txt.text = dsPurchasable.Description;

                for (int i = 0; i < dsPurchasable.PrerequisitAbilities.Count; i++)
                {
                    if (i < dsPurchasable.PrerequisitAbilities.Count - 1)
                    {
                        prerequistList_Txt.text += dsPurchasable.PrerequisitAbilities[i].CmndName + " : ";
                    }
                    else
                    {
                        prerequistList_Txt.text += dsPurchasable.PrerequisitAbilities[i].CmndName;
                    }
                }
            }
            else if (purchasable.GetType() == typeof(CP_Purchasable_Proc))
            {
                var procPurchasable = (CP_Purchasable_Proc)purchasable;

                description_Txt.text = procPurchasable.Description;

                for (int i = 0; i < procPurchasable.PrerequisitAbilities.Count; i++)
                {
                    if (i < procPurchasable.PrerequisitAbilities.Count - 1)
                    {
                        prerequistList_Txt.text += procPurchasable.PrerequisitAbilities[i].CmndName + " : ";
                    }
                    else
                    {
                        prerequistList_Txt.text += procPurchasable.PrerequisitAbilities[i].CmndName;
                    }
                }
            }

            descriptionPanel.SetActive(true);
        }

        //hide the description sub-panel
        void HideDescriptionPanel()
        {
            descriptionPanel.SetActive(false);
        }
    }
}
