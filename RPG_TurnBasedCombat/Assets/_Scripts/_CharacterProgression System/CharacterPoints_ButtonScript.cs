using UnityEngine;
using UnityEngine.EventSystems;

//Last Reviewed: 1/16/20
namespace RPG.ProgressionSystem
{
    //attached to each button used for purchasables (CharacterPointsScene > Canvas > InnerPanel_PurchaseButtons > Viewport_Inner > Content_Inner > "DaButtons")
    public class CharacterPoints_ButtonScript : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        CP_Purchasable purchasableAttachedToButton;

        //tell the CP_Enter_Exit_ButtonManager to show the proper info for the purchasable attached to this button
        public void OnPointerEnter(PointerEventData eventData)
        {
            if (purchasableAttachedToButton != null)
            {
                CP_Enter_Exit_ButtonManager.Instance.ShowPurchasableInfoPanel(purchasableAttachedToButton);
            }
        }

        //alert the CP_Enter_Exit_ButtonManager to hide the info on mouse exit
        public void OnPointerExit(PointerEventData eventData)
        {
            CP_Enter_Exit_ButtonManager.Instance.HidePurchasableInfoPanel();
        }

        //called by CharacterPointsManager.cs when buttons are set up or cleared
        public void AttachPurchasableToButton(CP_Purchasable p)
        {
            purchasableAttachedToButton = p;
        }
    }
}
