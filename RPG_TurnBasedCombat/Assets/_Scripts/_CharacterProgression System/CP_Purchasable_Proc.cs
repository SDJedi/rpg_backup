using System.Collections.Generic;
using RPG.CombatSystem;
using RPG.Messaging;
using UnityEngine;

//Last Reviewed: 1/20/20
namespace RPG.ProgressionSystem
{
    //allows for the creation of purchasable damage procs that can be added to a PC_SkillTree.
    //once purchased, the proc is always active for the PC
    [CreateAssetMenu(menuName = "CP_Purchasable / Purchasable_Proc")]
    public class CP_Purchasable_Proc : CP_Purchasable
    {
        [SerializeField] string nameOfProc = "";
        [SerializeField] BaseSpell_Config spellToProc = null;
        [SerializeField] Sprite _icon = null;
        [Range(0, 1)]
        [SerializeField] float procChance = .5f;
        [SerializeField] List<BaseCommand_Config> prerequisitAbilities = null;
        [TextArea]
        [SerializeField] string description = "";


        public List<BaseCommand_Config> PrerequisitAbilities { get { return prerequisitAbilities; } }
        public string Description { get { return description; } }


        protected override bool AllPrerequisitsMet()
        {
            if (PCManager.Instance.GetStatsForPC(CharName) == null)
            {
                Debug.Log("Stats for " + CharName + " came back null");
                return false;
            }

            if (PCManager.Instance.GetStatsForPC(CharName).CurLvl < RequiredLevel)
            {
                NotificationWindowManager.Instance.DisplayErrorMessage(CharName + " must be at least level " + RequiredLevel + " to purchase " + nameOfProc + ".", notificationWindowPosition);
                return false;
            }

            foreach (BaseCommand_Config command in prerequisitAbilities)
            {
                if (!PCManager.Instance.GetStatsForPC(CharName).KnownAbilities.Contains(command))
                {
                    NotificationWindowManager.Instance.DisplayErrorMessage("Missing prerequisite: " + command.CmndName, notificationWindowPosition);
                    return false;
                }
            }

            return true;
        }

        public override void CompletePurchase(bool activateForFree = false)
        {
            base.CompletePurchase(activateForFree);
            PC_ProcManager.Instance.AddProcToPC(CharName, CreateProcToAdd());
        }

        Proc CreateProcToAdd()
        {
            return new Proc(nameOfProc, spellToProc, procChance);
        }

        public override void ResetCP_Purchasable()
        {
            IsPurchased = false;
        }

        public override void InitializePurchasable()
        {
            nameOfPurchasable = nameOfProc;
            icon = _icon;
        }
    }
}