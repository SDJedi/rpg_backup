using System;
using RPG.CombatSystem;
using RPG.Messaging;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

//Last Reviewed: 2/3/20
namespace RPG.PartySelectSystem
{
    //attached to the AvailCharSlots and the PCs in party slots in the CharacterSelectScene.
    //allows the player to drop an image of a PC into a slot
    public class DropMe : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
    {
        public static event Action RenderTextureDropReceived;
        public static event Action RenderTextureDropFailed;

        Vector2 notificationWindowPos = new Vector2(-300, -812);

        public Color borderHighlightColor = new Color(107, 0, 142); //changes the border color (default purple)

        Image borderImage; //border around the slot
        Color borderColor; //default border color for the slot
        RawImage receivingImage; //attached to child object; holds the PC image
        GameObject objectBeingDragged; //set OnPointerEnter() by GetRenderTexture()


        void OnEnable()
        {
            borderImage = GetComponent<Image>();
            borderColor = borderImage.color;
            receivingImage = GetComponentInChildren<RawImage>(true);
        }

        public void OnDrop(PointerEventData data)
        {
            //reset border color
            borderImage.color = borderColor;

            //figure out what image we are dropping
            Texture texture = GetRenderTexture(data);

            //if we have a texture to work with...
            if (texture != null)
            {
                //check if it's ok to drop
                if (OkToCompleteTextureDrop(texture, gameObject, data.pointerDrag))
                {
                    //we're good to drop in this slot. will need to swap textures if this slot already contains an image
                    bool swappedTextures = SwapTexturesIfNeeded();

                    receivingImage.texture = texture; //change the image in this slot
                    receivingImage.enabled = true; //enable the component
                    //enable DeadIcon if PC is dead
                    var stats = PCManager.Instance.GetStatsForPC(PartySelectionManager.Instance.GetPCNameBasedOnImage(texture));
                    if (!stats.IsAlive())
                    {
                        transform.Find("DeadIcon").GetComponent<Image>().enabled = true;
                    }
                    //if image was dragged from a different slot && we didn't swap images...
                    if (objectBeingDragged != gameObject && !swappedTextures)
                    {
                        //erase image from dragged slot
                        objectBeingDragged.GetComponentInChildren<RawImage>().texture = null;
                    }

                    //drop was a success; fire event
                    if (RenderTextureDropReceived != null)
                    {
                        RenderTextureDropReceived();
                    }
                    return;
                }
            }

            //drop failed; fire event
            if (RenderTextureDropFailed != null)
            {
                RenderTextureDropFailed();
            }
        }

        //if we're dragging an image when pointer enters; highlight border
        public void OnPointerEnter(PointerEventData data)
        {
            Texture texture = GetRenderTexture(data);
            if (texture != null)
            {
                borderImage.color = borderHighlightColor;
            }
        }

        //reset border image color
        public void OnPointerExit(PointerEventData data)
        {
            borderImage.color = borderColor;
        }

        //get the image texture from the icon being dragged
        Texture GetRenderTexture(PointerEventData data)
        {
            objectBeingDragged = data.pointerDrag;
            if (objectBeingDragged == null)
            {
                return null;
            }

            var dragMe = objectBeingDragged.GetComponent<DragMe>();
            if (dragMe == null)
            {
                return null;
            }

            var srcImage = objectBeingDragged.GetComponentInChildren<RawImage>();
            if (srcImage == null)
            {
                return null;
            }

            return srcImage.texture;
        }

        //check to see if we already have an image in this slot.
        //if we do, replace the texure in the slot we were dragging from with the 
        //texture in this slot.
        bool SwapTexturesIfNeeded()
        {
            bool swapHappened = false;
            Texture curTexture = GetComponentInChildren<RawImage>(true).texture;
            if (curTexture != null)
            {
                objectBeingDragged.GetComponentInChildren<RawImage>().texture = curTexture;
                swapHappened = true;
            }
            return swapHappened;
        }

        //A series of checks to determine what happens when a Drop is attempted.
        //returns true if the drop is valid.
        bool OkToCompleteTextureDrop(Texture textureBeingDropped, GameObject slotReceiving, GameObject slotSending)
        {
            bool slotReceivingIsPartySlot = PartySelectionManager.Instance.PCsInPartySlots.Contains(slotReceiving);

            //the slot we are dropping into is one of the 4 party slots...
            if (slotReceivingIsPartySlot)
            {
                //check to see if there is already a character in this slot...
                var rtInSlot = slotReceiving.GetComponentInChildren<RawImage>().texture;
                if (rtInSlot == null)
                {
                    //it's empty, so we're good to drop any character from any slot into this slot...
                    return true;
                }

                //the party slot we are dropping into has a character in it...
                foreach (SelectablePC PC in PCManager.Instance.AcquiredTeamMembers)
                {
                    //loop through until we find the PC currently in the slot...
                    if (rtInSlot != PC.RenderTexture)
                    {
                        continue;
                    }
                    //Is the PC currently in this party slot a required party member?
                    if (PC.RequiredPartyMember)
                    {
                        //it is, so we can't move it into an AvailableCharactersSlot, BUT we could swap it
                        //with another character already in the party and move the existing PC to a 
                        //different Party Slot
                        if (!PartySelectionManager.Instance.PCsInPartySlots.Contains(slotSending))
                        {
                            //The PC we are trying to swap with came from an AvailableCharactersSlot,
                            //we can't make the swap because the PC in slotReceiving is required
                            NotificationWindowManager.Instance.DisplayErrorMessage(PC.PC_Prefab.GetComponent<PlayerCharacter>().CharName + " must remain in the party", notificationWindowPos);
                            return false;
                        }
                    }
                }
                //if we reach here, either the sendingslot PC was already in the party || the PC being replaced in the slot
                //is not a required Party member ... either way, we're gtg with a swap
                return true;
            }

            //we are attempting to move a PC to an AvailableCharacterSlot...
            foreach (SelectablePC PC in PCManager.Instance.AcquiredTeamMembers)
            {
                //Find a texture match...
                if (textureBeingDropped != PC.RenderTexture)
                {
                    continue;
                }
                //Check if the PC has to stay in the Party
                if (PC.RequiredPartyMember)
                {
                    NotificationWindowManager.Instance.DisplayErrorMessage(PC.PC_Prefab.GetComponent<PlayerCharacter>().CharName + " must remain in the party", notificationWindowPos);
                    return false;
                }
            }

            //By default ... allow the drop
            return true;
        }
    }
}
