using System.Collections.Generic;
using System.Linq;
using RPG.CombatSystem;
using UnityEngine;

//Last Reviewed: 2/3/20
namespace RPG.PartySelectSystem
{
    //enable this gameObject to add PCPrefab to the party
    public class PlacePCIntoFirstAvailablePartySlotOnEnable : MonoBehaviour
    {
        [SerializeField] GameObject PCPrefab = null;
        [SerializeField] bool requiredInParty = false;

        string pCName;


        void OnEnable()
        {
            //check if already in party...
            if (PCManager.Instance.PCsInParty_Prefabs.Contains(PCPrefab))
            {
                Debug.LogWarning(PCPrefab.name + " was already in the party");
                return;
            }

            //make sure we have a valid prefab...
            if (PCPrefab == null || PCPrefab.GetComponent<PlayerCharacter>() == null)
            {
                Debug.LogWarning("No valid PC to add");
                return;
            }

            pCName = PCPrefab.GetComponent<PlayerCharacter>().CharName;
            TryPlacePCInParty();
        }

        //Place a PC directly into the party if able
        void TryPlacePCInParty()
        {
            //make sure there is room in the party...
            int emptySlots = 4;

            foreach (GameObject PC in PCManager.Instance.PCsInParty_Prefabs)
            {
                if (PC != null)
                {
                    emptySlots--;
                }
            }

            //no room...
            if (emptySlots <= 0)
            {
                if (!requiredInParty)
                {
                    //PC not required in party, so set status to available. PC will appear in available slot
                    //in party select scene and we're done.
                    Debug.LogWarning("Party is full... placing " + pCName + " in available party slots");
                    SetStatus();
                    return;
                }

                //this PC needs to be in the party ... kick out an optional PC and
                //put the PC in the party...
                bool removedOne = false;

                foreach (SelectablePC sPC in PCManager.Instance.AcquiredTeamMembers)
                {
                    for (int i = 0; i < PCManager.Instance.PCsInParty_Prefabs.Length; i++)
                    {
                        if (sPC.PC_Prefab == PCManager.Instance.PCsInParty_Prefabs[i])
                        {
                            if (!sPC.RequiredPartyMember)
                            {
                                PCManager.Instance.PCsInParty_Prefabs[i] = null;
                                removedOne = true;
                                break;
                            }
                        }
                    }

                    if (removedOne)
                    {
                        break;
                    }
                }

                //failsafe ... could only happen if we have 4 members in the party marked
                //as required and we are trying to add a 5th required member.
                if (!removedOne)
                {
                    Debug.LogWarning("Trying to add " + pCName + " as a required party member, but couldn't because all members in party are set as required");
                    SetStatus();
                    return;
                }
            }

            //we have room ... find the first available slot
            List<int> takenSlots = new List<int>();

            foreach (PC_Stats_Config config in PCManager.Instance.PC_StatConfigs)
            {
                foreach (GameObject PC in PCManager.Instance.PCsInParty_Prefabs)
                {
                    if (PC != null && PC.GetComponent<PlayerCharacter>().CharName == config.CharName)
                    {
                        takenSlots.Add(config.PC_Slot_position);
                        break;
                    }
                }
            }

            //add the new PC to the party
            for (int i = 0; i < PCManager.Instance.PCsInParty_Prefabs.Length; i++)
            {
                if (!takenSlots.Contains(i))
                {
                    AddPrefabToPartyPrefabs();
                    SetStatus();
                    SetSlotPosition(i);
                    SetCScript();
                    return;
                }
            }
        }

        #region Helper Methods
        void AddPrefabToPartyPrefabs()
        {
            for (int i = 0; i < PCManager.Instance.PCsInParty_Prefabs.Length; i++)
            {
                if (PCManager.Instance.PCsInParty_Prefabs[i] == null)
                {
                    PCManager.Instance.PCsInParty_Prefabs[i] = PCPrefab;
                    return;
                }
            }
        }

        void SetStatus()
        {
            foreach (SelectablePC PC in PCManager.Instance.AcquiredTeamMembers)
            {
                if (pCName == PC.PC_Name)
                {
                    PC.IsAvailable = true;
                    PC.RequiredPartyMember = requiredInParty;
                }
            }
        }

        void SetSlotPosition(int index)
        {
            foreach (PC_Stats_Config config in PCManager.Instance.PC_StatConfigs)
            {
                if (config.CharName == pCName)
                {
                    config.PC_Slot_position = index;
                    return;
                }
            }
        }

        void SetCScript()
        {
            foreach (PC_Stats_Config config in PCManager.Instance.PC_StatConfigs)
            {
                if (config.CharName == pCName)
                {
                    config.SetCScript(PCPrefab.GetComponent<Character>());
                    return;
                }
            }
        }
    }
    #endregion
}