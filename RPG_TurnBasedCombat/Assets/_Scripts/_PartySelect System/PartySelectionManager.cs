using System;
using System.Collections.Generic;
using RPG.CombatSystem;
using RPG.Saving;
using RPG.SceneControl;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

//Last Reviewed: 2/3/20
namespace RPG.PartySelectSystem
{
    //a collection of data for a PC
    [System.Serializable]
    public class SelectablePC
    {
        public string PC_Name;
        public Texture RenderTexture;
        public GameObject PC_Prefab;
        public bool RequiredPartyMember;
        public bool IsAvailable;
    }

    //this class lives in the CharacterSelectScene and works with PCManager to 
    //determine which PCs are currently in the party and what slots they occupy
    public class PartySelectionManager : Singleton<PartySelectionManager>, ISaveable
    {
        public TextMeshProUGUI[] SlotBonusDescriptions_Txt = new TextMeshProUGUI[4]; //UI that tells what bonus is bestowed by each slot
        public List<GameObject> AvailableCharacterSlots; //Holds images of characters the player has access to that are NOT in the Party
        public List<GameObject> PCsInPartySlots; //Holds images of characters that ARE in the Party
        public string[] SlotBonusDescriptions = new string[4];


        void Update()
        {
            //exit scene with "P"
            if (Input.GetKeyDown(KeyCode.P))
            {
                UpdatePartyPrefabsAndLoadLastScene();
            }
        }

        void OnEnable()
        {
            SceneController.Instance.AfterSceneLoad += AfterSaveDataIsLoaded;
        }

        void OnDisable()
        {
            if (SceneController.InstanceExists)
            {
                SceneController.Instance.AfterSceneLoad -= AfterSaveDataIsLoaded;
            }
        }

        void AfterSaveDataIsLoaded()
        {
            LoadSlotBonusDescriptions(); //populate the slot bonus descriptions
            ClearPartySlots(); //erases all images from PartySlots
            LoadPartyMembersIntoProperSlots(); //Set the PartySlot images according to the PCs in Party (See PCManager.Instance.PCsInParty_Prefabs)
            LoadAllAvailableMembersNotShown();//ensure that all available party members are showing up in scene
        }

        //called by "Done" button or "P" press.
        //sets up the party based on the images in the party slots
        public void UpdatePartyPrefabsAndLoadLastScene()
        {
            //empty out the party prefab array
            Array.Clear(PCManager.Instance.PCsInParty_Prefabs, 0, PCManager.Instance.PCsInParty_Prefabs.Length);
            //loop through the PCsInPartySlots to see which images are in the 4 party slots
            for (int i = 0; i < PCsInPartySlots.Count; i++)
            {
                //store a reference to the image/texture
                var texture = PCsInPartySlots[i].GetComponentInChildren<RawImage>().texture;
                if (texture == null)
                {
                    //if the slot is empty, continue
                    continue;
                }
                //slot[i] has an image; load the PC prefab that coresponds to the image
                PCManager.Instance.PCsInParty_Prefabs[i] = PrefabThatMatchesTexture(texture);

                //set this PCs slot position = [i]
                var stats = PCManager.Instance.GetInstantiatedStatsConfigForPC(PCManager.Instance.PCsInParty_Prefabs[i]);
                stats.PC_Slot_position = i;
            }
            //exit the scene with our chosen party
            SceneController.Instance.FadeAndLoadScene(SceneController.Instance.nameOfMostRecentlyUnloadedScene);
        }

        //update the slot bonus descriptions
        void LoadSlotBonusDescriptions()
        {
            for (int i = 0; i < SlotBonusDescriptions.Length; i++)
            {
                SlotBonusDescriptions_Txt[i].text = SlotBonusDescriptions[i];
            }
        }

        //returns a PC prefab based on it's matching image/texture
        GameObject PrefabThatMatchesTexture(Texture texture)
        {
            foreach (SelectablePC PC in PCManager.Instance.AcquiredTeamMembers)
            {
                if (PC.RenderTexture == texture)
                {
                    return PC.PC_Prefab;
                }
            }
            return null;
        }

        //returns an image/texture based on a PCs name
        Texture TextureThatMatchesPCName(string name)
        {
            foreach (SelectablePC PC in PCManager.Instance.AcquiredTeamMembers)
            {
                if (PC.PC_Name == name)
                {
                    return PC.RenderTexture;
                }
            }
            return null;
        }

        //erases all images from PartySlots
        void ClearPartySlots()
        {
            foreach (GameObject slot in PCsInPartySlots)
            {
                var img = slot.GetComponentInChildren<RawImage>();
                img.texture = null;
                img.enabled = false;
            }
        }

        //loop through the PCs in the party;
        //place the correct PC image in the correct party slot
        //Note: available PCs that are NOT in the party are loaded into
        //available slots by saved data
        void LoadPartyMembersIntoProperSlots()
        {
            var pfbs = PCManager.Instance.PCsInParty_Prefabs;
            for (int i = 0; i < pfbs.Length; i++)
            {
                if (pfbs[i] == null)
                {
                    continue;
                }
                var stats = PCManager.Instance.GetStatsForPC(pfbs[i].GetComponent<PlayerCharacter>().CharName);

                int slotNum = stats.PC_Slot_position;
                PCsInPartySlots[slotNum].GetComponentInChildren<RawImage>().texture = TextureThatMatchesPCName(pfbs[i].GetComponent<PlayerCharacter>().CharName);
                PCsInPartySlots[slotNum].GetComponentInChildren<RawImage>().enabled = true;
                if (!stats.IsAlive())
                {
                    PCsInPartySlots[slotNum].transform.Find("DeadIcon").GetComponent<Image>().enabled = true;
                }
            }
        }

        void LoadAllAvailableMembersNotShown()
        {
            foreach (SelectablePC s_PC in PCManager.Instance.AcquiredTeamMembers)
            {
                if (s_PC.IsAvailable && !PortraitIsShowing(s_PC))
                {
                    ShowPortraitInFirstAvailableSlot(s_PC);
                }
            }
        }

        //check if available PC is showing up in either in-party slot or available slot.
        bool PortraitIsShowing(SelectablePC s_PC)
        {
            //is portrait showing in party slot?
            foreach (GameObject slot in PCsInPartySlots)
            {
                if (slot.GetComponentInChildren<RawImage>().texture == TextureThatMatchesPCName(s_PC.PC_Name))
                {
                    return true;
                }
            }
            //is portrait showing in available slot?
            foreach (GameObject slot in AvailableCharacterSlots)
            {
                if (slot.GetComponentInChildren<RawImage>().texture == TextureThatMatchesPCName(s_PC.PC_Name))
                {
                    return true;
                }
            }
            //uh oh! portrait of an available pc is not displayed anywhere!
            //Debug.Log("no p for " + s_PC.PC_Name);
            return false;
        }

        void ShowPortraitInFirstAvailableSlot(SelectablePC s_PC)
        {
            foreach (GameObject slot in AvailableCharacterSlots)
            {
                var img = slot.GetComponentInChildren<RawImage>();
                if (img.texture == null)
                {
                    img.texture = TextureThatMatchesPCName(s_PC.PC_Name);
                    img.enabled = true;
                    //Debug.Log("set img for " + s_PC.PC_Name);
                    return;
                }
            }
        }

        #region SaveData
        [System.Serializable]
        //for available PCs NOT in the party, we want to
        //save which PC is in which available slot
        class PSM_SaveData
        {
            public string[] availCharSlotNames = new string[8];
        }

        public object CaptureState()
        {
            PSM_SaveData saveData = new PSM_SaveData();

            //save the names of PCs in the available slots
            Texture texture;
            for (int i = 0; i < AvailableCharacterSlots.Count; i++)
            {
                if ((texture = AvailableCharacterSlots[i].GetComponentInChildren<RawImage>().texture) == null)
                {
                    saveData.availCharSlotNames[i] = "";
                }
                else
                {
                    saveData.availCharSlotNames[i] = GetPCNameBasedOnImage(texture);
                }
            }

            return saveData;
        }

        public void RestoreState(object state)
        {
            PSM_SaveData saveData = (PSM_SaveData)state;

            foreach (GameObject slot in AvailableCharacterSlots)
            {
                var img = slot.GetComponentInChildren<RawImage>(true);
                img.texture = null;
                img.enabled = false;
            }

            for (int i = 0; i < saveData.availCharSlotNames.Length; i++)
            {
                RawImage img = AvailableCharacterSlots[i].GetComponentInChildren<RawImage>(true);
                img.texture = TextureThatMatchesPCName(saveData.availCharSlotNames[i]);
                if (img.texture != null)
                {
                    img.enabled = true;
                    var stats = PCManager.Instance.GetStatsForPC(saveData.availCharSlotNames[i]);
                    if (!stats.IsAlive())
                    {
                        AvailableCharacterSlots[i].transform.Find("DeadIcon").GetComponent<Image>().enabled = true;
                    }
                }
            }
        }

        public string GetPCNameBasedOnImage(Texture texture)
        {
            foreach (SelectablePC PC in PCManager.Instance.AcquiredTeamMembers)
            {
                if (PC.RenderTexture == texture)
                {
                    return PC.PC_Name;
                }
            }
            return "";
        }
    }
    #endregion
}