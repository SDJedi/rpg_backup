using System.Collections.Generic;
using RPG.CombatSystem;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

//Last Reviewed: 2/3/20
namespace RPG.PartySelectSystem
{
    //attached to the AvailCharSlots and the PCs in party slots in the CharacterSelectScene.
    //allows the player to drag images of PCs in-and-out of party slots.
    public class DragMe : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        Dictionary<int, GameObject> m_DraggingIcons = new Dictionary<int, GameObject>();
        Dictionary<int, RectTransform> m_DraggingPlanes = new Dictionary<int, RectTransform>();


        void OnEnable()
        {
            DropMe.RenderTextureDropReceived += DropComplete;
            DropMe.RenderTextureDropFailed += DropFailed;
        }

        void OnDisable()
        {
            DropMe.RenderTextureDropReceived -= DropComplete;
            DropMe.RenderTextureDropFailed -= DropFailed;
        }

        //called by a BaseInputModule before a drag is started.
        public void OnBeginDrag(PointerEventData eventData)
        {
            var canvas = FindInParents<Canvas>(gameObject);
            if (canvas == null)
            {
                return;
            }

            if (!gameObject.GetComponentInChildren<RawImage>().enabled)
            {
                m_DraggingIcons[eventData.pointerId] = null;
                OnEndDrag(eventData);
                return;
            }

            // We have clicked something that can be dragged.
            // What we want to do is create a GameObject called "icon" for this.
            // we store icon in the m_DraggingIcons Dictionary where the key == pointerID [left-mouse == -1; right-mouse == -2; center-mouse == -3]
            m_DraggingIcons[eventData.pointerId] = new GameObject("icon");
            m_DraggingIcons[eventData.pointerId].transform.SetParent(canvas.transform, false);
            m_DraggingIcons[eventData.pointerId].transform.SetAsLastSibling();

            //add a <RawImage> to icon
            var image = m_DraggingIcons[eventData.pointerId].AddComponent<RawImage>();
            // The icon will be under the cursor.
            // We want it to be ignored by the event system.
            var group = m_DraggingIcons[eventData.pointerId].AddComponent<CanvasGroup>();
            group.blocksRaycasts = false;

            //copy the image texture in slot into our icon<RawImage>().texture; make it 50% bigger
            Vector2 imgSize = GetComponentInChildren<RawImage>().rectTransform.sizeDelta;
            Vector2 enhancedSize = (imgSize * 1.5f);
            image.rectTransform.sizeDelta = enhancedSize;
            image.texture = GetComponentInChildren<RawImage>().texture;

            //hide the img in slot while drag is happening
            gameObject.GetComponentInChildren<RawImage>().enabled = false;
            //hide the DeadIcon
            transform.Find("DeadIcon").GetComponent<Image>().enabled = false;

            //the canvas will act as our dragging plane
            m_DraggingPlanes[eventData.pointerId] = canvas.transform as RectTransform;

            SetDraggedPosition(eventData);
        }

        //move icon as we drag it
        public void OnDrag(PointerEventData eventData)
        {
            if (m_DraggingIcons[eventData.pointerId] != null)
            {
                SetDraggedPosition(eventData);
            }
        }

        //if cursor is inside the dragging plane, set the position of the icon to the cursor position
        void SetDraggedPosition(PointerEventData eventData)
        {
            var rt = m_DraggingIcons[eventData.pointerId].GetComponent<RectTransform>();
            Vector3 globalMousePos;
            if (RectTransformUtility.ScreenPointToWorldPointInRectangle(m_DraggingPlanes[eventData.pointerId], eventData.position, eventData.pressEventCamera, out globalMousePos))
            {
                rt.position = globalMousePos;
                rt.rotation = m_DraggingPlanes[eventData.pointerId].rotation;
            }
        }

        //called by a BaseInputModule when a drag is ended.
        public void OnEndDrag(PointerEventData eventData)
        {
            //destroy "icon" if it exists
            if (m_DraggingIcons[eventData.pointerId] != null)
            {
                Destroy(m_DraggingIcons[eventData.pointerId]);
            }

            m_DraggingIcons[eventData.pointerId] = null;
            eventData.pointerDrag = null;

            UpdateImage();
        }

        void DropComplete()
        {
            UpdateImage();
        }

        void DropFailed()
        {
            UpdateImage();
        }

        //enable or disable the RawImage based on whether or not it holds a texture
        void UpdateImage()
        {
            var img = gameObject.GetComponentInChildren<RawImage>(true);
            if (img.texture != null)
            {
                img.enabled = true;
                //if PC is dead, also enable DeadIcon
                var stats = PCManager.Instance.GetStatsForPC(PartySelectionManager.Instance.GetPCNameBasedOnImage(img.texture));
                var deadIcon = transform.Find("DeadIcon").GetComponent<Image>();
                if (!stats.IsAlive())
                {
                    deadIcon.enabled = true;
                }
                else
                {
                    deadIcon.enabled = false;
                }
            }
        }

        static public T FindInParents<T>(GameObject go)where T : Component
        {
            if (go == null)
            {
                return null;
            }

            var comp = go.GetComponent<T>();

            if (comp != null)
            {
                return comp;
            }

            var t = go.transform.parent;
            while (t != null && comp == null)
            {
                comp = t.gameObject.GetComponent<T>();
                t = t.parent;
            }
            return comp;
        }
    }
}
