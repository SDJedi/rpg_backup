//Last Reviewed: 2/18/20
namespace RPG.CombatSystem
{
    //Do something when a character gets hit in combat
    public interface IAttackable
    {
        void OnHit(Damage_Config damageConfig, Attack incomingAttack, Base_Stats_Config attacker, Base_Stats_Config defender, bool triggerProcs);
    }
}
