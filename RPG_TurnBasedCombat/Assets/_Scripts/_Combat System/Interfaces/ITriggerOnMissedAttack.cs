//Last Reviewed: 2/18/20
namespace RPG.CombatSystem
{
    //Do something when a character is attacked, but the attack misses
    public interface ITriggerOnMissedAttack
    {
        void OnMiss(Damage_Config damageConfig, Attack incomingAttack, Base_Stats_Config attacker, Base_Stats_Config defender, bool triggerProcs);
    }
}
