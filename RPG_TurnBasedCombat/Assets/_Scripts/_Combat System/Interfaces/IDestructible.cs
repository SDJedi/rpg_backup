//Last Reviewed: 2/18/20
namespace RPG.CombatSystem
{
    //Do something when a character dies in combat (called by Attacked_TakeDamage)
    public interface IDestructible
    {
        void OnDeath();
    }
}
