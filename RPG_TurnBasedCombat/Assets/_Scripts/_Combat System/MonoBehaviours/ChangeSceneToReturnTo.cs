﻿using UnityEngine;

//set the scene that the player will be sent to after combat
//Example: on Ed_Caravan enemy, this script returns the player to 
//OverWorld_2 after killing Ed instead of back to the DefendTheCaravan Scene
public class ChangeSceneToReturnTo : MonoBehaviour
{
    [SerializeField] string sceneName;
    [SerializeField] string spawnPointName;

    public string returnScene { get { return sceneName; } private set {} }
    public string spawnPoint { get { return spawnPointName; } private set {} }
}
