using System.Collections;
using UnityEngine;

//Last Reviewed: 2/25/20
namespace RPG.CombatSystem
{
    //attached to each character (on a child component that has the animator).
    //set the variables first, then allow an animation event to trigger a Command
    //by calling AttackFromAnimation() at the moment a hit should occur.
    public class GenerateAttackFromAnimationEvent : MonoBehaviour
    {
        Character character;
        Damage_Config dc;
        GameObject Effect;
        Transform EffectSpawnLoc;
        BaseCommand CallingCommand;
        bool triggerProcs;


        void Awake()
        {
            character = GetComponentInParent<Character>();
        }

        //Note: setup should happen just prior to triggering animation
        public void Setup_GAFAE(BaseCommand callingCommand, Damage_Config base_dc, GameObject effect, Transform effectSpawnLoc)
        {
            if (callingCommand == null)
            {
                Debug.LogWarning("Setup_GAFAE needs a <BaseCommand>callingCommand to work");
                return;
            }

            dc = base_dc;
            Effect = effect;
            EffectSpawnLoc = effectSpawnLoc;
            CallingCommand = callingCommand;
            triggerProcs = callingCommand.ThisCommandTriggersProcs;
        }

        //odd name is so that I don't use in an animation event
        void Reset_Internal()
        {
            dc = null;
            Effect = null;
            EffectSpawnLoc = null;
            CallingCommand = null;
            triggerProcs = false;
        }

#region Animation Events
        //add this event to an early frame of the animation clip
        public void SetIsAttackingToTrue()
        {
            if (CallingCommand != null && CallingCommand.OnCastSoundEffect != null)
            {
                SoundManager.Instance.PlayAudioClip(CallingCommand.OnCastSoundEffect);
            }
        }

        //add this near the end of the animation clip
        public void SetIsAttackingToFalse()
        {
            GetComponent<Animator>().ResetTrigger("Hit"); //reset the Hit trigger in case of counter-attacks
            Reset_Internal();
        }

        //add this to the point in the animation clip where a "hit" should happen
        public void AttackFromAnimation()
        {
            StartCoroutine(Attack_InternalCoroutine());
        }

        //odd name is so that I don't use in an animation event
        IEnumerator Attack_InternalCoroutine()
        {
            if (dc == null)
            {
                Debug.Log("No BaseAttackeDefinition set for animation event");
                yield break;
            }

            if (character.Stats_Config == null)
            {
                Debug.Log(character + " has no Stats_Config");
                yield break;
            }

            if (character.HostileTarget == null)
            {
                Debug.Log(character + " has a null HostileTarget");
                yield break;
            }

            if (CallingCommand == null)
            {
                Debug.Log("calling command not set");
                yield break;
            }

            if (GenerateAttack.ExecuteAttack(dc, character.Stats_Config, character.HostileTarget.gameObject, triggerProcs))
            {
                if (CallingCommand.OnHitSoundEffect != null)
                {
                    SoundManager.Instance.PlayAudioClip(CallingCommand.OnHitSoundEffect);
                }

                if (Effect != null)
                {
                    var e = Instantiate(Effect, EffectSpawnLoc);
                    ScaleObjectWithModel.MatchScale(character.HostileTarget.gameObject, e);
                }

                yield return SpellAndProcManager.Instance.ProcAdditionalEffects(character, CallingCommand);
            }
        }
#endregion
    }
}
