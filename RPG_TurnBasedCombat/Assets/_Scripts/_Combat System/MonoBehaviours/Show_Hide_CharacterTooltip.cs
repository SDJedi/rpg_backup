using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

//Last Reviewed: 2/26/20
namespace RPG.CombatSystem
{
    //found in CombatScene --> CombatScene_Canvas --> CharacterTooltip.
    //works with the CombatRaycaster to display character info on mouse over
    public class Show_Hide_CharacterTooltip : MonoBehaviour
    {
        public Text[] textFields;
        public List<TextMeshProUGUI> statEffects;
        public GameObject ToolTipChildObject;


        void Start()
        {
            CombatRaycaster.Instance.OnMouseOverEnemy += ShowTooltip;
            CombatRaycaster.Instance.OnMouseOverPC += ShowTooltip;
            CombatRaycaster.Instance.OnMouseNotOverEnemyOrPC += HideTooltip;
        }

        void OnDestroy()
        {
            if (CombatRaycaster.InstanceExists)
            {
                CombatRaycaster.Instance.OnMouseOverEnemy -= ShowTooltip;
                CombatRaycaster.Instance.OnMouseOverPC -= ShowTooltip;
                CombatRaycaster.Instance.OnMouseNotOverEnemyOrPC -= HideTooltip;
            }
        }

        void ShowTooltip(GameObject character)
        {
            Character CScript = character.GetComponentInParent<Character>();
            Base_Stats_Config stats = CScript.Stats_Config;
            ToolTipChildObject.SetActive(true);

            textFields[0].text = CScript.Stats_Config.CharName;
            textFields[1].text = stats.CurHP.ToString();
            textFields[2].text = stats.CurMP.ToString();
            textFields[3].text = stats.CurTP.ToString();
            textFields[4].text = stats.STR >= 30 ? "30 (MAX)" : stats.STR <= 1 ? "1 (MIN)" : stats.STR.ToString();
            textFields[5].text = stats.INT >= 30 ? "30 (MAX)" : stats.INT <= 1 ? "1 (MIN)" : stats.INT.ToString();
            textFields[6].text = stats.AGI >= 30 ? "30 (MAX)" : stats.AGI <= 1 ? "1 (MIN)" : stats.AGI.ToString();
            textFields[7].text = stats.DEF.ToString();
            textFields[8].text = stats.DmgReduction.ToString();
            textFields[9].text = stats.BaseAtkBonus.ToString();
            textFields[10].text = stats.MagRes >= 1 ? "100%" : stats.MagRes <= 0 ? "0%" : (stats.MagRes * 100).ToString() + "%";
            textFields[11].text = stats.InitiativeMod.ToString();
            textFields[12].text = stats.DamageBonus.ToString();

            for (int i = 0; i < statEffects.Count; i++)
            {
                if (CScript.ActiveSpellEffects.Count > i)
                {
                    statEffects[i].gameObject.SetActive(true);
                    statEffects[i].text = CScript.ActiveSpellEffects[i].CmndName + " (" + CScript.ActiveSpellEffects[i].turnsRemaining + ")";
                }
                else
                {
                    statEffects[i].gameObject.SetActive(false);
                }
            }
        }

        void HideTooltip()
        {
            if (ToolTipChildObject.activeSelf)
            {
                ToolTipChildObject.SetActive(false);
            }
        }
    }
}
