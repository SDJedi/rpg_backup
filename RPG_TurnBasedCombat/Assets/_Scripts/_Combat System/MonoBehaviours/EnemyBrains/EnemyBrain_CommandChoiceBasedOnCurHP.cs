using System.Collections.Generic;
using System.Linq;
using RPG.DialogueSystem;
using UnityEngine;

//Last Reviewed: 2/25/20
namespace RPG.CombatSystem
{
    //a "brain" that allows for more interesting enemy AI.
    //queues different commands in the TurnManager --> CommandQueue based on HP.
    //Note: also activates counter melee attack when below 50% if the enemy has
    //an "Attacked_CounterMeleeAttack" component that is disabled.
    public class EnemyBrain_CommandChoiceBasedOnCurHP : EnemyBrain
    {
        //optional stuff if we want to have this enemy speak 
        //in combat at certain hp thresholds
        [SerializeField] Sprite portrait = null;
        [SerializeField] string speakerName = "";
        [TextArea(3, 7)]
        [SerializeField] string Under75PercentMsg = "";
        [TextArea(3, 7)]
        [SerializeField] string Under50PercentMsg = "";

        //drag the 3 commands we want used into this list
        [Header("3 Commands: [0] == over 75% HP :: [1] == 50% - 75% HP :: [2] == below 50% HP")]
        [SerializeField] protected List<BaseCommand> AvailableCommands = new List<BaseCommand>(3);

        bool _75msgSent;
        bool _50msgSent;


        //over 75% HP, will queue AvailableCommands[0].
        //between 75% - 50%, will queue AvailableCommands[1].
        //under 50% will queue AvailableCommands[2].
        public override BaseCommand DetermineCommandToUse()
        {
            /////////////////////Failsafe - select a random command////////////////////////
            AvailableCommands.RemoveAll(n => (n == null));
            if (AvailableCommands.Count < 3)
            {
                AvailableCommands.Clear();
                AvailableCommands = (GetComponents<BaseCommand>()).ToList();
                for (int i = AvailableCommands.Count - 1; i >= 0; i--)
                {
                    if (!AvailableCommands[i].isActiveAndEnabled)
                    {
                        AvailableCommands.Remove(AvailableCommands[i]);
                    }
                }
                AvailableCommands.RemoveAll(n => (n == null));
                return AvailableCommands[Random.Range(0, AvailableCommands.Count)];
            }
            ////////////////////////////////////////////////////////////////////////////////

            Base_Stats_Config stats = GetComponent<Character>().Stats_Config;

            //Enemy is above 75% HP
            if (stats.CurHP > (stats.MaxHP * 0.75f))
            {
                //print("HP over 75% ... " + AvailableCommands[0] + " selected");
                return AvailableCommands[0];
            }
            //Enemy is between 50% and 75% HP
            else if (stats.CurHP > (stats.MaxHP * 0.5f))
            {
                //print("HP between 50% and 75% ... " + AvailableCommands[1] + " selected");
                if (!_75msgSent && Under75PercentMsg != "")
                {
                    CombatDialogueManager.Instance.SaySomethingInCombat(portrait, speakerName, Under75PercentMsg, 8);
                    _75msgSent = true;
                }
                return AvailableCommands[1];
            }
            //Enemy is under 50% HP ... activate cma
            else
            {
                var cma = GetComponent<Attacked_CounterMeleeAttack>();
                if (cma != null && !cma.isActiveAndEnabled)
                {
                    //print("Enemy can now counter melee attacks");
                    cma.enabled = true;
                }
                //print("HP under 50% ... " + AvailableCommands[2] + " selected");
                if (!_50msgSent && Under50PercentMsg != "")
                {
                    //Combat_UI_Manager.Instance.SetMsg_TopOfScreen(Under50PercentMsg, Color.white);
                    CombatDialogueManager.Instance.SaySomethingInCombat(portrait, speakerName, Under50PercentMsg, 8);
                    _50msgSent = true;
                }
                return AvailableCommands[2];
            }
        }
    }
}
