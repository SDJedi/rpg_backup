﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.CombatSystem
{
    //set a priority list of attacks ... if can't afford spell, check the next spell on the list
    public class EnemyBrain_CastIfCanAfford : EnemyBrain
    {
        [Header("Can be left empty. If not will draw from this list first")]
        [SerializeField] List<BaseCommand> randomizedCommands;
        [Header("If no random command available, will draw command from this list.")]
        [SerializeField] List<BaseCommand> commandsInOrder;


        public override BaseCommand DetermineCommandToUse()
        {
            //first, choose from the list of randomized commands.
            //if enemy can afford the casting cost, select it.
            if (randomizedCommands.Count > 0)
            {
                int i = Random.Range(0, randomizedCommands.Count);
                if (randomizedCommands[i].CanAffordCosts)
                {
                    return randomizedCommands[i];
                }
            }
            //if there are no commands in the randomized list, OR the enemy lacks the resources to 
            //cast the randomly selected command, move to the commandsInOrder list
            foreach (BaseCommand cmnd in commandsInOrder)
            {
                if (cmnd.CanAffordCosts)
                {
                    return cmnd;
                }
            }
            Debug.LogWarning("Could not find command for " + GetComponent<EnemyCharacter>().EnemyName);
            return null;
        }
    }
}