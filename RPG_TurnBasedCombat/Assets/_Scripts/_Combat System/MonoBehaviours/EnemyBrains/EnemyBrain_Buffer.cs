﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RPG.CombatSystem
{
    //prioritizes casting a buff. if buff exists on target(s), choses an alternate command instead
    public class EnemyBrain_Buffer : EnemyBrain
    {
        public enum BuffTargets { Self, All }

        [SerializeField] BuffTargets buffTargets = BuffTargets.All;
        [SerializeField] BaseCommand buffToUse = null;

        List<BaseCommand> alternateCommands = null;
        EnemyCharacter CScript;


        void Awake()
        {
            CScript = GetComponent<EnemyCharacter>();
            alternateCommands = GetComponents<BaseCommand>().ToList();
            alternateCommands.Remove(buffToUse);
        }

        public override BaseCommand DetermineCommandToUse()
        {
            //can afford buff?
            if (buffToUse.CanAffordCosts)
            {
                //check to see if buff already exists on target(s)...
                switch (buffTargets)
                {
                    case BuffTargets.Self:
                        if (!CScript.CheckForActiveSpellEffect(buffToUse.CommandName))
                        {
                            CScript.SetFriendlyTarget(CScript);
                            return buffToUse;
                        }
                        break;
                    case BuffTargets.All:
                        for (int i = 0; i < EnemyManager.Instance.EnemiesInEncounter.Count; i++)
                        {
                            Character c = EnemyManager.Instance.EnemiesInEncounter[i].GetComponent<Character>();
                            if (c.IsAlive && !c.CheckForActiveSpellEffect(buffToUse.CommandName))
                            {
                                CScript.SetFriendlyTarget(c);
                                return buffToUse;
                            }
                        }
                        Debug.Log(buffToUse.CommandName + " exists on all enemies");
                        break;
                }
            }

            if (alternateCommands.Count > 0)
            {
                return alternateCommands[Random.Range(0, alternateCommands.Count - 1)];
            }

            Debug.LogError("EnemyBrain_Buffer needs a list of alternate commands to use in place of the buff");
            return null;
        }
    }
}
