﻿using System.Collections;
using System.Collections.Generic;
using RPG.DialogueSystem;
using UnityEngine;
using UnityEngine.Assertions;

namespace RPG.CombatSystem
{
    public class Command_SummonReinforcements : BaseCommand
    {
        //optional stuff if we want to have this enemy speak 
        //when calling for backup
        [SerializeField] Sprite portrait = null;
        [SerializeField] string speakerName = "";
        [TextArea(3, 7)]
        [SerializeField] string mssg = "";
        [SerializeField] float secondsToDisplayMssg = 3;

        //command to call if reinforcements are already in play
        [SerializeField] BaseCommand alternateCommand = null;
        [SerializeField] bool moveReinforcementsAfterSpawn = true;

        protected SummonReinforcements_Config SRC;
        protected List<GameObject> spawnedReinforcements = new List<GameObject>();
        protected List<GameObject> destinations = new List<GameObject>();


        protected override void Start()
        {
            base.Start();

            if (speakerName == "")
            {
                speakerName = GetComponent<EnemyCharacter>().Stats_Config.CharName;
            }

            Assert.IsTrue(command_Config.GetType() == typeof(SummonReinforcements_Config));
            SRC = (SummonReinforcements_Config)command_Config;

            if (SRC.Destinations.Count > 0)
            {
                for (int i = 0; i < SRC.Destinations.Count; i++)
                {
                    destinations.Add(new GameObject());
                    destinations[i].transform.position = SRC.Destinations[i].position;
                    destinations[i].transform.eulerAngles = SRC.Destinations[i].eulerRotation;
                }
            }
        }

        protected override IEnumerator DoCommand()
        {
            if (OkToSummon())
            {
                if (mssg != "")
                {
                    CombatDialogueManager.Instance.SaySomethingInCombat(portrait, speakerName, mssg, secondsToDisplayMssg);
                    yield return new WaitForSeconds(secondsToDisplayMssg);
                }
                spawnedReinforcements = EnemyManager.Instance.AddEnemiesDuringCombat(SRC.Reinforcements);

                if (moveReinforcementsAfterSpawn)
                {
                    yield return MoveReinforcements();
                }

                yield return new WaitForSeconds(1);
            }
            else
            {
                Debug.Log("can't summon atm");
                yield return alternateCommand.CommandInstructions();
            }
        }

        public override bool TargetIsValid()
        {
            return true;
        }

        IEnumerator MoveReinforcements()
        {
            for (int i = 0; i < spawnedReinforcements.Count; i++)
            {
                spawnedReinforcements[i].GetComponent<EnemyCharacter>().SetHomePosition(destinations[i].transform);
                StartCoroutine(MovementManager.Instance.MoveACharacterToItsHomePosition(spawnedReinforcements[i], 30));
            }
            yield return new WaitForSeconds(2.5f);
        }

        //check if reinforcements have already been summoned
        bool OkToSummon()
        {
            spawnedReinforcements.RemoveAll(n => n == null);

            foreach (GameObject enemy in spawnedReinforcements)
            {
                if (EnemyManager.Instance.EnemiesInEncounter.Contains(enemy))
                {
                    if (enemy.GetComponent<EnemyCharacter>().IsAlive)
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}
