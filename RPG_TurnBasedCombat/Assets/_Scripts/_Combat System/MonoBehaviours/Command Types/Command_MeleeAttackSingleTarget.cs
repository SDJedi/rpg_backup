using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

//Last Reviewed: 2/18/20
namespace RPG.CombatSystem
{
    //a basic, "move to target, play an animation, return home" command
    public class Command_MeleeAttackSingleTarget : BaseCommand
    {
        protected MeleeAttack_Config MAC;
        protected GenerateAttackFromAnimationEvent gafae;

        public Damage_Config DamageConfig { get; protected set; }


        protected override void Start()
        {
            base.Start();
            Assert.IsTrue(command_Config.GetType() == typeof(MeleeAttack_Config));
            MAC = (MeleeAttack_Config)command_Config;
            gafae = GetComponentInChildren<GenerateAttackFromAnimationEvent>();
            DamageConfig = Instantiate(MAC.DamageConfig);
        }

        void OnDestroy()
        {
            Destroy(DamageConfig);
        }

        public override IEnumerator CommandInstructions()
        {
            if (anim == null || gafae == null)
            {
                Debug.LogWarning("Cannot execute " + CommandName + " without an <Animator> && a <GenerateAttackFromAnimationEvent>");
                yield break;
            }

            yield return base.CommandInstructions();
        }

        protected override IEnumerator DoCommand()
        {
            //if there's a Hit animation pending, reset it.
            anim.ResetTrigger("Hit");
            
            //set a default attack message if OnCastMessage is blank
            if (OnCastMessage == "")
            {
                OnCastMessage = "<name> attacks <hTarget>!";
            }

            ShowCastMessage();
            AddToCombatLog(CombatLogMssgColor);

            //get our "attack from anim" component set up with the correct info
            gafae.Setup_GAFAE(this, DamageConfig, ParticleEffect, CScript.HostileTarget.transform);

            //adjust the "front" position of the target if needed for the animation to look right...
            CScript.HostileTarget.Character_Front.localPosition = new Vector3(CScript.HostileTarget.Character_Front.localPosition.x, CScript.HostileTarget.Character_Front.localPosition.y, CScript.HostileTarget.Character_Front.localPosition.z + MAC.HostileFrontZOffset);

            //move to target and play attack animation
            yield return MovementManager.Instance.MoveCharacterThenPlayAnimation(gameObject, CScript.HostileTarget.Character_Front, CScript.HostileTarget.gameObject.transform, MAC.MoveToTargetSpeed, anim, AnimatorTriggerName); //MoveToPos_PlayAnimation_ReturnToStartPos(gameObject, CScript.HostileTarget.Character_Front, MAC.MoveToTargetSpeed, anim, AnimatorTriggerName);

            //short pause after attack...
            yield return new WaitForSeconds(0.1f);

            //wait if attack is being countered...
            while (TurnManager.Instance.CounterDamageInProgress)
            {
                yield return null;
            }

            //if there's a Hit animation pending, reset it.
            anim.ResetTrigger("Hit");

            //return home...
            yield return MovementManager.Instance.MoveACharacterToItsHomePosition(gameObject, MAC.MoveToTargetSpeed);

            //put the "front" position back to its default position...
            if (CScript.HostileTarget != null)
            {
                CScript.HostileTarget.Character_Front.localPosition = new Vector3(CScript.HostileTarget.Character_Front.localPosition.x, CScript.HostileTarget.Character_Front.localPosition.y, CScript.HostileTarget.Character_Front.localPosition.z - MAC.HostileFrontZOffset);
            }
        }

        public override bool TargetIsValid()
        {
            return (CScript.HostileTarget != null && CScript.HostileTarget.IsAlive);
        }
    }
}
