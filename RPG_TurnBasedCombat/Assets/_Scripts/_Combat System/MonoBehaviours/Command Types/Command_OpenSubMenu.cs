using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Last Reviewed: 2/19/20
namespace RPG.CombatSystem
{
    //for MainMenu button that opens a sub-menu (e.g. Special and Magic).
    //for "Special" and "Magic" get's automatically added to each PC gameObject by ButtonCommandManager.cs.
    public class Command_OpenSubMenu : BaseCommand
    {
        public string SubMenuName; //"Special" : "Magic"

        public List<BaseCommand> SubMenu_Commands { get; private set; } = new List<BaseCommand>();


        protected override void Start()
        {
            //leave this blank: overrides the base Start() method
        }

        //execute CommandInstructions() on button press / call static event
        public override void OnButtonClick()
        {
            if (!TurnManager.Instance.PlayerInputAllowed)
            {
                return;
            }

            if (ButtonCommandManager.CommandButtonPressed != null)
            {
                ButtonCommandManager.CommandButtonPressed(this);
            }

            StartCoroutine(CommandInstructions());
        }

        //show the sub-menu and all available options
        public override IEnumerator CommandInstructions()
        {
            //clear targets and CommandsToQueue List for PC
            var PCScript = gameObject.GetComponent<PlayerCharacter>();
            PCScript.ClearAllTargets();
            PCScript.ClearCommandsToQueueList();

            /*PCScript.*/
            PopulateAndSortSubMenu_Commands_List(SubMenuName);
            //open the sub-menu window
            Combat_UI_Manager.Instance.SetUpUI_SubMenu(gameObject);

            //Note: we call this twice to set both PreviousStep && CurrentStep in
            //TurnManager.cs == TurnStep.SelectSubMenuCommand.
            TurnManager.Instance.SetCurrentTurnStep(TurnStep.SelectSubMenuCommand);
            TurnManager.Instance.SetCurrentTurnStep(TurnStep.SelectSubMenuCommand);

            //set up the sub-menu buttons
            ButtonCommandManager.Instance.PopulatePCSubMenuButtons(this);
            yield return null;
        }

        //gets called when a sub-menu is opened
        public void PopulateAndSortSubMenu_Commands_List(string SubMenuName)
        {
            //start with a clean list
            SubMenu_Commands.Clear();

            //get a reference to all Commands on PC
            BaseCommand[] SCs = GetComponents<BaseCommand>();

            //populate List<SubMenu_Commands>
            switch (SubMenuName)
            {
                case "Special":
                    foreach (BaseCommand sc in SCs)
                    {
                        if (sc.isActiveAndEnabled && sc.AddToSpecialMenu && sc.ButtonIndex == -1)
                        {
                            SubMenu_Commands.Add(sc);
                        }
                    }
                    break;
                case "Magic":
                    foreach (BaseCommand sc in SCs)
                    {
                        if (sc.isActiveAndEnabled && sc.AddToMagicMenu && sc.ButtonIndex == -1)
                        {
                            SubMenu_Commands.Add(sc);
                        }
                    }
                    break;
                default:
                    Debug.LogWarning("Invalid SubMenuName ... valid options: \"Special\" | \"Magic\"");
                    break;
            }

            //sort the list alphabetically
            SubMenu_Commands.Sort((a, b) => (a.CommandName.CompareTo(b.CommandName)));
        }

        public override bool TargetIsValid() { return true; }
        protected override IEnumerator DoCommand() { yield return null; }
    }
}
