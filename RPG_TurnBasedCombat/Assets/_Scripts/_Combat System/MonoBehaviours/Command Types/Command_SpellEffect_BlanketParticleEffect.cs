using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

//Last Reviewed: 2/20/20
namespace RPG.CombatSystem
{
    //for spells where the particle effect hits an area rather than one character
    public class Command_SpellEffect_BlanketParticleEffect : Command_SpellEffect
    {
        //set by the spell config
        protected Vector3 particleEffectLocation_CastByPC = Vector3.zero;
        protected Vector3 particleEffectLocation_CastByEnemy = Vector3.zero;
        

        protected override void Start()
        {
            base.Start();
            Assert.IsTrue(command_Config.GetType() == typeof(Spell_BlanketParticleEffect_Config));
        }

        public override void InitializeValuesFromScriptableObject()
        {
            Spell_Config = (Spell_BlanketParticleEffect_Config)command_Config;
            base.InitializeValuesFromScriptableObject();
            particleEffectLocation_CastByPC = ((Spell_BlanketParticleEffect_Config)Spell_Config).ParticleEffectLocation_CastByPC;
            particleEffectLocation_CastByEnemy = ((Spell_BlanketParticleEffect_Config)Spell_Config).ParticleEffectLocation_CastByEnemy;
        }

        protected override IEnumerator DoCommand()
        {
            //set a default message if OnCastMessage is blank
            if (OnCastMessage == "")
            {
                OnCastMessage = "<name> casts " + CommandName;
            }

            ShowCastMessage();
            AddToCombatLog(CombatLogMssgColor);

            //move forward...
            yield return MovementManager.Instance.ScoochObjectAlongFwdAxis(gameObject, 2);

            //play casting animation...
            anim.SetTrigger(AnimatorTriggerName);

            if (Spell_Config.OnCastSoundEffect != null)
            {
                SoundManager.Instance.PlayAudioClip(Spell_Config.OnCastSoundEffect);
            }

            yield return new WaitForSeconds(Spell_Config.AnimationDelay);

            //trigger main effect...
            switch (_TargetType)
            {
                case TargetType.AllFriendly:
                case TargetType.AllHostile:
                case TargetType.All:
                    SpellAndProcManager.Instance.ProcessSpellConfig(CScript, Spell_Config);
                    break;
                default:
                    Debug.Log(Spell_Config + " needs to have a target type of All, AllFriendly, or AllHostile");
                    break;
            }

            //trigger additional, built-in effects...
            yield return SpellAndProcManager.Instance.ProcAdditionalEffects(CScript, this);

            //trigger procs from other sources...
            if (ThisCommandTriggersProcs)
            {
                SpellAndProcManager.Instance.FireProcs(CScript);
            }

            //move back...
            yield return new WaitForSeconds(0.5f);
            yield return MovementManager.Instance.MoveACharacterToItsHomePosition(gameObject, 5);
            yield return new WaitForSeconds(1);
        }

        public override bool TargetIsValid()
        {
            return true;
        }
    }
}
