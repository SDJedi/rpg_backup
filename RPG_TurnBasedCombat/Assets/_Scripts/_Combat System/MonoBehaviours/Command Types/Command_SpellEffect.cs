using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

//Last Reviewed: 2/20/20
namespace RPG.CombatSystem
{
    //offers more features than the basic Command Type; "ranged" by default (i.e. move forward, cast, move back).
    //allows effects to be applied to a character that last multiple turns and "tick" each turn
    public class Command_SpellEffect : BaseCommand
    {
        //extends BaseCommand_Config
        public BaseSpell_Config Spell_Config { get; protected set; }


        protected override void Start()
        {
            base.Start();
            Assert.IsTrue(command_Config.GetType() == typeof(BaseSpell_Config) || command_Config.GetType().IsSubclassOf(typeof(BaseSpell_Config)));
            Spell_Config = (BaseSpell_Config)command_Config;
        }

        protected override IEnumerator DoCommand()
        {
            //set a default attack message if OnCastMessage is blank
            if (OnCastMessage == "")
            {
                OnCastMessage = "<name> casts " + CommandName;
            }

            ShowCastMessage();
            AddToCombatLog(CombatLogMssgColor);

            //move forward...
            yield return MovementManager.Instance.ScoochObjectAlongFwdAxis(gameObject, 2);

            //Note: no animation for Defend as of 2/20/20
            //play casting animation...
            if (AnimatorTriggerName != "")
            {
                anim.SetTrigger(AnimatorTriggerName);
            }

            if (Spell_Config.OnCastSoundEffect != null)
            {
                SoundManager.Instance.PlayAudioClip(Spell_Config.OnCastSoundEffect);
            }

            yield return new WaitForSeconds(Spell_Config.AnimationDelay);

            //trigger main effect...Note: should also handle particle and sound effects
            SpellAndProcManager.Instance.ProcessSpellConfig(CScript, Spell_Config);

            //trigger additional, built-in effects...
            yield return SpellAndProcManager.Instance.ProcAdditionalEffects(CScript, this);

            //trigger procs from other sources...
            if (ThisCommandTriggersProcs)
            {
                SpellAndProcManager.Instance.FireProcs(CScript);
            }

            //move back...
            yield return new WaitForSeconds(0.5f);
            yield return MovementManager.Instance.MoveACharacterToItsHomePosition(gameObject, 5);
            yield return new WaitForSeconds(1);
        }

        public override bool TargetIsValid()
        {
            switch (_TargetType)
            {
                case TargetType.Friendly:
                    return (CScript.FriendlyTarget != null && CScript.FriendlyTarget.IsAlive);
                case TargetType.Friendly_NotSelf:
                    return (CScript.FriendlyTarget != CScript && CScript.FriendlyTarget != null && CScript.FriendlyTarget.IsAlive);
                case TargetType.Hostile:
                    return (CScript.HostileTarget != null && CScript.HostileTarget.IsAlive);
                default:
                    return true;
            }
        }
    }
}
