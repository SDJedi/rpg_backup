using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

//Last Reviewed: 2/19/20
namespace RPG.CombatSystem
{
    //commands that bring a PC back to life
    public class Command_RezPlayer : BaseCommand
    {
        RezSpell_Config RezConfig;


        protected override void Start()
        {
            base.Start();
            Assert.IsTrue(command_Config.GetType() == typeof(RezSpell_Config));
            RezConfig = (RezSpell_Config)command_Config;
        }

        public override void OnButtonClick()
        {
            if (!TurnManager.Instance.PlayerInputAllowed)
            {
                return;
            }

            //case: button clicked, but there are no dead PCs to rez.
            if (PCManager.Instance.GetNextDeadPC(gameObject) == null)
            {
                if (ButtonCommandManager.CommandButtonPressed != null)
                {
                    ButtonCommandManager.CommandButtonPressed(this);
                }
                CScript.ClearAllTargets();
                CScript.ClearCommandsToQueueList();
                Combat_UI_Manager.Instance.SetMsg_TopOfScreen("There are no dead allies to resurrect", Color.white, 0, false);
                SoundManager.Instance.PlayBuzzer();
                return;
            }

            //case: button clicked, there is at least 1 dead PC.
            base.OnButtonClick();
        }

        protected override IEnumerator DoCommand()
        {
            //set a default message if OnCastMessage is blank
            if (OnCastMessage == "")
            {
                OnCastMessage = "<name> gives life to <fTarget>!";
            }

            ShowCastMessage();
            AddToCombatLog(CombatLogMssgColor);

            //move forward...
            yield return MovementManager.Instance.ScoochObjectAlongFwdAxis(gameObject, 2);

            //play casting animation...
            anim.SetTrigger(AnimatorTriggerName);
            yield return new WaitForSeconds(RezConfig.AnimationDelay);

            //trigger main effect...
            yield return CastRez();

            //trigger additional, built-in effects...
            yield return SpellAndProcManager.Instance.ProcAdditionalEffects(CScript, this);

            //trigger procs from other sources...
            if (ThisCommandTriggersProcs)
            {
                SpellAndProcManager.Instance.FireProcs(CScript);
            }

            yield return new WaitForSeconds(1);
            yield return MovementManager.Instance.MoveACharacterToItsHomePosition(gameObject, 5);
            yield return new WaitForSeconds(1);
        }

        IEnumerator CastRez()
        {
            Animator deadPCAnim = CScript.FriendlyTarget.GetComponentInChildren<Animator>();
            if (ParticleEffect != null)
            {
                var e = Instantiate(ParticleEffect, CScript.FriendlyTarget.transform.position, ParticleEffect.transform.rotation);
                ScaleObjectWithModel.MatchScale(CScript.FriendlyTarget.gameObject, e);
                deadPCAnim.SetTrigger("Rez");
            }

            if (OnCastSoundEffect != null)
            {
                SoundManager.Instance.PlayAudioClip(OnCastSoundEffect);
            }

            CScript.FriendlyTarget.ApplySpellEffect(CScript, Instantiate(RezConfig));
            yield return SendRezedPCBackToStartPos(CScript.FriendlyTarget.gameObject);
        }

        IEnumerator SendRezedPCBackToStartPos(GameObject PC)
        {
            yield return new WaitForSeconds(1);
            yield return MovementManager.Instance.MoveACharacterToItsHomePosition(PC, 20);
        }

        public override bool TargetIsValid()
        {
            return (CScript.FriendlyTarget != null && !CScript.FriendlyTarget.IsAlive);
        }
    }
}
