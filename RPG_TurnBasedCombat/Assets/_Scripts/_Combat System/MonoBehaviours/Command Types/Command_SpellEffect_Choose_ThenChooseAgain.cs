using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

//Last Reviewed: 2/20/20
namespace RPG.CombatSystem
{
    //allows for a second set of choices after 1st choice is selected
    public class Command_SpellEffect_Choose_ThenChooseAgain : Command_SpellEffect_ChoicesOnCast
    {
        protected string TopOfBoxMssg_2ndSet_0ChosenFirst; //message to show if Option 0 was chosen first
        protected string Btn0_Lbl_2ndSet_0ChosenFirst;
        protected string Btn1_Lbl_2ndSet_0ChosenFirst;

        protected string TopOfBoxMssg_2ndSet_1ChosenFirst; //message to show if Option 1 was chosen first
        protected string Btn0_Lbl_2ndSet_1ChosenFirst;
        protected string Btn1_Lbl_2ndSet_1ChosenFirst;

        BaseSpell_Config Option0_Option0 = null;
        BaseSpell_Config Option0_Option1 = null;
        BaseSpell_Config Option1_Option0 = null;
        BaseSpell_Config Option1_Option1 = null;

        bool firstSet_Option0Chosen;


        protected override void Start()
        {
            base.Start();
            Assert.IsTrue(command_Config.GetType() == typeof(Spell_ChooseThenChooseAgain_Config));
        }

        public override void InitializeValuesFromScriptableObject()
        {
            base.InitializeValuesFromScriptableObject();
            TopOfBoxMssg_2ndSet_0ChosenFirst = ((Spell_ChooseThenChooseAgain_Config)Spell_Config).TopOfBoxMssg_2ndSet_0ChosenFirst;
            Btn0_Lbl_2ndSet_0ChosenFirst = ((Spell_ChooseThenChooseAgain_Config)Spell_Config).Btn0_Lbl_2ndSet_0ChosenFirst;
            Btn1_Lbl_2ndSet_0ChosenFirst = ((Spell_ChooseThenChooseAgain_Config)Spell_Config).Btn1_Lbl_2ndSet_0ChosenFirst;
            TopOfBoxMssg_2ndSet_1ChosenFirst = ((Spell_ChooseThenChooseAgain_Config)Spell_Config).TopOfBoxMssg_2ndSet_1ChosenFirst;
            Btn0_Lbl_2ndSet_1ChosenFirst = ((Spell_ChooseThenChooseAgain_Config)Spell_Config).Btn0_Lbl_2ndSet_1ChosenFirst;
            Btn1_Lbl_2ndSet_1ChosenFirst = ((Spell_ChooseThenChooseAgain_Config)Spell_Config).Btn1_Lbl_2ndSet_1ChosenFirst;
            Option0_Option0 = ((Spell_ChooseThenChooseAgain_Config)Spell_Config).Option0_Option0;
            Option0_Option1 = ((Spell_ChooseThenChooseAgain_Config)Spell_Config).Option0_Option1;
            Option1_Option0 = ((Spell_ChooseThenChooseAgain_Config)Spell_Config).Option1_Option0;
            Option1_Option1 = ((Spell_ChooseThenChooseAgain_Config)Spell_Config).Option1_Option1;
        }

        protected override void Option0Chosen()
        {
            CScript.ClearCommandsToQueueList();
            firstSet_Option0Chosen = true;
            ClearBtnListenersAndDestroyChoiceBox();
            ShowSecondSetOfOptions();
        }

        protected override void Option1Chosen()
        {
            CScript.ClearCommandsToQueueList();
            firstSet_Option0Chosen = false;
            ClearBtnListenersAndDestroyChoiceBox();
            ShowSecondSetOfOptions();
        }

        void ShowSecondSetOfOptions()
        {
            choiceBox = Instantiate(ChoiceBoxPrefab, GameObject.Find("CombatScene_Canvas").transform);
            choiceBox_Config = choiceBox.GetComponent<ChoiceBoxConfig>();
            if (firstSet_Option0Chosen)
            {
                choiceBox_Config.TopOfBoxMessage.text = TopOfBoxMssg_2ndSet_0ChosenFirst;
                choiceBox_Config.btn0.GetComponentInChildren<Text>().text = Btn0_Lbl_2ndSet_0ChosenFirst;
                choiceBox_Config.btn1.GetComponentInChildren<Text>().text = Btn1_Lbl_2ndSet_0ChosenFirst;
            }
            else
            {
                choiceBox_Config.TopOfBoxMessage.text = TopOfBoxMssg_2ndSet_1ChosenFirst;
                choiceBox_Config.btn0.GetComponentInChildren<Text>().text = Btn0_Lbl_2ndSet_1ChosenFirst;
                choiceBox_Config.btn1.GetComponentInChildren<Text>().text = Btn1_Lbl_2ndSet_1ChosenFirst;
            }

            choiceBox_Config.btn0.onClick.AddListener(SecondSet_Option0);
            choiceBox_Config.btn1.onClick.AddListener(SecondSet_Option1);
            choiceBoxExists = true;
        }

        void SecondSet_Option0()
        {
            CScript.ClearCommandsToQueueList();

            if (firstSet_Option0Chosen)
            {
                chosenConfigThisRound = Option0_Option0;
                PullValuesFromScriptableObject(Option0_Option0);
            }
            else
            {
                chosenConfigThisRound = Option1_Option0;
                PullValuesFromScriptableObject(Option1_Option0);
            }

            ClearBtnListenersAndDestroyChoiceBox();
            AllowPlayerToChooseTargetOrQueueCommandIfAlreadyHaveTarget(CScript);
        }

        void SecondSet_Option1()
        {
            CScript.ClearCommandsToQueueList();

            if (firstSet_Option0Chosen)
            {
                chosenConfigThisRound = Option0_Option1;
                PullValuesFromScriptableObject(Option0_Option1);
            }
            else
            {
                chosenConfigThisRound = Option1_Option1;
                PullValuesFromScriptableObject(Option1_Option1);
            }

            ClearBtnListenersAndDestroyChoiceBox();
            AllowPlayerToChooseTargetOrQueueCommandIfAlreadyHaveTarget(CScript);
        }
    }
}
