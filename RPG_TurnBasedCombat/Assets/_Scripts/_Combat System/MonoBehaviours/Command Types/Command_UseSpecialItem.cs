﻿using System.Collections;
using System.Collections.Generic;
using RPG.Inventory;
using UnityEngine;
using UnityEngine.Assertions;

namespace RPG.CombatSystem
{
    //Adds a special item to inventory and uses it in combat (e.g. exploding chicken)
    public class Command_UseSpecialItem : Command_UseItem
    {
        UseSpecialItem_Config SIC;


        protected override void Start()
        {
            Assert.IsTrue(command_Config.GetType() == typeof(UseSpecialItem_Config));
            SIC = (UseSpecialItem_Config)command_Config;
            CScript = GetComponent<Character>();
            anim = GetComponentInChildren<Animator>();
            stats = CScript.Stats_Config; //Note: This MUST point to the cloned (instantiated) SO that was made in Awake() in PlayerCharacter.cs && EnemyCharacter.cs... if it references the assest itself instead of the clone, data gets corrupted

            InitializeValuesFromScriptableObject();
        }

        protected override void DeductCost()
        {
            if (HPCost > 0)
            {
                stats.ModifyCurrentHP(-HPCost);
            }
            if (MPCost > 0)
            {
                stats.ModifyCurrentMP(-MPCost);
            }
            if (TPCost > 0)
            {
                stats.ModifyCurrentTP(-TPCost);
            }
        }

        protected override bool CanAfford()
        {
            return (stats.CurHP > HPCost && stats.CurMP >= MPCost && stats.CurTP >= TPCost);
        }

        protected override IEnumerator DoCommand()
        {
            //add the special item into invemtory; set it as item queued for use
            InventoryManager.Instance.AddItemToInventory(Instantiate(SIC.ItemToCreateAndUse));
            itemQueuedForUse = SIC.ItemToCreateAndUse;

            //set a default message if OnCastMessage is blank
            if (OnCastMessage == "")
            {
                OnCastMessage = "<name> uses " + itemQueuedForUse.ItemName;
            }
            if (CombatLogMessage == "")
            {
                CombatLogMessage = "<name> used " + itemQueuedForUse.ItemName;
            }

            ShowCastMessage();
            AddToCombatLog(new Color32(255, 255, 255, 255));

            yield return MoveAndUseItem();
        }
    }
}