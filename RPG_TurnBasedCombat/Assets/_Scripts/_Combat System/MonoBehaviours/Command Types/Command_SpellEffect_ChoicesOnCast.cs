using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

//Last Reviewed: 2/20/20
namespace RPG.CombatSystem
{
    //when this command is selected, a window with two options appears
    public class Command_SpellEffect_ChoicesOnCast : Command_SpellEffect
    {
        //UI prefab (Two-button_ChoiceBox)
        protected GameObject ChoiceBoxPrefab;

        //for the ChoiceBox UI
        protected string TopOfBoxMssg;
        protected string Btn0_Lbl;
        protected string Btn1_Lbl;

        protected BaseSpell_Config Option0 = null; //spell config stored in option 0
        protected BaseSpell_Config Option1 = null; //spell config stored in option 1

        protected GameObject choiceBox;
        protected ChoiceBoxConfig choiceBox_Config;
        protected bool choiceBoxExists;

        protected BaseSpell_Config parentConfig; //Note: should be a Spell_ChoicesOnCast_Config
        protected BaseSpell_Config chosenConfigThisRound; //the spell effect that will ultimately be cast


        protected override void Start()
        {
            base.Start();
            //Note: we make this check so we can safely call base.Start() in the derived class, Command_SpellEffect_Choose_ThenChooseAgain
            if (GetType() == typeof(Command_SpellEffect_ChoicesOnCast))
            {
                Assert.IsTrue(command_Config.GetType() == typeof(Spell_ChoicesOnCast_Config));
            }
            ButtonCommandManager.CommandButtonPressed += CommandButtonPressed;
            parentConfig = Spell_Config;
        }

        public override void InitializeValuesFromScriptableObject()
        {
            base.InitializeValuesFromScriptableObject();
            Spell_Config = (Spell_ChoicesOnCast_Config)command_Config;
            ChoiceBoxPrefab = ((Spell_ChoicesOnCast_Config)Spell_Config).ChoiceBoxPrefab;
            TopOfBoxMssg = ((Spell_ChoicesOnCast_Config)Spell_Config).TopOfBoxMssg;
            Btn0_Lbl = ((Spell_ChoicesOnCast_Config)Spell_Config).Btn0_Lbl;
            Btn1_Lbl = ((Spell_ChoicesOnCast_Config)Spell_Config).Btn1_Lbl;
            Option0 = ((Spell_ChoicesOnCast_Config)Spell_Config).Option0;
            Option1 = ((Spell_ChoicesOnCast_Config)Spell_Config).Option1;
        }

        void Update()
        {
            if (choiceBoxExists && (Input.GetKey(KeyCode.Escape) || Input.GetMouseButton(1)))
            {
                CScript.ClearAllTargets();
                CScript.ClearCommandsToQueueList();
                Spell_Config = parentConfig;
                ClearBtnListenersAndDestroyChoiceBox();
                choiceBoxExists = false;
            }
        }

        void OnDestroy()
        {
            ButtonCommandManager.CommandButtonPressed -= CommandButtonPressed;
        }

        //when button clicked, if we can afford the spell, open up a choice box
        public override void OnButtonClick()
        {
            if (!TurnManager.Instance.PlayerInputAllowed)
            {
                return;
            }

            if (ButtonCommandManager.CommandButtonPressed != null)
            {
                ButtonCommandManager.CommandButtonPressed(this);
            }

            if (!CanAfford())
            {
                TurnManager.Instance.SetCurrentTurnStep(TurnStep.SelectSubMenuCommand);
                Combat_UI_Manager.Instance.SetMsg_TopOfScreen("Can't afford cost of " + CommandName, Color.white, 0, false);
                Combat_UI_Manager.Instance.SetUpUI_SubMenu(gameObject);
                SoundManager.Instance.PlayBuzzer();
                CScript.ClearAllTargets();
                CScript.ClearCommandsToQueueList();
                return;
            }

            if (CScript.CommandsToQueue.Count > 0 && CScript.CommandsToQueue[0] != this)
            {
                CScript.ClearAllTargets();
                CScript.ClearCommandsToQueueList();
            }

            Combat_UI_Manager.Instance.Un_OutlineAllCharacters();
            Combat_UI_Manager.Instance.OutlineCharacter(gameObject);

            if (!choiceBoxExists)
            {
                TurnManager.Instance.SetCurrentTurnStep(TurnStep.SelectSpellOption);
                choiceBox = Instantiate(ChoiceBoxPrefab, GameObject.Find("CombatScene_Canvas").transform);
                choiceBox_Config = choiceBox.GetComponent<ChoiceBoxConfig>();
                choiceBox_Config.TopOfBoxMessage.text = TopOfBoxMssg;
                choiceBox_Config.btn0.GetComponentInChildren<Text>().text = Btn0_Lbl;
                choiceBox_Config.btn1.GetComponentInChildren<Text>().text = Btn1_Lbl;
                choiceBox_Config.btn0.onClick.AddListener(Option0Chosen);
                choiceBox_Config.btn1.onClick.AddListener(Option1Chosen);
                choiceBoxExists = true;
            }
        }

        //called by pressing Option 0 button of the choice box
        protected virtual void Option0Chosen()
        {
            CScript.ClearCommandsToQueueList();
            chosenConfigThisRound = Option0;
            PullValuesFromScriptableObject(Option0);
            ClearBtnListenersAndDestroyChoiceBox();
            AllowPlayerToChooseTargetOrQueueCommandIfAlreadyHaveTarget(CScript);
        }

        //called by pressing Option 1 button of the choice box
        protected virtual void Option1Chosen()
        {
            CScript.ClearCommandsToQueueList();
            chosenConfigThisRound = Option1;
            PullValuesFromScriptableObject(Option1);
            ClearBtnListenersAndDestroyChoiceBox();
            AllowPlayerToChooseTargetOrQueueCommandIfAlreadyHaveTarget(CScript);
        }

        protected override IEnumerator DoCommand()
        {
            //update Spell_Config
            Spell_Config = chosenConfigThisRound;

            OnCastMessage = Spell_Config.OnCastMessage;

            yield return base.DoCommand();

            //reset the Spell_Config from the spell that was cast
            //back to the config that offers choices
            Spell_Config = parentConfig;
            PullValuesFromScriptableObject(Spell_Config);
        }

        protected void PullValuesFromScriptableObject(BaseSpell_Config chosenConfig)
        {
            _TargetType = chosenConfig._TargetType;
            ParticleEffect = chosenConfig.ParticleEffect;
            OnCastSoundEffect = chosenConfig.OnCastSoundEffect;
        }

        //if any other Command button is pressed,
        //kill the choice box
        void CommandButtonPressed(BaseCommand cmnd)
        {
            if (cmnd == this)
            {
                return;
            }

            if (choiceBox != null)
            {
                ClearBtnListenersAndDestroyChoiceBox();
            }

            Spell_Config = parentConfig;
            choiceBoxExists = false;
        }

        protected void ClearBtnListenersAndDestroyChoiceBox()
        {
            choiceBox_Config.btn0.onClick.RemoveAllListeners();
            choiceBox_Config.btn1.onClick.RemoveAllListeners();
            Destroy(choiceBox);
            choiceBoxExists = false;
        }
    }
}
