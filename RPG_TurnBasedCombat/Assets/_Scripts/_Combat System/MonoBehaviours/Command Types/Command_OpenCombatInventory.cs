using System.Collections;

//Last Reviewed: 2/19/20
namespace RPG.CombatSystem
{
    //allows PCs to open inventory in combat.
    //get's automatically added to each PC gameObject by ButtonCommandManager.cs.
    public class Command_OpenCombatInventory : BaseCommand
    {
        //whenever any CommandButton is pressed, close the CombatInventory window
        protected override void Start()
        {
            ButtonCommandManager.CommandButtonPressed += CloseCombatInv;
        }

        void OnDestroy()
        {
            ButtonCommandManager.CommandButtonPressed -= CloseCombatInv;
        }

        //execute CommandInstructions() on button press / call static event
        public override void OnButtonClick()
        {
            if (!TurnManager.Instance.PlayerInputAllowed)
            {
                return;
            }

            if (ButtonCommandManager.CommandButtonPressed != null)
            {
                ButtonCommandManager.CommandButtonPressed(this);
            }

            StartCoroutine(CommandInstructions());
        }

        //bring up the combat inv (all items that are useable in combat)
        public override IEnumerator CommandInstructions()
        {
            //clear targets and CommandsToQueue List for PC
            var PCScript = gameObject.GetComponent<PlayerCharacter>();
            PCScript.ClearAllTargets();
            PCScript.ClearCommandsToQueueList();

            //open the CombatInvWindow
            Combat_UI_Manager.Instance.SetUpUI_CombatInv(gameObject);

            //Note: we call this twice to set both PreviousStep && CurrentStep in
            //TurnManager.cs == TurnStep.SelectSubMenuCommand.
            TurnManager.Instance.SetCurrentTurnStep(TurnStep.SelectSubMenuCommand);
            TurnManager.Instance.SetCurrentTurnStep(TurnStep.SelectSubMenuCommand);

            //set up the buttons in the CombatInventory window
            ButtonCommandManager.Instance.PopulatePC_CombatInventoryButtons();
            yield return null;
        }

        //called whenever a command button is pressed
        void CloseCombatInv(BaseCommand cmnd)
        {
            if (cmnd != this && cmnd.GetType() != typeof(Command_UseItem))
            {
                Combat_UI_Manager.Instance.HideCombatInvPanel();
            }
        }

        public override bool TargetIsValid() { return true; }
        protected override IEnumerator DoCommand() { yield return null; }
    }
}
