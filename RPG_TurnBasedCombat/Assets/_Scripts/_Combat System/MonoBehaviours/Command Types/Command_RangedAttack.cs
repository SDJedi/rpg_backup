using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

//Last Reviewed: 1/17/22
namespace RPG.CombatSystem
{
    //command used to fire a projectile
    public class Command_RangedAttack : BaseCommand
    {
        const float rotSpeed = 100;
        const float scoochDist = 2;

        [SerializeField] protected Transform projectileSpawnLocation; //can set in inspector for enemies with ranged attacks

        protected RangedAttack_Config RAC;
        protected GenerateProjectileFromAnimationEvent gpfae;
        protected GameObject ProjectileToFire;
        protected string projectileSpawnPointName;
        protected bool moveBeforeFire;
        protected bool faceTargetBeforeFiring;


        protected override void Start()
        {
            base.Start();

            Assert.IsTrue(command_Config.GetType() == typeof(RangedAttack_Config));
            RAC = (RangedAttack_Config)command_Config;

            gpfae = GetComponentInChildren<GenerateProjectileFromAnimationEvent>();
            ProjectileToFire = RAC.ProjectileToFire;
            projectileSpawnPointName = RAC.ProjectileSpawnPointName;
            moveBeforeFire = RAC.MoveBeforeFire;
            faceTargetBeforeFiring = RAC.FaceTargetBeforeFiring;

            AssignProjectileSpawnLocation();
        }

        public override IEnumerator CommandInstructions()
        {
            if (anim == null || gpfae == null)
            {
                Debug.LogWarning("Cannot execute " + CommandName + " without an <Animator> && a <GenerateProjectileFromAnimationEvent>");
                yield break;
            }
            yield return base.CommandInstructions();
        }

        protected override IEnumerator DoCommand()
        {
            //set a default attack message if OnCastMessage is blank
            if (OnCastMessage == "")
            {
                OnCastMessage = "<name> attacks <hTarget>!";
            }

            ShowCastMessage();
            AddToCombatLog(CombatLogMssgColor);

            //set the projectile info for the animation event
            GameObject target = CScript.HostileTarget.gameObject;
            gpfae.SetProjectileInfo(this, ProjectileToFire, stats, target, projectileSpawnLocation.gameObject, target.transform);

            if (moveBeforeFire)
            {
                yield return MovementManager.Instance.ScoochObjectAlongFwdAxis(gameObject, scoochDist, RAC.MoveToPosSpeed);
            }

            bool rotated = false;
            Quaternion defaultRotation = transform.rotation;
            Vector3 targetDirection;
            Quaternion targetRotation;

            if (faceTargetBeforeFiring)
            {
                //rotate towards target
                targetDirection = (target.transform.position - transform.position).normalized;
                targetRotation = Quaternion.LookRotation(targetDirection);

                while (Quaternion.Angle(transform.rotation, targetRotation) > 0.01f)
                {
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotSpeed * Time.deltaTime);
                    yield return null;
                }
                rotated = true;
            }

            if (anim != null)
            {
                anim.SetTrigger(AnimatorTriggerName);
                if (OnCastSoundEffect != null)
                {
                    //sound to play at launch
                    SoundManager.Instance.PlayAudioClip(OnCastSoundEffect);
                }
                yield return new WaitForSeconds(RAC.AnimationDelay);
            }

            yield return new WaitForSeconds(0.5f);

            if (moveBeforeFire)
            {
                yield return MovementManager.Instance.MoveACharacterToItsHomePosition(gameObject, RAC.MoveToPosSpeed);
            }

            if (rotated)
            {
                //rotate back
                targetRotation = defaultRotation;
                while (Quaternion.Angle(transform.rotation, targetRotation) > 0.01f)
                {
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotSpeed * Time.deltaTime);
                    yield return null;
                }
            }

            CScript.SetHostileTarget(null);
            gpfae.Reset_Internal();
        }

        public override bool TargetIsValid()
        {
            return (CScript.HostileTarget != null && CScript.HostileTarget.IsAlive);
        }

        void AssignProjectileSpawnLocation()
        {
            if (projectileSpawnLocation == null)
            {
                projectileSpawnLocation = transform.Find(projectileSpawnPointName);
            }

            if (projectileSpawnLocation == null)
            {
                projectileSpawnLocation = transform.FindGrandChild(projectileSpawnPointName);
            }

            if (projectileSpawnLocation == null)
            {
                GameObject defaultProjectileSpawnPoint = new GameObject("projSpawnPoint");
                defaultProjectileSpawnPoint.transform.SetParent(transform);
                defaultProjectileSpawnPoint.transform.localPosition = new Vector3(0, 1, 1);
                defaultProjectileSpawnPoint.transform.localRotation = Quaternion.Euler(Vector3.zero);
                projectileSpawnLocation = defaultProjectileSpawnPoint.transform;
            }
        }
    }
}