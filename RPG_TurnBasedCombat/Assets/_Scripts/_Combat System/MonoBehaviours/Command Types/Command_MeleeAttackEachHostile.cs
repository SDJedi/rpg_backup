using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

//Last Reviewed: 2/18/20
namespace RPG.CombatSystem
{
    //character will move to and melee attack each hostile target
    public class Command_MeleeAttackEachHostile : Command_MeleeAttackSingleTarget
    {
        //the config type needed for this command
        MeleeAttackAll_Config MAaC;


        protected override void Start()
        {
            //////////////////////////////////////////////////////////////////////////
            //We don't want to run the Start() in Command_MeleeAttackSingleTarget
            CScript = GetComponent<Character>();
            anim = GetComponentInChildren<Animator>();
            stats = CScript.Stats_Config; //Note: This MUST point to the cloned (instantiated) SO that was made in Awake() in PlayerCharacter.cs && EnemyCharacter.cs... if it references the assest itself instead of the clone, data gets corrupted

            InitializeValuesFromScriptableObject();
            //////////////////////////////////////////////////////////////////////////

            Assert.IsTrue(command_Config.GetType() == typeof(MeleeAttackAll_Config));
            MAaC = (MeleeAttackAll_Config)command_Config;
            gafae = GetComponentInChildren<GenerateAttackFromAnimationEvent>();
            DamageConfig = Instantiate(MAaC.DamageConfig);
        }

        protected override IEnumerator DoCommand()
        {
            //set a default attack message if OnCastMessage is blank
            if (OnCastMessage == "")
            {
                OnCastMessage = "<name> storms across the battlefield!";
            }

            ShowCastMessage();
            AddToCombatLog(CombatLogMssgColor);

            //this character is a PC...
            if (CScript.Is_A_PC)
            {
                //move to each living enemy and play the attack animation
                for (int i = EnemyManager.Instance.EnemiesInEncounter.Count - 1; i >= 0; i--)
                {
                    if (CScript.IsAlive && EnemyManager.Instance.EnemiesInEncounter.Count > i && EnemyManager.Instance.EnemiesInEncounter[i] != null)
                    {
                        //remove any dead enemies
                        for (int j = i; j >= 0; j--)
                        {
                            if (EnemyManager.Instance.EnemiesInEncounter.Count > 0 && !EnemyManager.Instance.EnemiesInEncounter[j].GetComponent<Character>().IsAlive)
                            {
                                //print("Removing dead enemy at [" + j + "]");
                                EnemyManager.Instance.EnemiesInEncounter.RemoveAt(j);
                                i--;
                            }
                        }
                        //get first target
                        Character enemy = EnemyManager.Instance.EnemiesInEncounter[i].GetComponent<Character>();
                        if (enemy.IsAlive)
                        {
                            CScript.SetHostileTarget(enemy);
                            gafae.Setup_GAFAE(this, DamageConfig, ParticleEffect, enemy.transform);

                            //adjust the "front" position of the target if needed for the animation to look right...
                            enemy.Character_Front.localPosition = new Vector3(enemy.Character_Front.localPosition.x, enemy.Character_Front.localPosition.y, enemy.Character_Front.localPosition.z + MAaC.HostileFrontZOffset);

                            yield return MovementManager.Instance.MoveCharacterThenPlayAnimation(gameObject, enemy.Character_Front, enemy.gameObject.transform, MAaC.MoveToTargetSpeed, anim, AnimatorTriggerName);
                            
                            //wait if attack is being countered...
                            while (TurnManager.Instance.CounterDamageInProgress)
                            {
                                yield return null;
                            }
                            
                            //adjust "i" if aoe procs killed an enemy or enemies other than the one PC just hit
                            for (int j = i - 1; j >= 0; j--)
                            {
                                if (EnemyManager.Instance.EnemiesInEncounter.Count > 0 && !EnemyManager.Instance.EnemiesInEncounter[j].GetComponent<Character>().IsAlive)
                                {
                                    EnemyManager.Instance.EnemiesInEncounter.RemoveAt(j);
                                    i--;
                                }
                            }

                            //put the "front" position back to its default position...
                            if (enemy != null && enemy.IsAlive)
                            {
                                enemy.Character_Front.localPosition = new Vector3(enemy.Character_Front.localPosition.x, enemy.Character_Front.localPosition.y, enemy.Character_Front.localPosition.z - MAaC.HostileFrontZOffset);
                            }
                        }
                    }
                }
            }
            //this character is an enemy...
            else
            {
                List<PlayerCharacter> hitPCs = new List<PlayerCharacter>();
                List<PlayerCharacter> livingPCs = PCManager.Instance.GetLivingPCsInParty();

                //move to each living PC and play the attack animation
                while (CScript.IsAlive && hitPCs.Count < livingPCs.Count)
                {
                    int index = UnityEngine.Random.Range(0, livingPCs.Count);
                    PlayerCharacter PC = livingPCs[index];

                    if (!hitPCs.Contains(PC))
                    {
                        CScript.SetHostileTarget(PC);
                        gafae.Setup_GAFAE(this, DamageConfig, ParticleEffect, PC.transform);
                        var eScript = (EnemyCharacter)CScript;

                        //adjust the "front" position of the target if needed for the animation to look right...
                        PC.Character_Front.localPosition = new Vector3(PC.Character_Front.localPosition.x, PC.Character_Front.localPosition.y, PC.Character_Front.localPosition.z + MAaC.HostileFrontZOffset);

                        yield return MovementManager.Instance.MoveCharacterThenPlayAnimation(gameObject, PC.Character_Front, PC.gameObject.transform, MAaC.MoveToTargetSpeed, anim, AnimatorTriggerName);

                        //wait if attack is being countered...
                        while (TurnManager.Instance.CounterDamageInProgress)
                        {
                            yield return null;
                        }

                        //put the "front" position back to its default position...
                        PC.Character_Front.localPosition = new Vector3(PC.Character_Front.localPosition.x, PC.Character_Front.localPosition.y, PC.Character_Front.localPosition.z - MAaC.HostileFrontZOffset);

                        hitPCs.Add(PC);
                    }
                }
            }

            if (CScript.IsAlive)
            {
                yield return MovementManager.Instance.MoveACharacterToItsHomePosition(gameObject, MAaC.MoveToTargetSpeed);
            }
        }

        public override bool TargetIsValid()
        {
            return true;
        }
    }
}
