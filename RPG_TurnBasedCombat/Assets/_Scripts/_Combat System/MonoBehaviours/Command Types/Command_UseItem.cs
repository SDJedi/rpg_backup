using System;
using System.Collections;
using System.Collections.Generic;
using RPG.Inventory;
using UnityEngine;

//Last Reviewed: 2/20/20
namespace RPG.CombatSystem
{
    //allows PCs to use items in combat
    public class Command_UseItem : BaseCommand
    {
        protected UseableItem itemQueuedForUse;


        protected override void Start()
        {
            CScript = GetComponent<Character>();
            anim = GetComponentInChildren<Animator>();
            stats = CScript.Stats_Config; //Note: This MUST point to the cloned (instantiated) SO that was made in Awake() in PlayerCharacter.cs && EnemyCharacter.cs... if it references the assest itself instead of the clone, data gets corrupted
            ThisCommandTriggersProcs = false;
        }

        public override bool TargetIsValid()
        {
            switch (_TargetType)
            {
                case TargetType.Hostile:
                    return CScript.HostileTarget != null && CScript.HostileTarget.IsAlive;
                case TargetType.Friendly:
                    return CScript.FriendlyTarget != null && CScript.FriendlyTarget.IsAlive;
                case TargetType.Friendly_NotSelf:
                    return CScript.FriendlyTarget != CScript && CScript.FriendlyTarget != null && CScript.FriendlyTarget.IsAlive;
                case TargetType.DeadFriendly:
                    return CScript.FriendlyTarget != null && !CScript.FriendlyTarget.IsAlive;
                default:
                    return true;
            }
        }

        protected override void DeductCost() { } //no resource cost for using items
        protected override bool CanAfford() { return InventoryManager.Instance.ItemExistsInInventory(itemQueuedForUse) && itemQueuedForUse.ItemQuantity > 0; }

        protected override IEnumerator NotEnoughResources()
        {
            SoundManager.Instance.PlayBuzzer();
            Combat_UI_Manager.Instance.SetMsg_TopOfScreen(CScript.Stats_Config.CharName + " tried to use an item that no longer exists.", Color.white);
            yield return new WaitForSeconds(2);
        }

        //takes the place of OnButtonClick() ; called when a combat inventory button is clicked
        public void SelectTargetsForItem(UseableItem itemToUse)
        {
            //alert interested parties that a CommandButton has been pressed
            if (ButtonCommandManager.CommandButtonPressed != null)
            {
                ButtonCommandManager.CommandButtonPressed(this);
            }

            if (!itemToUse.PCCanUseItem(stats.CharName))
            {
                SoundManager.Instance.PlayBuzzer();
                Combat_UI_Manager.Instance.SetMsg_TopOfScreen(CScript.Stats_Config.CharName + " can not use " + itemToUse.ItemName + ".", Color.white, 0, false);
                return;
            }

            if (((PC_Stats_Config)stats).CurLvl < itemToUse.MinLvl)
            {
                SoundManager.Instance.PlayBuzzer();
                Combat_UI_Manager.Instance.SetMsg_TopOfScreen(CScript.Stats_Config.CharName + " is too low level to use " + itemToUse.ItemName + ".", Color.white, 0, false);
                return;
            }

            //if we click a new item while selecting a target for
            //a previously clicked item...
            if (itemQueuedForUse != itemToUse)
            {
                CScript.ClearAllTargets();
            }

            itemQueuedForUse = itemToUse;
            CommandName = itemQueuedForUse.ItemName;
            _TargetType = itemToUse.TargetType;

            //if we have a command in the queue, but this isn't it, clear the queue and targets for this PC
            if (CScript.CommandsToQueue.Count > 0 && CScript.CommandsToQueue[0] != this)
            {
                CScript.ClearAllTargets();
                CScript.ClearCommandsToQueueList();
            }

            Combat_UI_Manager.Instance.Un_OutlineAllCharacters();
            Combat_UI_Manager.Instance.OutlineCharacter(gameObject);

            AllowPlayerToChooseTargetOrQueueCommandIfAlreadyHaveTarget(CScript);
        }

        protected override IEnumerator DoCommand()
        {
            //set use item message...
            OnCastMessage = "<name> uses " + itemQueuedForUse.ItemName;
            CombatLogMessage = "<name> used " + itemQueuedForUse.ItemName;
            ShowCastMessage();
            AddToCombatLog(new Color32(255, 255, 255, 255));

            yield return MoveAndUseItem();
        }

        protected IEnumerator MoveAndUseItem()
        {
            //move forward...
            yield return MovementManager.Instance.ScoochObjectAlongFwdAxis(gameObject, 2);

            //play casting animation...
            anim.SetTrigger("UseItem");
            yield return new WaitForSeconds(0.5f);

            //use the item...
            List<GameObject> itmTargets = InventoryManager.Instance.UseItemInCombat(CScript, itemQueuedForUse);

            //case: item is a grenade being thrown at enemies. we'll need to pause a bit
            bool itemIsGrenade = itemQueuedForUse.GetType() == typeof(UseableItem_Grenade) && ((UseableItem_Grenade)itemQueuedForUse).GrenadePrefab != null;
            if (itemIsGrenade)
            {
                yield return ((UseableItem_Grenade)itemQueuedForUse).pauseAfterThrow;
            }

            HandleParticleEffects(itmTargets, itemIsGrenade);

            //grenades have their own audio timing...
            if (!itemIsGrenade)
            {
                SoundManager.Instance.PlayAudioClip(itemQueuedForUse.OnUseSoundEffect);
            }

            //move back...
            yield return new WaitForSeconds(1.5f);
            yield return MovementManager.Instance.MoveACharacterToItsHomePosition(gameObject, 5);
            yield return new WaitForSeconds(1);
        }

        void HandleParticleEffects(List<GameObject> itmTargets, bool itemIsGrenade)
        {
            if (!itemIsGrenade || !((UseableItem_Grenade)itemQueuedForUse).UseDmgConfigParticleEffect)
            {
                //spawn particle effect associated with item on each target
                SpawnPEOnEachTarget(itemQueuedForUse.ParticleEffect, itmTargets);
            }

            //item IS a grenade && we want to use the PE from the spell config
            else
            {
                UseableItem_Grenade g = ((UseableItem_Grenade)itemQueuedForUse);
                DamageSpell_Config dsc = g.DmgConfig;

                //we're using the PE from the spell && it's a blanket effect
                if (dsc.GetType() == typeof(Spell_BlanketParticleEffect_Config))
                {
                    var e = Instantiate(dsc.ParticleEffect, dsc.ParticleEffect.transform.position, dsc.ParticleEffect.transform.rotation);
                }
                //using the spell config PE, but it's NOT a blanket effect
                else
                {
                    SpawnPEOnEachTarget(dsc.ParticleEffect, itmTargets);
                }
            }
        }

        void SpawnPEOnEachTarget(GameObject PE, List<GameObject> itmTargets)
        {
            if (PE == null) return;

            foreach (GameObject character in itmTargets)
            {
                var e = Instantiate(PE, character.transform.position, PE.transform.rotation, character.transform);
                ScaleObjectWithModel.MatchScale(character, e);
            }
        }
    }
}
