using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

//Last Reviewed: 2/18/20
namespace RPG.CombatSystem
{
    //command used to remove beneficial or detrimental effects from a character
    public class Command_Dispel_Cure : BaseCommand
    {
        //Dispel_Cure_Config extends BaseCommand_Config
        Dispel_Cure_Config DoC_Config;


        protected override void Start()
        {
            base.Start();
            Assert.IsTrue(command_Config.GetType() == typeof(Dispel_Cure_Config));
            DoC_Config = (Dispel_Cure_Config)command_Config;
        }

        protected override IEnumerator DoCommand()
        {
            ShowCastMessage();
            AddToCombatLog(CombatLogMssgColor);

            //move forward...
            yield return MovementManager.Instance.ScoochObjectAlongFwdAxis(gameObject, 2);

            //play casting animation...
            anim.SetTrigger(AnimatorTriggerName);

            if (OnCastSoundEffect != null)
            {
                SoundManager.Instance.PlayAudioClip(DoC_Config.OnCastSoundEffect);
            }

            yield return new WaitForSeconds(0.5f);

            //trigger main effect...
            switch (_TargetType)
            {
                case TargetType.Friendly:
                case TargetType.Friendly_NotSelf:
                    CureFriendlyTarget(DoC_Config.MaxNumOfEffectsToClear);
                    break;
                case TargetType.Hostile:
                    DispelHostileTarget(DoC_Config.MaxNumOfEffectsToClear);
                    break;
                case TargetType.Self:
                    CureSelf(DoC_Config.MaxNumOfEffectsToClear);
                    break;
                case TargetType.All:
                    switch (DoC_Config.TypeToRemove)
                    {
                        case TypeToRemove.Any:
                            RemoveMagic_All(DoC_Config.MaxNumOfEffectsToClear);
                            break;
                        case TypeToRemove.Beneficial:
                            Dispel_All(DoC_Config.MaxNumOfEffectsToClear);
                            break;
                        case TypeToRemove.Detrimental:
                            Cure_All(DoC_Config.MaxNumOfEffectsToClear);
                            break;
                    }
                    break;
                case TargetType.AllHostile:
                    DispelAllHostile(DoC_Config.MaxNumOfEffectsToClear);
                    break;
                case TargetType.AllFriendly:
                    CureAllFriendly(DoC_Config.MaxNumOfEffectsToClear);
                    break;
            }

            //trigger additional, built-in effects...
            yield return SpellAndProcManager.Instance.ProcAdditionalEffects(CScript, this);

            //trigger procs from other sources...
            if (ThisCommandTriggersProcs)
            {
                SpellAndProcManager.Instance.FireProcs(CScript);
            }

            yield return new WaitForSeconds(1);

            yield return MovementManager.Instance.MoveACharacterToItsHomePosition(gameObject);

            yield return new WaitForSeconds(1);
        }

        public override bool TargetIsValid()
        {
            switch (_TargetType)
            {
                case TargetType.Friendly:
                    return (CScript.FriendlyTarget != null && CScript.FriendlyTarget.IsAlive);
                case TargetType.Friendly_NotSelf:
                    return (CScript.FriendlyTarget != null && CScript.FriendlyTarget.IsAlive && CScript.FriendlyTarget != CScript);
                case TargetType.Hostile:
                    return (CScript.HostileTarget != null && CScript.HostileTarget.IsAlive);
                default:
                    return true;
            }
        }

        #region Dispel / Cure Methods
        void CureFriendlyTarget(int numToClear)
        {
            var fChar = CScript.FriendlyTarget;
            for (int i = fChar.ActiveSpellEffects.Count - 1; i >= 0; i--)
            {
                if (fChar.ActiveSpellEffects[i].IsCureable_Dispelable && fChar.ActiveSpellEffects[i].IsDetrimental && numToClear > 0)
                {
                    fChar.RemoveOneSpellEffect(fChar.ActiveSpellEffects[i]);
                    numToClear--;
                }
            }

            if (DoC_Config.ParticleEffect != null)
            {
                var e = Instantiate(DoC_Config.ParticleEffect, CScript.FriendlyTarget.transform.position, DoC_Config.ParticleEffect.transform.rotation);
                ScaleObjectWithModel.MatchScale(CScript.FriendlyTarget.gameObject, e);
            }

            if (DoC_Config.OnHitSoundEffect != null)
            {
                SoundManager.Instance.PlayAudioClip(DoC_Config.OnHitSoundEffect);
            }
        }

        void DispelHostileTarget(int numToClear)
        {
            var hChar = CScript.HostileTarget;
            for (int i = hChar.ActiveSpellEffects.Count - 1; i >= 0; i--)
            {
                if (hChar.ActiveSpellEffects[i].IsCureable_Dispelable && hChar.ActiveSpellEffects[i].IsBeneficial && numToClear > 0)
                {
                    hChar.RemoveOneSpellEffect(hChar.ActiveSpellEffects[i]);
                    numToClear--;
                }
            }

            if (DoC_Config.ParticleEffect != null)
            {
                var e = Instantiate(DoC_Config.ParticleEffect, CScript.HostileTarget.transform.position, DoC_Config.ParticleEffect.transform.rotation);
                ScaleObjectWithModel.MatchScale(CScript.HostileTarget.gameObject, e);
            }

            if (DoC_Config.OnHitSoundEffect != null)
            {
                SoundManager.Instance.PlayAudioClip(DoC_Config.OnHitSoundEffect);
            }
        }

        void CureSelf(int numToClear)
        {
            for (int i = CScript.ActiveSpellEffects.Count - 1; i >= 0; i--)
            {
                if (CScript.ActiveSpellEffects[i].IsCureable_Dispelable && CScript.ActiveSpellEffects[i].IsDetrimental && numToClear > 0)
                {
                    CScript.RemoveOneSpellEffect(CScript.ActiveSpellEffects[i]);
                    numToClear--;
                }
            }

            if (DoC_Config.ParticleEffect != null)
            {
                var e = Instantiate(DoC_Config.ParticleEffect, transform.position, DoC_Config.ParticleEffect.transform.rotation);
                ScaleObjectWithModel.MatchScale(gameObject, e);
            }

            if (DoC_Config.OnHitSoundEffect != null)
            {
                SoundManager.Instance.PlayAudioClip(DoC_Config.OnHitSoundEffect);
            }
        }

        void RemoveMagic_All(int numToClear)
        {
            if (DoC_Config.OnHitSoundEffect != null)
            {
                SoundManager.Instance.PlayAudioClip(DoC_Config.OnHitSoundEffect);
            }

            numToClear = DoC_Config.MaxNumOfEffectsToClear;
            foreach (GameObject PC in PCManager.Instance.PCsInParty)
            {
                if (PC == null)
                    continue;

                var c = PC.GetComponent<Character>();
                if (c.IsAlive)
                {
                    for (int i = c.ActiveSpellEffects.Count - 1; i >= 0; i--)
                    {
                        if (numToClear > 0 && c.ActiveSpellEffects[i].IsCureable_Dispelable)
                        {
                            c.RemoveOneSpellEffect(c.ActiveSpellEffects[i]);
                            numToClear--;
                        }
                    }
                    if (DoC_Config.ParticleEffect != null)
                    {
                        var e = Instantiate(DoC_Config.ParticleEffect, c.transform.position, DoC_Config.ParticleEffect.transform.rotation);
                        ScaleObjectWithModel.MatchScale(c.gameObject, e);
                    }

                }
                numToClear = DoC_Config.MaxNumOfEffectsToClear;
            }
            foreach (GameObject enemy in EnemyManager.Instance.EnemiesInEncounter)
            {
                if (enemy == null)
                    continue;

                var c = enemy.GetComponent<Character>();
                if (c != null && c.IsAlive)
                {
                    for (int i = c.ActiveSpellEffects.Count - 1; i >= 0; i--)
                    {
                        if (numToClear > 0 && c.ActiveSpellEffects[i].IsCureable_Dispelable)
                        {
                            c.RemoveOneSpellEffect(c.ActiveSpellEffects[i]);
                            numToClear--;
                        }
                    }
                    if (DoC_Config.ParticleEffect != null)
                    {
                        var e = Instantiate(DoC_Config.ParticleEffect, c.transform.position, DoC_Config.ParticleEffect.transform.rotation);
                        ScaleObjectWithModel.MatchScale(c.gameObject, e);
                    }

                }
                numToClear = DoC_Config.MaxNumOfEffectsToClear;
            }
        }

        void Dispel_All(int numToClear)
        {
            if (DoC_Config.OnHitSoundEffect != null)
            {
                SoundManager.Instance.PlayAudioClip(DoC_Config.OnHitSoundEffect);
            }

            numToClear = DoC_Config.MaxNumOfEffectsToClear;
            foreach (GameObject PC in PCManager.Instance.PCsInParty)
            {
                if (PC == null)
                    continue;

                var c = PC.GetComponent<Character>();
                if (c.IsAlive)
                {
                    for (int i = c.ActiveSpellEffects.Count - 1; i >= 0; i--)
                    {
                        if (c.ActiveSpellEffects[i].IsCureable_Dispelable && c.ActiveSpellEffects[i].IsBeneficial && numToClear > 0)
                        {
                            c.RemoveOneSpellEffect(c.ActiveSpellEffects[i]);
                            numToClear--;
                        }
                    }
                    if (DoC_Config.ParticleEffect != null)
                    {
                        var e = Instantiate(DoC_Config.ParticleEffect, c.transform.position, DoC_Config.ParticleEffect.transform.rotation);
                        ScaleObjectWithModel.MatchScale(c.gameObject, e);
                    }

                }
                numToClear = DoC_Config.MaxNumOfEffectsToClear;
            }
            foreach (GameObject enemy in EnemyManager.Instance.EnemiesInEncounter)
            {
                if (enemy == null)
                    continue;

                var c = enemy.GetComponent<Character>();
                if (c != null && c.IsAlive)
                {
                    for (int i = c.ActiveSpellEffects.Count - 1; i >= 0; i--)
                    {
                        if (c.ActiveSpellEffects[i].IsCureable_Dispelable && c.ActiveSpellEffects[i].IsBeneficial && numToClear > 0)
                        {
                            c.RemoveOneSpellEffect(c.ActiveSpellEffects[i]);
                            numToClear--;
                        }
                    }
                    if (DoC_Config.ParticleEffect != null)
                    {
                        var e = Instantiate(DoC_Config.ParticleEffect, c.transform.position, DoC_Config.ParticleEffect.transform.rotation);
                        ScaleObjectWithModel.MatchScale(c.gameObject, e);
                    }
                }
                numToClear = DoC_Config.MaxNumOfEffectsToClear;
            }
        }

        void Cure_All(int numToClear)
        {
            if (DoC_Config.OnHitSoundEffect != null)
            {
                SoundManager.Instance.PlayAudioClip(DoC_Config.OnHitSoundEffect);
            }

            numToClear = DoC_Config.MaxNumOfEffectsToClear;
            foreach (GameObject PC in PCManager.Instance.PCsInParty)
            {
                if (PC == null)
                    continue;

                var c = PC.GetComponent<Character>();
                if (c.IsAlive)
                {
                    for (int i = c.ActiveSpellEffects.Count - 1; i >= 0; i--)
                    {
                        if (c.ActiveSpellEffects[i].IsCureable_Dispelable && c.ActiveSpellEffects[i].IsDetrimental && numToClear > 0)
                        {
                            c.RemoveOneSpellEffect(c.ActiveSpellEffects[i]);
                            numToClear--;
                        }
                    }
                    if (DoC_Config.ParticleEffect != null)
                    {
                        var e = Instantiate(DoC_Config.ParticleEffect, c.transform.position, DoC_Config.ParticleEffect.transform.rotation);
                        ScaleObjectWithModel.MatchScale(c.gameObject, e);
                    }
                }
                numToClear = DoC_Config.MaxNumOfEffectsToClear;
            }
            foreach (GameObject enemy in EnemyManager.Instance.EnemiesInEncounter)
            {
                if (enemy == null)
                    continue;

                var c = enemy.GetComponent<Character>();
                if (c != null && c.IsAlive)
                {
                    for (int i = c.ActiveSpellEffects.Count - 1; i >= 0; i--)
                    {
                        if (c.ActiveSpellEffects[i].IsCureable_Dispelable && c.ActiveSpellEffects[i].IsDetrimental && numToClear > 0)
                        {
                            c.RemoveOneSpellEffect(c.ActiveSpellEffects[i]);
                            numToClear--;
                        }
                    }
                    if (DoC_Config.ParticleEffect != null)
                    {
                        var e = Instantiate(DoC_Config.ParticleEffect, c.transform.position, DoC_Config.ParticleEffect.transform.rotation);
                        ScaleObjectWithModel.MatchScale(c.gameObject, e);
                    }
                }
                numToClear = DoC_Config.MaxNumOfEffectsToClear;
            }
        }

        void DispelAllHostile(int numToClear)
        {
            if (DoC_Config.OnHitSoundEffect != null)
            {
                SoundManager.Instance.PlayAudioClip(DoC_Config.OnHitSoundEffect);
            }

            if (!CScript.Is_A_PC)
            {
                numToClear = DoC_Config.MaxNumOfEffectsToClear;
                foreach (GameObject PC in PCManager.Instance.PCsInParty)
                {
                    if (PC == null)
                        continue;

                    var c = PC.GetComponent<Character>();
                    if (c.IsAlive)
                    {
                        for (int i = c.ActiveSpellEffects.Count - 1; i >= 0; i--)
                        {
                            if (c.ActiveSpellEffects[i].IsCureable_Dispelable && c.ActiveSpellEffects[i].IsBeneficial && numToClear > 0)
                            {
                                c.RemoveOneSpellEffect(c.ActiveSpellEffects[i]);
                                numToClear--;
                            }
                        }
                        if (DoC_Config.ParticleEffect != null)
                        {
                            var e = Instantiate(DoC_Config.ParticleEffect, c.transform.position, DoC_Config.ParticleEffect.transform.rotation);
                            ScaleObjectWithModel.MatchScale(c.gameObject, e);
                        }

                    }
                    numToClear = DoC_Config.MaxNumOfEffectsToClear;
                }
            }
            else
            {
                numToClear = DoC_Config.MaxNumOfEffectsToClear;
                foreach (GameObject enemy in EnemyManager.Instance.EnemiesInEncounter)
                {
                    if (enemy == null)
                        continue;

                    var c = enemy.GetComponent<Character>();
                    if (c != null && c.IsAlive)
                    {
                        for (int i = c.ActiveSpellEffects.Count - 1; i >= 0; i--)
                        {
                            if (c.ActiveSpellEffects[i].IsCureable_Dispelable && c.ActiveSpellEffects[i].IsBeneficial && numToClear > 0)
                            {
                                c.RemoveOneSpellEffect(c.ActiveSpellEffects[i]);
                                numToClear--;
                            }
                        }
                        if (DoC_Config.ParticleEffect != null)
                        {
                            var e = Instantiate(DoC_Config.ParticleEffect, c.transform.position, DoC_Config.ParticleEffect.transform.rotation);
                            ScaleObjectWithModel.MatchScale(c.gameObject, e);
                        }

                    }
                    numToClear = DoC_Config.MaxNumOfEffectsToClear;
                }
            }
        }

        void CureAllFriendly(int numToClear)
        {
            if (DoC_Config.OnHitSoundEffect != null)
            {
                SoundManager.Instance.PlayAudioClip(DoC_Config.OnHitSoundEffect);
            }

            if (CScript.Is_A_PC)
            {
                numToClear = DoC_Config.MaxNumOfEffectsToClear;
                foreach (GameObject PC in PCManager.Instance.PCsInParty)
                {
                    if (PC == null)
                        continue;

                    var c = PC.GetComponent<Character>();
                    if (c.IsAlive)
                    {
                        for (int i = c.ActiveSpellEffects.Count - 1; i >= 0; i--)
                        {
                            if (c.ActiveSpellEffects[i].IsCureable_Dispelable && c.ActiveSpellEffects[i].IsDetrimental && numToClear > 0)
                            {
                                c.RemoveOneSpellEffect(c.ActiveSpellEffects[i]);
                                numToClear--;
                            }
                        }
                        if (DoC_Config.ParticleEffect != null)
                        {
                            var e = Instantiate(DoC_Config.ParticleEffect, c.transform.position, DoC_Config.ParticleEffect.transform.rotation);
                            ScaleObjectWithModel.MatchScale(c.gameObject, e);
                        }
                    }
                    numToClear = DoC_Config.MaxNumOfEffectsToClear;
                }
            }
            else
            {
                numToClear = DoC_Config.MaxNumOfEffectsToClear;
                foreach (GameObject enemy in EnemyManager.Instance.EnemiesInEncounter)
                {
                    if (enemy == null)
                        continue;

                    var c = enemy.GetComponent<Character>();
                    if (c != null && c.IsAlive)
                    {
                        for (int i = c.ActiveSpellEffects.Count - 1; i >= 0; i--)
                        {
                            if (c.ActiveSpellEffects[i].IsCureable_Dispelable && c.ActiveSpellEffects[i].IsDetrimental && numToClear > 0)
                            {
                                c.RemoveOneSpellEffect(c.ActiveSpellEffects[i]);
                                numToClear--;
                            }
                        }
                        if (DoC_Config.ParticleEffect != null)
                        {
                            var e = Instantiate(DoC_Config.ParticleEffect, c.transform.position, DoC_Config.ParticleEffect.transform.rotation);
                            ScaleObjectWithModel.MatchScale(c.gameObject, e);
                        }

                    }
                    numToClear = DoC_Config.MaxNumOfEffectsToClear;
                }
            }
        }
        #endregion
    }
}
