using UnityEngine;
using UnityEngine.Assertions;

//Last Reviewed: 1/15/22
namespace RPG.CombatSystem
{
    [RequireComponent(typeof(Destroyed_RemoveCommandFromQueue))]
    [RequireComponent(typeof(Attacked_TakeDamage))]
    [RequireComponent(typeof(Attacked_DamageAttacker))]
    public class PlayerCharacter : Character
    {
        [Header("PC Specific")]
        public string CharName = ""; //Note: must be unique


        protected override void Awake()
        {
            base.Awake();
            Is_A_PC = true; //always false in the base class

            Assert.IsTrue(stats_Config.GetType() == typeof(PC_Stats_Config));
            stats_Config = (PC_Stats_Config)stats_Config;
        }
    }
}