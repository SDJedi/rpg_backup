using UnityEngine;

//Last Reviewed: 1/17/22
namespace RPG.CombatSystem
{
    //attached to projectile prefabs (excluding grenades)
    public class Projectile : MonoBehaviour
    {
        const float projectileSpeed = 45;

        public Damage_Config DamageConfig;
        public AudioClip OnSpawnSound;
        public AudioClip OnImpactSound;
        public GameObject OnImpactParticleEffect;

        Collider col;
        Vector3 aimPosition;

        public Base_Stats_Config shooterStats { get; private set; }
        public GameObject target { get; private set; }
        public BaseCommand callingCommand { get; private set; }


        //set up self-destruct of projectile
        void Start()
        {
            col = GetComponent<Collider>();
            DamageConfig = Instantiate(DamageConfig);
            Destroy(DamageConfig, 7);
            Destroy(gameObject, 7.1f);
        }

        //move proj toward target
        void Update()
        {
            if (target != null)
            {
                gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, aimPosition, projectileSpeed * Time.deltaTime);
            }
        }

        //proccess impact of proc on target
        void OnTriggerEnter(Collider other)
        {
            Character character;
            character = other.gameObject.GetComponentInParent<Character>();

            if (character != null)
            {
                if (character.gameObject == target)
                {
                    col.enabled = false;
                    HideProjectile();
                    //if attack hits...
                    if (GenerateAttack.ExecuteAttack(DamageConfig, shooterStats, target, callingCommand.ThisCommandTriggersProcs))
                    {
                        if (OnImpactSound != null)
                        {
                            SoundManager.Instance.PlayAudioClip(OnImpactSound);
                        }
                        if (OnImpactParticleEffect != null)
                        {
                            Instantiate(OnImpactParticleEffect, target.transform.position, OnImpactParticleEffect.transform.rotation);
                        }
                        StartCoroutine(SpellAndProcManager.Instance.ProcAdditionalEffects(shooterStats.CScript, callingCommand));
                    }
                }
            }
        }

        //called just after animation event instantiates this projectile
        public void SetupProjectile(BaseCommand callingCommand, Base_Stats_Config shooterStats, GameObject target)
        {
            this.callingCommand = callingCommand;
            this.shooterStats = shooterStats;
            this.target = target;
            aimPosition = new Vector3(target.transform.position.x, target.transform.position.y + 1, target.transform.position.z);
        }

        //hide on impact, but keep parent object active to allow coroutine to finish;
        void HideProjectile()
        {
            var mr = GetComponent<MeshRenderer>();

            if (mr != null)
            {
                mr.enabled = false;
            }

            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(false);
            }
        }
    }
}