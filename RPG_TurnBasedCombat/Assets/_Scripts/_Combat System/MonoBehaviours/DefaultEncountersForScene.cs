using System.Collections.Generic;
using UnityEngine;
//Last Reviewed: 2/25/20
namespace RPG.CombatSystem
{
    //works with RandomEncounterManager.cs to determine which encounters the player might face
    //in a particular scene.
    public class DefaultEncountersForScene : Singleton<DefaultEncountersForScene>
    {
        [Header("Default encounters the player might face in this scene")]
        public List<EncounterAndSpawnChance> encountersForThisScene;


        //set the default random encounters for the scene
        void Start()
        {
            SetEncountersList();
        }

        public void SetEncountersList()
        {
            RandomEncounterManager.Instance.SetPossibleEncountersList(encountersForThisScene);
        }
    }
}