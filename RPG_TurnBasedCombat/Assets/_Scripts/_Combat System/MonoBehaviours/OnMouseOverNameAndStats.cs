﻿using System.Collections;
using System.Collections.Generic;
using RPG.CombatSystem;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class OnMouseOverNameAndStats : MonoBehaviour, IPointerEnterHandler
{
    [SerializeField] TextMeshProUGUI nameTxt;

    GameObject PC;

    //called by button press
    public void OnMouseClick()
    {
        var friendlyTarget = PC.GetComponentInParent<Character>();
        var activePC = TurnManager.Instance.PCReceivingCommands.GetComponent<Character>();

        if (activePC.CommandsToQueue.Count > 0 && activePC.CommandsToQueue[0].TargetIsValid())
        {
            TurnManager.Instance.MoveToNextPC(TurnManager.Instance.PCReceivingCommands);
        }
        else
        {
            if (activePC.CommandsToQueue.Count > 0)
            {
                SoundManager.Instance.PlayBuzzer();
                Combat_UI_Manager.Instance.SetMsg_TopOfScreen(friendlyTarget.Stats_Config.CharName + " is not a valid target for " + activePC.CommandsToQueue[0].CommandName, Color.white, 0, false);
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        PC = PCManager.Instance.GetPCGameObjectFromName(nameTxt.text);
        if (PC != null)
        {
            TargetManager.Instance.PC_SetFriendlyTarget(PC);
        }
    }
}
