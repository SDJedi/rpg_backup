using System.Collections.Generic;
using RPG.Inventory;
using UnityEngine;

//Last Reviewed: 2/25/20
namespace RPG.CombatSystem
{
    //extends the Character class, providing things enemies need, like loot, gold, xp, etc
    [RequireComponent(typeof(Destroyed_RemoveCommandFromQueue))]
    [RequireComponent(typeof(Attacked_TakeDamage))]
    [RequireComponent(typeof(Attacked_DamageAttacker))]
    public class EnemyCharacter : Character
    {
        public string EnemyName;
        public int GPValue;
        public int XPValue;
        public int numOfActionsPerTurn = 1;
        public EnemyLootTable LootTable = new EnemyLootTable();
        public List<Proc> AcitveProcs;

        public ProcContainer procContainer { get; private set; }
        public Transform homePos { get; private set; }
        public Item ItemToDrop { get; private set; } = null;


        protected override void Awake()
        {
            Set_Stats_Config(Instantiate(stats_Config)); //Note: Command.cs grabs a reference to this in Start(), which is why the Stats_Config MUST be instantiated in Awake()   
            stats_Config.CharName = EnemyName;
            procContainer = (ProcContainer)ScriptableObject.CreateInstance("ProcContainer");
            CreateEnemyHomePos();

            if (numOfActionsPerTurn < 1)
            {
                Debug.LogWarning(gameObject + "EnemyCharacter script is set for no actions per turn. Is this intended?");
            }

            base.Awake();
        }

        void Start()
        {
            ItemToDrop = LootTable.GetItemFromPossibleItems();

            if (ItemToDrop != null)
            {
                PostBattleManager.Instance.ItemsEarned.Add(ItemToDrop);
            }

            //add any damage shields
            var damageShieldContainer = GetComponent<Attacked_DamageAttacker>();
            if (damageShieldContainer != null)
            {
                foreach (DamageShield ds in stats_Config.DamageShields)
                {
                    if (ds.nameOfDmgShield == "")
                    {
                        Debug.LogWarning("Damage Shield needs a name to be removed properly... not adding");
                        continue;
                    }
                    damageShieldContainer.AddDamageShieldToIAttackable(ds);
                }
            }

            //add any procs
            AcitveProcs.RemoveAll(n => n == null);
            foreach (Proc p in AcitveProcs)
            {
                procContainer.AddProcToList(p);
            }
        }

        void OnDestroy()
        {
            Destroy(Stats_Config);

            if (homePos != null)
            {
                Destroy(homePos.gameObject);
            }
        }

        void CreateEnemyHomePos()
        {
            homePos = new GameObject(gameObject.name + " homePos").transform;
            homePos.transform.position = gameObject.transform.position;
            homePos.transform.rotation = gameObject.transform.rotation;
            homePos.transform.parent = GameObject.Find("Enemy_Positions").transform;
        }

        public void SetHomePosition(Transform pos)
        {
            homePos.transform.position = pos.position;
            homePos.transform.rotation = pos.rotation;
        }
    }

    [System.Serializable]
    public class EnemyLootTable
    {
        [System.Serializable]
        public class DroppableItem
        {
            public Item item;
            [Range(1, 20)][Header("Higher SpawnWeightChance (relative to other items) == greater odds of item dropping")]
            public int spawnWeightChance = 1;
        }

        public List<DroppableItem> PossibleItems = new List<DroppableItem>();


        //Note: specific roll required for specific items can change with item order, but % chance remains accurate regardless.
        public Item GetItemFromPossibleItems()
        {
            int itemSpawnChance = 0;
            int totalSpawnWeight = 0;
            int roll = 0;

            for (int i = 0; i < PossibleItems.Count; i++)
            {
                totalSpawnWeight += PossibleItems[i].spawnWeightChance;
            }

            roll = Random.Range(1, totalSpawnWeight + 1);

            for (int i = 0; i < PossibleItems.Count; i++)
            {
                itemSpawnChance += PossibleItems[i].spawnWeightChance;
                if (roll <= itemSpawnChance)
                {
                    return PossibleItems[i].item;
                }
            }

            return null;
        }
    }
}
