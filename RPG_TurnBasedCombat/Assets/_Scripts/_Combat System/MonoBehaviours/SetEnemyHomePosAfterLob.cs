﻿using UnityEngine;

namespace RPG.CombatSystem
{
    [RequireComponent(typeof(LobObjectIntoPosition))] 
    public class SetEnemyHomePosAfterLob : MonoBehaviour
    {
        LobObjectIntoPosition lobber;


        void Awake()
        {
            lobber = GetComponent<LobObjectIntoPosition>();
            lobber.LobComplete += SetHomePos;
        }

        void OnDestroy()
        {
            lobber.LobComplete -= SetHomePos;
        }

        void SetHomePos()
        {
            GetComponent<EnemyCharacter>().SetHomePosition(transform);
        }
    }
}
