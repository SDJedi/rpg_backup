using UnityEngine;

//Last Reviewed: 2/22/20
namespace RPG.CombatSystem
{
    //required component for all characters
    public class Destroyed_RemoveCommandFromQueue : MonoBehaviour, IDestructible
    {
        Character CScript;


        void Awake()
        {
            CScript = GetComponent<Character>();
        }

        //remove any and all Commands in the CommandQueue that belong to this character
        public void OnDeath()
        {
            if (TurnManager.InstanceExists)
            {
                for (int i = TurnManager.Instance.CommandQueue.Count - 1; i >= 0; i--)
                {
                    if (TurnManager.Instance.CommandQueue[i].gameObject == gameObject)
                    {
                        TurnManager.Instance.CommandQueue.RemoveAt(i);
                    }
                }
                TurnManager.Instance.CommandQueue.RemoveAll(n => n == null);
            }
        }
    }
}
