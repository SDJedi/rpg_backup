using UnityEngine;

//Last Reviewed: 2/22/20
namespace RPG.CombatSystem
{
    public class Destroyed_PlayDeathAnimation : MonoBehaviour, IDestructible
    {
        Animator anim;


        void Awake()
        {
            anim = GetComponentInChildren<Animator>();
        }

        public void OnDeath()
        {
            if (anim != null)
            {
                anim.SetTrigger("Die");
            }
        }
    }
}
