﻿using UnityEngine;

//Last Reviewed: 3/7/20
namespace RPG.CombatSystem
{
    public class Destroyed_RemoveFromTurnOrderList : MonoBehaviour, IDestructible
    {
        public void OnDeath()
        {
            TurnManager.Instance.RemoveFromTurnOrderList(gameObject);
        }
    }
}
