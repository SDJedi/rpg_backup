using System.Collections;
using UnityEngine;

//Last Reviewed: 2/22/20
namespace RPG.CombatSystem
{
    //Note: added a delay to when enemies are removed from list.
    //one reason for this is that Command_MeleeAttackEachHostile needs to re-evaluate this
    //list after each attack for dead enemies and make adjustments
    public class Destroyed_RemoveFromEnemyList : MonoBehaviour, IDestructible
    {
        public readonly float delayBeforeRemove = 3;


        public void OnDeath()
        {
            if (EnemyManager.InstanceExists)
            {
                StartCoroutine(RemoveEnemy());
            }
        }

        IEnumerator RemoveEnemy()
        {
            yield return new WaitForSeconds(delayBeforeRemove);
            if (EnemyManager.Instance.EnemiesInEncounter.Contains(gameObject))
            {
                EnemyManager.Instance.EnemiesInEncounter.Remove(gameObject);
                EnemyManager.Instance.EnemiesInEncounter.RemoveAll(n => n == null);
            }
        }
    }
}
