using UnityEngine;

//Last Reviewed: 2/22/20
namespace RPG.CombatSystem
{
    public class Destroyed_IncreaseGPAward : MonoBehaviour, IDestructible
    {
        public void OnDeath()
        {
            EnemyCharacter EScript = GetComponent<EnemyCharacter>();
            if (EScript != null)
            {
                PostBattleManager.Instance.TotalGPEarned += EScript.GPValue;
            }
        }
    }
}
