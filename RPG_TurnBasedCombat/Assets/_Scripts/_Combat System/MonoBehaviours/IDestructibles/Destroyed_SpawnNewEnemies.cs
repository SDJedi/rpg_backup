﻿using System.Collections;
using UnityEngine;

namespace RPG.CombatSystem
{
    public class Destroyed_SpawnNewEnemies : MonoBehaviour, IDestructible
    {
        [Range(0, 2.5f)] //less than 3 seconds so that spawn happens before body disappears
        [SerializeField] float delayBeforeSpawn = 0;
        [SerializeField] float delayAfterSpawn = 1;
        [SerializeField] bool spawnAtThisTransformPosition = true;
        [SerializeField] EnemyEncounter encounterToSpawn = null;
        [SerializeField] SkinnedMeshRenderer meshToDisable = null;

        Character CScript;


        void Awake()
        {
            CScript = GetComponent<Character>();
            encounterToSpawn = Instantiate(encounterToSpawn);
        }

        public void OnDeath()
        {
            //hide mesh of the dead enemy if desired
            if (meshToDisable != null)
            {
                meshToDisable.enabled = false;
            }

            if (spawnAtThisTransformPosition)
            {
                for (int i = 0; i < encounterToSpawn.EnemiesInEncounter.Count; i++)
                {
                    encounterToSpawn.EnemiesInEncounter[i].enemySpawnPos = transform.position;
                }
            }

            TurnManager.Instance.PauseTurnExecution(delayBeforeSpawn + delayAfterSpawn); //don't allow next char to take turn yet
            CScript.Stats_Config.ModifyCurrentHP(999999999); //set hp so that victory condition is not met
            StartCoroutine(SpawnNewEnemies());
        }

        IEnumerator SpawnNewEnemies()
        {
            yield return new WaitForSeconds(delayBeforeSpawn);
            EnemyManager.Instance.AddEnemiesDuringCombat(encounterToSpawn);
            CScript.Stats_Config.ModifyCurrentHP(-999999999); //set hp so that it doesn't register as alive any more
        }
    }
}
