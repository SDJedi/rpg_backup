using UnityEngine;

//Last Reviewed: 2/22/20
namespace RPG.CombatSystem
{
    public class Destroyed_IncreaseXPAward : MonoBehaviour, IDestructible
    {
        public void OnDeath()
        {
            EnemyCharacter EScript = GetComponent<EnemyCharacter>();
            if (EScript != null)
            {
                PostBattleManager.Instance.TotalXPEarned += EScript.XPValue;
            }
        }
    }
}
