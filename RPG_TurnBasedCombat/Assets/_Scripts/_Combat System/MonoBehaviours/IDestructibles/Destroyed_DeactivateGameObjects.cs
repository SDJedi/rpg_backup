using UnityEngine;

//Last Reviewed: 2/22/20
namespace RPG.CombatSystem
{
    public class Destroyed_DeactivateGameObjects : MonoBehaviour, IDestructible
    {
        [SerializeField] GameObject[] objectsToDeactivate = null;


        public void OnDeath()
        {
            foreach (GameObject obj in objectsToDeactivate)
            {
                if (obj != null)
                {
                    obj.SetActive(false);
                }
            }
        }
    }
}
