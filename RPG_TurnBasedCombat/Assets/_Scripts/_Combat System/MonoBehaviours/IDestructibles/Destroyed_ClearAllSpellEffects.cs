using UnityEngine;

//Last Reviewed: 2/22/20
namespace RPG.CombatSystem
{
    public class Destroyed_ClearAllSpellEffects : MonoBehaviour, IDestructible
    {
        Character CScript;


        void Start()
        {
            CScript = GetComponent<Character>();
        }

        public void OnDeath()
        {
            //print("OnDeath() clearing all spell effects on " + gameObject);
            CScript.ClearAllSpellEffects();
        }
    }
}
