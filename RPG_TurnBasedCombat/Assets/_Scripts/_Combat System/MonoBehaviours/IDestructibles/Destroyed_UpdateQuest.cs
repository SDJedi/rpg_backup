using RPG.QuestSystem;
using UnityEngine;

//Last Reviewed: 2/22/20
namespace RPG.CombatSystem
{
    //allow the death of this character to update a Quest
    public class Destroyed_UpdateQuest : MonoBehaviour, IDestructible
    {
        public Quest Quest;
        public int ObjectiveIndex;
        public int[] ObjectivesThatMustBeCompletedFirst;


        public void OnDeath()
        {
            UpdateQuestObjective();
        }

        void UpdateQuestObjective()
        {
            Quest q = QuestManager.Instance.GetQuestByName(Quest.Title);

            if (q == null || q.State != QuestState.Active)
            {
                //Debug.Log("Quest \"" + q.Title + "\" does not exist or is not active. Cannot update objective.");
                return;
            }

            for (int i = 0; i < ObjectivesThatMustBeCompletedFirst.Length; i++)
            {
                if (!QuestManager.Instance.ObjectiveIsComplete(q, ObjectivesThatMustBeCompletedFirst[i]))
                {
                    Debug.LogWarning("Quest \"" + q.Title + "\":" + " a prerequisite objective is not yet complete. Cannot update objective.");
                    return;
                }
            }

            QuestManager.Instance.UpdateQuestObjective(q.Title, ObjectiveIndex);
        }
    }
}
