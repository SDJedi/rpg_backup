using UnityEngine;

//Last Reviewed: 2/22/20
namespace RPG.CombatSystem
{
    public class Destroyed_DestroyGameObject : MonoBehaviour, IDestructible
    {
        public GameObject ObjectToDestroy;
        public float Delay;


        //if this is an enemy, ensure that it gets removed from EnemyList before we destroy the gameObject
        void Awake()
        {
            if (ObjectToDestroy == gameObject)
            {
                var remover = GetComponent<Destroyed_RemoveFromEnemyList>();
                if (remover != null && Delay < remover.delayBeforeRemove)
                {
                    Delay = remover.delayBeforeRemove + 0.5f;
                }
            }
        }

        public void OnDeath()
        {
            Destroy(ObjectToDestroy, Delay);
        }
    }
}
