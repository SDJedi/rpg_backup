using System;
using UnityEngine;
using UnityEngine.UI;

//Last Reviewed: 2/25/20
namespace RPG.CombatSystem
{
    //shoot a raycast into the scene; invoke an event based on what the ray hits
    //(e.g. one event if over a PC, another event if over an enemy, and yet another event if over neither)
    public class CombatRaycaster : Singleton<CombatRaycaster>
    {
        const float maxRaycastDepth = 200f;

        //events to invoke
        public event Action<GameObject> OnMouseOverPC;
        public event Action<GameObject> OnMouseOverEnemy;
        public event Action OnMouseNotOverEnemyOrPC;


        void Update()
        {
            PerformRaycasts();
        }

        void PerformRaycasts()
        {
            if (CameraManager.InstanceExists && CameraManager.Instance.ActiveCam != null)
            {
                Ray ray = CameraManager.Instance.ActiveCam.ScreenPointToRay(Input.mousePosition);

                if (RaycastForCharacter(ray))
                {
                    return;
                }

                if (OnMouseNotOverEnemyOrPC != null)
                {
                    OnMouseNotOverEnemyOrPC();
                }
            }
        }

        bool RaycastForCharacter(Ray ray)
        {
            RaycastHit hitInfo;
            Physics.Raycast(ray, out hitInfo, maxRaycastDepth);
            if (hitInfo.collider != null && hitInfo.collider.gameObject.tag == "PC")
            {
                if (OnMouseOverPC != null)
                {
                    OnMouseOverPC(hitInfo.collider.gameObject);
                }
                //print("mouse is over PC:" + hitInfo.collider.gameObject);
                return true;
            }
            else if (hitInfo.collider != null && (hitInfo.collider.gameObject.layer == LayerMask.NameToLayer("Enemy")))
            {
                if (OnMouseOverEnemy != null)
                {
                    OnMouseOverEnemy(hitInfo.collider.gameObject);
                }
                //print("mouse is over enemy: " + hitInfo.collider.gameObject);
                return true;
            }
            //print("mouse is NOT over a PC or an enemy: ");
            return false;
        }
    }
}
