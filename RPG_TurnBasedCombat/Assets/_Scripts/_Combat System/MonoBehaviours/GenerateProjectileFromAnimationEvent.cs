using UnityEngine;

//Last Reviewed: 2/26/20
namespace RPG.CombatSystem
{
    //attached to each character (on a child component that has the animator).
    //set the variables first, then allow an animation event to spawn a projectile
    //by calling SpawnProjectileFromAnimation() at the moment launch should occur.
    public class GenerateProjectileFromAnimationEvent : MonoBehaviour
    {
        BaseCommand callingCommand;
        GameObject proj;
        GameObject spawnPos;
        Transform lookAt;
        Base_Stats_Config shooterStats;
        GameObject target;


        //called by ranged attacks
        public void SetProjectileInfo(BaseCommand CallingCommand, GameObject Proj, Base_Stats_Config ShooterStats, GameObject Target, GameObject Pos, Transform LookAt)
        {
            callingCommand = CallingCommand;
            proj = Proj;
            spawnPos = Pos;
            lookAt = LookAt;
            shooterStats = ShooterStats;
            target = Target;
        }

        //reset after each attack (see Command_RangedAttack.cs)
        public void Reset_Internal()
        {
            callingCommand = null;
            proj = null;
            spawnPos = null;
            lookAt = null;
            shooterStats = null;
            target = null;
        }

        #region Animation Events
        public void SpawnProjectileFromAnimation()
        {
            if (proj != null)
            {
                var p = Instantiate(proj, spawnPos != null ? spawnPos.transform.position : transform.position, Quaternion.identity, null);
                var projComp = p.GetComponent<Projectile>();

                if (projComp.OnSpawnSound != null)
                {
                    SoundManager.Instance.PlayAudioClip(projComp.OnSpawnSound);
                }

                projComp.SetupProjectile(callingCommand, shooterStats, target);
                p.transform.LookAt(lookAt);
                p.transform.eulerAngles = new Vector3(0, p.transform.rotation.eulerAngles.y, 0);
            }
        }
        #endregion
    }
}
