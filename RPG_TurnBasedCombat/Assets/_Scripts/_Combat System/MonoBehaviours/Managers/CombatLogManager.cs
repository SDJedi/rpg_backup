using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//Last Reviewed: 2/23/20
namespace RPG.CombatSystem
{
    //attached to : CombatScene --> CombatScene_Canvas --> NameAndStats_Panel --> CombatLogCanvas --> CombatLog.
    //manages the combat text info at the bottom of the combat screen
    public class CombatLogManager : Singleton<CombatLogManager>
    {
        GameObject CombatLogContent;
        GameObject OriginalTextGameObject;
        GameObject TextClone;
        ScrollRect cLogScrollRect;


        //cache references to necessary UI components in the CombatScene
        protected override void Awake()
        {
            base.Awake();
            CombatLogContent = transform.Find("ViewPort").transform.Find("Content").gameObject;
            cLogScrollRect = GetComponent<ScrollRect>();
            OriginalTextGameObject = CombatLogContent.transform.GetChild(0).gameObject; //called "CombatLogText"
            TextClone = Instantiate(OriginalTextGameObject);
            SceneManager.MoveGameObjectToScene(TextClone, gameObject.scene);
            OriginalTextGameObject.SetActive(false);
        }

        //add an entry to the bottom of the log and force scroll down
        public void AddToCombatLog(string mssg, Color32 color)
        {
            StartCoroutine(UpdateCombatLogContent(mssg, color));
        }

        IEnumerator UpdateCombatLogContent(string mssg, Color32 color)
        {
            if (OriginalTextGameObject != null && !OriginalTextGameObject.activeSelf)
            {
                OriginalTextGameObject.SetActive(true);
                var textComp = OriginalTextGameObject.GetComponent<TextMeshProUGUI>();
                SetCombatLogContentText(mssg, color, textComp);
            }
            else
            {
                var newEntry = Instantiate(TextClone, CombatLogContent.transform);
                var textComp = newEntry.GetComponent<TextMeshProUGUI>();
                SetCombatLogContentText(mssg, color, textComp);
            }

            // if (CombatLogContent.transform.childCount > 150)
            // {
            //     print("over 150!");
            //     Destroy(CombatLogContent.transform.GetChild(0).gameObject);
            // }
            yield return new WaitForEndOfFrame();
            cLogScrollRect.verticalNormalizedPosition = 0f;
        }

        void SetCombatLogContentText(string mssg, Color32 color, TextMeshProUGUI textComp)
        {
            textComp.color = color;
            textComp.text = mssg;
        }
    }
}
