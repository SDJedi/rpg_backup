using System;
using System.Collections.Generic;
using RPG.Inventory;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

//Last Reviewed: 2/23/20
namespace RPG.CombatSystem
{
    //The job of this class is to make sure that buttons do what they are supposed to when they are clicked.
    //As the turn progresses from PC to PC and from menu to menu, ButtonCommandManager displays and
    //labels buttons, and assigns each button its proper "listener". This info comes from PlayerCharacter.cs
    //Note: the process is slightly different for "MainMenu" commands and "Sub-Menu" commands
    public class ButtonCommandManager : Singleton<ButtonCommandManager>
    {
        public static Action<BaseCommand> CommandButtonPressed;

        //The (up to) 9 "Main Menu" buttons for a PC (e.g. "Attack", "Defend", "Special", "Magic", "Item")
        public Button[] PC_MainMenuButtons = new Button[9];
        //The (up to) 40 buttons that appear when the player opens the sub menu
        public Button[] PC_SubMenuButtons = new Button[40];
        //The (up to) 50 buttons that represent items that are useable in combat
        public List<Button> PC_CombatInventoryButtons;
        //default icon for abilities that have no icon yet
        public Sprite defaultIcon = null;


        //Called by TurnManager.cs in StartACharactersTurn()...
        public void PopulatePC_MainMenuCommand_Buttons(GameObject PC)
        {
            //Remove all listeners attached to the PCCommandButtons
            RemoveAllListenersFromPCMainMenuButtons();

            //Populate an array with all Command components on the PC
            BaseCommand[] cmndArray = PC.GetComponents<BaseCommand>();
            //We will add all commands that belong on the MainMenu for this PC ("Attack" "Defend" "Item" at the least) to cmndList
            List<BaseCommand> mainMenuCommands = new List<BaseCommand>();

            bool hasSpecials = false; //flag if this PC has abilities that should be filed under the "Special" button
            bool hasMagic = false; //flag if this PC has abilities that should be filed under the "Magic" button

            foreach (BaseCommand cmnd in cmndArray)
            {
                if (cmnd.ButtonIndex >= 0 && cmnd.ButtonIndex < PC_MainMenuButtons.Length)
                {
                    mainMenuCommands.Add(cmnd);
                    continue;
                }
                if (cmnd.AddToSpecialMenu)
                {
                    hasSpecials = true;
                }
                if (cmnd.AddToMagicMenu)
                {
                    hasMagic = true;
                }
            }

            ////////////////////Error Checking//////////////////////////
            //If there are no MainMenuCommands, abort
            if (mainMenuCommands.Count == 0)
            {
                Debug.LogWarning(PC + " does not have any MainMenuCommand components");
                return;
            }
            //If there are more than 9 MainMenuCommands, abort
            if (mainMenuCommands.Count > PC_MainMenuButtons.Length)
            {
                Debug.LogWarning(PC + " has too many MainMenuCommand components. Limit == " + PC_MainMenuButtons.Length);
                return;
            }
            ////////////////////////////////////////////////////


            //Note: the 1st 2 Main Menu command buttons are "Attack" and "Defend" .. these should never change.
            //If this PC has commands flagged as "Add to Special Menu" and/or "Add to Magic Menu",
            //we need to ensure that buttons labeled "Special" and/or "Magic" appear in the Main Menu.
            //"Item" will be added to every PC automatically.
            Add_Special_Magic_Item_MainMenuButtons(PC, cmndArray, mainMenuCommands, hasSpecials, hasMagic);
            SetUpMainMenuButtonText(PC);
            //Assign each MainMenuCommand to a button based on the index set for that MainMenuCommand
            foreach (BaseCommand cmnd in mainMenuCommands)
            {
                AssignACommandToAMainMenuButton(cmnd.ButtonIndex, cmnd);
            }
            Combat_UI_Manager.Instance.HideCombatInvPanel();
        }

        //Add "Item" for sure, "Special" and "Magic" if needed
        void Add_Special_Magic_Item_MainMenuButtons(GameObject PC, BaseCommand[] cmndsOnPC, List<BaseCommand> cmndsForMainMenu, bool hasSpecials, bool hasMagic)
        {
            if (hasSpecials) //at least one Command on this PC is marked "Add to Special Menu"
            {
                //we loop through cmndArray and look for an existing Command_OpenSubMenu with a SubMenuName == "Special"
                Command_OpenSubMenu specailSubMenu = GetSubMenu(cmndsOnPC, "Special");
                //if we didn't find a Command_OpenSubMenu for "Special", we need to add one since we DO have at least 1 command marked for the
                //"Special" menu
                if (specailSubMenu == null)
                {
                    //add the component and set its values
                    var cmd = PC.AddComponent<Command_OpenSubMenu>();
                    cmd.SubMenuName = "Special";
                    cmd.ButtonIndex = 2;
                    //update the PC Stats_Config to reflect the change
                    ((PC_Stats_Config)PC.GetComponent<PlayerCharacter>().Stats_Config).Button2 = "Special";
                    //add the newly created Command_OpenSubMenu for "Special" to the list of cmndsForMainMenu
                    cmndsForMainMenu.Add(cmd);
                }
                else
                {
                    //An OpenSubMenu for "Special" already exists... make certain it has the proper button index (0 == "Attack", 1 == "Defend", "Special" == 2, "Magic" == 3, "Items" == 4)
                    specailSubMenu.ButtonIndex = 2;
                    //update the PC Stats_Config
                    ((PC_Stats_Config)PC.GetComponent<PlayerCharacter>().Stats_Config).Button2 = "Special";
                }
            }
            else
            {
                //there are no commands flagged for the "Special" sub menu...
                //Do we have a component for opening the "Special" menu that we don't need?
                Command_OpenSubMenu specailSubMenu = GetSubMenu(cmndsOnPC, "Special");
                if (specailSubMenu != null)
                {
                    //yes, we did. Destroy it.
                    Destroy(specailSubMenu);
                }
                //we know we don't need a "Special" Menu, so tell the Stats_Config to leave the button at index 2
                //blank for now.
                ((PC_Stats_Config)PC.GetComponent<PlayerCharacter>().Stats_Config).Button2 = "";
            }

            if (hasMagic) //at least one Command on this PC is marked "Add to Magic Menu"
            {
                //Do we have a component already for opening the "Magic" submenu?
                Command_OpenSubMenu magicSubMenu = GetSubMenu(cmndsOnPC, "Magic");

                if (magicSubMenu == null)
                {
                    //Nope ... let's make one...
                    var cmd = PC.AddComponent<Command_OpenSubMenu>();
                    cmd.SubMenuName = "Magic";
                    //Magic will show up beneath Special if the PC has Special abilities
                    cmd.ButtonIndex = 3;
                    ((PC_Stats_Config)PC.GetComponent<PlayerCharacter>().Stats_Config).Button3 = "Magic";
                    //add the newly created Command_OpenSubMenu for "Magic" to the list of cmndsForMainMenu
                    cmndsForMainMenu.Add(cmd);
                }
                else
                {
                    //An OpenSubMenu for "Special" already exists... make certain it has the proper button index (0 == "Attack", 1 == "Defend", "Special" == 2, "Magic" == 3, "Items" == 4)
                    magicSubMenu.ButtonIndex = 3;
                    //update the PC Stats_Config
                    ((PC_Stats_Config)PC.GetComponent<PlayerCharacter>().Stats_Config).Button3 = "Magic";
                }
            }
            else
            {
                //if we're here, we have no commands flagged as "Add To Magic Menu"
                //but does a component for opening the Magic sub-menu exist on the PC?
                Command_OpenSubMenu magicSubMenu = GetSubMenu(cmndsOnPC, "Magic");
                if (magicSubMenu != null)
                {
                    //Yup ... destroy it.
                    Destroy(magicSubMenu);
                }
                //update stats config
                ((PC_Stats_Config)PC.GetComponent<PlayerCharacter>().Stats_Config).Button3 = "";
            }

            //Now we deal with the "Item" Command ...
            //First, we need to make sure the PC has a component of type Command_UseItem
            //which handles "casting / using" the item when the PC takes their combat action for the round
            if (PC.GetComponent<Command_UseItem>() == null)
            {
                PC.AddComponent<Command_UseItem>();
            }

            //Next, we check to see if the PC has the script that is used to open the combat inventory when the
            //"Items" btn is pressed...
            var cmndOpenInv = PC.GetComponent<Command_OpenCombatInventory>();
            if (PC.GetComponent<Command_OpenCombatInventory>() == null)
            {
                //the component we need is missing on this PC... add it
                cmndOpenInv = PC.AddComponent<Command_OpenCombatInventory>();
                //add it to cmndList so that the OnButtonClick() listener gets added
                cmndsForMainMenu.Add(cmndOpenInv);
            }
            //Make sure the button index is 4 (0 == "Attack", 1 == "Defend", "Special" == 2, "Magic" == 3, "Items" == 4)
            cmndOpenInv.ButtonIndex = 4;
            //update the PC Stats_Config to reflect the change
            ((PC_Stats_Config)PC.GetComponent<PlayerCharacter>().Stats_Config).Button4 = "Items";
        }

        //Called by Command_OpenCombatInventory.cs when the "Items" button is pressed
        public void PopulatePC_CombatInventoryButtons()
        {
            //clear listeners and items from all buttons
            foreach (Button btn in PC_CombatInventoryButtons)
            {
                btn.onClick.RemoveAllListeners();
                btn.GetComponent<Inventory_ItemAttachedToCombatInvButton>().item = null;
            }

            //create a list of all items that are useable in combat...
            List<UseableItem> useableItems = new List<UseableItem>();
            foreach (Item itm in InventoryManager.Instance.ItemsInInventory)
            {
                if (itm.GetType().IsSubclassOf(typeof(UseableItem)) && ((UseableItem)itm).UseableInCombat)
                {
                    useableItems.Add((UseableItem)itm);
                }
            }
            //sort alphabetically...
            useableItems.Sort((a, b) => (a.ItemName.CompareTo(b.ItemName)));

            //loop through the combat inv btns..
            for (int i = 0; i < PC_CombatInventoryButtons.Count; i++)
            {
                //while we still have useable items to add to the list...
                if (i < useableItems.Count)
                {
                    //set the button text, icon, and listener based on the item...
                    PC_CombatInventoryButtons[i].GetComponentInChildren<TextMeshProUGUI>().text = useableItems[i].ItemName + " x (" + useableItems[i].ItemQuantity.ToString() + ")";
                    PC_CombatInventoryButtons[i].transform.Find("Icon_Img").GetComponent<Image>().sprite = useableItems[i].Icon;
                    var btnScript = PC_CombatInventoryButtons[i].GetComponent<Inventory_ItemAttachedToCombatInvButton>();
                    btnScript.item = useableItems[i];
                    PC_CombatInventoryButtons[i].onClick.AddListener(() => btnScript.OnButtonClick());
                }
                else
                {
                    //we still have buttons, but no more items ... hide the button
                    PC_CombatInventoryButtons[i].gameObject.SetActive(false);
                }
            }
        }

        //called when a MainMenu button that is used for opening a submenu is pressed ("Special", "Magic") see above for "Items"
        public void PopulatePCSubMenuButtons(Command_OpenSubMenu command)
        {
            //clear out all listeners
            RemoveAllListenersFromPCSubMenuButtons();

            //deactivate all sub-menu buttons
            foreach (Button btn in PC_SubMenuButtons)
            {
                btn.gameObject.SetActive(false);
            }

            //Each SubMenuCommand on a PC should already be in an alphabetically sorted List<SubMenu_Commands> on PlayerCharacter.cs
            //   for each Command...
            //1) activate a button
            //2) attach a listener to the button (OnButtonClick() from the SubMenuCommand)
            //3) set the text of the button to the CommandName of the Command
            //4) set the icon
            //5) set the description of the Command to display in the info card
            //PlayerCharacter PCScript = PC.GetComponent<PlayerCharacter>();
            if (command.SubMenu_Commands.Count > PC_SubMenuButtons.Length)
            {
                Debug.LogWarning("ERROR! PC has more than " + PC_SubMenuButtons.Length.ToString() + " Command componenets");
                return;
            }

            for (int i = 0; i < command.SubMenu_Commands.Count; i++)
            {
                PC_SubMenuButtons[i].gameObject.SetActive(true);
                PC_SubMenuButtons[i].onClick.AddListener(command.SubMenu_Commands[i].OnButtonClick);
                PC_SubMenuButtons[i].GetComponentInChildren<TextMeshProUGUI>().text = command.SubMenu_Commands[i].CommandName;
                var s = command.SubMenu_Commands[i].Command_Config.Icon;
                if (s != null)
                {
                    PC_SubMenuButtons[i].transform.Find("Icon_Img").GetComponent<Image>().sprite = s;
                }
                else
                {
                    PC_SubMenuButtons[i].transform.Find("Icon_Img").GetComponent<Image>().sprite = defaultIcon;
                }
                PC_SubMenuButtons[i].GetComponent<ShowCommandInfoWhenCursorTouchesButton>().StoreReferenceToCommandCurrentlyOnThisButton(command.SubMenu_Commands[i]);
            }
        }

        //helper methods
        Command_OpenSubMenu GetSubMenu(BaseCommand[] cmndsOnPC, string subMenuName)
        {
            for (int i = 0; i < cmndsOnPC.Length; i++)
            {
                if (cmndsOnPC[i].GetType() == typeof(Command_OpenSubMenu) && ((Command_OpenSubMenu)cmndsOnPC[i]).SubMenuName == subMenuName)
                {
                    return cmndsOnPC[i] as Command_OpenSubMenu;
                }
            }
            return null;
        }

        void SetUpMainMenuButtonText(GameObject PC)
        {
            PlayerCharacter PCScript = PC.GetComponent<PlayerCharacter>();
            var stats = (PC_Stats_Config)PCScript.Stats_Config;

            PC_MainMenuButtons[0].GetComponentInChildren<TextMeshProUGUI>().text = stats.Button0; //always "Attack"
            PC_MainMenuButtons[1].GetComponentInChildren<TextMeshProUGUI>().text = stats.Button1; //always "Defend"
            PC_MainMenuButtons[2].GetComponentInChildren<TextMeshProUGUI>().text = stats.Button2; //"Special" or empty
            PC_MainMenuButtons[3].GetComponentInChildren<TextMeshProUGUI>().text = stats.Button3; //"Magic" or empty
            PC_MainMenuButtons[4].GetComponentInChildren<TextMeshProUGUI>().text = stats.Button4; //always "Items"
            PC_MainMenuButtons[5].GetComponentInChildren<TextMeshProUGUI>().text = stats.Button5;
            PC_MainMenuButtons[6].GetComponentInChildren<TextMeshProUGUI>().text = stats.Button6;
            PC_MainMenuButtons[7].GetComponentInChildren<TextMeshProUGUI>().text = stats.Button7;
            PC_MainMenuButtons[8].GetComponentInChildren<TextMeshProUGUI>().text = stats.Button8;

            foreach (Button btn in PC_MainMenuButtons)
            {
                var txt = btn.GetComponentInChildren<TextMeshProUGUI>();
                if (txt.text == "")
                {
                    txt.transform.parent.gameObject.SetActive(false);
                }
                else
                {
                    txt.transform.parent.gameObject.SetActive(true);
                }
            }
        }

        void AssignACommandToAMainMenuButton(int btnIndex, BaseCommand cmnd)
        {
            if (btnIndex < PC_MainMenuButtons.Length && btnIndex >= 0)
            {
                PC_MainMenuButtons[btnIndex].onClick.AddListener(cmnd.OnButtonClick);
                if (!PC_MainMenuButtons[btnIndex].gameObject.activeSelf)
                {
                    PC_MainMenuButtons[btnIndex].gameObject.SetActive(true);
                    PC_MainMenuButtons[btnIndex].GetComponentInChildren<TextMeshProUGUI>().text = cmnd.CommandName;
                    //Debug.LogWarning(cmnd + " has been assigned to button at index " + btnIndex + ". That button's gameObject is not active");
                }
            }
            else
            {
                Debug.LogWarning("btnIndex for " + cmnd + " is out of range. Set a value 0 - 8");
            }
        }

        void RemoveAllListenersFromPCMainMenuButtons()
        {
            foreach (Button btn in PC_MainMenuButtons)
            {
                btn.onClick.RemoveAllListeners();
            }
        }

        void RemoveAllListenersFromPCSubMenuButtons()
        {
            foreach (Button btn in PC_SubMenuButtons)
            {
                btn.onClick.RemoveAllListeners();
            }
        }
    }
}
