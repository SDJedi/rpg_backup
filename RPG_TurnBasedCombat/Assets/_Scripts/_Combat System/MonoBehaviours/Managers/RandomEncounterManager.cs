using System.Collections.Generic;
using RPG.Saving;
using RPG.SceneControl;
using UnityEngine;
using UnityEngine.UI;

//Last Reviewed: 2/24/20
namespace RPG.CombatSystem
{
    [System.Serializable]
    public class EncounterAndSpawnChance
    {
        public EnemyEncounter Encounter;
        [Header("Higher SpawnWeightChance (relative to other encounters) == greater odds of spawning")]
        [Range(1, 100)]
        public int SpawnWeightChance = 1;
    }

    //This script stays in the persistent scene(BootScene). All "OverWorld" scenes should have a "DefaultEncountersForScene.cs"
    //that will SetPossibleEncountersList() in Start().
    public class RandomEncounterManager : Singleton<RandomEncounterManager>, ISaveable
    {
        [SerializeField] Toggle RandomEncountersToggle = null;
        [Range(1, 100)]
        [SerializeField] int percentChanceOfEncounterPerSecond = 10;
        [SerializeField] float minSecondsBetweenEncounters = 5;

        PC_MoveOrStartInteraction mover = null;
        float timeSinceLastRandomEncounter = 0;
        float checkForEncounterTimer = 0;
        bool loadingCombatScene = false;

        public bool RandomEncountersDisabled { get; set; } = false;
        public List<EncounterAndSpawnChance> possibleEncounters = new List<EncounterAndSpawnChance>();


        void OnEnable()
        {
            SceneController.Instance.LeavingCombatScene += ResetTimersWhenLeavingCombat;
        }

        void OnDisable()
        {
            if (SceneController.InstanceExists)
            {
                SceneController.Instance.LeavingCombatScene -= ResetTimersWhenLeavingCombat;
            }
        }

        void ResetTimersWhenLeavingCombat()
        {
            checkForEncounterTimer = 0;
            timeSinceLastRandomEncounter = 0;
        }

        //run checks to see if the party should enter a battle
        void Update()
        {
            //hard disable on...
            if (RandomEncountersDisabled)
            {
                print("Random encounters have been disabled");
                return;
            }

            //if there are no encounters, return
            if (possibleEncounters == null || possibleEncounters.Count < 1)
            {
                //print("No random encounters set ... not checking for encounters");
                return;
            }

            //if debug toggle is off, return
            if (!RandomEncountersToggle.isOn)
            {
                //print("toggle off ... not checking for encounters");
                return;
            }

            //only check for encounters when OverWorldPC is around
            if (mover == null || !mover.isActiveAndEnabled)
            {
                //print("no OverWorldPC ... not checking for encounters");
                return;
            }

            //no combat when pc is moving to an interactable
            if (mover.HasInteractable)
            {
                //Debug.Log("PC has interactable; no combat possible");
                return;
            }

            //we should be checking for random encounters...
            //print("checking for encounters");

            //timers only get updated if player is moving
            if (mover.IsMoving)
            {
                checkForEncounterTimer += Time.deltaTime;
                timeSinceLastRandomEncounter += Time.deltaTime;
            }

            //check that
            //a) PC is moving
            //b) timeSinceLastRandomEncounter >= minSecondsBetweenEncounters
            // and
            //c) it's been at least one second since we last checked
            if (mover.IsMoving && timeSinceLastRandomEncounter >= minSecondsBetweenEncounters && checkForEncounterTimer >= 1)
            {
                CheckForEncounter(percentChanceOfEncounterPerSecond);
            }
        }

        void CheckForEncounter(int percChance)
        {
            float chance = (float)percChance / 100;

            if (!loadingCombatScene && Random.value <= chance && !SceneController.Instance.sceneChangeInProgress && !SceneController.Instance.isFading)
            {
                mover.StopMovementAndInputHandling();
                LoadCombatScene();
            }
        }

        public void LoadCombatScene(bool playBattleStartSFX = true)
        {
            loadingCombatScene = true;
            SceneController.Instance.LoadCombatScene(playBattleStartSFX);
            timeSinceLastRandomEncounter = 0;
            loadingCombatScene = false;
        }

        public void SetPossibleEncountersList(List<EncounterAndSpawnChance> encounters)
        {
            possibleEncounters.Clear();

            if (encounters.Count < 1)
            {
                //Debug.LogWarning("possibleEncounters <List> is empty");
                return;
            }

            foreach (EncounterAndSpawnChance e in encounters)
            {
                if (e != null)
                {
                    possibleEncounters.Add(e);
                }
            }
        }

        //called by PC_MoveOrStartInteraction.cs
        public void SetMover(PC_MoveOrStartInteraction mover)
        {
            this.mover = mover;
        }

        #region SaveData
        public object CaptureState()
        {
            return RandomEncountersDisabled;
        }

        public void RestoreState(object state)
        {
            RandomEncountersDisabled = (bool)state;
        }
        #endregion
    }
}