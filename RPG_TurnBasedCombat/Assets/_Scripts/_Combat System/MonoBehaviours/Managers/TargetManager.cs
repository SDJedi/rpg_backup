using System.Collections;
using UnityEngine;

//Last Reviewed: 2/24/20
namespace RPG.CombatSystem
{
    //exists in the CombatScene; handles setting of friendly and hostile targets for PCs.
    //works with CombatRaycaster.cs to set targets OnMouseOver
    public class TargetManager : Singleton<TargetManager>
    {
        void Update()
        {
            //If a target has been selected and player presses Tab...
            SelectNextAvailableTargetWithTab();
        }

        void OnEnable()
        {
            CombatRaycaster.Instance.OnMouseOverEnemy += PC_SetHostileTarget;
            CombatRaycaster.Instance.OnMouseOverPC += PC_SetFriendlyTarget;
        }

        void OnDisable()
        {
            if (CombatRaycaster.InstanceExists)
            {
                CombatRaycaster.Instance.OnMouseOverEnemy -= PC_SetHostileTarget;
                CombatRaycaster.Instance.OnMouseOverPC -= PC_SetFriendlyTarget;
            }
        }

        public void SelectFirstLivingPCIn_PCsInParty_AndSetItAsActivePCsFriendlyTarget()
        {
            PC_SetFriendlyTarget(PCManager.Instance.GetLivingPCWithLowestPC_Slot_position());
        }

        public void SelectFirstLivingPCIn_PCsInPartyThatIsNotSelf_AndSetItAsActivePCsFriendlyTarget(Character self)
        {
            PC_SetFriendlyTarget(PCManager.Instance.GetLivingPCWithLowestPC_Slot_position_NotSelf(self));
        }

        public void SetFirstDeadPCAsFriendlyTarget(Character PC)
        {
            PC.SetFriendlyTarget(null);
            var deadPC = PCManager.Instance.GetNextDeadPC(PC.gameObject);
            if (deadPC != null)
            {
                PC_SetFriendlyTarget(deadPC); //needed during target selection
                PC.SetFriendlyTarget(deadPC.GetComponent<Character>()); //needed during combat
            }
        }

        public void SelectFirstLivingEnemyIn_EnemiesInParty_AndSetItAsActivePCsHostileTarget()
        {
            for (int i = 0; i < EnemyManager.Instance.EnemiesInEncounter.Count; i++)
            {
                if (EnemyManager.Instance.EnemiesInEncounter[i] != null && EnemyManager.Instance.EnemiesInEncounter[i].GetComponent<Character>().IsAlive)
                {
                    PC_SetHostileTarget(EnemyManager.Instance.EnemiesInEncounter[i]);
                    break;
                }
                continue;
            }
        }

        //Assign a random living (friendly or hostile) Character as [FriendlyTarget] or [HostileTarget] for a PC or an enemy
        public void SetARandomLivingFriendlyCharacterAsFriendlyTarget(Character character, bool allowSelf = true)
        {
            character.SetFriendlyTarget(null);
            character.SetFriendlyTarget(GetRandomFriendlyCharacter(character, allowSelf));
        }

        public void SetARandomLivingHostileCharacterAsHostileTarget(Character character)
        {
            character.SetHostileTarget(null);
            character.SetHostileTarget(GetRandomHostileCharacter(character));
        }

        //methods that return a random, living (friendly or hostile) Character
        public Character GetRandomFriendlyCharacter(Character character, bool allowSelf)
        {
            Character rfc = null;

            if (character.Is_A_PC && PCManager.Instance.GetNumOfLivingPCsInParty() > 0)
            {
                //if random friendly target can't be self...
                if (!allowSelf)
                {
                    //...but character is the only living PC...
                    if (PCManager.Instance.GetNumOfLivingPCsInParty() == 1 && character.IsAlive)
                    {
                        //return a null target
                        return rfc;
                    }
                }

                while (rfc == null)
                {
                    var potentialFriendly = PCManager.Instance.PCsInParty[Random.Range(0, PCManager.Instance.PCsInParty.Length)];
                    if (potentialFriendly != null)
                    {
                        var CScript = potentialFriendly.GetComponent<Character>();
                        if (CScript != null && CScript.IsAlive)
                        {
                            if (CScript != character)
                            {
                                rfc = CScript;
                            }
                            else if (allowSelf)
                            {
                                rfc = CScript;
                            }
                        }
                    }
                }
            }
            else if (!character.Is_A_PC && EnemyManager.Instance.EnemiesRemaining() > 0)
            {
                //if random friendly target can't be self...
                if (!allowSelf)
                {
                    //...but character is the only living enemy...
                    if (EnemyManager.Instance.EnemiesInEncounter.Count == 1 && character.IsAlive)
                    {
                        //return a null target
                        return rfc;
                    }
                }

                while (rfc == null)
                {
                    var potentialFriendly = EnemyManager.Instance.EnemiesInEncounter[Random.Range(0, EnemyManager.Instance.EnemiesInEncounter.Count)];
                    if (potentialFriendly != null)
                    {
                        var CScript = potentialFriendly.GetComponent<Character>();
                        if (CScript != null && CScript.IsAlive)
                        {
                            if (CScript != character)
                            {
                                rfc = CScript;
                            }
                            else if (allowSelf)
                            {
                                rfc = CScript;
                            }
                        }
                    }
                }
            }

            return rfc;
        }

        public Character GetRandomHostileCharacter(Character character)
        {
            Character rhc = null;

            if (character.Is_A_PC && EnemyManager.Instance.EnemiesRemaining() > 0)
            {
                while (rhc == null)
                {
                    var potentialHostile = EnemyManager.Instance.EnemiesInEncounter[Random.Range(0, EnemyManager.Instance.EnemiesInEncounter.Count)];
                    if (potentialHostile != null)
                    {
                        var CScript = potentialHostile.GetComponent<Character>();
                        if (CScript != null && CScript.IsAlive)
                        {
                            rhc = CScript;
                        }
                    }
                }
            }
            else if (!character.Is_A_PC && PCManager.Instance.GetNumOfLivingPCsInParty() > 0)
            {
                //Debug.Log(character.Stats_Config.CharName + " is looking to set hostile target");
                while (rhc == null)
                {

                    var potentialHostile = PCManager.Instance.PCsInParty[Random.Range(0, PCManager.Instance.PCsInParty.Length)];
                    if (potentialHostile != null)
                    {
                        var CScript = potentialHostile.GetComponent<Character>();
                        if (CScript != null && CScript.IsAlive)
                        {
                            rhc = CScript;
                        }
                    }
                }
            }
            //Debug.Log(character.Stats_Config.CharName + " has chosen target: " + rhc.Stats_Config.CharName);
            return rhc;
        }

        //called continually while mouse is over an enemy target
        void PC_SetHostileTarget(GameObject enemy)
        {
            if (TurnManager.Instance.CurrentStep == TurnStep.SelectHostileTarget)
            {
                var enemyTarget = enemy.GetComponent<Character>();
                TurnManager.Instance.PCReceivingCommands.GetComponent<Character>().SetHostileTarget(enemyTarget);
                Combat_UI_Manager.Instance.UpdateUI_SingleEnemyTarget((EnemyCharacter)enemyTarget);
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    TurnManager.Instance.MoveToNextPC(TurnManager.Instance.PCReceivingCommands);
                }
            }
        }

        //called continually while mouse is over a PC ; called once when mouse enters "NameAndStats" UI (see OnMouseOverNameAndStats.cs)
        public void PC_SetFriendlyTarget(GameObject PC)
        {
            if (TurnManager.Instance.CurrentStep == TurnStep.SelectFriendlyTarget)
            {
                var friendlyTarget = PC.GetComponentInParent<Character>();
                var activePC = TurnManager.Instance.PCReceivingCommands.GetComponent<Character>();

                activePC.SetFriendlyTarget(friendlyTarget);
                Combat_UI_Manager.Instance.UpdateUI_SingleFriendlyTarget((PlayerCharacter)friendlyTarget);
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    if (activePC.CommandsToQueue.Count > 0 && activePC.CommandsToQueue[0].TargetIsValid())
                    {
                        TurnManager.Instance.MoveToNextPC(TurnManager.Instance.PCReceivingCommands);
                    }
                    else
                    {
                        if (activePC.CommandsToQueue.Count > 0)
                        {
                            SoundManager.Instance.PlayBuzzer();
                            Combat_UI_Manager.Instance.SetMsg_TopOfScreen(friendlyTarget.Stats_Config.CharName + " is not a valid target for " + activePC.CommandsToQueue[0].CommandName, Color.white, 0, false);
                        }
                    }
                }
            }
        }

        void SelectNextAvailableTargetWithTab()
        {
            //if we're on the select hostile target step, we have a target, and we press Tab...
            if (Input.GetKeyDown(KeyCode.Tab) &&
                TurnManager.Instance.CurrentStep == TurnStep.SelectHostileTarget)
            {
                var PCScript = TurnManager.Instance.PCReceivingCommands.GetComponent<Character>();
                if (TurnManager.Instance.PCReceivingCommands == null || PCScript.HostileTarget == null)
                {
                    return;
                }

                //store a reference to our current target
                GameObject enemyTarget = PCScript.HostileTarget.gameObject;

                //loop through EnemiesInParty[]... 
                for (int i = 0; i < EnemyManager.Instance.EnemiesInEncounter.Count; i++)
                {
                    //find a match to our current target...
                    if (enemyTarget == EnemyManager.Instance.EnemiesInEncounter[i])
                    {
                        //we found a match... continue through the array to see if any slots with a higher index contain an enemy...
                        for (int x = i + 1; x < EnemyManager.Instance.EnemiesInEncounter.Count; x++)
                        {
                            if (EnemyManager.Instance.EnemiesInEncounter[x] != null)
                            {
                                //there was at least one enemy in EnemiesInParty[] with a higher index than our current target...
                                //we set the first one we come across as our new target
                                PC_SetHostileTarget(EnemyManager.Instance.EnemiesInEncounter[x]);
                                return;
                            }
                        }
                        //if we are here, and our target has changed, we're done
                        if (enemyTarget != PCScript.HostileTarget.gameObject)
                        {
                            return;
                        }
                        else
                        {
                            //there were no enemies in EnemiesInParty[] at a higher index.
                            //we need to loop back through the array to see if there are any other enemies at a lower index now
                            for (int x = 0; x < EnemyManager.Instance.EnemiesInEncounter.Count; x++)
                            {
                                //if the first enemy we come to is the same as our current target,
                                //we know now that our target is the ONLY enemy in EnemiesInParty[]
                                if (EnemyManager.Instance.EnemiesInEncounter[x] == enemyTarget)
                                {
                                    return;
                                }

                                if (EnemyManager.Instance.EnemiesInEncounter[x] != null)
                                {
                                    //if we reach here, it means there were no enemies in EnemiesInParty[] with a 
                                    //higher index than our current target, BUT
                                    //we found another enemy in EnemiesInParty[] at a lower index and can swap to that target
                                    PC_SetHostileTarget(EnemyManager.Instance.EnemiesInEncounter[x]);
                                    return;
                                }
                            }
                        }
                    }
                }
            }

            //If we're selecting a friendly target and press tab...
            if (Input.GetKeyDown(KeyCode.Tab) &&
                TurnManager.Instance.CurrentStep == TurnStep.SelectFriendlyTarget)
            {
                var PCScript = TurnManager.Instance.PCReceivingCommands.GetComponent<Character>();
                if (TurnManager.Instance.PCReceivingCommands == null || PCScript.FriendlyTarget == null)
                {
                    return;
                }

                //store a reference to our current target
                GameObject friendlyTarget = PCScript.FriendlyTarget.gameObject;

                //attempt to get a value for nextPC
                GameObject nextPC;
                //is target type == DeadFriendly?
                if (PCScript.CommandsToQueue.Count > 0 && PCScript.CommandsToQueue[0]._TargetType == TargetType.DeadFriendly)
                {
                    nextPC = PCManager.Instance.GetNextDeadPC(friendlyTarget);
                }
                //...Friendly_NotSelf?
                else if (PCScript.CommandsToQueue.Count > 0 && PCScript.CommandsToQueue[0]._TargetType == TargetType.Friendly_NotSelf)
                {
                    nextPC = PCManager.Instance.GetNextLivingPC_NotSelf(PCScript, friendlyTarget);
                }
                //...Friendly?
                else
                {
                    nextPC = PCManager.Instance.GetNextPC_DeadOrAlive(friendlyTarget);
                }

                //if one of the methods above found us a PC, set the friendly target
                if (nextPC != null)
                {
                    PC_SetFriendlyTarget(nextPC);
                }
            }
        }
    }
}
