using System.Collections;
using System.Collections.Generic;
using System.Linq;
using RPG.SceneControl;
using UnityEngine;
using UnityEngine.UI;

//Last Reviewed: 2/24/20
namespace RPG.CombatSystem
{
    public enum TurnStep { ChooseFightOrFlee, IssueCommands, SelectHostileTarget, SelectFriendlyTarget, SelectSubMenuCommand, SelectSpellOption, Combat }

    //This class is the heart of the combat system and performs the following tasks:
    // 1) manages and tracks the current and previous "TurnStep"
    // 2) handles player input (Escape Key / right-mouse-button)
    // 3) keeps track of which PC is currently receiving commands, and communicates with
    //    other managers, telling them to move that PC(MovementManager.cs), set up the UI for that PC(UIManager.cs), and to populate the buttons for that PC(ButtonCommandManager.cs).
    // 4) tracks the initiative score of all characters
    // 5) gathers all the Commands that need to be executed for the current turn and executes them in order of initiative score
    // 6) deals with status effects (calling for them to tick and removing them when they expire)
    public class TurnManager : Singleton<TurnManager>
    {
        enum BattleRoundResult { Victory, Defeat, Continue }

        public List<Text> intitNames; //text fields for names in the initiative list
        public List<BaseCommand> CommandQueue = new List<BaseCommand>(); //CommandQueue contains all the Commands (PC and Enemy) that should be executed during the next round of combat.


        List<GameObject> initiativeList = new List<GameObject>();
        bool waitToStartTurn = false;
        bool battleEnd = false;
        int roundNum = 1;

        public GameObject PCReceivingCommands { get; private set; }
        public Character ActingCharacter { get; private set; } //the character that is currently executing their queued Command
        public TurnStep PreviousStep { get; private set; }
        public TurnStep CurrentStep { get; private set; }
        public string sceneToReturnTo { get; private set; }
        public bool PlayerInputAllowed { get; private set; }

        public bool UseDynamicCamera { get; set; } = true; //currently controled by a toggle; toggle state gets saved; bool gets updated AfterSceneLoad

        //various classes set these bools, allowing ExecuteAllCommands to yield when certain
        //conditions are true
        public bool SpellEffectsProcing { get; set; }
        public bool DamageShieldsProcing { get; set; }
        public bool MeleeCounterAttackProcing { get; set; }
        public bool CounterDamageInProgress { get { return DamageShieldsProcing || MeleeCounterAttackProcing; } }


        void Start()
        {
            sceneToReturnTo = SceneController.Instance.nameOfMostRecentlyUnloadedScene;
            StartNewRoundOfCombat();
        }

        void Update()
        {
            //Escape Handling
            if (PlayerInputAllowed)
            {
                HandleEscKeyPressed();
            }
        }

        void OnEnable()
        {
            SceneController.Instance.AfterSceneLoad += AfterSceneLoad;
        }

        void OnDisable()
        {
            if (SceneController.InstanceExists)
            {
                SceneController.Instance.AfterSceneLoad -= AfterSceneLoad;
            }
        }

        //called after all SaveData for the scene has been restored
        void AfterSceneLoad()
        {
            UseDynamicCamera = GameObject.Find("DynamicCam_Toggle").GetComponent<Toggle>().isOn;
        }

        //Use this method to set the CurrentStep so that we can always keep track of the PreviousStep
        public void SetCurrentTurnStep(TurnStep newStep)
        {
            PreviousStep = CurrentStep;
            CurrentStep = newStep;
        }

        //Start the next PC's turn (i.e. allow that PC to choose their action for the next round of combat)
        public void MoveToNextPC(GameObject currentPC)
        {
            if (PlayerInputAllowed)
            {
                StartCoroutine(StartTurnForNextPC(currentPC));
            }
        }

        //When "Fight" is chosen:
        //1)Find the living PC in the lowest slot (Note: usually slot 0, but this covers cases like slots 0 - 2 are empty and the only PC is in slot 3)
        //2)Set PCReceivingCommands = that PC
        //3)Issue a Command to that PC
        public void FightButtonPressed()
        {
            PlayerInputAllowed = false;
            GameObject PC = PCManager.Instance.GetLivingPCWithLowestPC_Slot_position();
            PCReceivingCommands = PC;
            StartCoroutine(Start_PC_ChooseCommand(PC));
        }

        public void FleeButtonPressed()
        {
            foreach (PlayerCharacter PC in PCManager.Instance.GetLivingPCsInParty())
            {
                PC.ClearAllSpellEffects();
            }
            SceneController.Instance.FadeAndLoadScene(sceneToReturnTo);
        }

        //Starts the turn for the PC in the next, non-empty slot from currentPC
        //If currentPC is the PC in the HighestSlotWithAPC, all PC commands have been issued: moves TurnStep to Combat
        IEnumerator StartTurnForNextPC(GameObject currentPC)
        {
            GameObject nextPC = PCManager.Instance.GetNextLivingPC(currentPC);
            if (nextPC == null)
            {
                //print("Last living PC has received orders, time to do combat round");
                //reset UI
                PlayerInputAllowed = false;
                Combat_UI_Manager.Instance.SetUpUIForPCToChooseCommand(currentPC);
                Combat_UI_Manager.Instance.SetUpUIForNewCombatRoundStart();
                Combat_UI_Manager.Instance.HideMasterCommandsPanel();
                SetCurrentTurnStep(TurnStep.Combat);
                yield return MovementManager.Instance.MoveAllPCsToStartPositions();
                yield return ExecuteAllCommands();

                yield break;
            }

            yield return Start_PC_ChooseCommand(nextPC);
        }

        //Add/Remove a Command to/from CommandQueue
        void AddCommandToQueue(BaseCommand cmnd)
        {
            CommandQueue.Add(cmnd);
        }

        void RemoveCommandFromQueue(BaseCommand cmnd)
        {
            if (CommandQueue.Contains(cmnd))
            {
                CommandQueue.Remove(cmnd);
            }
        }

        //1) Moves all PCs back to thier startingPos
        //2) sets PCReceivingCommands to specified character
        //3) Moves specified character fwd
        //4) calls on UIManager to setup character's UI
        //5) calls on ButtonCommandManager to make the buttons work
        //6) sets CurrentActivePC_Slot to specified character's PC_Slot_position
        //7) sets the CurrentStep to Issue Commands
        IEnumerator Start_PC_ChooseCommand(GameObject PC_gameObject)
        {
            PlayerInputAllowed = false;
            SetCurrentTurnStep(TurnStep.IssueCommands);
            PCReceivingCommands = PC_gameObject;
            var PCScript = PCReceivingCommands.GetComponent<Character>();

            //Set up the UI: 
            //1) Outline the PC
            //2) Highlight the PC's PC_Slot
            //3) Hide the FightOrFlee panel / Show the CharacterSpecific panel with this PCs available commands
            Combat_UI_Manager.Instance.SetUpUIForPCToChooseCommand(PC_gameObject);

            //Add the proper listeners to the command buttons ...Note: SetUpUIForPCsToChooseCommand() needs to be called first
            ButtonCommandManager.Instance.PopulatePC_MainMenuCommand_Buttons(PC_gameObject);

            //Move all PCs to their start positions
            yield return MovementManager.Instance.MoveAllPCsToStartPositions();
            //Move this PC forward
            yield return MovementManager.Instance.ScoochObjectAlongFwdAxis(PC_gameObject, 2);

            PCScript.ClearAllTargets();
            PCScript.ClearCommandsToQueueList();
            PlayerInputAllowed = true;
        }

        //Deal with what happens when escape key is pressed during different phases
        void HandleEscKeyPressed()
        {
            StartCoroutine(EscKeyCoroutine());
        }

        IEnumerator EscKeyCoroutine()
        {
            if (Input.GetKeyDown(KeyCode.Escape) || Input.GetMouseButtonDown(1))
            {
                Character CScript = null;
                switch (CurrentStep)
                {
                    case TurnStep.IssueCommands:
                        if ((PCManager.Instance.GetPreviousLivingPC(PCReceivingCommands) == null))
                        {
                            PlayerInputAllowed = false;
                            yield return MovementManager.Instance.MoveAllPCsToStartPositions();
                            Combat_UI_Manager.Instance.SetUpUIForNewCombatRoundStart();
                            SetCurrentTurnStep(TurnStep.ChooseFightOrFlee);
                            PCReceivingCommands = null;
                            PlayerInputAllowed = true;
                        }
                        else
                        {
                            PlayerInputAllowed = false;
                            GameObject PrevPC = PCManager.Instance.GetPreviousLivingPC(PCReceivingCommands);
                            CScript = PrevPC.GetComponent<Character>();
                            yield return Start_PC_ChooseCommand(PrevPC);
                        }
                        yield break;
                    case TurnStep.SelectSubMenuCommand:
                        SetCurrentTurnStep(TurnStep.IssueCommands);
                        CScript = PCReceivingCommands.GetComponent<Character>();
                        CScript.ClearAllTargets();
                        CScript.ClearCommandsToQueueList();
                        Combat_UI_Manager.Instance.SetUpUIForPCToChooseCommand(PCReceivingCommands);
                        yield break;
                    case TurnStep.SelectHostileTarget:
                    case TurnStep.SelectFriendlyTarget:
                        GoBackFromTargetSelectToPreviousTurnStep();
                        yield break;
                    case TurnStep.SelectSpellOption:
                        PlayerInputAllowed = false;
                        SetCurrentTurnStep(TurnStep.IssueCommands);
                        SetCurrentTurnStep(TurnStep.SelectSubMenuCommand);
                        PlayerInputAllowed = true;
                        yield break;
                    default:
                        yield break;
                }
            }
        }

        void GoBackFromTargetSelectToPreviousTurnStep()
        {
            PlayerInputAllowed = false;
            Character CScript = null;
            SetCurrentTurnStep(PreviousStep);
            CScript = PCReceivingCommands.GetComponent<Character>();
            CScript.ClearAllTargets();
            CScript.ClearCommandsToQueueList();
            Combat_UI_Manager.Instance.Un_OutlineAllCharacters();
            Combat_UI_Manager.Instance.OutlineCharacter(PCReceivingCommands);
            Combat_UI_Manager.Instance.ResetTargetPanel();
            PlayerInputAllowed = true;
        }

        //1) Execute CommandInstructions for each Command in order until all Commands have been dealt with.
        //2) When the CommandQueue is empty, if !battleEnd, start a new round of combat.
        IEnumerator ExecuteAllCommands()
        {
            //determine which abilities enemies will be using this round
            Set_EnemyCommandsToQueue();
            //turn order has already been established... pull the CommandToQueue from each character in the (already sorted) initiativeList
            PopulateCommandQueue();
            Combat_UI_Manager.Instance.HideCombatInvPanel();
            Combat_UI_Manager.Instance.AnimateNameAndStatsPanel("SlideRight");
            //Add message to combat log:
            CombatLogManager.Instance.AddToCombatLog("ROUND " + roundNum.ToString() + ":", new Color32(255, 215, 0, 255));

            //pause half a second before executing commands
            yield return new WaitForSeconds(0.5f);

            //Execute commands from the CommandQueue until all have been executed
            while (CommandQueue.Count > 0)
            {
                while (waitToStartTurn)
                {
                    yield return null;
                }

                ActingCharacter = CommandQueue[0].gameObject.GetComponent<Character>();

                //First, handle any status effects on the character who is about to act...
                yield return ActingCharacter.TickSpellEffects();

                //Second, if ActingCharacter is still alive, execute the command at the top of the queue
                if (ActingCharacter != null && ActingCharacter.IsAlive)
                {
                    //change camera angle dynamically if desired, no need to pause while it happens
                    if (UseDynamicCamera)
                    {
                        StartCoroutine(DynamicCamera());
                    }

                    //execute the Command at the top of the queue
                    yield return CommandQueue[0].CommandInstructions();

                    //wait for procs to finish... (Note: often finish before end of CommandInstructions())
                    while (SpellEffectsProcing || CounterDamageInProgress)
                    {
                        yield return new WaitForEndOfFrame();
                        yield return new WaitForEndOfFrame(); //added second wait for frame on 12/19/20 to ensure procs are done
                    }
                }

                //short pause after command, procs, and damage shields have run their course...
                yield return new WaitForSeconds(0.1f);

                //Third, if ActingCharacter died from spell effects or a counter attack, pause before moving on to next character
                if (ActingCharacter != null && !ActingCharacter.IsAlive)
                {
                    //print(ActingCharacter + "died to a counter-attack or spell effect. Pausing for half second");
                    yield return new WaitForSeconds(0.5f);
                }

                //Fourth, remove CommandQueue[0] from the queue. Note: If ActingCharacter dies during its attack, then Destroyed_RemoveCommandFromQueue.cs handles this
                if ((CommandQueue.Count > 0 && ActingCharacter != null && ActingCharacter.IsAlive))
                {
                    RemoveCommandFromQueue(CommandQueue[0]);
                }

                //Fifth update the turnorder list
                RemoveFirstInstanceOfCharFromTurnOrderList(ActingCharacter.gameObject);

                //Sixth, remove effects whose turnsRemaining == 0
                if (ActingCharacter != null)
                {
                    ActingCharacter.RemoveExpiredEffects();
                }

                yield return new WaitForSeconds(0.1f);

                //Seventh, check to see if (allEnemiesAreDead || allPCsAreDead).
                if (CheckForBattleEnd() != BattleRoundResult.Continue)
                {
                    if (CheckForBattleEnd() == BattleRoundResult.Victory)
                    {
                        yield return new WaitForSeconds(1f);
                        PostBattleManager.Instance.ProccessPostBattle_Victory();
                    }
                    else if (CheckForBattleEnd() == BattleRoundResult.Defeat)
                    {
                        PostBattleManager.Instance.ProccessPostBattle_Defeat();
                    }
                }
                yield return new WaitForSeconds(0.1f);
            }

            //Start a new round if at least 1 PC and 1 Enemy are still alive
            if (!battleEnd)
            {
                roundNum++;
                StartNewRoundOfCombat();
            }
        }

        //swap to a different camera near the start of Command execution
        IEnumerator DynamicCamera()
        {
            //wait a beat after attack starts, then swap camera
            yield return new WaitForSeconds(Mathf.Max(0.25f, UnityEngine.Random.value - 0.25f));
            CameraManager.Instance.ActivateRandomCamera();
        }

        public void PauseTurnExecution(float seconds)
        {
            StartCoroutine(PauseExecution(seconds));
        }

        IEnumerator PauseExecution(float seconds)
        {
            waitToStartTurn = true;
            yield return new WaitForSeconds(seconds);
            waitToStartTurn = false;
        }

        void StartNewRoundOfCombat()
        {
            //reposition camera to cam[0]
            CameraManager.Instance.ActivateCameraAtIndex(0);
            //clear ActingCharacter
            ActingCharacter = null;
            //clear targets
            ClearAllCharactersTargets();
            //clear any commands still in CommandsToQueue
            ClearAllCharacters_CommandToQueue();
            //determine what order characters will act in
            DetermineInitiativeOrder();
            //set the turn step to Fight/Flee option
            SetCurrentTurnStep(TurnStep.ChooseFightOrFlee);
            //show the MasterCommandPanel
            Combat_UI_Manager.Instance.ShowMasterCommandPanel();

            if (roundNum != 1)
            {
                Combat_UI_Manager.Instance.AnimateNameAndStatsPanel("SlideLeft");
            }
            PlayerInputAllowed = true;
        }

        //determines what Command(s) each enemy will execute in the next round of combat
        void Set_EnemyCommandsToQueue()
        {
            foreach (GameObject enemy in EnemyManager.Instance.EnemiesInEncounter)
            {
                if (enemy != null)
                {
                    //if enemy has a brain, use that to determine which Command(s) to queue
                    var enemyBrain = enemy.GetComponent<EnemyBrain>();
                    if (enemyBrain != null)
                    {
                        int actionsPerTurn = enemy.GetComponent<EnemyCharacter>().numOfActionsPerTurn;
                        for (int i = actionsPerTurn; i > 0; i--)
                        {
                            enemy.GetComponent<Character>().AddCommandForQueuing(enemyBrain.DetermineCommandToUse());
                        }
                    }
                    //no brain? no prob ... choose random Command(s) attached to the enemy
                    else
                    {
                        List<BaseCommand> availCommands = (enemy.GetComponents<BaseCommand>()).ToList();

                        //don't select commands that are inactive or that the enemy can't afford
                        availCommands.RemoveAll(cmnd => !cmnd.isActiveAndEnabled || !cmnd.CanAffordCosts);

                        int actionsPerTurn = enemy.GetComponent<EnemyCharacter>().numOfActionsPerTurn;
                        if (availCommands.Count < 1)
                        {
                            Debug.LogWarning(enemy.GetComponent<EnemyCharacter>().EnemyName + "had no avialable commands.");
                            continue;
                        }
                        for (int i = actionsPerTurn; i > 0; i--)
                        {
                            enemy.GetComponent<Character>().AddCommandForQueuing(availCommands[UnityEngine.Random.Range(0, availCommands.Count)]);
                        }
                    }
                }
            }
        }

        BattleRoundResult CheckForBattleEnd()
        {
            if (PCManager.Instance.GetNumOfLivingPCsInParty() <= 0)
            {
                battleEnd = true;
                CommandQueue.Clear();

                return BattleRoundResult.Defeat;
            }

            if (EnemyManager.Instance.EnemiesRemaining() <= 0)
            {
                battleEnd = true;
                CommandQueue.Clear();
                foreach (PlayerCharacter PC in PCManager.Instance.GetLivingPCsInParty())
                {
                    PC.ClearAllSpellEffects();
                }

                return BattleRoundResult.Victory;
            }

            return BattleRoundResult.Continue;
        }

        void ClearAllCharactersTargets()
        {
            foreach (GameObject PC in PCManager.Instance.PCsInParty)
            {
                if (PC != null)
                {
                    PC.GetComponent<Character>().ClearAllTargets();
                }
            }
            foreach (GameObject enemy in EnemyManager.Instance.EnemiesInEncounter)
            {
                if (enemy != null)
                {
                    enemy.GetComponent<Character>().ClearAllTargets();
                }
            }
        }

        void ClearAllCharacters_CommandToQueue()
        {
            foreach (GameObject PC in PCManager.Instance.PCsInParty)
            {
                if (PC != null)
                {
                    PC.GetComponent<Character>().ClearCommandsToQueueList();
                }
            }
            foreach (GameObject enemy in EnemyManager.Instance.EnemiesInEncounter)
            {
                if (enemy != null)
                {
                    enemy.GetComponent<Character>().ClearCommandsToQueueList();
                }
            }
        }

        void DetermineInitiativeOrder()
        {
            initiativeList.Clear();

            foreach (GameObject PC in PCManager.Instance.PCsInParty)
            {
                if (PC != null)
                {
                    Character CScript = PC.GetComponent<Character>();
                    if (CScript != null && CScript.IsAlive)
                    {
                        CScript.RollForInitiative();
                        initiativeList.Add(PC);
                        //print(PC + " Init: " + PC.GetComponent<Character>().InitiativeTotal);
                    }
                }
            }

            foreach (GameObject Enemy in EnemyManager.Instance.EnemiesInEncounter)
            {
                if (Enemy != null)
                {
                    Character CScript = Enemy.GetComponent<Character>();
                    if (CScript != null && CScript.IsAlive)
                    {
                        CScript.RollForInitiative();
                        initiativeList.Add(Enemy);
                        //print(Enemy + " Init: " + Enemy.GetComponent<Character>().InitiativeTotal);
                    }
                }
            }

            initiativeList.RemoveAll(n => n == null);
            initiativeList.Sort((b, a) => (a.gameObject.GetComponent<Character>().InitiativeTotal.CompareTo(b.gameObject.GetComponent<Character>().InitiativeTotal)));

            //check for enemies with multiple actions per turn... Add a duplicate entry into the initiative list for each additional action
            foreach (GameObject Enemy in EnemyManager.Instance.EnemiesInEncounter)
            {
                if (Enemy == null)
                {
                    continue;
                }

                EnemyCharacter EScript = Enemy.GetComponent<EnemyCharacter>();
                if (EScript.numOfActionsPerTurn <= 1 || !EScript.IsAlive)
                {
                    continue;
                }

                int numOfActionsToAdd = EScript.numOfActionsPerTurn - 1;
                for (int i = numOfActionsToAdd; i > 0; i--)
                {
                    EScript.RollForInitiative();
                    //if roll is higher than worst roll, insert in the right spot
                    bool inserted = false;
                    for (int j = 0; j < initiativeList.Count; j++)
                    {
                        if (EScript.gameObject != initiativeList[j] && EScript.InitiativeTotal >= initiativeList[j].GetComponent<Character>().InitiativeTotal)
                        {
                            initiativeList.Insert(j, EScript.gameObject);
                            inserted = true;
                            break;
                        }
                    }
                    //... this was the worst init roll, just add to the end of List
                    if (!inserted)
                    {
                        initiativeList.Add(EScript.gameObject);
                    }
                }
            }
            //////////////////////////////temp way to see initiative order////////////////////////////////////
            DisplayInitList(initiativeList);
            //////////////////////////////////////////////////////////////////////////////////////////////////
        }

        void PopulateCommandQueue()
        {
            foreach (GameObject character in initiativeList)
            {
                if (character != null)
                {
                    Character CScript = character.GetComponent<Character>();
                    if (CScript != null)
                    {
                        if (CScript.CommandsToQueue.Count > 0)
                        {
                            //Note: enemies with multiple actions per turn are added into the initiativeList multiple times
                            AddCommandToQueue(CScript.CommandsToQueue[0]);
                            CScript.CommandsToQueue.RemoveAt(0);
                        }
                    }
                }
            }
            //remove any null entries from CommandQueue ... shouldn't be any
            CommandQueue.RemoveAll(n => n == null);
        }

        //Removes ALL instances of the char from the list. important for characters with more
        //than one attack per turn die.
        public void RemoveFromTurnOrderList(GameObject character)
        {
            for (int i = initiativeList.Count - 1; i >= 0; i--)
            {
                if (initiativeList[i] == character)
                {
                    initiativeList.Remove(character);
                }
            }

            DisplayInitList(initiativeList);
        }

        //character might be in the initiativeList more than once if it has multiple attacks per turn.
        //this method removes only the first instance.
        void RemoveFirstInstanceOfCharFromTurnOrderList(GameObject character)
        {
            if (initiativeList.Contains(character))
            {
                initiativeList.Remove(character);
            }

            DisplayInitList(initiativeList);
        }

        public void DisplayInitList(List<GameObject> characters)
        {
            foreach (Text txt in intitNames)
            {
                txt.text = "";
            }

            for (int i = 0; i < characters.Count; i++)
            {
                if (characters[i] == null) continue;
                if (i == 0)
                {
                    intitNames[i].color = Color.red;
                    intitNames[i].text = "[" + characters[i].GetComponentInParent<Character>().Stats_Config.CharName + "]";
                }
                else
                {
                    intitNames[i].color = Color.white;
                    intitNames[i].text = characters[i].GetComponentInParent<Character>().Stats_Config.CharName;
                }
            }
        }
    }
}
