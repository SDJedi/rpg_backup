using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Last Reviewed: 2/23/20
namespace RPG.CombatSystem
{
    //works with the RandomEncounterManager to select an encounter and instantiate enemies
    //into the combat scene
    public class EnemyManager : Singleton<EnemyManager>
    {
        public List<GameObject> EnemiesInEncounter = new List<GameObject>();

        GameObject Enemies;

        public EnemyEncounter ChosenEncounter { get; private set; }


        protected override void Awake()
        {
            base.Awake();
            //create a parent object for the enemy instances...
            Enemies = new GameObject("Enemies");
            //make sure this parent object ("Enemies") is in the CombatScene...
            SceneManager.MoveGameObjectToScene(Enemies, gameObject.scene);
            RandomEncounterManager.Instance.possibleEncounters.RemoveAll(n => n == null);
            //select an encounter
            ChosenEncounter = SelectEncounterFromWeightedList();
            //populate the EnemiesInEncounter List using chosen encounter...
            EnemiesInEncounter = ChosenEncounter.InstantiateEnemyEncounter();
            //make each newly instantiated enemy a child of the "Enemies" gameObject...
            foreach (GameObject enemy in EnemiesInEncounter)
            {
                enemy.transform.parent = Enemies.transform;
            }
        }

        void Start()
        {
            if (!ChosenEncounter.CanFlee)
            {
                Combat_UI_Manager.Instance.DisableFleeButton();
            }
        }

        public List<GameObject> AddEnemiesDuringCombat(EnemyEncounter enemiesToAdd)
        {
            if (enemiesToAdd == null)
            {
                Debug.LogWarning("EnemyEncounter was null");
                return null;
            }

            var newEnemies = enemiesToAdd.InstantiateEnemyEncounter();
            foreach (GameObject enemy in newEnemies)
            {
                EnemiesInEncounter.Add(enemy);
                enemy.transform.parent = Enemies.transform;
            }
            return newEnemies;
        }

        //returns the number of enemies in the encounter that are
        //still alive
        public int EnemiesRemaining()
        {
            int livingEnemies = 0;
            foreach (GameObject enemy in EnemiesInEncounter)
            {
                if (enemy != null && enemy.GetComponent<Character>().IsAlive)
                {
                    livingEnemies++;
                }
            }
            return livingEnemies;
        }

        EnemyEncounter SelectEncounterFromWeightedList()
        {
            int encounterSpawnChance = 0;
            int totalSpawnWeight = 0;
            int roll = 0;

            foreach (EncounterAndSpawnChance e in RandomEncounterManager.Instance.possibleEncounters)
            {
                totalSpawnWeight += e.SpawnWeightChance;
            }

            roll = UnityEngine.Random.Range(1, totalSpawnWeight + 1);

            foreach (EncounterAndSpawnChance e in RandomEncounterManager.Instance.possibleEncounters)
            {
                encounterSpawnChance += e.SpawnWeightChance;
                if (roll <= encounterSpawnChance)
                {
                    return e.Encounter;
                }
            }
            return null;
        }
    }
}