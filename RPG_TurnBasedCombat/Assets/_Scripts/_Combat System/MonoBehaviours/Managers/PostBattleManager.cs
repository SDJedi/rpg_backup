using System;
using System.Collections;
using System.Collections.Generic;
using RPG.Inventory;
using RPG.SceneControl;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

//Last Reviewed: 2/24/20
namespace RPG.CombatSystem
{
    //after a successful battle, grants the player xp, gp, items, etc.
    //after a failed battle, calls "Game Over, Man!"
    public class PostBattleManager : Singleton<PostBattleManager>
    {
        //helper calss to hold references to the UI fields
        //in the XP_Panel
        [System.Serializable]
        public class PC_XP_UIInfo
        {
            public TextMeshProUGUI PC_Name_Txt;
            public TextMeshProUGUI CurLvl_Txt;
            public TextMeshProUGUI GainedXPAmnt_Text;
            public TextMeshProUGUI XPToNextLvl_Txt;
            public TextMeshProUGUI LvlUp_Lbl;
            public Image XPBar_Fill;
            public RawImage Portrait;
            public Image DeadIcon;
        }

        //called whenever an encounter is defeated
        public event Action<EnemyEncounter> EncounterBeaten;

        //misc items we need to manipulate
        public GameObject CombatScene_Canvas;
        public GameObject CombatLog;
        public Button XP_OKBtn;

        //xp panel
        public GameObject Xp_Panel;
        public GameObject[] XP_PCSlots = new GameObject[4];
        public PC_XP_UIInfo[] XP_PCSlotInfo = new PC_XP_UIInfo[4];

        //reward panel
        public GameObject Rewards_Panel;
        public TextMeshProUGUI GP_Txt;
        public List<Item> ItemsEarned;
        public List<TextMeshProUGUI> ItemNameSlots;

        //flags to control flow of XP gain
        bool skipShowXPGain;
        bool fillingXPBars;

        //total XP divided by all living PCs
        int xpPerChar;

        //total xp / gp from all defeated enemies this combat
        public int TotalGPEarned { get; set; } = 0;
        public int TotalXPEarned { get; set; } = 0;

        public string sceneToReturnTo { get; private set; }


        void Start()
        {
            sceneToReturnTo = SceneController.Instance.nameOfMostRecentlyUnloadedScene;
            //if we don't want the player to return to the scene they were in before they entered combat...
            ChangeSceneToReturnTo changeScene = GameObject.FindObjectOfType<ChangeSceneToReturnTo>();
            if (changeScene != null)
            {
                sceneToReturnTo = changeScene.returnScene;
                SceneController.Instance.NameOfMostRecentSpawnPoint = changeScene.spawnPoint;
            }
        }

        //called by TurnManager.cs at the end of winning battle
        public void ProccessPostBattle_Victory()
        {
            if (EncounterBeaten != null)
            {
                EncounterBeaten(EnemyManager.Instance.ChosenEncounter);
            }
            CombatLog.transform.SetParent(transform, true);
            CombatScene_Canvas.SetActive(false);
            SoundManager.Instance.StartMusic("BattleVictoryMusic");
            XPCalculator.AwardCharPoints(TotalXPEarned);
            AwardBattleXP();
        }

        //called by TurnManager.cs at the end of losing battle
        public void ProccessPostBattle_Defeat()
        {
            Combat_UI_Manager.Instance.SetMsg_TopOfScreen("GAME OVER!", Color.white);
            SceneController.Instance.GameOver_LoadMainMenu(5);
        }

        //called by "Done" button press
        public void LeaveCombatScene()
        {
            SceneController.Instance.FadeAndLoadScene(sceneToReturnTo);
        }

        void AwardBattleXP()
        {
            ShowXPScreen();
            //divide the xp amongst the living characters
            xpPerChar = TotalXPEarned / PCManager.Instance.GetNumOfLivingPCsInParty();

            //loop through PCsInParty
            for (int i = 0; i < PCManager.Instance.PCsInParty.Length; i++)
            {
                if (PCManager.Instance.PCsInParty[i] != null)
                {
                    //grab references to the character script, the stats config, and the PC_Slot_position...
                    PlayerCharacter PCScript = PCManager.Instance.PCsInParty[i].GetComponent<PlayerCharacter>();
                    PC_Stats_Config PCStats = (PC_Stats_Config)PCScript.Stats_Config;
                    int slot = PCStats.PC_Slot_position;

                    //set up the UI for this PC...
                    XP_PCSlots[slot].SetActive(true);
                    XP_PCSlotInfo[slot].LvlUp_Lbl.enabled = false;
                    XP_PCSlotInfo[slot].PC_Name_Txt.text = PCStats.CharName;
                    XP_PCSlotInfo[slot].Portrait.texture = PCStats.PC_Portrait;
                    if (!PCStats.IsAlive())
                    {
                        XP_PCSlotInfo[slot].DeadIcon.enabled = true;
                    }
                    //Note: presentation of the following fields changes at MaxLevel...
                    XP_PCSlotInfo[slot].CurLvl_Txt.text = PCStats.CurLvl == XPCalculator.MaxLevel ? "Lvl: " + PCStats.CurLvl.ToString() + " (MAX)" : "Lvl: " + PCStats.CurLvl.ToString();
                    XP_PCSlotInfo[slot].XPToNextLvl_Txt.text = PCStats.CurLvl == XPCalculator.MaxLevel ? "" : (XPCalculator.XPCHart[PCStats.CurLvl] - PCStats.CurXP).ToString();
                    XP_PCSlotInfo[slot].XPBar_Fill.fillAmount = PCStats.CurLvl == XPCalculator.MaxLevel ? 1 : (float)((float)(PCStats.CurXP - XPCalculator.XPCHart[PCStats.CurLvl - 1]) / (float)(XPCalculator.XPCHart[PCStats.CurLvl] - XPCalculator.XPCHart[PCStats.CurLvl - 1]));

                    //only award xp if the PC is alive...
                    if (PCScript.IsAlive)
                    {
                        XP_PCSlotInfo[slot].GainedXPAmnt_Text.text = "+" + xpPerChar.ToString();
                        StartCoroutine(GrantXP(PCStats, slot, (PCStats.CurXP + xpPerChar)));
                    }
                    else
                    {
                        //dead PCs get 0 XP
                        XP_PCSlotInfo[slot].GainedXPAmnt_Text.text = "0";
                    }
                }
            }
        }

        //this coroutine slowly fills the bar in the UI until all XP has been awarded
        //If the PC reaches a new lvl, IncreasePCLvl() is called
        IEnumerator GrantXP(PC_Stats_Config stats, int slotNum, int finalXP)
        {
            //save a reference to current level to check if we leveled up this combat
            int initialLvl = stats.CurLvl;
            int xpTillLvl;

            //add xp each frame until: all xp has been awarded OR "OK" button is presses OR we reach MaxLevel
            while (stats.CurXP < finalXP && !skipShowXPGain && stats.CurLvl != XPCalculator.MaxLevel)
            {
                fillingXPBars = true;

                //the amount of xp added per frame varries based on how much total xp we are adding
                //it will never be less than 1xp per frame added 
                int amntToAddThisFrame = Mathf.CeilToInt(xpPerChar / 250) < 3 ? 3 : Mathf.CeilToInt(xpPerChar / 250);

                // don't allow total to go beyond finalXP
                if (stats.CurXP + amntToAddThisFrame <= finalXP)
                {
                    stats.ModifyCurrentXP(amntToAddThisFrame);
                }
                else
                {
                    stats.SetCurXPValue(finalXP);
                }

                //check to see if we leveld up
                xpTillLvl = XPCalculator.XPCHart[stats.CurLvl] - stats.CurXP;
                if (xpTillLvl <= 0)
                {
                    stats.IncreasePCLvl();
                }

                //update the UI
                UpdateXP_UI(stats, slotNum, initialLvl);

                yield return new WaitForEndOfFrame();
            }

            //we're done adding XP
            fillingXPBars = false;

            //set CurXP to the finalXP value we calculated at the beginning;
            //important for the case where we exit the XP-gain loop early by pressing "OK" button
            stats.SetCurXPValue(finalXP);

            //make sure our final lvl is correct
            int lvlAfterAllXPAwarded = XPCalculator.GetLvlBasedOnCurrentXP(stats.CurXP);

            //in case we need to add more than one level...
            while (stats.CurLvl < lvlAfterAllXPAwarded)
            {
                stats.IncreasePCLvl();
            }

            //update the UI
            UpdateXP_UI(stats, slotNum, initialLvl);
        }

        void UpdateXP_UI(PC_Stats_Config stats, int slotNum, int initialLvl)
        {
            int xpTillLvl;

            //change "OK" button color based on bars filling or not
            if (!fillingXPBars)
            {
                XP_OKBtn.GetComponent<Image>().color = Color.green;
            }
            else
            {
                XP_OKBtn.GetComponent<Image>().color = Color.white;
            }

            //if the PC leveled up this battle, show indication
            if (stats.CurLvl > initialLvl)
            {
                XP_PCSlotInfo[slotNum].LvlUp_Lbl.enabled = true;
            }

            //if PC is at max level...
            if (stats.CurLvl == XPCalculator.MaxLevel)
            {
                xpTillLvl = 0;
                XP_PCSlotInfo[slotNum].CurLvl_Txt.text = "Lvl: " + stats.CurLvl.ToString() + " (MAX)";
            }
            //PC is NOT at max level...
            else
            {
                xpTillLvl = XPCalculator.XPCHart[stats.CurLvl] - stats.CurXP;
                XP_PCSlotInfo[slotNum].CurLvl_Txt.text = "Lvl: " + stats.CurLvl.ToString();
            }

            //update xp bar and "til next level" text
            XP_PCSlotInfo[slotNum].XPBar_Fill.fillAmount = stats.CurLvl == XPCalculator.MaxLevel ? 1 : (float)((float)(stats.CurXP - XPCalculator.XPCHart[stats.CurLvl - 1]) / (float)(XPCalculator.XPCHart[stats.CurLvl] - XPCalculator.XPCHart[stats.CurLvl - 1]));
            XP_PCSlotInfo[slotNum].XPToNextLvl_Txt.text = xpTillLvl.ToString();
        }

        //called by "OK" btn press...
        public void AwardItemsAndGP()
        {
            if (fillingXPBars)
            {
                skipShowXPGain = true;
            }
            else
            {
                HideXPScreen();
                ShowRewardScreen();
                GP_Txt.text = TotalGPEarned.ToString();
                InventoryManager.Instance.ModifyGPAmnt(TotalGPEarned);

                for (int i = 0; i < ItemNameSlots.Count; i++)
                {
                    if (i < ItemsEarned.Count && ItemsEarned[i] != null)
                    {
                        ItemNameSlots[i].text = ItemsEarned[i].ItemName;
                        ItemNameSlots[i].color = ItemsEarned[i].GetItemTextColor();
                        ItemNameSlots[i].GetComponentInChildren<Image>().sprite = ItemsEarned[i].Icon;
                        InventoryManager.Instance.AddItemToInventory(Instantiate(ItemsEarned[i]));
                        continue;
                    }
                    ItemNameSlots[i].text = "";
                    ItemNameSlots[i].GetComponentInChildren<Image>().gameObject.SetActive(false);
                }
            }
        }

        void ShowXPScreen()
        {
            Xp_Panel.SetActive(true);
        }

        void HideXPScreen()
        {
            Xp_Panel.SetActive(false);
        }

        void ShowRewardScreen()
        {
            Rewards_Panel.SetActive(true);
        }

        public void OverrideSceneToReturnTo(string nameOfScene)
        {
            sceneToReturnTo = nameOfScene;
        }
    }
}
