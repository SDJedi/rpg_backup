using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

//Last Reviewed: 2/24/20
namespace RPG.CombatSystem
{
    //attached to the PCManager in the BootScene.
    //handles processing of spells/procs ... SaveData_PC_Procs.cs stores procs
    public class SpellAndProcManager : Singleton<SpellAndProcManager>
    {
        public static readonly float minTimeBetweenProcs = 0.5f; //1.5f; may need to change back

        WaitForSeconds shortPause = new WaitForSeconds(0.25f);
        int numOfSpellEffectsProcessing = 0;
        float failSafeTimer;


        void Update()
        {
            if (SceneManager.GetActiveScene().name != "CombatScene")
            {
                return;
            }

            //whenever 1 or more procs are running, let the TurnManager know
            if (numOfSpellEffectsProcessing > 0)
            {
                TurnManager.Instance.SpellEffectsProcing = true;
                failSafeTimer += Time.deltaTime;
            }
            else
            {
                TurnManager.Instance.SpellEffectsProcing = false;
                failSafeTimer = 0;
            }

            //should never need this in theory...
            if (failSafeTimer > 30)
            {
                Debug.LogWarning("Had procs running for over 30 seconds, setting to 0, This is an error that needs fixed!");
                numOfSpellEffectsProcessing = 0;
            }
        }

        void OnEnable()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        void OnDisable()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            //Just in case we ever leave the combat scene when the numOfRunningProcs is > 0,
            //we reset as soon as we enter the combat scene
            if (scene.name == "CombatScene")
            {
                numOfSpellEffectsProcessing = 0;
            }
        }

        //basically gets called whenever an attack/spell lands
        public void FireProcs(Character CScript)
        {
            if (!CScript.IsAlive) return;

            //Note: PC ProcContainers are generated at the start of the game,
            //persist between scenes, and get saved in the save file (see PC_ProcManager.cs)
            if (CScript.GetType() == typeof(PlayerCharacter))
            {
                StartCoroutine(ExecuteProcs(CScript, PC_ProcManager.Instance.GetPCProcContainerInstance(((PlayerCharacter)CScript).CharName)));
            }
            //Enemy ProcContainers are generated by EnemyCharacter.cs in Awake(), no need to track or save once
            //combat ends.
            if (CScript.GetType() == typeof(EnemyCharacter))
            {
                StartCoroutine(ExecuteProcs(CScript, ((EnemyCharacter)CScript).procContainer));
            }
        }

        //for each proc in the character's ProcContainer, roll to see if it
        //triggers, then process the effect
        IEnumerator ExecuteProcs(Character CScript, ProcContainer pCont)
        {
            if (pCont == null)
            {
                print("PrcoContainer for " + CScript.Stats_Config.CharName + " was null");
                yield break;
            }

            if (pCont.ActiveProcs.Count > 0)
            {
                yield return shortPause;
                for (int i = 0; i < pCont.ActiveProcs.Count; i++)
                {
                    float rollForProc = UnityEngine.Random.value;

                    if (rollForProc <= pCont.ActiveProcs[i].procChance)
                    {
                        //Note: we never want procs to trigger procs as this can cause an endless loop
                        ProcessSpellConfig(CScript, pCont.ActiveProcs[i].spell_Config, false);
                        yield return shortPause;
                    }
                    else
                    {
                        ////////for debugging ////////
                        // GenerateSpecialScrollingText.GenerateScrollingText(CScript.transform, "Proc: " + pCont.ActiveProcs[i].nameOfProc + " failed. (" + rollForProc.ToString("n2") + ")", Color.white);
                        // yield return shortPause;
                    }
                }
            }
        }

        //general purpose way to process any BaseSpell_Config
        public void ProcessSpellConfig(Character caster, BaseSpell_Config spellConfig, bool triggerProcs = true)
        {
            numOfSpellEffectsProcessing++;
            switch (spellConfig._TargetType)
            {
                case TargetType.NONE:
                    Debug.LogWarning("spellConfig for " + spellConfig.CmndName + " has a target type of NONE");
                    numOfSpellEffectsProcessing--;
                    break;
                case TargetType.Friendly:
                case TargetType.Friendly_NotSelf:
                    StartCoroutine(EffectFriendlyTarget(caster, spellConfig));
                    break;
                case TargetType.Hostile:
                    StartCoroutine(EffectHostileTarget(caster, spellConfig));
                    break;
                case TargetType.Self:
                    StartCoroutine(EffectSelf(caster, spellConfig));
                    break;
                case TargetType.All:
                    StartCoroutine(EffectAll(caster, spellConfig, triggerProcs));
                    break;
                case TargetType.AllHostile:
                    StartCoroutine(EffectAllHostile(caster, spellConfig, triggerProcs));
                    break;
                case TargetType.AllFriendly:
                    StartCoroutine(EffectAllFriendly(caster, spellConfig, triggerProcs));
                    break;
                default:
                    break;
            }
        }

        //some commands can trigger more than one effect per cast
        public IEnumerator ProcAdditionalEffects(Character caster, BaseCommand command)
        {
            if (caster == null || command == null)
            {
                Debug.LogWarning("Error: Null value when trying to Proc additional effects. Caster: " + caster + " | Command: " + command);
                yield break;
            }

            foreach (Proc p in command.AdditionalOnHitEffects)
            {
                if (p != null && Random.value <= p.procChance)
                {
                    yield return new WaitForSeconds(0.25f);
                    //Note: additional effects of a Command should never trigger procs
                    ProcessSpellConfig(caster, p.spell_Config, false);
                }
            }
        }

        #region Proc Methods
        IEnumerator EffectFriendlyTarget(Character CScript, BaseSpell_Config spellConfig)
        {
            //check for null friendly target and attempt to set if possible
            if (spellConfig._TargetType == TargetType.Friendly)
            {
                //can take any friendly, so just set to self
                if (CScript.FriendlyTarget == null)
                {
                    CScript.SetFriendlyTarget(CScript);
                }
                //can't set to self ... try to set another PC as friendly target
                else if (spellConfig._TargetType == TargetType.Friendly_NotSelf)
                {
                    if (CScript.FriendlyTarget == null || CScript.FriendlyTarget == CScript)
                    {
                        CScript.SetFriendlyTarget(PCManager.Instance.GetNextLivingPC_NotSelf(CScript, CScript.gameObject).GetComponent<Character>());
                    }
                }
            }

            //if friendly target is still null, we're out of luck ... may have to do some debugging
            if (CScript.FriendlyTarget == null)
            {
                Debug.LogWarning(CScript.Stats_Config.CharName + " has no friendly target for " + spellConfig.CmndName);
                numOfSpellEffectsProcessing--;
                yield break;
            }

            if (spellConfig.ParticleEffect != null)
            {
                var e = Instantiate(spellConfig.ParticleEffect, CScript.FriendlyTarget.transform.position, spellConfig.ParticleEffect.transform.rotation, CScript.FriendlyTarget.transform);
                ScaleObjectWithModel.MatchScale(CScript.FriendlyTarget.gameObject, e);
                yield return new WaitForSeconds((spellConfig).DelayAfterParticleEffect);
            }

            if (spellConfig.OnHitSoundEffect != null)
            {
                SoundManager.Instance.PlayAudioClip(spellConfig.OnHitSoundEffect);
            }

            CScript.FriendlyTarget.ApplySpellEffect(CScript, Instantiate(spellConfig));
            numOfSpellEffectsProcessing--;
        }

        IEnumerator EffectHostileTarget(Character CScript, BaseSpell_Config spellConfig)
        {
            if (CScript.HostileTarget == null || !CScript.Stats_Config.IsAlive())
            {
                Debug.Log("no hostile target for effect || hostile target is dead");
                numOfSpellEffectsProcessing--;
                yield break;
            }

            if (spellConfig.ParticleEffect != null)
            {
                var e = Instantiate(spellConfig.ParticleEffect, CScript.HostileTarget.transform.position, spellConfig.ParticleEffect.transform.rotation, CScript.HostileTarget.transform);
                ScaleObjectWithModel.MatchScale(CScript.HostileTarget.gameObject, e);
                yield return new WaitForSeconds((spellConfig).DelayAfterParticleEffect);
            }

            if (spellConfig.OnHitSoundEffect != null)
            {
                SoundManager.Instance.PlayAudioClip(spellConfig.OnHitSoundEffect);
            }

            CScript.HostileTarget.ApplySpellEffect(CScript, Instantiate(spellConfig));
            numOfSpellEffectsProcessing--;
        }

        IEnumerator EffectSelf(Character CScript, BaseSpell_Config spellConfig)
        {
            if (spellConfig.ParticleEffect != null)
            {
                var e = Instantiate(spellConfig.ParticleEffect, CScript.transform.position, spellConfig.ParticleEffect.transform.rotation, transform);
                ScaleObjectWithModel.MatchScale(gameObject, e);
                yield return new WaitForSeconds((spellConfig).DelayAfterParticleEffect);
            }

            if (spellConfig.OnHitSoundEffect != null)
            {
                SoundManager.Instance.PlayAudioClip(spellConfig.OnHitSoundEffect);
            }

            CScript.ApplySpellEffect(CScript, Instantiate(spellConfig));
            numOfSpellEffectsProcessing--;
        }

        IEnumerator EffectAll(Character CScript, BaseSpell_Config spellConfig, bool triggerProcs = true)
        {
            //check if particle effect is blanket or hits each char
            bool isBlanketParticleEffect = spellConfig.GetType() == typeof(Spell_BlanketParticleEffect_Config);

            //case: blanket particle effect; cast by enemy
            if (isBlanketParticleEffect && spellConfig.ParticleEffect != null && !CScript.Is_A_PC)
            {
                Instantiate(spellConfig.ParticleEffect, ((Spell_BlanketParticleEffect_Config)spellConfig).ParticleEffectLocation_CastByEnemy, spellConfig.ParticleEffect.transform.rotation);
                yield return new WaitForSeconds((spellConfig).DelayAfterParticleEffect);
            }

            //case: blanket particle effect; cast by PC
            if (isBlanketParticleEffect && spellConfig.ParticleEffect != null && CScript.Is_A_PC)
            {
                Instantiate(spellConfig.ParticleEffect, ((Spell_BlanketParticleEffect_Config)spellConfig).ParticleEffectLocation_CastByPC, spellConfig.ParticleEffect.transform.rotation);
                yield return new WaitForSeconds((spellConfig).DelayAfterParticleEffect);
            }

            if (spellConfig.OnHitSoundEffect != null)
            {
                SoundManager.Instance.PlayAudioClip(spellConfig.OnHitSoundEffect);
            }

            //apply the effect to each PC
            foreach (GameObject PC in PCManager.Instance.PCsInParty)
            {
                if (PC == null)
                {
                    continue;
                }

                ApplyAoEEffectToIndividualCharacter(CScript, spellConfig, triggerProcs, isBlanketParticleEffect, PC);
            }

            //apply the effect to each enemy
            for (int i = EnemyManager.Instance.EnemiesInEncounter.Count - 1; i >= 0; i--)
            {
                if (EnemyManager.Instance.EnemiesInEncounter[i] == null)
                {
                    continue;
                }

                ApplyAoEEffectToIndividualCharacter(CScript, spellConfig, triggerProcs, isBlanketParticleEffect, EnemyManager.Instance.EnemiesInEncounter[i]);
            }

            numOfSpellEffectsProcessing--;
        }

        IEnumerator EffectAllHostile(Character CScript, BaseSpell_Config spellConfig, bool triggerProcs = true)
        {
            //check if particle effect is blanket or hits each hostile char
            bool isBlanketParticleEffect = spellConfig.GetType() == typeof(Spell_BlanketParticleEffect_Config);

            //is this an enemy casting the spell?
            if (!CScript.Is_A_PC)
            {
                //if we are dealing with a blanket particle effect...
                if (isBlanketParticleEffect && spellConfig.ParticleEffect != null)
                {
                    //spawn the effect where an enemy character should, then wait for the designated time before moving forward with the effect
                    Instantiate(spellConfig.ParticleEffect, ((Spell_BlanketParticleEffect_Config)spellConfig).ParticleEffectLocation_CastByEnemy, spellConfig.ParticleEffect.transform.rotation);
                    yield return new WaitForSeconds((spellConfig).DelayAfterParticleEffect);
                }

                //apply the effect to each PC
                foreach (GameObject PC in PCManager.Instance.PCsInParty)
                {
                    if (PC == null)
                    {
                        continue;
                    }

                    ApplyAoEEffectToIndividualCharacter(CScript, spellConfig, triggerProcs, isBlanketParticleEffect, PC);
                }
            }
            //a PC cast the spell...
            else
            {
                //if we are dealing with a blanket particle effect...
                if (isBlanketParticleEffect && spellConfig.ParticleEffect != null)
                {
                    //spawn the effect where a PC should, then wait for the designated time before moving forward with the effect
                    Instantiate(spellConfig.ParticleEffect, ((Spell_BlanketParticleEffect_Config)spellConfig).ParticleEffectLocation_CastByPC, spellConfig.ParticleEffect.transform.rotation);
                    yield return new WaitForSeconds((spellConfig).DelayAfterParticleEffect);
                }

                //apply the effect to each enemy
                for (int i = EnemyManager.Instance.EnemiesInEncounter.Count - 1; i >= 0; i--)
                {
                    if (EnemyManager.Instance.EnemiesInEncounter[i] == null)
                    {
                        continue;
                    }

                    ApplyAoEEffectToIndividualCharacter(CScript, spellConfig, triggerProcs, isBlanketParticleEffect, EnemyManager.Instance.EnemiesInEncounter[i]);
                }
            }

            if (spellConfig.OnHitSoundEffect != null)
            {
                SoundManager.Instance.PlayAudioClip(spellConfig.OnHitSoundEffect);
            }

            numOfSpellEffectsProcessing--;
        }

        IEnumerator EffectAllFriendly(Character CScript, BaseSpell_Config spellConfig, bool triggerProcs = true)
        {
            bool isBlanketParticleEffect = spellConfig.GetType() == typeof(Spell_BlanketParticleEffect_Config);

            if (!CScript.Is_A_PC)
            {
                if (isBlanketParticleEffect && spellConfig.ParticleEffect != null)
                {
                    Instantiate(spellConfig.ParticleEffect, ((Spell_BlanketParticleEffect_Config)spellConfig).ParticleEffectLocation_CastByEnemy, spellConfig.ParticleEffect.transform.rotation);
                    yield return new WaitForSeconds((spellConfig).DelayAfterParticleEffect);
                }

                for (int i = EnemyManager.Instance.EnemiesInEncounter.Count - 1; i >= 0; i--)
                {
                    if (EnemyManager.Instance.EnemiesInEncounter[i] == null)
                    {
                        continue;
                    }

                    ApplyAoEEffectToIndividualCharacter(CScript, spellConfig, triggerProcs, isBlanketParticleEffect, EnemyManager.Instance.EnemiesInEncounter[i]);
                }
            }
            else
            {
                if (isBlanketParticleEffect && spellConfig.ParticleEffect != null)
                {
                    Instantiate(spellConfig.ParticleEffect, ((Spell_BlanketParticleEffect_Config)spellConfig).ParticleEffectLocation_CastByPC, spellConfig.ParticleEffect.transform.rotation);
                    yield return new WaitForSeconds((spellConfig).DelayAfterParticleEffect);
                }

                foreach (GameObject PC in PCManager.Instance.PCsInParty)
                {
                    if (PC == null)
                    {
                        continue;
                    }

                    ApplyAoEEffectToIndividualCharacter(CScript, spellConfig, triggerProcs, isBlanketParticleEffect, PC);
                }
            }

            if (spellConfig.OnHitSoundEffect != null)
            {
                SoundManager.Instance.PlayAudioClip(spellConfig.OnHitSoundEffect);
            }

            numOfSpellEffectsProcessing--;
        }
        #endregion

        void ApplyAoEEffectToIndividualCharacter(Character caster, BaseSpell_Config spellConfig, bool triggerProcs, bool isBlanketParticleEffect, GameObject char_GO)
        {
            //get a reference to the Character component
            var c = char_GO.GetComponent<Character>();

            if (c != null)
            {
                //if this was a blanket particle effect, it's already been instantiated,
                //but if not, spawn the particle effect on this character
                if (spellConfig.ParticleEffect != null && !isBlanketParticleEffect)
                {
                    var e = Instantiate(spellConfig.ParticleEffect, char_GO.transform.position, spellConfig.ParticleEffect.transform.rotation, c.transform);
                    ScaleObjectWithModel.MatchScale(char_GO, e);
                }
                //only apply the effect if the character is still alive
                if (c != null && c.IsAlive)
                {
                    //create an instance of the spellConfig
                    var configInstance = Instantiate(spellConfig);
                    //if triggerProcs is == false, ensure that the instantiated 
                    //spellConfig will not trigger procs, even if it normally does
                    if (!triggerProcs)
                    {
                        configInstance.ThisCommandTriggersProcs = false;
                    }
                    //apply the effect
                    c.ApplySpellEffect(caster, configInstance);
                }
            }
        }
    }
}
