using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

//Last Reviewed: 2/23/20
namespace RPG.CombatSystem
{
    //The job of this class is to manipulate text elements and to highlight / unhighlight characters as needed
    public class Combat_UI_Manager : Singleton<Combat_UI_Manager>
    {
        //helper class and variables used to display timed messages at the top of the screen
        ////////////////////////////////////////////////////////////////////////////////////
        public class Instruction
        {
            public string message;
            public Color textColor;
            public float startTime;
            public float clearTime;
        }

        const float displayTimePerCharacter = 0.1f;
        const float additionalDisplayTime = 0.5f;

        List<Instruction> instructions = new List<Instruction>();
        ///////////////////////////////////////////////////////////////////////////////////

        const float highlightAlpha = 0.39f;
        const float unhighlightedAlpha = 0.05f;

        public Button FleeButton;

        [Header("Player Character Slots")] //UI panels that have text children : Name / HP / MP / TP
        public Image[] PC_Slots = new Image[4];

        [Header("UI Panels")] //UI panels that hold buttons that the player uses to issue commands such as [fight] or [flee]
        public GameObject NameAndStatsPanel;
        public GameObject CommandPanel_Master;
        public GameObject CommandPanel_FightOrFlee;
        public GameObject CommandPanel_CharacterSpecific;
        public GameObject SubMenuPanel;
        public GameObject TargetPanel;
        public GameObject CombatInventoryPanel;
        public GameObject TextPanel_TopOfScreen;
        public GameObject TextPanel_CommandInfoPanel;
        public GameObject CommandInfo_Duration_Panel;

        [Header("UI Text Fields")] //All the text fields on the combat screen 
#region UI Text Fields
        public TextMeshProUGUI PC_Slot0_Name;
        public TextMeshProUGUI PC_Slot0_HP_Lbl;
        public TextMeshProUGUI PC_Slot0_CurHP;
        public TextMeshProUGUI PC_Slot0_HPSeparator;
        public TextMeshProUGUI PC_Slot0_MaxHP;
        public TextMeshProUGUI PC_Slot0_MP_Lbl;
        public TextMeshProUGUI PC_Slot0_CurMP;
        public TextMeshProUGUI PC_Slot0_MPSeparator;
        public TextMeshProUGUI PC_Slot0_MaxMP;
        public TextMeshProUGUI PC_Slot0_TP_Lbl;
        public TextMeshProUGUI PC_Slot0_CurTP;
        public TextMeshProUGUI PC_Slot0_TPSeparator;
        public TextMeshProUGUI PC_Slot0_MaxTP;

        [Space(20)]
        public TextMeshProUGUI PC_Slot1_Name;
        public TextMeshProUGUI PC_Slot1_HP_Lbl;
        public TextMeshProUGUI PC_Slot1_CurHP;
        public TextMeshProUGUI PC_Slot1_HPSeparator;
        public TextMeshProUGUI PC_Slot1_MaxHP;
        public TextMeshProUGUI PC_Slot1_MP_Lbl;
        public TextMeshProUGUI PC_Slot1_CurMP;
        public TextMeshProUGUI PC_Slot1_MPSeparator;
        public TextMeshProUGUI PC_Slot1_MaxMP;
        public TextMeshProUGUI PC_Slot1_TP_Lbl;
        public TextMeshProUGUI PC_Slot1_CurTP;
        public TextMeshProUGUI PC_Slot1_TPSeparator;
        public TextMeshProUGUI PC_Slot1_MaxTP;

        [Space(20)]
        public TextMeshProUGUI PC_Slot2_Name;
        public TextMeshProUGUI PC_Slot2_HP_Lbl;
        public TextMeshProUGUI PC_Slot2_CurHP;
        public TextMeshProUGUI PC_Slot2_HPSeparator;
        public TextMeshProUGUI PC_Slot2_MaxHP;
        public TextMeshProUGUI PC_Slot2_MP_Lbl;
        public TextMeshProUGUI PC_Slot2_CurMP;
        public TextMeshProUGUI PC_Slot2_MPSeparator;
        public TextMeshProUGUI PC_Slot2_MaxMP;
        public TextMeshProUGUI PC_Slot2_TP_Lbl;
        public TextMeshProUGUI PC_Slot2_CurTP;
        public TextMeshProUGUI PC_Slot2_TPSeparator;
        public TextMeshProUGUI PC_Slot2_MaxTP;

        [Space(20)]
        public TextMeshProUGUI PC_Slot3_Name;
        public TextMeshProUGUI PC_Slot3_HP_Lbl;
        public TextMeshProUGUI PC_Slot3_CurHP;
        public TextMeshProUGUI PC_Slot3_HPSeparator;
        public TextMeshProUGUI PC_Slot3_MaxHP;
        public TextMeshProUGUI PC_Slot3_MP_Lbl;
        public TextMeshProUGUI PC_Slot3_CurMP;
        public TextMeshProUGUI PC_Slot3_MPSeparator;
        public TextMeshProUGUI PC_Slot3_MaxMP;
        public TextMeshProUGUI PC_Slot3_TP_Lbl;
        public TextMeshProUGUI PC_Slot3_CurTP;
        public TextMeshProUGUI PC_Slot3_TPSeparator;
        public TextMeshProUGUI PC_Slot3_MaxTP;

        [Space(20)]
        public TextMeshProUGUI TopOfScreenMsg_Txt;

        [Space(20)]
        public TextMeshProUGUI CommandInfo_Name;
        public TextMeshProUGUI CommandInfo_Description;
        public TextMeshProUGUI CommandInfo_HPCost;
        public TextMeshProUGUI CommandInfo_MPCost;
        public TextMeshProUGUI CommandInfo_TPCost;
        public TextMeshProUGUI CommandInfo_TargetType;
        public TextMeshProUGUI CommandInfo_ReapplicationType;
        public TextMeshProUGUI CommandInfo_Duration_txt;

        [Space(20)]
        public TextMeshProUGUI Target_Lbl;
        public TextMeshProUGUI Target_Txt;

        [Space(20)]
        public Text DebugText;
#endregion

        PlayerCharacter[] PCs = new PlayerCharacter[4];


        void Start()
        {
            InitializePCNamesAndStatsUI();
            HideTopOfScreenPanel();
        }

        void Update()
        {
            if (instructions.Count > 0 && Time.time >= instructions[0].startTime && Time.time < instructions[0].clearTime)
            {
                ShowTopOfScreenPanel();
                TopOfScreenMsg_Txt.text = instructions[0].message;
                TopOfScreenMsg_Txt.color = instructions[0].textColor;
                for (int i = 1; i < instructions.Count; i++)
                {
                    instructions[i].startTime += Time.deltaTime;
                    instructions[i].clearTime += Time.deltaTime;
                }
            }
            else if (instructions.Count > 0 && Time.time >= instructions[0].clearTime)
            {
                instructions.RemoveAt(0);
                TopOfScreenMsg_Txt.text = string.Empty;
                HideTopOfScreenPanel();
            }

            /////Just in case things start going south with my message system
            if (instructions.Count > 10)
            {
                Debug.LogWarning("More than 10 messages in the instruction queue for top-of-screen messages");
            }
        }

#region PC_Slot Methods
        //Hide ALL fields for a specific character slot
        public void HidePC_Slot(int index)
        {
            switch (index)
            {
                case 0:
                    PC_Slot0_Name.text = "";
                    break;
                case 1:
                    PC_Slot1_Name.text = "";
                    break;
                case 2:
                    PC_Slot2_Name.text = "";
                    break;
                case 3:
                    PC_Slot3_Name.text = "";
                    break;
            }
            HideHPField(index);
            HideMPField(index);
            HideTPField(index);
        }

        //Hide or show HP Text for a specific character slot
        public void HideHPField(int slotNum)
        {
            switch (slotNum)
            {
                case 0:
                    PC_Slot0_HP_Lbl.enabled = false;
                    PC_Slot0_CurHP.enabled = false;
                    PC_Slot0_HPSeparator.enabled = false;
                    PC_Slot0_MaxHP.enabled = false;
                    break;
                case 1:
                    PC_Slot1_HP_Lbl.enabled = false;
                    PC_Slot1_CurHP.enabled = false;
                    PC_Slot1_HPSeparator.enabled = false;
                    PC_Slot1_MaxHP.enabled = false;
                    break;
                case 2:
                    PC_Slot2_HP_Lbl.enabled = false;
                    PC_Slot2_CurHP.enabled = false;
                    PC_Slot2_HPSeparator.enabled = false;
                    PC_Slot2_MaxHP.enabled = false;
                    break;
                case 3:
                    PC_Slot3_HP_Lbl.enabled = false;
                    PC_Slot3_CurHP.enabled = false;
                    PC_Slot3_HPSeparator.enabled = false;
                    PC_Slot3_MaxHP.enabled = false;
                    break;
                default:
                    Debug.LogWarning("Invalid slotNum used for HideHPField()");
                    break;
            }
        }
        public void ShowHPField(int slotNum)
        {
            switch (slotNum)
            {
                case 0:
                    PC_Slot0_HP_Lbl.enabled = true;
                    PC_Slot0_CurHP.enabled = true;
                    PC_Slot0_HPSeparator.enabled = true;
                    PC_Slot0_MaxHP.enabled = true;
                    break;
                case 1:
                    PC_Slot1_HP_Lbl.enabled = true;
                    PC_Slot1_CurHP.enabled = true;
                    PC_Slot1_HPSeparator.enabled = true;
                    PC_Slot1_MaxHP.enabled = true;
                    break;
                case 2:
                    PC_Slot2_HP_Lbl.enabled = true;
                    PC_Slot2_CurHP.enabled = true;
                    PC_Slot2_HPSeparator.enabled = true;
                    PC_Slot2_MaxHP.enabled = true;
                    break;
                case 3:
                    PC_Slot3_HP_Lbl.enabled = true;
                    PC_Slot3_CurHP.enabled = true;
                    PC_Slot3_HPSeparator.enabled = true;
                    PC_Slot3_MaxHP.enabled = true;
                    break;
                default:
                    Debug.LogWarning("Invalid slotNum used for ShowHPField()");
                    break;
            }
        }

        //Hide or show MP Text for a specific character slot
        public void HideMPField(int slotNum)
        {
            switch (slotNum)
            {
                case 0:
                    PC_Slot0_MP_Lbl.enabled = false;
                    PC_Slot0_CurMP.enabled = false;
                    PC_Slot0_MPSeparator.enabled = false;
                    PC_Slot0_MaxMP.enabled = false;
                    break;
                case 1:
                    PC_Slot1_MP_Lbl.enabled = false;
                    PC_Slot1_CurMP.enabled = false;
                    PC_Slot1_MPSeparator.enabled = false;
                    PC_Slot1_MaxMP.enabled = false;
                    break;
                case 2:
                    PC_Slot2_MP_Lbl.enabled = false;
                    PC_Slot2_CurMP.enabled = false;
                    PC_Slot2_MPSeparator.enabled = false;
                    PC_Slot2_MaxMP.enabled = false;
                    break;
                case 3:
                    PC_Slot3_MP_Lbl.enabled = false;
                    PC_Slot3_CurMP.enabled = false;
                    PC_Slot3_MPSeparator.enabled = false;
                    PC_Slot3_MaxMP.enabled = false;
                    break;
                default:
                    Debug.LogWarning("Invalid slotNum used for HideMPField()");
                    break;
            }
        }
        public void ShowMPField(int slotNum)
        {
            switch (slotNum)
            {
                case 0:
                    PC_Slot0_MP_Lbl.enabled = true;
                    PC_Slot0_CurMP.enabled = true;
                    PC_Slot0_MPSeparator.enabled = true;
                    PC_Slot0_MaxMP.enabled = true;
                    break;
                case 1:
                    PC_Slot1_MP_Lbl.enabled = true;
                    PC_Slot1_CurMP.enabled = true;
                    PC_Slot1_MPSeparator.enabled = true;
                    PC_Slot1_MaxMP.enabled = true;
                    break;
                case 2:
                    PC_Slot2_MP_Lbl.enabled = true;
                    PC_Slot2_CurMP.enabled = true;
                    PC_Slot2_MPSeparator.enabled = true;
                    PC_Slot2_MaxMP.enabled = true;
                    break;
                case 3:
                    PC_Slot3_MP_Lbl.enabled = true;
                    PC_Slot3_CurMP.enabled = true;
                    PC_Slot3_MPSeparator.enabled = true;
                    PC_Slot3_MaxMP.enabled = true;
                    break;
                default:
                    Debug.LogWarning("Invalid slotNum used for ShowMPField()");
                    break;
            }
        }

        //Hide or show TP Text for a specific character slot
        public void HideTPField(int slotNum)
        {
            switch (slotNum)
            {
                case 0:
                    PC_Slot0_TP_Lbl.enabled = false;
                    PC_Slot0_CurTP.enabled = false;
                    PC_Slot0_TPSeparator.enabled = false;
                    PC_Slot0_MaxTP.enabled = false;
                    break;
                case 1:
                    PC_Slot1_TP_Lbl.enabled = false;
                    PC_Slot1_CurTP.enabled = false;
                    PC_Slot1_TPSeparator.enabled = false;
                    PC_Slot1_MaxTP.enabled = false;
                    break;
                case 2:
                    PC_Slot2_TP_Lbl.enabled = false;
                    PC_Slot2_CurTP.enabled = false;
                    PC_Slot2_TPSeparator.enabled = false;
                    PC_Slot2_MaxTP.enabled = false;
                    break;
                case 3:
                    PC_Slot3_TP_Lbl.enabled = false;
                    PC_Slot3_CurTP.enabled = false;
                    PC_Slot3_TPSeparator.enabled = false;
                    PC_Slot3_MaxTP.enabled = false;
                    break;
                default:
                    Debug.LogWarning("Invalid slotNum used for HideTPField()");
                    break;
            }
        }
        public void ShowTPField(int slotNum)
        {
            switch (slotNum)
            {
                case 0:
                    PC_Slot0_TP_Lbl.enabled = true;
                    PC_Slot0_CurTP.enabled = true;
                    PC_Slot0_TPSeparator.enabled = true;
                    PC_Slot0_MaxTP.enabled = true;
                    break;
                case 1:
                    PC_Slot1_TP_Lbl.enabled = true;
                    PC_Slot1_CurTP.enabled = true;
                    PC_Slot1_TPSeparator.enabled = true;
                    PC_Slot1_MaxTP.enabled = true;
                    break;
                case 2:
                    PC_Slot2_TP_Lbl.enabled = true;
                    PC_Slot2_CurTP.enabled = true;
                    PC_Slot2_TPSeparator.enabled = true;
                    PC_Slot2_MaxTP.enabled = true;
                    break;
                case 3:
                    PC_Slot3_TP_Lbl.enabled = true;
                    PC_Slot3_CurTP.enabled = true;
                    PC_Slot3_TPSeparator.enabled = true;
                    PC_Slot3_MaxTP.enabled = true;
                    break;
                default:
                    Debug.LogWarning("Invalid slotNum used for ShowTPField()");
                    break;
            }
        }

        //Set the name Text for a specific character slot
        public void SetNameField(string name, int slotNum)
        {
            switch (slotNum)
            {
                case 0:
                    PC_Slot0_Name.text = name;
                    break;
                case 1:
                    PC_Slot1_Name.text = name;
                    break;
                case 2:
                    PC_Slot2_Name.text = name;
                    break;
                case 3:
                    PC_Slot3_Name.text = name;
                    break;
                default:
                    Debug.LogWarning("Invalid slotNum used for SetPCNameField()");
                    break;
            }
        }

        //Set max or current HP Text for a specific character slot
        public void SetMaxHPField(string maxHP, int slotNum)
        {
            switch (slotNum)
            {
                case 0:
                    PC_Slot0_MaxHP.text = maxHP;
                    break;
                case 1:
                    PC_Slot1_MaxHP.text = maxHP;
                    break;
                case 2:
                    PC_Slot2_MaxHP.text = maxHP;
                    break;
                case 3:
                    PC_Slot3_MaxHP.text = maxHP;
                    break;
                default:
                    Debug.LogWarning("Invalid slotNum used for SetMaxHPField()");
                    break;
            }
        }
        public void SetCurrentHPField(string curHP, int slotNum)
        {
            switch (slotNum)
            {
                case 0:
                    PC_Slot0_CurHP.text = curHP;
                    break;
                case 1:
                    PC_Slot1_CurHP.text = curHP;
                    break;
                case 2:
                    PC_Slot2_CurHP.text = curHP;
                    break;
                case 3:
                    PC_Slot3_CurHP.text = curHP;
                    break;
                default:
                    Debug.LogWarning("Invalid slotNum used for SetCurrentHPField()");
                    break;
            }
        }

        //Set max or current MP Text for a specific character slot
        public void SetMaxMPField(string maxMP, int slotNum)
        {
            switch (slotNum)
            {
                case 0:
                    PC_Slot0_MaxMP.text = maxMP;
                    break;
                case 1:
                    PC_Slot1_MaxMP.text = maxMP;
                    break;
                case 2:
                    PC_Slot2_MaxMP.text = maxMP;
                    break;
                case 3:
                    PC_Slot3_MaxMP.text = maxMP;
                    break;
                default:
                    Debug.LogWarning("Invalid slotNum used for SetMaxMPField()");
                    break;
            }
        }
        public void SetCurrentMPField(string curMP, int slotNum)
        {
            switch (slotNum)
            {
                case 0:
                    PC_Slot0_CurMP.text = curMP;
                    break;
                case 1:
                    PC_Slot1_CurMP.text = curMP;
                    break;
                case 2:
                    PC_Slot2_CurMP.text = curMP;
                    break;
                case 3:
                    PC_Slot3_CurMP.text = curMP;
                    break;
                default:
                    Debug.LogWarning("Invalid slotNum used for SetCurrentMPField()");
                    break;
            }
        }

        //Set max or current TP Text for a specific character slot
        public void SetMaxTPField(string maxTP, int slotNum)
        {
            switch (slotNum)
            {
                case 0:
                    PC_Slot0_MaxTP.text = maxTP;
                    break;
                case 1:
                    PC_Slot1_MaxTP.text = maxTP;
                    break;
                case 2:
                    PC_Slot2_MaxTP.text = maxTP;
                    break;
                case 3:
                    PC_Slot3_MaxTP.text = maxTP;
                    break;
                default:
                    Debug.LogWarning("Invalid slotNum used for SetMaxTPField()");
                    break;
            }
        }
        public void SetCurrentTPField(string curTP, int slotNum)
        {
            switch (slotNum)
            {
                case 0:
                    PC_Slot0_CurTP.text = curTP;
                    break;
                case 1:
                    PC_Slot1_CurTP.text = curTP;
                    break;
                case 2:
                    PC_Slot2_CurTP.text = curTP;
                    break;
                case 3:
                    PC_Slot3_CurTP.text = curTP;
                    break;
                default:
                    Debug.LogWarning("Invalid slotNum used for SetCurrentTPField()");
                    break;
            }
        }

        //Highlight a specific character slot
        public void HighlightPC_Slot(int slotNum)
        {
            Color highlightColor;
            switch (slotNum)
            {
                case 0:
                    highlightColor = PC_Slots[0].color;
                    highlightColor.a = highlightAlpha;
                    PC_Slots[0].color = highlightColor;
                    break;
                case 1:
                    highlightColor = PC_Slots[1].color;
                    highlightColor.a = highlightAlpha;
                    PC_Slots[1].color = highlightColor;
                    break;
                case 2:
                    highlightColor = PC_Slots[2].color;
                    highlightColor.a = highlightAlpha;
                    PC_Slots[2].color = highlightColor;
                    break;
                case 3:
                    highlightColor = PC_Slots[3].color;
                    highlightColor.a = highlightAlpha;
                    PC_Slots[3].color = highlightColor;
                    break;
                default:
                    Debug.LogWarning("Invalid slotNum used for HighlightPC_Slot()");
                    break;
            }
        }

        //Un-highlight all character slots
        public void UnhighlightAllPC_Slots()
        {
            Color normalColor;

            foreach (Image slot in PC_Slots)
            {
                normalColor = slot.color;
                normalColor.a = unhighlightedAlpha;
                slot.color = normalColor;
            }
        }
#endregion

#region UI Panel Methods
        //Show or Hide UI Panels
        public void HideMasterCommandsPanel()
        {
            CommandPanel_Master.SetActive(false);
        }
        public void ShowMasterCommandPanel()
        {
            CommandPanel_Master.SetActive(true);
        }

        public void HideFightOrFleePanel()
        {
            CommandPanel_FightOrFlee.SetActive(false);
        }
        public void ShowFightOrFleePanel()
        {
            CommandPanel_FightOrFlee.SetActive(true);
        }

        public void HideCharacterSpecificCommandPanel()
        {
            CommandPanel_CharacterSpecific.SetActive(false);
        }
        public void ShowCharacterSpecificCommandPanel()
        {
            CommandPanel_CharacterSpecific.SetActive(true);
        }

        public void HideTopOfScreenPanel()
        {
            TextPanel_TopOfScreen.SetActive(false);
        }
        public void ShowTopOfScreenPanel()
        {
            TextPanel_TopOfScreen.SetActive(true);
        }

        public void HideCommandInfoPanel()
        {
            TextPanel_CommandInfoPanel.SetActive(false);
        }
        public void ShowCommandInfoPanel()
        {
            TextPanel_CommandInfoPanel.SetActive(true);
        }

        public void HideCombatInvPanel()
        {
            CombatInventoryPanel.SetActive(false);
        }
        public void ShowCombatInvPanel()
        {
            CombatInventoryPanel.SetActive(true);
        }

        public void PopulateCommandInfoPanel(BaseCommand cmnd)
        {
            CommandInfo_Name.text = cmnd.CommandName;
            CommandInfo_Description.text = cmnd.CommandDescription;
            CommandInfo_HPCost.text = cmnd.HPCost.ToString();
            CommandInfo_MPCost.text = cmnd.MPCost.ToString();
            CommandInfo_TPCost.text = cmnd.TPCost.ToString();
            CommandInfo_TargetType.text = cmnd._TargetType.ToString();

            if (IsSameOrSubclass(typeof(Command_SpellEffect), cmnd.GetType()) && ((Command_SpellEffect)cmnd).Spell_Config.OnReApply != ReapplicationType.NONE)
            {
                CommandInfo_ReapplicationType.text = ((Command_SpellEffect)cmnd).Spell_Config.OnReApply.ToString();
            }
            else
            {
                CommandInfo_ReapplicationType.text = "NA";
            }
            if (IsSameOrSubclass(typeof(Command_SpellEffect), cmnd.GetType()) && ((Command_SpellEffect)cmnd).Spell_Config.TurnDuration > 0)
            {
                CommandInfo_Duration_Panel.SetActive(true);
                CommandInfo_Duration_txt.text = ((Command_SpellEffect)cmnd).Spell_Config.TurnDuration.ToString();
            }
            else
            {
                CommandInfo_Duration_Panel.SetActive(false);
            }
        }

        bool IsSameOrSubclass(Type potentialBase, Type potentialDescendant)
        {
            return potentialDescendant.IsSubclassOf(potentialBase) || potentialDescendant == potentialBase;
        }

        public void HideTargetPanel()
        {
            TargetPanel.SetActive(false);
        }
        public void ShowTargetPanel()
        {
            TargetPanel.SetActive(true);
        }
        public void ResetTargetPanel()
        {
            Target_Lbl.color = Color.white;
            Target_Txt.text = "";
        }

        public void Hide_SubMenuPanel()
        {
            SubMenuPanel.SetActive(false);
        }
        public void Show_SubMenuPanel()
        {
            SubMenuPanel.SetActive(true);
        }

        public void HideNameAndStatsPanel()
        {
            NameAndStatsPanel.SetActive(false);
        }
        public void ShowNameAndStatsPanel()
        {
            NameAndStatsPanel.SetActive(true);
        }

        public void DisableFleeButton()
        {
            FleeButton.interactable = false;
        }
#endregion

        //Show the current PC info in each PC_Slot
        public void UpdateAllPC_SlotInfo()
        {
            for (int i = 0; i < PCs.Length; i++)
            {
                if (PCs[i] != null)
                {
                    var stats = (PC_Stats_Config)PCs[i].Stats_Config;
                    SetNameField(stats.CharName, stats.PC_Slot_position);

                    ShowHPField(stats.PC_Slot_position);
                    SetCurrentHPField(stats.CurHP.ToString(), stats.PC_Slot_position);
                    SetMaxHPField(stats.MaxHP.ToString(), stats.PC_Slot_position);

                    if (PCs[i].HasMP)
                    {
                        ShowMPField(stats.PC_Slot_position);
                        SetCurrentMPField(stats.CurMP.ToString(), stats.PC_Slot_position);
                        SetMaxMPField(stats.MaxMP.ToString(), stats.PC_Slot_position);
                    }
                    else
                    {
                        HideMPField(stats.PC_Slot_position);
                    }

                    if (PCs[i].HasTP)
                    {
                        ShowTPField(stats.PC_Slot_position);
                        SetCurrentTPField(stats.CurTP.ToString(), stats.PC_Slot_position);
                        SetMaxTPField(stats.MaxTP.ToString(), stats.PC_Slot_position);
                    }
                    else
                    {
                        HideTPField(stats.PC_Slot_position);
                    }
                }
            }

            if (PC_Slot0_Name.text == "Placeholder" || PC_Slot0_Name.text == "")
            {
                HidePC_Slot(0);
            }

            if (PC_Slot1_Name.text == "Placeholder" || PC_Slot1_Name.text == "")
            {
                HidePC_Slot(1);
            }

            if (PC_Slot2_Name.text == "Placeholder" || PC_Slot2_Name.text == "")
            {
                HidePC_Slot(2);
            }

            if (PC_Slot3_Name.text == "Placeholder" || PC_Slot3_Name.text == "")
            {
                HidePC_Slot(3);
            }
        }

        //Display a mssg at the top of the screen
        public void SetMsg_TopOfScreen(string msg, Color textColor, float delay = 0, bool allowDuplicate = true)
        {
            ShowTopOfScreenPanel();
            float startTime = Time.time + delay;
            float displayDuration = msg.Length * displayTimePerCharacter + additionalDisplayTime;
            float newClearTime = startTime + displayDuration;

            Instruction newInstruction = new Instruction
            {
                message = msg,
                textColor = textColor,
                startTime = startTime,
                clearTime = newClearTime
            };

            if (!allowDuplicate)
            {
                for (int i = instructions.Count - 1; i >= 0; i--)
                {
                    if (msg == instructions[i].message)
                    {
                        return;
                    }
                }
            }
            //print("adding");
            instructions.Add(newInstruction);
        }

        //Have the UI show what it needs to at the START of a particular PCs turn:
        //1) hide the FightOrFlee Panel if it is showing
        //2) highlight the "PC_slot" that PC is in (slot == the ui panel that holds the text for: Name / HP / MP / TP)
        //3) Set up the command button text labels unique to PC
        //4) Outline PC
        //5) Show the command panel that has PCs unique command options
        public void SetUpUIForPCToChooseCommand(GameObject PC)
        {
            //Start with a fresh UI...
            Un_OutlineAllCharacters();
            UnhighlightAllPC_Slots();
            HideCommandInfoPanel();
            HideCombatInvPanel();

            PlayerCharacter PCScript = PC.GetComponent<PlayerCharacter>();

            if (PCScript != null)
            {
                ShowMasterCommandPanel();
                Hide_SubMenuPanel();
                HideFightOrFleePanel();
                HighlightPC_Slot(((PC_Stats_Config)PCScript.Stats_Config).PC_Slot_position);
                OutlineCharacter(PC);
                ShowCharacterSpecificCommandPanel();
                ResetTargetPanel();
                ShowTargetPanel();
            }
        }

        //Called when the player opens the SubMenu
        public void SetUpUI_SubMenu(GameObject PC)
        {
            //Start with a fresh UI...
            Un_OutlineAllCharacters();
            ResetTargetPanel();

            OutlineCharacter(PC);
            Show_SubMenuPanel();
            HideMasterCommandsPanel();
        }

        //Called when the player opens the combat inv
        public void SetUpUI_CombatInv(GameObject PC)
        {
            //Start with a fresh UI...
            Un_OutlineAllCharacters();
            ResetTargetPanel();

            OutlineCharacter(PC);
            ShowCombatInvPanel();
        }

        public void SetUpUIForNewCombatRoundStart()
        {
            Un_OutlineAllCharacters();
            UnhighlightAllPC_Slots();
            HideCharacterSpecificCommandPanel();
            HideCombatInvPanel();
            ShowFightOrFleePanel();
            HideTargetPanel();
        }

        public void AnimateNameAndStatsPanel(string animTrigger)
        {
            NameAndStatsPanel.GetComponent<Animator>().SetTrigger(animTrigger); //"SlideLeft" or "SlideRight"
        }

        //Add or remove an outline around a PC or NPC ... requires Outline.cs attached to character
        public void OutlineCharacter(GameObject character)
        {
            OutlinerObject outline = character.GetComponent<OutlinerObject>();
            if (outline != null)
            {
                outline.enabled = true;
                return;
            }
            Debug.LogWarning(character + " does not have an OutlinerObject.cs script attached");
        }
        
        public void Un_OutlineCharacter(GameObject character)
        {
            OutlinerObject outline = character.GetComponent<OutlinerObject>();
            if (outline != null)
            {
                outline.enabled = false;
                return;
            }
            Debug.LogWarning(character + " does not have an OutlinerObject.cs script attached");
        }
        public void Un_OutlineAllPCs()
        {
            foreach (GameObject PC in PCManager.Instance.PCsInParty)
            {
                if (PC != null)
                {
                    Un_OutlineCharacter(PC);
                }
            }
        }
        public void Un_OutlineAllEnemies()
        {
            foreach (GameObject enemy in EnemyManager.Instance.EnemiesInEncounter)
            {
                if (enemy != null)
                {
                    Un_OutlineCharacter(enemy);
                }
            }
        }
        public void Un_OutlineAllCharacters()
        {
            Un_OutlineAllPCs();
            Un_OutlineAllEnemies();
        }

        //Outline a single enemy and display its name in the target txt
        public void UpdateUI_SingleEnemyTarget(EnemyCharacter enemy)
        {
            Un_OutlineAllEnemies();
            OutlineCharacter(enemy.gameObject);
            Target_Lbl.color = Color.red;
            Target_Txt.text = enemy.Stats_Config.CharName;
        }

        //Outline a single PC and display its name in the target txt
        public void UpdateUI_SingleFriendlyTarget(PlayerCharacter PC = null)
        {
            if (PC == null)
            {
                Target_Txt.text = "";
                return;
            }
            Un_OutlineAllCharacters();
            OutlineCharacter(PC.gameObject);
            Target_Lbl.color = Color.green;
            Target_Txt.text = PC.Stats_Config.CharName;
        }

        //1) Sets Name based on UI_Config info 
        //2) Shows or Hides MP and TP fields based on UI_Config info 
        //3) Sets the value of HP / MP / TP based on Stats_Config 
        //4) Hides all text fields for any PC_Slot that still has the name "Placeholder" || ""
        //5) Unhighlights all PC_Slots
        void InitializePCNamesAndStatsUI()
        {
            PopulatePCsArray();
            UpdateAllPC_SlotInfo();
            UnhighlightAllPC_Slots();
        }

        //For each PC in the party: store a reference to the <PlayerCharacter> component in PCs[]
        void PopulatePCsArray()
        {
            for (int i = 0; i < PCManager.Instance.PCsInParty.Length; i++)
            {
                if (PCManager.Instance.PCsInParty[i] != null)
                {
                    PCs[i] = PCManager.Instance.PCsInParty[i].GetComponent<PlayerCharacter>();
                }
            }
        }
    }
}
