using System;
using UnityEngine;

//Last Reviewed: 2/23/20
namespace RPG.CombatSystem
{
    //manage combat cameras
    public class CameraManager : Singleton<CameraManager>
    {
        public event Action<Outliner> CameraSwap; //called whenever we change cameras

        public Camera[] Cameras;

        Outliner activeOutliner; //attached to each camera; provides the blue outline on "selected" characters
        int index = 0; //index of currently active camera

        public Camera ActiveCam { get; private set; }


        protected override void Awake()
        {
            base.Awake();
            ActiveCam = Cameras[index];
            UseSkyBoxBackground();
        }

        void Update()
        {
            //press "C" to swap to another camera
            if (Input.GetKeyDown(KeyCode.C))
            {
                SwapToNextCam();
            }
        }

        void SwapToNextCam()
        {
            if (index == Cameras.Length - 1)
            {
                //we have reached the last camera in the array; time to cycle back to the Cameras[0]
                index = 0;
            }
            else
            {
                //increase index by 1 to get to the next camera in the array
                index++;
            }

            //loop through each camera in the array...
            foreach (Camera cam in Cameras)
            {
                //if cam matches the camera at Cameras[index]...
                if (cam == Cameras[index])
                {
                    //activate it...
                    cam.gameObject.SetActive(true);
                    ActiveCam = cam;
                    activeOutliner = cam.gameObject.GetComponent<Outliner>();
                }
                else
                {
                    //deactivate all cams that != Cameras[index]
                    cam.gameObject.SetActive(false);
                }
            }
            //if our event has any listeners, broadcast ... see OutlinerObject.cs
            if (CameraSwap != null)
            {
                CameraSwap(activeOutliner);
            }
        }

        //called by TurnManager if dynamic camera option is on
        public void ActivateRandomCamera()
        {
            index = UnityEngine.Random.Range(0, Cameras.Length);
            //loop through each camera in the array...
            foreach (Camera cam in Cameras)
            {
                //if cam matches the camera at Cameras[index]...
                if (cam == Cameras[index])
                {
                    //activate it...
                    cam.gameObject.SetActive(true);
                    ActiveCam = cam;
                    activeOutliner = cam.gameObject.GetComponent<Outliner>();
                }
                else
                {
                    //deactivate all cams that != Cameras[index]
                    cam.gameObject.SetActive(false);
                }
            }
            //if our event has any listeners, broadcast ... see OutlinerObject.cs
            if (CameraSwap != null)
            {
                CameraSwap(activeOutliner);
            }
        }

        //activate a specific camera
        public void ActivateCameraAtIndex(int index)
        {
            if (index < 0 || index > Cameras.Length - 1)
            {
                print("Camera Index out of range");
                return;
            }

            foreach (Camera cam in Cameras)
            {
                //if cam matches the camera at Cameras[index]...
                if (cam == Cameras[index])
                {
                    //activate it...
                    cam.gameObject.SetActive(true);
                    ActiveCam = cam;
                    activeOutliner = cam.gameObject.GetComponent<Outliner>();
                }
                else
                {
                    //deactivate all cams that != Cameras[index]
                    cam.gameObject.SetActive(false);
                }
            }
            //if our event has any listeners, broadcast ... see OutlinerObject.cs
            if (CameraSwap != null)
            {
                CameraSwap(activeOutliner);
            }
        }

        //default option
        public void UseSkyBoxBackground()
        {
            foreach (Camera cam in Cameras)
            {
                if (cam != null)
                {
                    cam.clearFlags = CameraClearFlags.Skybox;
                }
            }
        }

        //called when certain battlefield environments 
        //are enabled (e.g. OrcFortress)
        public void UseSolidColorBackground(Color col)
        {
            foreach (Camera cam in Cameras)
            {
                cam.backgroundColor = col;
                cam.clearFlags = CameraClearFlags.SolidColor;
            }
        }
    }
}
