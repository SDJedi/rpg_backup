using System;
using System.Collections.Generic;
using System.Linq;
using RPG.Inventory;
using RPG.PartySelectSystem;
using RPG.Saving;
using RPG.SceneControl;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

//Last Reviewed: 2/23/20
namespace RPG.CombatSystem
{
    //this class exists in the BootScene and does the following:
    //1) keeps track of which PCs are in the party (determined by PCsInParty_Prefabs)
    //2) keeps track of every PCs stats (stored in PC_StatConfigs)
    //3) keeps track of which PCs are available and which are recquired to be in the party
    //4) works with the SavingSystem to save data related to each PC
    public class PCManager : Singleton<PCManager>, ISaveable
    {
        public GameObject[] PCsInParty_Prefabs = new GameObject[4]; //the prefabs we will create our party of 1 - 4 PCs from
        public List<PC_Stats_Config> PC_StatConfigs; //the (cloned / instantiated) stat configs of ALL PCs (those in party and those not)
        public List<SelectablePC> AcquiredTeamMembers; //All the PCs in game

        public GameObject[] PCsInParty { get; private set; } = new GameObject[4]; //the instances of our PCs that appear on-screen in the CombatScene
        //Note: this array gets sorted by slot position


        protected override void Awake()
        {
            base.Awake();
            InitializePCs();
        }

        void Start()
        {
            SceneController.Instance.LeavingCombatScene += RemoveAllSlotPositionBonuses;
            SavingWrapper.Instance.StartingNewGame += InitializePCs;
        }

        void OnDisable()
        {
            if (SceneController.InstanceExists)
            {
                SceneController.Instance.LeavingCombatScene -= RemoveAllSlotPositionBonuses;
            }

            if (SavingWrapper.InstanceExists)
            {
                SavingWrapper.Instance.StartingNewGame -= InitializePCs;
            }
        }

        //Called in Awake() && when New Game is started 
        void InitializePCs()
        {
            //start with a clean slate...
            for (int i = PC_StatConfigs.Count - 1; i >= 0; i--)
            {
                Destroy(PC_StatConfigs[i]);
            }
            PC_StatConfigs.Clear();

            Array.Clear(PCsInParty_Prefabs, 0, 4);

            for (int i = 0; i < AcquiredTeamMembers.Count; i++)
            {
                //add Saul to the party
                if (AcquiredTeamMembers[i].PC_Name == "Saul")
                {
                    AcquiredTeamMembers[i].RequiredPartyMember = true;
                    AcquiredTeamMembers[i].IsAvailable = true;
                    PCsInParty_Prefabs[0] = AcquiredTeamMembers[i].PC_Prefab;
                }
                //add Karl to the party
                else if (AcquiredTeamMembers[i].PC_Name == "Karl")
                {
                    AcquiredTeamMembers[i].RequiredPartyMember = false;
                    AcquiredTeamMembers[i].IsAvailable = true;
                    PCsInParty_Prefabs[1] = AcquiredTeamMembers[i].PC_Prefab;
                }
                //reset status of all other PCs
                else
                {
                    AcquiredTeamMembers[i].RequiredPartyMember = false;
                    AcquiredTeamMembers[i].IsAvailable = false;
                }

                GetInstantiatedStatsConfigForPC(AcquiredTeamMembers[i].PC_Prefab);
            }
            //print("Party reset to Saul / Karl. StatConfigs reset");
        }

        //Called whenever the "CombatScene" is loaded......................................
        //  Create an instance of each PC from a prefab; add that instance to PCsInParty[].
        //  After each instance is created, sort PCsInParty[] by PC_Slot_Position (0 - 3)
        //  Assign each PC its proper instance of its stats config
        void LoadPCsIntoCombatScene()
        {
            List<int> availablePCSlots = new List<int> { 0, 1, 2, 3 };
            //loop through our party prefabs...
            for (int i = 0; i < PCsInParty_Prefabs.Length; i++)
            {
                //if we have less than a full party, there will be null values ... ignore them and move on
                if (PCsInParty_Prefabs[i] == null)
                {
                    continue;
                }

                //Get stats config
                PC_Stats_Config stats = GetStatsForPC(PCsInParty_Prefabs[i].GetComponent<PlayerCharacter>().CharName);

                //check that we can spawn this PC in the slot listed in its config...
                bool slotWasAvailable = false;
                for (int j = availablePCSlots.Count - 1; j >= 0; j--)
                {
                    if (availablePCSlots[j] == stats.PC_Slot_position)
                    {
                        slotWasAvailable = true;
                        availablePCSlots.RemoveAt(j);
                        break;
                    }
                }

                //if slot was not available... 
                if (!slotWasAvailable)
                {
                    Debug.LogWarning("slot pos issue here... running failsafe code");
                    stats.PC_Slot_position = availablePCSlots[0];
                    availablePCSlots.RemoveAt(0);
                }

                //instantiate the PC and load its stats...
                PCsInParty[i] = Instantiate(PCsInParty_Prefabs[i]);
                PlayerCharacter PCScript = PCsInParty[i].GetComponent<PlayerCharacter>();
                PCScript.Set_Stats_Config(stats);
                stats.SetCScript(PCScript);

                //add commands (abilities)
                AddKnownCommands(PCsInParty[i], stats);
                AddCommandsFromEquippedGear(PCsInParty[i], stats);
                //add damage shields
                AddDamageShieldsToPC(PCsInParty[i], stats);
                //add slot position bonuses
                AddSlotPositionBonus(stats);
                //take out any duplicate commands
                RemoveDuplicateCommands(PCsInParty[i]);
            }

            //now that all our PCs have been instantiated...
            Sort_PCsInPartyArray_AccordingToPCSlotPosition();
            //any dead PCs should appear dead, but we don't want to play the death animation from the start, so...
            HandleAnyDeadPCs();
        }

        //Loop through KnownAbilities and add those Commands to the PC
        void AddKnownCommands(GameObject PC, PC_Stats_Config stats)
        {
            stats.KnownAbilities.RemoveAll(n => n == null);
            foreach (BaseCommand_Config cnfg in stats.KnownAbilities)
            {
                AddACommandToAPC(PC, cnfg);
            }
        }

        //give a PC a new ability
        void AddACommandToAPC(GameObject PC, BaseCommand_Config cmndConfig)
        {
            var existingCommands = PC.GetComponents<BaseCommand>();
            foreach (BaseCommand cmnd in existingCommands)
            {
                if (cmnd.Command_Config == null)
                {
                    Debug.LogWarning(PC.name + " has a BaseCommand, <" + cmnd + "> that has no config. Destroying the cmnd, but it should be removed from " + PC.name);
                    Destroy(cmnd);
                    continue;
                }

                if (cmndConfig.CmndName == cmnd.Command_Config.CmndName)
                {
                    Debug.Log("Command " + cmndConfig.CmndName + " already exists on " + PC.GetComponent<PlayerCharacter>().CharName + " ... setting to active.");
                    cmnd.enabled = true;
                    return;
                }
            }

            var newCmnd = PC.AddComponent(cmndConfig.CommandType);
            ((BaseCommand)newCmnd).Command_Config = cmndConfig;
        }

        //add any innate damage shields the PC has
        void AddDamageShieldsToPC(GameObject PC, Base_Stats_Config stats)
        {
            var attackableScript = PC.GetComponent<Attacked_DamageAttacker>();

            stats.DamageShields.RemoveAll(n => n == null);
            foreach (DamageShield ds in stats.DamageShields)
            {
                if (ds.nameOfDmgShield == "")
                {
                    Debug.LogWarning("Damage Shield needs a name to be removed properly... not adding");
                    continue;
                }
                attackableScript.AddDamageShieldToIAttackable(ds);
            }
        }

        //Loop through all equipped gear and look for equipment that adds abilities
        void AddCommandsFromEquippedGear(GameObject PC, PC_Stats_Config stats)
        {
            //Head
            if (stats.EquippedGear.Head != null && stats.EquippedGear.Head.GetType() == (typeof(EquipableItem_AddCommand)))
            {
                AddACommandToAPC(PC, ((EquipableItem_AddCommand)stats.EquippedGear.Head).Config);
            }
            //Chest
            if (stats.EquippedGear.Chest != null && stats.EquippedGear.Chest.GetType() == (typeof(EquipableItem_AddCommand)))
            {
                AddACommandToAPC(PC, ((EquipableItem_AddCommand)stats.EquippedGear.Chest).Config);
            }
            //Waist
            if (stats.EquippedGear.Waist != null && stats.EquippedGear.Waist.GetType() == (typeof(EquipableItem_AddCommand)))
            {
                AddACommandToAPC(PC, ((EquipableItem_AddCommand)stats.EquippedGear.Waist).Config);
            }
            //Legs
            if (stats.EquippedGear.Legs != null && stats.EquippedGear.Legs.GetType() == (typeof(EquipableItem_AddCommand)))
            {
                AddACommandToAPC(PC, ((EquipableItem_AddCommand)stats.EquippedGear.Legs).Config);
            }
            //Feet
            if (stats.EquippedGear.Feet != null && stats.EquippedGear.Feet.GetType() == (typeof(EquipableItem_AddCommand)))
            {
                AddACommandToAPC(PC, ((EquipableItem_AddCommand)stats.EquippedGear.Feet).Config);
            }
            //Adornment1
            if (stats.EquippedGear.Adorn_1 != null && stats.EquippedGear.Adorn_1.GetType() == (typeof(EquipableItem_AddCommand)))
            {
                AddACommandToAPC(PC, ((EquipableItem_AddCommand)stats.EquippedGear.Adorn_1).Config);
            }
            //Adornment2
            if (stats.EquippedGear.Adorn_2 != null && stats.EquippedGear.Adorn_2.GetType() == (typeof(EquipableItem_AddCommand)))
            {
                AddACommandToAPC(PC, ((EquipableItem_AddCommand)stats.EquippedGear.Adorn_2).Config);
            }
            //Weapon
            if (stats.EquippedGear.Weapon != null && stats.EquippedGear.Weapon.GetType() == (typeof(EquipableItem_AddCommand)))
            {
                AddACommandToAPC(PC, ((EquipableItem_AddCommand)stats.EquippedGear.Weapon).Config);
            }
        }

        //remove any duplicated abilities (for example, the same ability from two different pieces of gear should only show once)
        void RemoveDuplicateCommands(GameObject PC)
        {
            //get a reference to all Commands on PC
            List<BaseCommand> PC_Commands = PC.GetComponents<BaseCommand>().ToList();

            for (int i = PC_Commands.Count - 1; i > 0; i--)
            {
                if (PC_Commands[i].Command_Config != null && PC_Commands[i].isActiveAndEnabled)
                {
                    for (int j = i - 1; j >= 0; j--)
                    {
                        if (PC_Commands[j].Command_Config == null || !PC_Commands[j].isActiveAndEnabled)
                        {
                            continue;
                        }

                        if (PC_Commands[i].Command_Config.CmndName == PC_Commands[j].Command_Config.CmndName)
                        {
                            Destroy(PC_Commands[i]);
                            PC_Commands.RemoveAll(n => n == null);
                            break;
                        }
                    }
                }
            }
        }

        //arrange the array holding the in-combat PCs by slot position
        void Sort_PCsInPartyArray_AccordingToPCSlotPosition()
        {
            List<GameObject> temp = PCsInParty.ToList();
            temp.RemoveAll(n => n == null);
            temp = temp.OrderBy(x => ((PC_Stats_Config)x.GetComponent<PlayerCharacter>().Stats_Config).PC_Slot_position).ToList();
            Array.Clear(PCsInParty, 0, PCsInParty.Length);
            for (int i = 0; i < temp.Count; i++)
            {
                PCsInParty[i] = temp[i];
            }
        }

        //set dead pc IsAlive = false; play the "Die" animation from the last frame (i.e. just show the PC as dead)
        void HandleAnyDeadPCs()
        {
            foreach (GameObject PC in PCsInParty)
            {
                if (PC != null && !PC.GetComponent<PlayerCharacter>().IsAlive)
                {
                    Animator a = PC.GetComponentInChildren<Animator>();
                    a.Play("Die", 0, 1);
                }
            }
        }

        //The only method that should be used to create and store an instance of a PC's stats
        public PC_Stats_Config GetInstantiatedStatsConfigForPC(GameObject PC_Prefab)
        {
            PlayerCharacter PCScript = PC_Prefab.GetComponent<PlayerCharacter>();

            //check for existing
            foreach (PC_Stats_Config sConfig in PC_StatConfigs)
            {
                if (sConfig.UniqueID == PCScript.CharName)
                {
                    return sConfig;
                }
            }

            //we don't have a Base_Stats_Config stored for this PC_Prefab... store one
            Assert.IsTrue(PCScript.Stats_Config.GetType() == typeof(PC_Stats_Config));
            var stats = Instantiate((PC_Stats_Config)PCScript.Stats_Config);
            stats.CharName = PCScript.CharName;
            stats.UniqueID = stats.CharName; //UniqueID in the non-instanced version will be == ""
            PC_StatConfigs.Add(stats);

            return stats;
        }

        //returns a List containing the PlayerCharacter script of each living PC in the party
        public List<PlayerCharacter> GetLivingPCsInParty()
        {
            List<PlayerCharacter> livingPcs = new List<PlayerCharacter>();
            foreach (GameObject pc in PCsInParty)
            {
                if (pc != null)
                {
                    PlayerCharacter PCScript = pc.GetComponent<PlayerCharacter>();
                    if (PCScript.IsAlive)
                    {
                        livingPcs.Add(PCScript);
                    }
                }
            }
            return livingPcs;
        }

        //returns the number of living PCs in party
        public int GetNumOfLivingPCsInParty()
        {
            int num = 0;
            foreach (GameObject PC in PCsInParty)
            {
                if (PC != null && PC.GetComponent<PlayerCharacter>().IsAlive)
                {
                    num++;
                }
            }
            return num;
        }

        //returns the PC at PCsInParty[0] ... dead OR alive
        public GameObject GetPCInLowestPC_Slot_position()
        {
            //PCsInParty[] get's sorted by slot position on scene load by LoadPCsIntoCombatScene()
            if (PCsInParty[0] == null)
            {
                Debug.LogWarning("No PCs in PCsInParty[]");
            }

            return PCsInParty[0];
        }

        //returns the PC in party in the highest PC_Slot_position ... dead OR alive
        public GameObject GetPCInHighestPC_Slot_position()
        {
            for (int i = PCsInParty.Length - 1; i >= 0; i--)
            {
                if (PCsInParty[i] != null)
                {
                    return PCsInParty[i];
                }
            }
            Debug.LogWarning("No PCs in PCsInParty[]");
            return PCsInParty[PCsInParty.Length - 1];
        }

        //Returns the LIVING PC with the lowest PC_Slot_position
        public GameObject GetLivingPCWithLowestPC_Slot_position()
        {
            foreach (GameObject PC in PCManager.Instance.PCsInParty)
            {
                if (PC != null && PC.GetComponent<Character>().IsAlive)
                {
                    return PC;
                }
            }

            Debug.LogWarning("No living PC found in PCsInParty[]");
            return null;
        }

        //Returns the LIVING PC with the lowest PC_Slot_position that is NOT character
        public GameObject GetLivingPCWithLowestPC_Slot_position_NotSelf(Character character)
        {
            foreach (GameObject PC in PCManager.Instance.PCsInParty)
            {
                if (PC != null && PC.GetComponent<Character>() != character && PC.GetComponent<Character>().IsAlive)
                {
                    return PC;
                }
            }

            Debug.LogWarning("No living PC found in PCsInParty[] that was not " + character.Stats_Config.CharName);
            return null;
        }

        //Returns the PC in the next slot from currentPC... ignores empty slots and dead PCs ... returns null if there is no higher PC_Slot than currentPC
        public GameObject GetNextLivingPC(GameObject currentPC)
        {
            if (currentPC != null)
            {
                int currentPCSlotPos = ((PC_Stats_Config)currentPC.GetComponent<PlayerCharacter>().Stats_Config).PC_Slot_position;
                int nextPCSlotPos = currentPCSlotPos + 1;

                while (nextPCSlotPos < Combat_UI_Manager.Instance.PC_Slots.Length)
                {
                    foreach (GameObject PC in PCsInParty)
                    {
                        if (PC != null && ((PC_Stats_Config)PC.GetComponent<PlayerCharacter>().Stats_Config).PC_Slot_position == nextPCSlotPos)
                        {
                            if (PC.GetComponent<Character>().IsAlive)
                            {
                                return PC;
                            }
                        }
                    }
                    nextPCSlotPos++;
                }
                //Debug.Log("Could not find a living PC in a higher PC_Slot than " + currentPC);
                return null;
            }
            Debug.Log("currentPC was null");
            return null;
        }

        //Returns the PC in the previous slot from currentPC... ignores empty slots and dead PCs ... returns null if there is no lower PC_Slot than currentPC
        public GameObject GetPreviousLivingPC(GameObject currentPC)
        {
            if (currentPC != null)
            {
                int currentPCSlotPos = ((PC_Stats_Config)currentPC.GetComponent<PlayerCharacter>().Stats_Config).PC_Slot_position;
                int prevPCSlotPos = currentPCSlotPos - 1;

                while (prevPCSlotPos >= 0)
                {
                    foreach (GameObject PC in PCsInParty)
                    {
                        if (PC != null && ((PC_Stats_Config)PC.GetComponent<PlayerCharacter>().Stats_Config).PC_Slot_position == prevPCSlotPos)
                        {
                            if (PC.GetComponent<Character>().IsAlive)
                                return PC;
                        }
                    }
                    prevPCSlotPos--;
                }
                //Debug.Log("Could not find a living PC in a lower PC_Slot than " + currentPC);
                return null;
            }
            //Debug.Log("currentPC was null");
            return null;
        }

        //Returns the PC in the next slot from currentPC... dead OR alive ... Note: if currentPC is in last slot, will return the PC in slot 0
        public GameObject GetNextPC_DeadOrAlive(GameObject currentPC)
        {
            if (currentPC != null)
            {
                int currentPCSlotPos = ((PC_Stats_Config)currentPC.GetComponent<PlayerCharacter>().Stats_Config).PC_Slot_position;
                int nextPCSlotPos = currentPCSlotPos + 1;

                while (nextPCSlotPos < Combat_UI_Manager.Instance.PC_Slots.Length)
                {
                    foreach (GameObject PC in PCsInParty)
                    {
                        if (PC != null && ((PC_Stats_Config)PC.GetComponent<PlayerCharacter>().Stats_Config).PC_Slot_position == nextPCSlotPos)
                        {
                            return PC;
                        }
                    }
                    nextPCSlotPos++;
                }

                return GetPCInLowestPC_Slot_position();
            }
            Debug.Log("currentPC was null");
            return null;
        }

        //Will return the next dead PC from currentPC, OR currentPC if currentPC is the only dead PC... if currentPC is in the last PC_Slot
        //will loop back through PCsInParty starting at slot 0 and return the first dead PC
        public GameObject GetNextDeadPC(GameObject currentPC)
        {
            if (currentPC != null)
            {
                List<GameObject> deadPCs = new List<GameObject>();
                foreach (GameObject PC in PCsInParty)
                {
                    if (PC == null)
                    {
                        continue;
                    }
                    if (!PC.GetComponent<PlayerCharacter>().IsAlive)
                    {
                        deadPCs.Add(PC);
                    }
                }

                if (deadPCs.Count < 1)
                {
                    //print("there are no dead PCs");
                    return null;
                }

                if (deadPCs.Count == 1)
                {
                    //print("only 1 dead PC");
                    return deadPCs[0];
                }

                //there are at least 2 dead PCs
                int currentPCSlotPos = ((PC_Stats_Config)currentPC.GetComponent<PlayerCharacter>().Stats_Config).PC_Slot_position;
                int nextPCSlotPos = currentPCSlotPos + 1;

                if (nextPCSlotPos >= Combat_UI_Manager.Instance.PC_Slots.Length)
                {
                    nextPCSlotPos = 0;
                }

                //Keep looping through deadPCs List until we find the one that is next in line from the currentPC
                while (true)
                {
                    for (int i = 0; i < deadPCs.Count; i++)
                    {
                        if (((PC_Stats_Config)deadPCs[i].GetComponent<PlayerCharacter>().Stats_Config).PC_Slot_position == nextPCSlotPos)
                        {
                            return deadPCs[i];
                        }
                    }

                    if (nextPCSlotPos < Combat_UI_Manager.Instance.PC_Slots.Length)
                    {
                        nextPCSlotPos++;
                    }
                    else
                    {
                        nextPCSlotPos = 0;
                    }
                }
            }
            Debug.Log("currentPC was null");
            return null;
        }

        //Will return the next living PC from currentPC, OR null. if currentPC is in the last PC_Slot
        //will loop back through PCsInParty starting at slot 0 and return the first living PC that is not self
        public GameObject GetNextLivingPC_NotSelf(Character self, GameObject currentPC)
        {
            if (currentPC != null)
            {
                List<GameObject> livingPCs_NotSelf = new List<GameObject>();
                foreach (GameObject PC in PCsInParty)
                {
                    if (PC == null)
                    {
                        continue;
                    }
                    if (PC.GetComponent<PlayerCharacter>().IsAlive && PC.GetComponent<Character>() != self)
                    {
                        livingPCs_NotSelf.Add(PC);
                    }
                }

                if (livingPCs_NotSelf.Count < 1)
                {
                    print("there are no living PCs that are not " + self.Stats_Config.CharName);
                    return null;
                }

                if (livingPCs_NotSelf.Count == 1)
                {
                    print("only 1 living PC that is not " + self.Stats_Config.CharName);
                    return livingPCs_NotSelf[0];
                }

                //there are at least 2 living PCs that are not "self"
                int currentPCSlotPos = ((PC_Stats_Config)currentPC.GetComponent<PlayerCharacter>().Stats_Config).PC_Slot_position;
                int nextPCSlotPos = currentPCSlotPos + 1;

                if (nextPCSlotPos >= Combat_UI_Manager.Instance.PC_Slots.Length)
                {
                    nextPCSlotPos = 0;
                }

                //Keep looping through List until we find the one that is next in line from the currentPC
                while (true)
                {
                    for (int i = 0; i < livingPCs_NotSelf.Count; i++)
                    {
                        if (((PC_Stats_Config)livingPCs_NotSelf[i].GetComponent<PlayerCharacter>().Stats_Config).PC_Slot_position == nextPCSlotPos)
                        {
                            return livingPCs_NotSelf[i];
                        }
                    }

                    if (nextPCSlotPos < Combat_UI_Manager.Instance.PC_Slots.Length)
                    {
                        nextPCSlotPos++;
                    }
                    else
                    {
                        nextPCSlotPos = 0;
                    }
                }
            }
            Debug.Log("currentPC was null");
            return null;
        }

        //Returns a specific, stored stats config based on a PC's name, or null if no match is found
        public PC_Stats_Config GetStatsForPC(string PCName)
        {
            foreach (PC_Stats_Config stats in PC_StatConfigs)
            {
                if (PCName == stats.UniqueID)
                {
                    return stats;
                }
            }
            Debug.LogWarning("No saved stats match " + PCName);
            return null;
        }

        //Returns a List containing the stat config of each PC in the party
        public List<PC_Stats_Config> GetStatsForPCsInParty(bool onlyLivingPCs = false)
        {
            List<PC_Stats_Config> stats = new List<PC_Stats_Config>();
            foreach (GameObject PC in PCsInParty_Prefabs)
            {
                if (PC != null)
                {
                    var s = GetStatsForPC(PC.GetComponent<PlayerCharacter>().CharName);
                    if (onlyLivingPCs == false)
                    {
                        stats.Add(s);
                    }
                    else if (s.IsAlive())
                    {
                        stats.Add(s);
                    }
                }
            }

            return stats;
        }

        //returns an in-combat PC GameObject based on a given stats config
        public GameObject GetPCGameObjectFromStatsConfig(Base_Stats_Config stats)
        {
            foreach (GameObject PC in PCsInParty)
            {
                if (PC != null)
                {
                    if (PC.GetComponent<Character>().Stats_Config == stats)
                    {
                        return PC;
                    }
                }
            }
            Debug.LogWarning("Could not find a matching PC gameobject for " + stats);
            return null;
        }

        public GameObject GetPCGameObjectFromName(string pcName)
        {
            foreach (GameObject PC in PCsInParty)
            {
                if (PC != null)
                {
                    if (PC.GetComponent<PlayerCharacter>().CharName == pcName)
                    {
                        return PC;
                    }
                }
            }
            if (pcName != "")
            {
                Debug.LogWarning("Could not find a matching PC gameobject for " + pcName);
            }

            return null;
        }

        //returns a PlayerCharacter script for a specific PC prefab
        public PlayerCharacter GetCScriptForPC(string pcName)
        {
            foreach (GameObject pc in GetAllPCPrefabs())
            {
                var CScript = pc.GetComponent<PlayerCharacter>();
                if (CScript.CharName == pcName)
                {
                    return CScript;
                }
            }
            return null;
        }

        #region Slot_Position Bonuses
        void AddSlotPositionBonus(PC_Stats_Config stats)
        {
            switch (stats.PC_Slot_position)
            {
                case 0:
                    ApplySlotPosition_0_Bonus(stats);
                    break;
                case 1:
                    ApplySlotPosition_1_Bonus(stats);
                    break;
                case 2:
                    ApplySlotPosition_2_Bonus(stats);
                    break;
                case 3:
                    ApplySlotPosition_3_Bonus(stats);
                    break;
            }
        }

        //Slot_0: +10 initiative
        void ApplySlotPosition_0_Bonus(Base_Stats_Config stats)
        {
            stats.ModifyStat(StatAbbreviation.InitMod, 10);
        }

        void RemoveSlotPosition_0_Bonus(Base_Stats_Config stats)
        {
            stats.ModifyStat(StatAbbreviation.InitMod, -10);
        }

        //Slot_1: +5 atk bonus
        void ApplySlotPosition_1_Bonus(Base_Stats_Config stats)
        {
            stats.ModifyStat(StatAbbreviation.AtkBonus, 5);
        }

        void RemoveSlotPosition_1_Bonus(Base_Stats_Config stats)
        {
            stats.ModifyStat(StatAbbreviation.AtkBonus, -5);
        }

        //Slot_2: +5 dmg bonus
        void ApplySlotPosition_2_Bonus(Base_Stats_Config stats)
        {
            stats.ModifyStat(StatAbbreviation.DmgBonus, 5);
        }

        void RemoveSlotPosition_2_Bonus(Base_Stats_Config stats)
        {
            stats.ModifyStat(StatAbbreviation.DmgBonus, -5);
        }

        //Slot_3: +5 def
        void ApplySlotPosition_3_Bonus(Base_Stats_Config stats)
        {
            stats.ModifyStat(StatAbbreviation.DEF, 5);
        }

        void RemoveSlotPosition_3_Bonus(Base_Stats_Config stats)
        {
            stats.ModifyStat(StatAbbreviation.DEF, -5);
        }

        void RemoveAllSlotPositionBonuses()
        {
            foreach (GameObject PC in PCsInParty)
            {
                if (PC != null)
                {
                    RemoveSlotPositionBonus((PC_Stats_Config)PC.GetComponent<Character>().Stats_Config);
                }
            }
        }

        void RemoveSlotPositionBonus(PC_Stats_Config stats)
        {
            switch (stats.PC_Slot_position)
            {
                case 0:
                    RemoveSlotPosition_0_Bonus(stats);
                    break;
                case 1:
                    RemoveSlotPosition_1_Bonus(stats);
                    break;
                case 2:
                    RemoveSlotPosition_2_Bonus(stats);
                    break;
                case 3:
                    RemoveSlotPosition_3_Bonus(stats);
                    break;
            }
        }
        #endregion

        #region SaveData
        [System.Serializable]
        class PCManagerSaveData
        {
            //save names of all PCs in party
            public List<string> PCsInParty_Names = new List<string>();

            //save the slot position of all PCs
            public List<int> AllPCs_SlotPositions = new List<int>();

            //save the "IsAvailable" flag for all PCs
            public List<bool> AllPCs_IsAvailable = new List<bool>();

            //save the "Required Party Member" flag for all PCs
            public List<bool> AllPCs_IsRequiredInParty = new List<bool>();

            //save current Lvl for all PCs
            public List<int> AllPCs_Lvl = new List<int>();

            //save current XP for all PCs
            public List<int> AllPCs_CurrentXP = new List<int>();

            //save stats for all PCs...
            //HP, MP, TP...
            public List<int> AllPCs_MaxHP = new List<int>();
            public List<int> AllPCs_CurHP = new List<int>();
            public List<int> AllPCs_MaxMP = new List<int>();
            public List<int> AllPCs_CurMP = new List<int>();
            public List<int> AllPCs_MaxTP = new List<int>();
            public List<int> AllPCs_CurTP = new List<int>();

            //STR, INT, AGI, DEF, DmgRed, AtkBonus, DmgBonus, InitMod
            public List<int> AllPCs_STR = new List<int>();
            public List<int> AllPCs_INT = new List<int>();
            public List<int> AllPCs_AGI = new List<int>();
            public List<int> AllPCs_DEF = new List<int>();
            public List<int> AllPCs_DmgRed = new List<int>();
            public List<int> AllPCs_AtkBonus = new List<int>();
            public List<int> AllPCs_DmgBonus = new List<int>();
            public List<int> AllPCs_InitMod = new List<int>();

            //MagRes
            public List<float> ALlPCs_MagRes = new List<float>();

            //save worn equipment for all PCs
            public List<string> AllPCs_HeadSlotItemName = new List<string>();
            public List<string> AllPCs_ChestSlotItemName = new List<string>();
            public List<string> AllPCs_WaistSlotItemName = new List<string>();
            public List<string> AllPCs_LegsSlotItemName = new List<string>();
            public List<string> AllPCs_FeetSlotItemName = new List<string>();
            public List<string> AllPCs_Adorn_1SlotItemName = new List<string>();
            public List<string> AllPCs_Adorn_2SlotItemName = new List<string>();
            public List<string> AllPCs_WeaponSlotItemName = new List<string>();

            //save known abilities
            public List<List<string>> AllPCs_KnownAbilities = new List<List<string>>();

            //save active damage shields
            public List<List<string>> AllPCs_ActiveDamageShields = new List<List<string>>();

            //save strengths and weaknesses
            public List<List<DamageType>> ALLPCs_WeakAgainst = new List<List<DamageType>>();
            public List<List<DamageType>> ALLPCs_StrongAgainst = new List<List<DamageType>>();
            public List<List<DamageType>> ALLPCs_AttunedTo = new List<List<DamageType>>();
        }

        public object CaptureState()
        {
            //print("Capturing PC Data");
            PCManagerSaveData saveData = new PCManagerSaveData();

            //save names of the PCsInParty
            foreach (GameObject PC in PCsInParty_Prefabs)
            {
                if (PC == null)
                {
                    continue;
                }

                var PCScript = PC.GetComponent<PlayerCharacter>();

                saveData.PCsInParty_Names.Add(PCScript.CharName);
            }

            //save the "IsAvailable" && "Required Party Member" bools for all AcquiredTeamMembers
            for (int i = 0; i < AcquiredTeamMembers.Count; i++)
            {
                saveData.AllPCs_IsAvailable.Add(AcquiredTeamMembers[i].IsAvailable);
                saveData.AllPCs_IsRequiredInParty.Add(AcquiredTeamMembers[i].RequiredPartyMember);
            }

            //save lvl, xp, stats, etc...
            for (int i = 0; i < PC_StatConfigs.Count; i++)
            {
                //SlotPosition:
                saveData.AllPCs_SlotPositions.Add(PC_StatConfigs[i].PC_Slot_position);
                //Lvl:
                saveData.AllPCs_Lvl.Add(PC_StatConfigs[i].CurLvl);
                //XP:
                saveData.AllPCs_CurrentXP.Add(PC_StatConfigs[i].CurXP);


                //STATS:
                //HP
                saveData.AllPCs_MaxHP.Add(PC_StatConfigs[i].MaxHP);
                saveData.AllPCs_CurHP.Add(PC_StatConfigs[i].CurHP);
                //MP
                saveData.AllPCs_MaxMP.Add(PC_StatConfigs[i].MaxMP);
                saveData.AllPCs_CurMP.Add(PC_StatConfigs[i].CurMP);
                //TP
                saveData.AllPCs_MaxTP.Add(PC_StatConfigs[i].MaxTP);
                saveData.AllPCs_CurTP.Add(PC_StatConfigs[i].CurTP);
                //STR
                saveData.AllPCs_STR.Add(PC_StatConfigs[i].STR);
                //INT
                saveData.AllPCs_INT.Add(PC_StatConfigs[i].INT);
                //AGI
                saveData.AllPCs_AGI.Add(PC_StatConfigs[i].AGI);
                //DEF
                saveData.AllPCs_DEF.Add(PC_StatConfigs[i].DEF);
                //DmgRed
                saveData.AllPCs_DmgRed.Add(PC_StatConfigs[i].DmgReduction);
                //AtkBonus
                saveData.AllPCs_AtkBonus.Add(PC_StatConfigs[i].BaseAtkBonus);
                //DmgBonus
                saveData.AllPCs_DmgBonus.Add(PC_StatConfigs[i].DamageBonus);
                //InitMod
                saveData.AllPCs_InitMod.Add(PC_StatConfigs[i].InitiativeMod);
                //MagRes
                saveData.ALlPCs_MagRes.Add(PC_StatConfigs[i].MagRes);


                //EQUIPMENT:
                //Head
                saveData.AllPCs_HeadSlotItemName.Add(PC_StatConfigs[i].EquippedGear.Head == null ? "" : PC_StatConfigs[i].EquippedGear.Head.ItemName);
                //Chest
                saveData.AllPCs_ChestSlotItemName.Add(PC_StatConfigs[i].EquippedGear.Chest == null ? "" : PC_StatConfigs[i].EquippedGear.Chest.ItemName);
                //Waist
                saveData.AllPCs_WaistSlotItemName.Add(PC_StatConfigs[i].EquippedGear.Waist == null ? "" : PC_StatConfigs[i].EquippedGear.Waist.ItemName);
                //Legs
                saveData.AllPCs_LegsSlotItemName.Add(PC_StatConfigs[i].EquippedGear.Legs == null ? "" : PC_StatConfigs[i].EquippedGear.Legs.ItemName);
                //Feet
                saveData.AllPCs_FeetSlotItemName.Add(PC_StatConfigs[i].EquippedGear.Feet == null ? "" : PC_StatConfigs[i].EquippedGear.Feet.ItemName);
                //Adorn_1
                saveData.AllPCs_Adorn_1SlotItemName.Add(PC_StatConfigs[i].EquippedGear.Adorn_1 == null ? "" : PC_StatConfigs[i].EquippedGear.Adorn_1.ItemName);
                //Adorn_2
                saveData.AllPCs_Adorn_2SlotItemName.Add(PC_StatConfigs[i].EquippedGear.Adorn_2 == null ? "" : PC_StatConfigs[i].EquippedGear.Adorn_2.ItemName);
                //Weapon
                saveData.AllPCs_WeaponSlotItemName.Add(PC_StatConfigs[i].EquippedGear.Weapon == null ? "" : PC_StatConfigs[i].EquippedGear.Weapon.ItemName);


                //KNOWN ABILITIES:
                List<string> abilityList = new List<string>();

                for (int j = 0; j < PC_StatConfigs[i].KnownAbilities.Count; j++)
                {
                    if (PC_StatConfigs[i].KnownAbilities[j] == null)
                    {
                        Debug.LogWarning("Null entry in KnownAbilities of " + PC_StatConfigs[i].CharName);
                    }
                    else
                    {
                        abilityList.Add(PC_StatConfigs[i].KnownAbilities[j].name);
                    }
                }
                saveData.AllPCs_KnownAbilities.Add(abilityList);

                //DAMAGE SHIELDS:
                List<string> dmgShieldList = new List<string>();

                for (int j = 0; j < PC_StatConfigs[i].DamageShields.Count; j++)
                {
                    if (PC_StatConfigs[i].DamageShields[j] == null)
                    {
                        Debug.LogWarning("Null entry in DamageShields of " + PC_StatConfigs[i].CharName);
                    }
                    else
                    {
                        dmgShieldList.Add(PC_StatConfigs[i].DamageShields[j].name);
                    }
                }
                saveData.AllPCs_ActiveDamageShields.Add(dmgShieldList);

                //STRENGTHS / WEAKNESSES
                //Weak
                List<DamageType> weaknesses = new List<DamageType>();

                for (int j = 0; j < PC_StatConfigs[i].WeakAgainst.Count; j++)
                {
                    weaknesses.Add(PC_StatConfigs[i].WeakAgainst[j]);
                }
                saveData.ALLPCs_WeakAgainst.Add(weaknesses);

                //Strong
                List<DamageType> strengths = new List<DamageType>();

                for (int j = 0; j < PC_StatConfigs[i].StrongAgainst.Count; j++)
                {
                    strengths.Add(PC_StatConfigs[i].StrongAgainst[j]);
                }
                saveData.ALLPCs_StrongAgainst.Add(strengths);

                //Attuned
                List<DamageType> attuned = new List<DamageType>();

                for (int j = 0; j < PC_StatConfigs[i].AttunedTo.Count; j++)
                {
                    attuned.Add(PC_StatConfigs[i].AttunedTo[j]);
                }
                saveData.ALLPCs_AttunedTo.Add(attuned);
            }

            return saveData;
        }

        public void RestoreState(object state)
        {
            //print("Restoring PC Data ");
            //aliases
            var IMI = InventoryManager.Instance;

            //cast state as PCManagerSaveData
            PCManagerSaveData saveData = (PCManagerSaveData)state;

            //create a list of ALL PCs (both in party and not)
            List<GameObject> AllPCPrefabs = GetAllPCPrefabs();

            //Restore "IsAvailable" && "Required Party Member" info for AcquiredTeamMembers
            for (int i = 0; i < AcquiredTeamMembers.Count; i++)
            {
                if (i < saveData.AllPCs_IsAvailable.Count)
                {
                    AcquiredTeamMembers[i].IsAvailable = saveData.AllPCs_IsAvailable[i];
                    AcquiredTeamMembers[i].RequiredPartyMember = saveData.AllPCs_IsRequiredInParty[i];
                }
                else
                {
                    Debug.LogWarning("failed to restore available/required Data for " + AcquiredTeamMembers[i].PC_Name);
                }
            }

            //Restore Lvl, XP, Stats, etc...
            for (int i = 0; i < PC_StatConfigs.Count; i++)
            {
                //SlotPosition:
                PC_StatConfigs[i].PC_Slot_position = saveData.AllPCs_SlotPositions[i];
                //Lvl:
                PC_StatConfigs[i].SetCurLvl(saveData.AllPCs_Lvl[i]);
                //XP:
                PC_StatConfigs[i].SetCurXPValue(saveData.AllPCs_CurrentXP[i]);

                //STATS:
                //HP
                //Debug.Log("restoring stats from save:"); //changed calculations 1/18/22 make sure no issues 
                PC_StatConfigs[i].ModifyMaxHP(saveData.AllPCs_MaxHP[i] - PC_StatConfigs[i].MaxHP);
                PC_StatConfigs[i].ModifyCurrentHP(saveData.AllPCs_CurHP[i] - PC_StatConfigs[i].CurHP);
                //MP
                PC_StatConfigs[i].ModifyMaxMP(saveData.AllPCs_MaxMP[i] - PC_StatConfigs[i].MaxMP);
                PC_StatConfigs[i].ModifyCurrentMP(saveData.AllPCs_CurMP[i] - PC_StatConfigs[i].CurMP);
                //TP
                PC_StatConfigs[i].ModifyMaxTP(saveData.AllPCs_MaxTP[i] - PC_StatConfigs[i].MaxTP);
                PC_StatConfigs[i].ModifyCurrentTP(saveData.AllPCs_CurTP[i] - PC_StatConfigs[i].CurTP);
                //STR
                PC_StatConfigs[i].ModifyStat(StatAbbreviation.STR, (saveData.AllPCs_STR[i] - PC_StatConfigs[i].STR));
                //INT
                PC_StatConfigs[i].ModifyStat(StatAbbreviation.INT, (saveData.AllPCs_INT[i] - PC_StatConfigs[i].INT));
                //AGI
                PC_StatConfigs[i].ModifyStat(StatAbbreviation.AGI, (saveData.AllPCs_AGI[i] - PC_StatConfigs[i].AGI));
                //DEF
                PC_StatConfigs[i].ModifyStat(StatAbbreviation.DEF, (saveData.AllPCs_DEF[i] - PC_StatConfigs[i].DEF));
                //DmgRed
                PC_StatConfigs[i].ModifyStat(StatAbbreviation.DmgRed, (saveData.AllPCs_DmgRed[i] - PC_StatConfigs[i].DmgReduction));
                //AtkBonus
                PC_StatConfigs[i].ModifyStat(StatAbbreviation.AtkBonus, (saveData.AllPCs_AtkBonus[i] - PC_StatConfigs[i].BaseAtkBonus));
                //DmgBonus
                PC_StatConfigs[i].ModifyStat(StatAbbreviation.DmgBonus, (saveData.AllPCs_DmgBonus[i] - PC_StatConfigs[i].DamageBonus));
                //InitMod
                PC_StatConfigs[i].ModifyStat(StatAbbreviation.InitMod, (saveData.AllPCs_InitMod[i] - PC_StatConfigs[i].InitiativeMod));
                //MagRes
                PC_StatConfigs[i].AdjustMagicResist(saveData.ALlPCs_MagRes[i] - PC_StatConfigs[i].MagRes);


                //EQUIPMENT:
                //Head
                if (saveData.AllPCs_HeadSlotItemName[i] != "")
                {
                    PC_StatConfigs[i].EquippedGear.Head = (EquipableItem)IMI.PointToDatabaseInstance(saveData.AllPCs_HeadSlotItemName[i]);
                }
                else
                {
                    PC_StatConfigs[i].EquippedGear.Head = null;
                }
                //Chest
                if (saveData.AllPCs_ChestSlotItemName[i] != "")
                {
                    PC_StatConfigs[i].EquippedGear.Chest = (EquipableItem)IMI.PointToDatabaseInstance(saveData.AllPCs_ChestSlotItemName[i]);
                }
                else
                {
                    PC_StatConfigs[i].EquippedGear.Chest = null;
                }
                //Waist
                if (saveData.AllPCs_WaistSlotItemName[i] != "")
                {
                    PC_StatConfigs[i].EquippedGear.Waist = (EquipableItem)IMI.PointToDatabaseInstance(saveData.AllPCs_WaistSlotItemName[i]);
                }
                else
                {
                    PC_StatConfigs[i].EquippedGear.Waist = null;
                }
                //Legs
                if (saveData.AllPCs_LegsSlotItemName[i] != "")
                {
                    PC_StatConfigs[i].EquippedGear.Legs = (EquipableItem)IMI.PointToDatabaseInstance(saveData.AllPCs_LegsSlotItemName[i]);
                }
                else
                {
                    PC_StatConfigs[i].EquippedGear.Legs = null;
                }
                //Feet
                if (saveData.AllPCs_FeetSlotItemName[i] != "")
                {
                    PC_StatConfigs[i].EquippedGear.Feet = (EquipableItem)IMI.PointToDatabaseInstance(saveData.AllPCs_FeetSlotItemName[i]);
                }
                else
                {
                    PC_StatConfigs[i].EquippedGear.Feet = null;
                }
                //Adorn_1
                if (saveData.AllPCs_Adorn_1SlotItemName[i] != "")
                {
                    PC_StatConfigs[i].EquippedGear.Adorn_1 = (EquipableItem)IMI.PointToDatabaseInstance(saveData.AllPCs_Adorn_1SlotItemName[i]);
                }
                else
                {
                    PC_StatConfigs[i].EquippedGear.Adorn_1 = null;
                }
                //Adorn_2
                if (saveData.AllPCs_Adorn_2SlotItemName[i] != "")
                {
                    PC_StatConfigs[i].EquippedGear.Adorn_2 = (EquipableItem)IMI.PointToDatabaseInstance(saveData.AllPCs_Adorn_2SlotItemName[i]);
                }
                else
                {
                    PC_StatConfigs[i].EquippedGear.Adorn_2 = null;
                }
                //Weapon
                if (saveData.AllPCs_WeaponSlotItemName[i] != "")
                {
                    PC_StatConfigs[i].EquippedGear.Weapon = (EquipableItem)IMI.PointToDatabaseInstance(saveData.AllPCs_WeaponSlotItemName[i]);
                }
                else
                {
                    PC_StatConfigs[i].EquippedGear.Weapon = null;
                }

                //ABILITIES:
                PC_StatConfigs[i].KnownAbilities.Clear();
                //PC_StatConfigs[i].KnownAbilities.RemoveAll(n => n == null);
                for (int j = 0; j < saveData.AllPCs_KnownAbilities[i].Count; j++)
                {
                    var abil = Resources.Load<BaseCommand_Config>(saveData.AllPCs_KnownAbilities[i][j]);

                    if (abil == null)
                    {
                        Debug.LogWarning("tried to load saved ability: " + saveData.AllPCs_KnownAbilities[i][j] + " : ability was null");
                    }
                    else
                    {
                        PC_StatConfigs[i].KnownAbilities.Add(abil);
                    }
                }

                //DAMAGE SHIELDS:
                PC_StatConfigs[i].DamageShields.Clear();
                PC_StatConfigs[i].DamageShields.RemoveAll(n => n == null);
                for (int j = 0; j < saveData.AllPCs_ActiveDamageShields[i].Count; j++)
                {
                    var ds = Resources.Load<DamageShield>(saveData.AllPCs_ActiveDamageShields[i][j]);
                    if (ds == null)
                    {
                        Debug.LogWarning("tried to load saved damage shield: " + saveData.AllPCs_ActiveDamageShields[i][j] + " : damage shield was null");
                    }
                    else
                    {
                        PC_StatConfigs[i].DamageShields.Add(ds);
                    }
                }

                //STRENGTHS / WEAKNESSES
                PC_StatConfigs[i].WeakAgainst.Clear();
                for (int j = 0; j < saveData.ALLPCs_WeakAgainst[i].Count; j++)
                {
                    PC_StatConfigs[i].WeakAgainst.Add(saveData.ALLPCs_WeakAgainst[i][j]);
                }

                PC_StatConfigs[i].StrongAgainst.Clear();
                for (int j = 0; j < saveData.ALLPCs_StrongAgainst[i].Count; j++)
                {
                    PC_StatConfigs[i].StrongAgainst.Add(saveData.ALLPCs_StrongAgainst[i][j]);
                }

                PC_StatConfigs[i].AttunedTo.Clear();
                for (int j = 0; j < saveData.ALLPCs_AttunedTo[i].Count; j++)
                {
                    PC_StatConfigs[i].AttunedTo.Add(saveData.ALLPCs_AttunedTo[i][j]);
                }
            }

            //Clear out the PC Prefabs in PCsInParty_Prefabs
            Array.Clear(PCsInParty_Prefabs, 0, PCsInParty_Prefabs.Length);

            //Use the saveData to put the saved PCs into the party in their proper slots
            for (int i = 0; i < AllPCPrefabs.Count; i++)
            {
                for (int j = 0; j < saveData.PCsInParty_Names.Count; j++)
                {
                    if (AllPCPrefabs[i] != null && AllPCPrefabs[i].GetComponent<PlayerCharacter>().CharName == saveData.PCsInParty_Names[j])
                    {
                        PCsInParty_Prefabs[j] = AllPCPrefabs[i];
                    }
                }
            }

            //if we are in the combat scene, load in the PCs
            if (SceneManager.GetActiveScene().name == "CombatScene")
            {
                LoadPCsIntoCombatScene();
            }
        }

        //returns a list of all PC prefabs in the game (in party and not)
        List<GameObject> GetAllPCPrefabs()
        {
            List<GameObject> listOfPCs = new List<GameObject>();

            foreach (SelectablePC PC in AcquiredTeamMembers)
            {
                if (PC.PC_Prefab == null)
                {
                    Debug.LogWarning("Error: null entry in AcquiredTeamMembers");
                    continue;
                }
                listOfPCs.Add(PC.PC_Prefab);
            }

            return listOfPCs;
        }
        #endregion
    }
}