using System.Collections.Generic;
using RPG.Saving;
using UnityEngine;

//Last Reviewed: 2/26/20
namespace RPG.CombatSystem
{
    //attached to PCManager in the BootScene.
    //manages a List of ProcContainers (one container per PC).
    //each container holds a list of active procs for a specific PC.
    //also responsible for saving / loading PC proc data.
    public class PC_ProcManager : Singleton<PC_ProcManager>, ISaveable
    {
        [SerializeField] List<ProcContainer> PC_ProcContainers = new List<ProcContainer>(); //Instantiated ScriptableObject for each PC to track their active procs; created in Awake()
        [SerializeField] GameObject[] PC_Prefabs = new GameObject[9]; //reference to the 9 PC prefabs


        protected override void Awake()
        {
            base.Awake();
            CreateProcContainersForAllPCs();
        }

        void Start()
        {
            SavingWrapper.Instance.StartingNewGame += ResetPCProcContainers;
        }

        void OnDisable()
        {
            if (SavingWrapper.InstanceExists)
            {
                SavingWrapper.Instance.StartingNewGame -= ResetPCProcContainers;
            }
        }

        void CreateProcContainersForAllPCs()
        {
            //start with an empty List
            PC_ProcContainers.Clear();

            //loop through the 9 PC prefabs...
            for (int i = 0; i < PC_Prefabs.Length; i++)
            {
                //create a new, empty proc container for the PC
                CreatePCProcContainer(PC_Prefabs[i]);
            }
        }

        void CreatePCProcContainer(GameObject PC_Prefab)
        {
            //grab a reference to the PlayerCharacter so we can access the PC's name...
            PlayerCharacter PCScript = PC_Prefab.GetComponent<PlayerCharacter>();
            //instantiate the container...
            ProcContainer cont = (ProcContainer)ScriptableObject.CreateInstance(typeof(ProcContainer));
            //name the container for the PC and set the UniqueID...
            cont.name = PCScript.CharName + "_ProcContainer";
            cont.UniqueID = PCScript.CharName;
            //add the new container to the List
            PC_ProcContainers.Add(cont);
        }

        //returns a ProcContainer for a specific PC
        public ProcContainer GetPCProcContainerInstance(string PCName)
        {
            for (int i = 0; i < PC_ProcContainers.Count; i++)
            {
                if (PC_ProcContainers[i].UniqueID == PCName)
                {
                    return PC_ProcContainers[i];
                }
            }
            Debug.Log("No matching ProcContainer for " + PCName);
            return null;
        }

        //at New Game start...
        public void ResetPCProcContainers()
        {
            foreach (ProcContainer cont in PC_ProcContainers)
            {
                cont.ClearAllProcs();
            }
        }

        public void AddProcToPC(string PCName, Proc proc)
        {
            GetPCProcContainerInstance(PCName).AddProcToList(proc);
        }

        public void RemoveProcFromPC(string PCName, Proc proc)
        {
            GetPCProcContainerInstance(PCName).RemoveProcFromList(proc);
        }

        #region SaveData
        [System.Serializable]
        class PCProcSaveData
        {
            //save active procs
            public List<List<string>> AllPCs_ActiveProcs = new List<List<string>>();
            public List<List<float>> AllPCs_ActiveProc_ProcChance = new List<List<float>>();
        }

        public object CaptureState()
        {
            PCProcSaveData saveData = new PCProcSaveData();

            //SAVE ACTIVE PROCS:
            for (int i = 0; i < PC_ProcContainers.Count; i++)
            {
                List<string> procList = new List<string>();
                List<float> chanceList = new List<float>();

                for (int j = 0; j < PC_ProcContainers[i].ActiveProcs.Count; j++)
                {
                    procList.Add(PC_ProcContainers[i].ActiveProcs[j].spell_Config.name);
                    chanceList.Add(PC_ProcContainers[i].ActiveProcs[j].procChance);
                }

                saveData.AllPCs_ActiveProcs.Add(procList);
                saveData.AllPCs_ActiveProc_ProcChance.Add(chanceList);
            }

            return saveData;
        }

        public void RestoreState(object state)
        {
            PCProcSaveData saveData = (PCProcSaveData)state;

            //PROCS
            for (int i = 0; i < PC_ProcContainers.Count; i++)
            {
                PC_ProcContainers[i].ClearAllProcs();
                for (int j = 0; j < saveData.AllPCs_ActiveProcs[i].Count; j++)
                {
                    BaseSpell_Config spellConfig = Resources.Load<BaseSpell_Config>(saveData.AllPCs_ActiveProcs[i][j]);
                    float procChance = saveData.AllPCs_ActiveProc_ProcChance[i][j];
                    Proc newProc = new Proc(spellConfig.CmndName, spellConfig, procChance);
                    PC_ProcContainers[i].AddProcToList(newProc);
                }
            }
        }
        #endregion
    }
}
