using System;
using UnityEngine;

//Last Reviewed: 2/23/20
namespace RPG.CombatSystem
{
    public enum BattlefieldEnvironments { Ship, Mountains, Forest, Bridge, Forest_v2, OrcFortress, AncientRuins }

    //this class lives in the CombatScene. Start(), sets the battlefield based on the chosen encounter
    public class BattlefieldEnvironmentManager : Singleton<BattlefieldEnvironmentManager>
    {
        //each "Environment" consists of terrain and props that make up the battlefield.
        //Note: the names of these GameObjects must match a string reference in SetBattlefieldEnvironment().
        [SerializeField] GameObject[] Environments = null;
        [SerializeField] Light mainDirectionalLight = null;
        [SerializeField] Material nightSkyBox = null;


        void Start()
        {
            SetBattlefieldEnvironment(EnemyManager.Instance.ChosenEncounter.BattleEnvironment);
        }

        //Temporary debug//////////////////////////////////
        public void ChangeBattlefieldBackground()
        {
            if (Environments[Environments.Length - 1].activeSelf)
            {
                Environments[Environments.Length - 1].SetActive(false);
                Environments[0].SetActive(true);
                return;
            }

            for (int i = 0; i < Environments.Length; i++)
            {
                if (Environments[i].activeSelf)
                {
                    Environments[i].SetActive(false);
                    Environments[i + 1].SetActive(true);
                    return;
                }
            }
        }
        ////////////////////////////////////////////////////

        //show a specific environment
        public void SetBattlefieldEnvironment(BattlefieldEnvironments env)
        {
            switch (env)
            {
                case BattlefieldEnvironments.Ship:
                    ActivateSpecificEnvironment("Ship"); //name of environment gameObject
                    break;
                case BattlefieldEnvironments.Mountains:
                    ActivateSpecificEnvironment("Mountains"); //name of environment gameObject
                    break;
                case BattlefieldEnvironments.Forest:
                    ActivateSpecificEnvironment("Forest"); //name of environment gameObject
                    break;
                case BattlefieldEnvironments.Bridge:
                    ActivateSpecificEnvironment("Bridge");
                    break;
                case BattlefieldEnvironments.Forest_v2:
                    ActivateSpecificEnvironment("Forest_v2");
                    break;
                case BattlefieldEnvironments.OrcFortress:
                    ActivateSpecificEnvironment("OrcFortress");
                    break;
                case BattlefieldEnvironments.AncientRuins:
                    ActivateSpecificEnvironment("AncientRuins");
                    break;
                default:
                    ActivateSpecificEnvironment("Mountains");
                    break;
            }

            if (EnemyManager.Instance.ChosenEncounter.AtNight)
            {
                ChangeSkyBoxAndLights();
            }
        }

        void ChangeSkyBoxAndLights()
        {
            mainDirectionalLight.intensity = 0;
            RenderSettings.skybox = nightSkyBox;
        }

        void ActivateSpecificEnvironment(string envName)
        {
            foreach (GameObject env in Environments)
            {
                if (env.name == envName)
                {
                    env.SetActive(true);
                }
                else
                {
                    env.SetActive(false);
                }
            }
        }
    }
}
