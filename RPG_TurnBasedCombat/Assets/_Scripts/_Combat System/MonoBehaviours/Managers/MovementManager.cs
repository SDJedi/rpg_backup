using System.Collections;
using UnityEngine;

//Last Reviewed: 2/23/20
namespace RPG.CombatSystem
{
    //The primary job of this class is to move characters from point a to point b. Since move, then attack is such a common
    //operation, this class also has a couple of methods that trigger animations
    public class MovementManager : Singleton<MovementManager>
    {
        const float defaultMoveBackSpeed = 5;
        const float defaultMoveFwdSpeed = 5;

        public Transform[] PCStartingPositions = new Transform[4];


        void Start()
        {
            PlaceAllPCsAccordingToTheirSlotPosition();
        }

        //Move any character to its startPosition
        public IEnumerator MoveACharacterToItsHomePosition(GameObject character, float speed = 0)
        {
            /////////////////////Error Checking///////////////////////
            if (character == null)
            {
                Debug.LogWarning("character was null ... cannot move to start pos");
                yield break;
            }
            //////////////////////////////////////////////////////////
            if (speed <= 0)
            {
                speed = defaultMoveBackSpeed;
            }

            //check if PC or enemy
            Transform homePos;
            if (character.GetComponent<PlayerCharacter>() != null)
            {
                homePos = PCStartingPositions[((PC_Stats_Config)character.GetComponent<PlayerCharacter>().Stats_Config).PC_Slot_position];
            }
            else
            {
                homePos = character.GetComponent<EnemyCharacter>().homePos;
            }

            yield return MoveCharacter(character, homePos, true, speed, false);
        }

        //Move each PC to a PCStartingPosition based on each PCs UI_Config.PC_Slot_position
        public IEnumerator MoveAllPCsToStartPositions(float speed = 0)
        {
            if (speed <= 0)
            {
                speed = defaultMoveBackSpeed;
            }

            foreach (GameObject PC in PCManager.Instance.PCsInParty)
            {
                if (PC != null)
                {
                    PlayerCharacter PCScript = PC.GetComponent<PlayerCharacter>();
                    if (PCScript != null)
                    {
                        int posIndex = ((PC_Stats_Config)PCScript.Stats_Config).PC_Slot_position;
                        yield return MoveCharacter(PC, PCStartingPositions[posIndex], true, speed, false);
                    }
                }
            }
        }

        //moves a character to a destination; upon arrival sets an animation trigger
        public IEnumerator MoveCharacterThenPlayAnimation(GameObject character, Transform destination, Transform lookAtTarget, float speed, Animator anim, string animTrigger)
        {
            Character CScript = character.GetComponent<Character>();
            if (CScript.IsAlive)
            {
                character.transform.LookAt(lookAtTarget);
                yield return MoveCharacter(character, destination, false, speed);
                character.transform.LookAt(lookAtTarget);
                yield return new WaitForSeconds(0.1f);
                anim.ResetTrigger("Hit");
                anim.SetTrigger(animTrigger);

                //wait for animation to finish...
                yield return WaitForAnimationToFinish(CScript, anim, animTrigger);
            }
        }

        IEnumerator WaitForAnimationToFinish(Character CScript, Animator anim, string animTrigger)
        {
            float maxWaitTime = 3;
            yield return new WaitForSeconds(0.5f);
            int animState = anim.GetCurrentAnimatorStateInfo(0).fullPathHash;

            while (CScript.IsAlive && anim != null && animState == anim.GetCurrentAnimatorStateInfo(0).fullPathHash)
            {
                maxWaitTime -= Time.deltaTime;
                if (maxWaitTime <= 0)
                {
                    Debug.LogWarning("error with wait for animation code ... exceeded 3 second wait.");
                    yield break;
                }
                yield return new WaitForEndOfFrame();
            }
        }

        //move an object over time. If object has <Character> component, sets "IsMoving" and animator variables
        public IEnumerator ScoochObjectAlongFwdAxis(GameObject obj, float distance, float speed = defaultMoveFwdSpeed, float pauseAfterScooch = 0)
        {
            var d = distance;
            var CScript = obj.GetComponent<Character>();
            Animator anim = obj.GetComponentInChildren<Animator>();

            if (d == 0)
            {
                yield break;
            }

            if (d > 0)
            {
                if (CScript != null)
                {
                    if (!CScript.IsAlive)
                    {
                        yield break;
                    }

                    anim.SetBool("IsMoving_Fwd", true);
                    anim.SetBool("IsMoving_Bwd", false);
                }

                while (d > 0)
                {
                    obj.transform.Translate(Vector3.forward * speed * Time.deltaTime, Space.Self);
                    d -= (speed * Time.deltaTime);
                    yield return new WaitForEndOfFrame();
                }

                if (CScript != null)
                {
                    anim.SetBool("IsMoving_Fwd", false);
                    anim.SetBool("IsMoving_Bwd", false);
                }
            }
            else
            {
                if (CScript != null)
                {
                    if (!CScript.IsAlive)
                    {
                        yield break;
                    }

                    anim.SetBool("IsMoving_Fwd", false);
                    anim.SetBool("IsMoving_Bwd", true);
                }

                while (d < 0)
                {
                    obj.transform.Translate(-Vector3.forward * speed * Time.deltaTime, Space.Self);
                    d += (speed * Time.deltaTime);
                    yield return new WaitForEndOfFrame();
                }

                if (CScript != null)
                {
                    anim.SetBool("IsMoving_Fwd", false);
                    anim.SetBool("IsMoving_Bwd", false);
                }

                if (pauseAfterScooch > 0)
                {
                    yield return new WaitForSeconds(pauseAfterScooch);
                }
            }
        }

        //Does the actual work of moving a character to a new location over time
        IEnumerator MoveCharacter(GameObject character, Transform pos, bool matchDestinationRotation, float speed = 0, bool movingFwd = true)
        {
            Character CScript = character.GetComponent<Character>();

            if (CScript == null || !CScript.IsAlive)
            {
                yield break;
            }

            while (TurnManager.Instance.CounterDamageInProgress)
            {
                yield return new WaitForEndOfFrame();
            }

            Animator anim = character.GetComponentInChildren<Animator>();

            if (speed <= 0)
            {
                speed = defaultMoveFwdSpeed;
                Debug.LogWarning("No move speed set for " + character + " defaulting to " + defaultMoveFwdSpeed);
            }
            else
            {
                if (movingFwd)
                {
                    anim.SetBool("IsMoving_Fwd", true);
                    anim.SetBool("IsMoving_Bwd", false);
                }
                else
                {
                    anim.SetBool("IsMoving_Bwd", true);
                    anim.SetBool("IsMoving_Fwd", false);
                }

                Vector3 targetPos = pos.position;

                while (character != null && CScript.IsAlive && Vector3.Distance(character.transform.position, targetPos) > 0.01f)
                {
                    character.transform.position = Vector3.MoveTowards(character.transform.position, targetPos, speed * Time.deltaTime);
                    yield return new WaitForEndOfFrame();
                }

                anim.SetBool("IsMoving_Fwd", false);
                anim.SetBool("IsMoving_Bwd", false);

                if (matchDestinationRotation)
                {
                    MatchRotations(character.transform, pos);
                }
            }
        }

        void PlaceAllPCsAccordingToTheirSlotPosition()
        {
            foreach (GameObject PC in PCManager.Instance.PCsInParty)
            {
                if (PC != null)
                {
                    int posIndex = ((PC_Stats_Config)PC.GetComponent<PlayerCharacter>().Stats_Config).PC_Slot_position;
                    if (posIndex < PCStartingPositions.Length)
                    {
                        PC.transform.position = PCStartingPositions[posIndex].position;
                    }
                    else
                    {
                        Debug.LogWarning(PC + " PlayerCharacter.cs has an invalid PC_Slot_position");
                    }
                }
            }
        }

        //match the rotation of a Transform with that of a different Transform
        void MatchRotations(Transform matchThis, Transform withThat)
        {
            matchThis.rotation = withThat.rotation;
        }
    }
}
