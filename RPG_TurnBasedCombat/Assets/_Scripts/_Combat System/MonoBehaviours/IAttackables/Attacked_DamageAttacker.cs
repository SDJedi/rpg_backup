using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Last Reviewed: 2/22/20
namespace RPG.CombatSystem
{
    //this class keeps a List of all active DamageShields on this character and
    //processes them OnHit()
    public class Attacked_DamageAttacker : MonoBehaviour, IAttackable
    {
        [SerializeField] List<DamageShield> damageShields = null;

        Character CScript;
        bool damagingAttacker = false;


        void Start()
        {
            CScript = GetComponent<Character>();
        }

        void OnDestroy()
        {
            //destroy the damageShield instances we created
            for (int i = damageShields.Count - 1; i >= 0; i--)
            {
                Destroy(damageShields[i]);
            }
        }

        //the entry point for adding damageShields to a character. can be called during combat (i.e. as buffs),
        //but is also called when CombatScene is first loaded to apply any inate damageShields
        //to PCs and Enemies
        public void AddDamageShieldToIAttackable(DamageShield ds)
        {
            if (ds.nameOfDmgShield == "")
            {
                Debug.LogWarning("DamageShield " + ds + " needs a name to be removed properly... not adding");
                return;
            }
            damageShields.Add(Instantiate(ds));
            damageShields[damageShields.Count - 1].damageConfig = Instantiate(damageShields[damageShields.Count - 1].damageConfig);
            damageShields[damageShields.Count - 1].damageConfig.AttackCanMiss = false;
            damageShields[damageShields.Count - 1].damageConfig.AtkType = AttackType.DamageShield;
            damageShields[damageShields.Count - 1].damageConfig.CritChance = 0;
        }

        //called when a DamageShield that was added as a buff expires
        public void RemoveADamageShieldFromIAttackable(DamageShield ds)
        {
            for (int i = damageShields.Count - 1; i >= 0; i--)
            {
                if (damageShields[i].nameOfDmgShield == ds.nameOfDmgShield)
                {
                    damageShields.RemoveAt(i);
                    return;
                }
            }
        }

        //process DamageShields in the List...
        public void OnHit(Damage_Config damageConfig, Attack incomingAttack, Base_Stats_Config attacker, Base_Stats_Config defender, bool triggerProcs)
        {
            if (!this.enabled)
            {
                return;
            }

            //check conditions:
            if (CScript.IsAlive && //is alive...
                damageConfig.AtkType == AttackType.Melee && //the incoming attack was a melee attack
                CScript.Stats_Config == defender && //is the defender of the attack
                damageShields.Count >= 1)//has damage shield(s)
            {
                Character curHostileTarget = CScript.HostileTarget; //if we already have a hostile target, make a note of it
                CScript.SetHostileTarget(attacker.CScript); //change the hostile target to our attacker.
                StartCoroutine(ProcDamageShields(defender, attacker.CScript.gameObject, curHostileTarget));
            }
        }

        //foreach DamageShield, roll for proc, execute an attack if roll is good
        IEnumerator ProcDamageShields(Base_Stats_Config thisCharStats, GameObject attacker, Character prevHostileTarget)
        {
            damagingAttacker = true;
            StartCoroutine(SetBoolInTurnManager());

            //loop through each DamageShield in the List...
            for (int i = 0; i < damageShields.Count && CScript.IsAlive; i++)
            {
                float rollForProc = Random.value;
                if (rollForProc > damageShields[i].procChance)
                {
                    GenerateSpecialScrollingText.GenerateScrollingText(CScript.transform, "DamageShield: " + damageShields[i].nameOfDmgShield + " failed. (" + rollForProc.ToString("n2") + ")", Color.white);
                    yield return new WaitForSeconds(0.1f);
                    continue;
                }

                GenerateAttack.ExecuteAttack(damageShields[i].damageConfig, thisCharStats, attacker, false);
                //play FX...
                if (damageShields[i].particleEffectOnProc != null)
                {
                    var e = Instantiate(damageShields[i].particleEffectOnProc, attacker.transform);
                    ScaleObjectWithModel.MatchScale(attacker, e);
                }
                if (damageShields[i].soundOnProc != null)
                {
                    SoundManager.Instance.PlayAudioClip(damageShields[i].soundOnProc);
                }

                //wait between procs
                yield return new WaitForSeconds(0.1f);
            }

            //reset the hostile target
            CScript.SetHostileTarget(prevHostileTarget);
            damagingAttacker = false;
        }

        //makes sure TurnManager is aware that we are dealing with DamageShields
        IEnumerator SetBoolInTurnManager()
        {
            while (CScript.IsAlive && damagingAttacker)
            {
                TurnManager.Instance.DamageShieldsProcing = true;
                yield return null;
            }

            yield return new WaitForSeconds(0.1f);
            TurnManager.Instance.DamageShieldsProcing = false;
        }
    }
}
