using System.Collections;
using UnityEngine;

//Last Reviewed: 2/20/20
namespace RPG.CombatSystem
{
    //allows a character to respond to melee attacks with a single-target melee attack.
    //can enable/disable component to remove behaviour
    public class Attacked_CounterMeleeAttack : MonoBehaviour, IAttackable, ITriggerOnMissedAttack
    {
        const float minTimeBetweenCounters = 2;

        [SerializeField] Command_MeleeAttackSingleTarget CommandToCounterWith = null;

        Animator anim;
        Character CScript;
        GenerateAttackFromAnimationEvent gafae;
        float timeSinceLastCountered = Mathf.Infinity;
        bool countering;


        void Awake()
        {
            anim = GetComponentInChildren<Animator>();
            CScript = GetComponent<Character>();
            gafae = GetComponentInChildren<GenerateAttackFromAnimationEvent>();
        }

        void Update()
        {
            timeSinceLastCountered += Time.deltaTime;
        }

        //not in use as of 2/22/20, but should come in handy for upgrading counter attack during the game
        public void SetCommandToCounterWith(Command_MeleeAttackSingleTarget command)
        {
            CommandToCounterWith = command;
        }

        //counter can trigger OnHit and OnMiss (see OnMiss() below)
        public void OnHit(Damage_Config damageConfig, Attack incomingAttack, Base_Stats_Config attacker, Base_Stats_Config defender, bool triggerProcs)
        {
            if (CommandToCounterWith == null)
            {
                Debug.LogWarning("Need to set a CommandToCounterWith in inspector for " + CScript.Stats_Config.CharName);
                return;
            }

            if (OkToCounter(damageConfig, attacker, defender))
            {
                StartCoroutine(WaitForDamageShieldsThenCounter(damageConfig, attacker, defender));
            }
        }

        //in case a damage shield kills the attacker
        IEnumerator WaitForDamageShieldsThenCounter(Damage_Config damageConfig, Base_Stats_Config attacker, Base_Stats_Config defender)
        {
            //wait for damage shields to take effect
            yield return new WaitForSeconds(0.1f);
            while (TurnManager.Instance.DamageShieldsProcing)
            {
                yield return null;
            }

            //if we are still good to counter, do so...
            if (OkToCounter(damageConfig, attacker, defender))
            {
                yield return CounterAttack(attacker);
            }
            else if (!countering)
            {
                //let TurnManager know we're done with counter damage.
                TurnManager.Instance.MeleeCounterAttackProcing = false;
            }
        }

        //check CounterMeleeAttack conditions:
        bool OkToCounter(Damage_Config damageConfig, Base_Stats_Config attacker, Base_Stats_Config defender)
        {
            return this.enabled && //component is enabled
                timeSinceLastCountered >= minTimeBetweenCounters && //haven't countered in the last (x) seconds...
                CScript.IsAlive && //defender alive...
                attacker.IsAlive() && //attacker alive...
                damageConfig.AtkType == AttackType.Melee && //the incoming attack is a melee attack
                CScript.Stats_Config == defender && //this char is the defender of said attack
                TurnManager.Instance.ActingCharacter != CScript; //this char is not the acting character
        }

        IEnumerator CounterAttack(Base_Stats_Config attacker)
        {
            //let TurnManager know we're dealing with counter damage.
            countering = true;
            TurnManager.Instance.MeleeCounterAttackProcing = true;
            Character curHostileTarget = CScript.HostileTarget; //if we already have a hostile target, make a note of it
            CScript.SetHostileTarget(attacker.CScript); //change the hostile target to our attacker
            gafae.Setup_GAFAE(CommandToCounterWith, CommandToCounterWith.DamageConfig, CommandToCounterWith.ParticleEffect, CScript.HostileTarget.transform);
            anim.SetTrigger(CommandToCounterWith.AnimatorTriggerName); //play the animation, which will call events that deal damage / fire procs
            timeSinceLastCountered = 0; //reset the timer
            yield return WaitForAnimationThenResetHostileTarget(curHostileTarget); //reset the hostile target
        }

        IEnumerator WaitForAnimationThenResetHostileTarget(Character hosTarget)
        {
            //wait half a second to ensure we are in the correct animation (Attack usually, but determined by CommandToCounterWith.AnimatorTriggerName)
            yield return new WaitForSeconds(0.5f);

            //set a timer based on the animation clip
            float animTime = 2f;
            if (anim.GetCurrentAnimatorClipInfoCount(0) > 0)
            {
                animTime = (Mathf.Max(anim.GetCurrentAnimatorClipInfo(0)[0].clip.length - 0.5f, animTime));
            }

            anim.ResetTrigger("Hit"); //"Hit" trigger gets stuck if we don't reset it since we interrupt the hit animation above

            while (animTime > 0)
            {
                //adjust the timer.
                animTime -= Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }

            yield return new WaitForSeconds(0.1f);

            //reset the hostile target of the defender
            CScript.SetHostileTarget(hosTarget);

            //let TurnManager know we're done with counter damage.
            TurnManager.Instance.MeleeCounterAttackProcing = false;
            countering = false;
        }

        //do same as OnHit()
        public void OnMiss(Damage_Config damageConfig, Attack incomingAttack, Base_Stats_Config attacker, Base_Stats_Config defender, bool triggerProcs)
        {
            OnHit(damageConfig, incomingAttack, attacker, defender, triggerProcs);
        }
    }
}
