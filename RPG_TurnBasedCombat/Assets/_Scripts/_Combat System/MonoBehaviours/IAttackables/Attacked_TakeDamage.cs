using UnityEngine;

//Last Reviewed: 1/21/22
namespace RPG.CombatSystem
{
    //deal damage to a character OnHit() and add to the combat log
    public class Attacked_TakeDamage : MonoBehaviour, IAttackable
    {
        Character CScript;
        Color32 defaultHitColor = new Color32(255, 0, 0, 255); //red
        Color32 healColor = new Color32(0, 255, 0, 255); //green
        Color32 critColor = new Color32(190, 85, 255, 255); //purple


        void Awake()
        {
            CScript = GetComponent<Character>();
        }

        public void OnHit(Damage_Config damageConfig, Attack attack, Base_Stats_Config attacker, Base_Stats_Config defender, bool triggerProcs)
        {
            //update combat log
            UpdateCombatLog(damageConfig, attack, attacker, defender);

            //modify HP
            Base_Stats_Config stats = CScript.Stats_Config;
            stats.ModifyCurrentHP(-attack.Damage);

            //call Death() if HP <= 0
            if (stats.CurHP <= 0)
            {
                CombatLogManager.Instance.AddToCombatLog(attacker.CharName + " has killed " + defender.CharName + "!", new Color32(255, 255, 255, 255));
                Death();
            }
        }

        //add an entry into the combat log
        void UpdateCombatLog(Damage_Config damageConfig, Attack attack, Base_Stats_Config attacker, Base_Stats_Config defender)
        {
            Color32 color = DetermineTextColor(attack);
            string hitMssg = DetermineHitMssg(damageConfig, attack, attacker, defender);
            CombatLogManager.Instance.AddToCombatLog(hitMssg, color);
        }

        //trigger OnDeath() on all IDestructibles
        void Death()
        {
            CScript.ClearAllTargets();
            var destructibles = GetComponents(typeof(IDestructible));

            foreach (IDestructible d in destructibles)
            {
                d.OnDeath();
            }
        }

        //returns the color we want the combat log message to show up in
        Color32 DetermineTextColor(Attack attack)
        {
            if (attack.Damage < 0)
            {
                return healColor;
            }
            else if (attack.IsCritical)
            {
                return critColor;
            }
            else
            {
                return defaultHitColor;
            }
        }

        //returns the hit message
        string DetermineHitMssg(Damage_Config damageConfig, Attack attack, Base_Stats_Config attacker, Base_Stats_Config defender)
        {
            if (attack.IsCritical)
            {
                return attacker.CharName + " hit " + defender.CharName + " with a critical " + damageConfig.AtkType + " attack for {" + attack.Damage + "} points of " + damageConfig.DmgType + " damage.";
            }
            else
            {
                return attacker.CharName + " hit " + defender.CharName + " with a " + damageConfig.AtkType + " attack for {" + attack.Damage + "} points of " + damageConfig.DmgType + " damage.";
            }
        }
    }
}