using UnityEngine;

//Last Reviewed: 2/22/20
namespace RPG.CombatSystem
{
    //show the damage inflicted OnHit() as a scrolling text
    public class Attacked_ScrollingText : MonoBehaviour, IAttackable
    {
        public ScrollingText Text; //the ScrollingText prefab
        public Color HitColor;
        public Color CritColor;
        public Color HealHPColor;
        public float Y_Offset = 1.5f;
        private float x_Offset = -0.7f;


        public void OnHit(Damage_Config damageConfig, Attack attack, Base_Stats_Config attacker, Base_Stats_Config defender, bool triggerProcs)
        {
            //if defender is attuned to damage type, the attack can heal
            bool attackHealedDefender = false;
            if (attack.Damage < 0)
            {
                attackHealedDefender = true;
            }

            //create a string for the damage amount
            string text = attackHealedDefender ? "+" + (attack.Damage * -1).ToString() : attack.Damage.ToString();
            //use offsets to create a spawn position
            Vector3 pos = new Vector3(transform.position.x + x_Offset, transform.position.y + Y_Offset, transform.position.z);
            //create an instance of the prefab
            ScrollingText scrollingText = Instantiate(Text, pos, Quaternion.identity);

            //set the alpha values of all colors to 1
            HitColor.a = 1;
            CritColor.a = 1;
            HealHPColor.a = 1;

            //vary the pos of the text
            x_Offset += 0.4f;

            if (x_Offset > 0.9f)
            {
                x_Offset = -0.7f;
            }

            //assign the string dmg value to the scrollingText instance we created
            scrollingText.SetText(text);

            //set the appropriate text color
            if (attack.IsCritical && !attackHealedDefender)
            {
                scrollingText.SetColor(CritColor);
            }
            else if (!attackHealedDefender)
            {
                scrollingText.SetColor(HitColor);
            }
            else
            {
                scrollingText.SetColor(HealHPColor);
            }
        }
    }
}
