using UnityEngine;

//Last Reviewed: 2/22/20
namespace RPG.CombatSystem
{
    //trigger "Hit" in animator OnHit()
    public class Attacked_PlayHitAnimation : MonoBehaviour, IAttackable
    {
        Animator anim;
        Character CScript;
        [SerializeField] AudioClip hitSound = null;


        void Start()
        {
            anim = GetComponentInChildren<Animator>();
            CScript = GetComponent<Character>();
        }

        public void OnHit(Damage_Config damageConfig, Attack attack, Base_Stats_Config attacker, Base_Stats_Config defender, bool triggerProcs)
        {
            if (CScript.IsAlive &&
                attack.Damage > 0 &&
                !anim.GetBool("Attack") &&
                !anim.GetBool("FastAttack") &&
                !anim.GetBool("Special1") &&
                !anim.GetBool("Special2") &&
                !anim.GetBool("Special3") &&
                !anim.GetBool("Special4") &&
                !anim.GetBool("Hit"))
            {
                anim.SetTrigger("Hit");
                if (hitSound != null)
                {
                    SoundManager.Instance.PlayAudioClip(hitSound);
                }
            }
        }
    }
}
