using System.Collections.Generic;
using System.Linq;
using UnityEngine;

//Last Reviewed: 3/1/20
namespace RPG.CombatSystem
{
    //when entering combat, EnemyEncounter determines what enemies are present, where they are positioned, and what
    //the battlefield looks like.
    [CreateAssetMenu]
    public class EnemyEncounter : ScriptableObject
    {
        public List<EncounterDetails> EnemiesInEncounter;

        [Header("Background Scene:")]
        public BattlefieldEnvironments BattleEnvironment = BattlefieldEnvironments.Mountains;
        public bool AtNight = false;
        public bool CanFlee = true;


        public List<GameObject> InstantiateEnemyEncounter()
        {
            List<GameObject> enemies = new List<GameObject>();

            for (int i = 0; i < EnemiesInEncounter.Count; i++)
            {
                if (EnemiesInEncounter[i].enemyPrefab == null)
                {
                    Debug.LogWarning("null entry in encounter, please add enemy details");
                    continue;
                }
                GameObject newEnemy = Instantiate(EnemiesInEncounter[i].enemyPrefab, EnemiesInEncounter[i].enemySpawnPos, Quaternion.Euler(EnemiesInEncounter[i].enemyRotation));
                enemies.Add(newEnemy);
            }

            GroupEnemies(enemies);
            return enemies;
        }


        //if there is more than 1 of a given type of enemy in the group, give enemies within the group number tags
        void GroupEnemies(List<GameObject> enemies)
        {
            var groupedEnemyLists = enemies.GroupBy(x => x.name).Select(grp => grp.ToList()).ToList();
            foreach (var list in groupedEnemyLists)
            {
                if (list.Count <= 1)continue;
                for (int i = 0; i < list.Count; i++)
                {
                    list[i].GetComponent<Character>().Stats_Config.CharName += " (" + (i + 1).ToString() + ")";
                }
            }
        }
    }

    [System.Serializable]
    public class EncounterDetails
    {
        public GameObject enemyPrefab;
        public Vector3 enemySpawnPos;
        public Vector3 enemyRotation;
    }
}
