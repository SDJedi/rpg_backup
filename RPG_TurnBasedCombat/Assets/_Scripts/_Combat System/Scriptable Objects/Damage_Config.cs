using UnityEngine;

//Last Reviewed: 3/1/20
namespace RPG.CombatSystem
{
    public enum DamageType { Physical, Fire, Ice, Water, Lightning, Poison, NONE }
    public enum AttackType { Melee, Ranged, Spell, DamageShield }

    //used to determine type and amount of damage an attack does.
    [CreateAssetMenu(menuName = "Damage_Config / New Damage_Config")]
    public class Damage_Config : ScriptableObject
    {
        [SerializeField] AttackType atkType = AttackType.Melee;
        [SerializeField] DamageType dmgType = DamageType.Physical;
        [SerializeField] bool attackCanMiss = true;
        [SerializeField] float minDamage = 1;
        [SerializeField] float maxDamage = 6;
        [SerializeField] float critChance = 0.05f; //1 in 20 default

        public AttackType AtkType { get { return atkType; } set { atkType = value; } }
        public DamageType DmgType { get { return dmgType; } }
        public bool AttackCanMiss { get { return attackCanMiss; } set { attackCanMiss = value; } }
        public float MinDamage { get { return minDamage; } set { minDamage = value; } }
        public float MaxDamage { get { return maxDamage; } set { maxDamage = value; } }
        public float CritChance { get { return critChance; } set { critChance = value; } }
    }
}
