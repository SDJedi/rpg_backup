using UnityEngine;

//Last Reviewed: 3/1/20
namespace RPG.CombatSystem
{
    //used to configure a DamageShield
    [CreateAssetMenu(menuName = "DamageShield / New DamageShield")]
    public class DamageShield : ScriptableObject
    {
        public string nameOfDmgShield;
        public Sprite icon;
        public Damage_Config damageConfig;
        [Range(0, 1)]
        public float procChance = 1f;
        public GameObject particleEffectOnProc;
        public AudioClip soundOnProc;
    }
}
