using System.Collections.Generic;
using UnityEngine;

//Last Reviewed: 3/1/20
namespace RPG.CombatSystem
{
    public enum StatAbbreviation { STR, INT, AGI, DEF, DmgRed, AtkBonus, InitMod, DmgBonus }

    //config data for stats that are shared by Enemies and PCs alike.
    //extended for PCs by PC_Stats_Config.
    [CreateAssetMenu(menuName = "Stats_Config / New Base_Stats_Config")]
    public class Base_Stats_Config : ScriptableObject
    {
        [Header("Damage Shields (Loaded into Attacked_DamageAttacker)")]
        [SerializeField] List<DamageShield> damageShields = new List<DamageShield>();
        [Header("AttackBonus increases chance to hit with any attack")]
        [Space(20)]
        [SerializeField] int attackBonus = 0;
        [Header("DamageBonus is added to all successful hits")]
        [SerializeField] int damageBonus = 0;
        [Header("Damage Reduction reduces all incoming damage")]
        [SerializeField] int damageReduction = 0;
        [Header("Defense Score makes character harder to hit")]
        [SerializeField] int DEFENSE = 0;
        [Header("Weak Against...(double damage)")]
        [SerializeField] List<DamageType> weakAgainst = new List<DamageType>();
        [Header("Strong against...(half damage)")]
        [SerializeField] List<DamageType> strongAgainst = new List<DamageType>();
        [Header("Attuned... (Damage of this type heals)")]
        [SerializeField] List<DamageType> attunedTo = new List<DamageType>();

        [Header("Resources")]
        [SerializeField] protected int curHP;
        [SerializeField] int maxHP;

        [SerializeField] protected int curMP;
        [SerializeField] int maxMP;

        [SerializeField] protected int curTP;
        [SerializeField] int maxTP;

        [Header("Stats")]
        [SerializeField] int STRENGTH;
        [SerializeField] int INTELLIGENCE;
        [SerializeField] int AGILITY;

        [Header("Initiative Modifier")]
        [SerializeField] int INITIATIVE_MOD;

        [Header("Magic Resitance: 0 - 1")]
        [Range(0, 1)]
        [SerializeField] protected float magRes = 0;

        [HideInInspector]
        public string CharName;

        public Character CScript { get; private set; }

        public List<DamageShield> DamageShields { get { return damageShields; } }
        public int BaseAtkBonus { get { return attackBonus; } }
        public int DamageBonus { get { return damageBonus; } }
        public int DmgReduction { get { return damageReduction; } }
        public List<DamageType> WeakAgainst { get { return weakAgainst; } }
        public List<DamageType> StrongAgainst { get { return strongAgainst; } }
        public List<DamageType> AttunedTo { get { return attunedTo; } }
        public int CurHP { get { return curHP; } }
        public int MaxHP { get { return maxHP; } }
        public int CurMP { get { return curMP; } }
        public int MaxMP { get { return maxMP; } }
        public int CurTP { get { return curTP; } }
        public int MaxTP { get { return maxTP; } }
        public int STR { get { return STRENGTH; } }
        public int INT { get { return INTELLIGENCE; } }
        public int AGI { get { return AGILITY; } }
        public int InitiativeMod { get { return INITIATIVE_MOD; } }
        public int DEF { get { return DEFENSE; } }
        public float MagRes { get { return magRes; } }


        //Add or subtract HP
        public virtual void ModifyCurrentHP(int amount)
        {
            curHP += amount;

            if (curHP < 0)
            {
                curHP = 0;
            }
            if (curHP > maxHP)
            {
                curHP = maxHP;
            }

            if (Combat_UI_Manager.InstanceExists)
            {
                Combat_UI_Manager.Instance.UpdateAllPC_SlotInfo();
            }
        }

        //Add or subtract MP
        public void ModifyCurrentMP(int amount)
        {
            curMP += amount;

            if (curMP < 0)
            {
                curMP = 0;
            }
            if (curMP > maxMP)
            {
                curMP = maxMP;
            }

            if (Combat_UI_Manager.InstanceExists)
            {
                Combat_UI_Manager.Instance.UpdateAllPC_SlotInfo();
            }
        }

        //Add or subtract TP
        public void ModifyCurrentTP(int amount)
        {
            curTP += amount;

            if (curTP < 0)
            {
                curTP = 0;
            }
            if (curTP > maxTP)
            {
                curTP = maxTP;
            }

            if (Combat_UI_Manager.InstanceExists)
            {
                Combat_UI_Manager.Instance.UpdateAllPC_SlotInfo();
            }
        }

        public void FullyRestoreCharacter()
        {
            //if (curHP > 0)
            //{
            ModifyCurrentHP(maxHP);
            ModifyCurrentMP(maxMP);
            ModifyCurrentTP(maxTP);
            //}
        }

        //Increase or Decrease Max of a resource value
        public void ModifyMaxHP(int amount)
        {
            maxHP += amount;
            if (curHP > maxHP)
            {
                curHP = maxHP;
            }
        }
        public void ModifyMaxMP(int amount)
        {
            maxMP += amount;
            if (curMP > maxMP)
            {
                curMP = maxMP;
            }
        }
        public void ModifyMaxTP(int amount)
        {
            maxTP += amount;
            if (curTP > maxTP)
            {
                curTP = maxTP;
            }
        }

        //Increase or Decrease a stat value 
        public void ModifyStat(StatAbbreviation Abbreviation, int amount)
        {
            switch (Abbreviation)
            {
                case StatAbbreviation.STR:
                    STRENGTH += amount;
                    break;
                case StatAbbreviation.INT:
                    INTELLIGENCE += amount;
                    break;
                case StatAbbreviation.AGI:
                    AGILITY += amount;
                    break;
                case StatAbbreviation.DEF:
                    DEFENSE += amount;
                    break;
                case StatAbbreviation.DmgRed:
                    damageReduction += amount;
                    break;
                case StatAbbreviation.AtkBonus:
                    attackBonus += amount;
                    break;
                case StatAbbreviation.DmgBonus:
                    damageBonus += amount;
                    break;
                case StatAbbreviation.InitMod:
                    INITIATIVE_MOD += amount;
                    break;
                default:
                    Debug.LogWarning("Invalid StatAbbreviation");
                    break;
            }
        }

        //returns a modifier based on a stat value
        public int GetStatModifier(StatAbbreviation abr)
        {
            //Formula: Mod == ((stat - 10) / 2) rounded down ... lowest possible mod == -5
            switch (abr)
            {
                case StatAbbreviation.STR:
                    if (STR < 1)
                    {
                        return -5;
                    }
                    if (STR > 30)
                    {
                        return 10;
                    }
                    else
                    {
                        var num1 = ((float)STR - 10f);
                        var num2 = Mathf.Floor(num1 / 2);
                        var strMod = (int)num2;
                        return strMod;
                    }
                case StatAbbreviation.INT:
                    if (INT < 1)
                    {
                        return -5;
                    }
                    if (INT > 30)
                    {
                        return 10;
                    }
                    else
                    {
                        var num1 = ((float)INT - 10f);
                        var num2 = Mathf.Floor(num1 / 2);
                        var intMod = (int)num2;
                        return intMod;
                    }
                case StatAbbreviation.AGI:
                    if (AGI < 1)
                    {
                        return -5;
                    }
                    if (AGI > 30)
                    {
                        return 10;
                    }
                    else
                    {
                        var num1 = ((float)AGI - 10f);
                        var num2 = Mathf.Floor(num1 / 2);
                        var agiMod = (int)num2;
                        return agiMod;
                    }
                default:
                    return 0;
            }
        }

        public void SetCScript(Character CScript)
        {
            this.CScript = CScript;
        }

        //if weak, remove. if nuetral, add strength. if strong, attune.
        public void StrengthenAgainstDamageType(DamageType type)
        {
            //if already attuned, we can't get any stronger against type
            foreach (DamageType t in AttunedTo)
            {
                if (t == type)
                {
                    Debug.Log("Already attuned to " + type);
                    return;
                }
            }

            //if currently weak against type, remove the weakness
            foreach (DamageType t in WeakAgainst)
            {
                if (t == type)
                {
                    Debug.Log("Character was weak against " + type + ". Removing weakness");
                    WeakAgainst.Remove(t);
                    return;
                }
            }

            //check to see if character is already strong against type...
            bool boostFromStrongAgainstToAttuned = false;
            foreach (DamageType t in StrongAgainst)
            {
                if (t == type)
                {
                    //already strong, move to attuned
                    Debug.Log("Character is already strong against " + type);
                    StrongAgainst.Remove(t);
                    boostFromStrongAgainstToAttuned = true;
                    break;
                }
            }

            if (!boostFromStrongAgainstToAttuned)
            {
                //add type to StrongAgainst
                Debug.Log("Adding strength against " + type + " to Character");
                StrongAgainst.Add(type);
                return;
            }

            foreach (DamageType t in AttunedTo)
            {
                if (t == type)
                {
                    //character is already attuned to type
                    Debug.Log("Character is already attuned to " + type);
                    return;
                }
            }
            //Attune character to damage type
            Debug.Log("Attuning character to DamageType: " + type);
            AttunedTo.Add(type);
        }

        public bool IsAlive()
        {
            return curHP > 0;
        }
    }
}
