using System;
using System.Collections.Generic;
using RPG.Inventory;
using RPG.Messaging;
using UnityEngine;
using UnityEngine.SceneManagement;

//Last Reviewed: 3/1/20
namespace RPG.CombatSystem
{
    [CreateAssetMenu(menuName = "Stats_Config / New PC_Stats_Config")]
    public class PC_Stats_Config : Base_Stats_Config
    {
        public event Action<string, int> PCLeveledUp;
        public event Action<PlayerCharacter> PCRezed;

        [System.Serializable]
        public struct WornEquipment
        {
            public EquipableItem Head;
            public EquipableItem Chest;
            public EquipableItem Waist;
            public EquipableItem Legs;
            public EquipableItem Feet;
            public EquipableItem Adorn_1;
            public EquipableItem Adorn_2;
            public EquipableItem Weapon;
        }

        [Header("Starting Equipment:")]
        public List<EquipableItem> PC_StartingEquipment;

        [Header("Portrait:")]
        public RenderTexture PC_Portrait;

        [Header("Currently Equipped Gear:")]
        public WornEquipment EquippedGear;

        [Header("Abilities:")]
        public List<BaseCommand_Config> KnownAbilities;
        [Space(20)]
        [Range(0, 3)]
        public int PC_Slot_position;
        public int StartingLvl = 1;

        public string Button0 { get; } = "Attack";
        public string Button1 { get; } = "Defend";
        public string Button2 { get; set; } = "";
        public string Button3 { get; set; } = "";
        public string Button4 { get; set; } = "";
        public string Button5 { get; set; } = "";
        public string Button6 { get; set; } = "";
        public string Button7 { get; set; } = "";
        public string Button8 { get; set; } = "";

        public string UniqueID { get; set; } = ""; //is set to the CharName in PCManager
        public int CurXP { get; private set; } = 0;
        public int CurLvl { get; private set; } = 1;

        //Add XP
        public void ModifyCurrentXP(int amount)
        {
            if (amount < 1)
            {
                //Debug.LogWarning("Trying to award 0 or less XP to " + CharName);
                return;
            }
            CurXP += amount;
        }

        //Reset XP
        public void ResetCurrentXP()
        {
            CurXP = 0;
        }

        //Set XP
        public void SetCurXPValue(int value)
        {
            CurXP = value;
        }

        public void SetCurLvl(int value)
        {
            value = Mathf.Clamp(value, 1, 20);
            CurLvl = value;
        }

        //Add or subtract HP
        public override void ModifyCurrentHP(int amount)
        {
            if (curHP <= 0 && curHP + amount > 0)
            {
                if (PCRezed != null)
                {
                    PCRezed((PlayerCharacter)CScript);
                }
            }

            curHP += amount;

            if (curHP < 0)
            {
                curHP = 0;
            }
            if (curHP > MaxHP)
            {
                curHP = MaxHP;
            }

            if (Combat_UI_Manager.InstanceExists)
            {
                Combat_UI_Manager.Instance.UpdateAllPC_SlotInfo();
            }
        }

        public void AddAbility(BaseCommand_Config commandToReplace, BaseCommand_Config newCommand)
        {
            if (commandToReplace != null && !KnownAbilities.Contains(commandToReplace))
            {
                Debug.LogWarning("Tried to remove [" + commandToReplace.CmndName + "] from " + CharName + " but command did not exist.");
            }
            else if (commandToReplace != null && KnownAbilities.Contains(commandToReplace))
            {
                KnownAbilities.Remove(commandToReplace);
            }
            AddAbility(newCommand);
            KnownAbilities.RemoveAll(n => n == null);
        }

        void AddAbility(BaseCommand_Config command)
        {
            if (KnownAbilities.Contains(command))
            {
                Debug.LogWarning("PC " + CharName + " already has command: " + command.CmndName);
                return;
            }
            KnownAbilities.Add(command);
        }

        public void AddNewDamageShield(DamageShield ds)
        {
            if (DamageShields.Contains(ds))
            {
                Debug.LogWarning("PC " + CharName + " already has damage shield: " + ds.name);
            }
            if (ds.nameOfDmgShield == "")
            {
                Debug.LogWarning("Damage Shield needs a name to be removed properly... not adding");
                return;
            }
            DamageShields.Add(ds);
        }

        public void RemoveADamageShield(DamageShield ds)
        {
            for (int i = 0; i < DamageShields.Count; i++)
            {
                if (DamageShields[i].nameOfDmgShield == ds.nameOfDmgShield)
                {
                    DamageShields.Remove(DamageShields[i]);
                    DamageShields.RemoveAll(n => n == null);
                    return;
                }
            }

            Debug.LogWarning("PC " + CharName + " does not contain damage shield: " + ds.name);
        }

        public void AdjustMagicResist(float amnt)
        {
            if (amnt >= 1)
            {
                Debug.LogWarning("Magic resist for " + CharName + " set at 100% Scale for stat is 0 to 1");
            }
            magRes += amnt;
        }

        public void IncreasePCLvl(bool showNotification = true)
        {
            CurLvl += 1;
            ///////////Common boosts for all PCs here/////////
            ModifyMaxHP((int)((float)MaxHP * .10)); //maxHP increase by 10%

            if ((float)CurLvl % 2f == 0)
            {
                if (CScript == null)
                {
                    SetCScript(PCManager.Instance.GetCScriptForPC(this.CharName));
                }
                if (CScript.HasMP)
                {
                    ModifyMaxMP(1);
                }
                if (CScript.HasTP)
                {
                    ModifyMaxTP(1);
                }
            }
            //////////////////////////////////////////////////
            //refill resources
            curHP = MaxHP;
            curMP = MaxMP;
            curTP = MaxTP;

            if (PCLeveledUp != null)
            {
                PCLeveledUp(CharName, CurLvl);
            }
            if (SceneManager.GetActiveScene().name != "CombatScene" && showNotification)
            {
                NotificationWindowManager.Instance.DisplayNotification(CharName + " has reached level " + CurLvl.ToString(), Color.yellow);
            }
        }
    }
}