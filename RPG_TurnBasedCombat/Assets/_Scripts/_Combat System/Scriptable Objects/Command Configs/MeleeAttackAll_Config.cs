using System;
using UnityEngine;

//Last Reviewed: 3/1/20
namespace RPG.CombatSystem
{
    //same configuration as MeleeAttack_Config, except for the CommandType.
    //Note: the different command type is needed for when we add a new Command at runtime.
    [CreateAssetMenu(menuName = "Attack Config / New Melee Attack (each hostile) Config")]
    public class MeleeAttackAll_Config : MeleeAttack_Config
    {
        public override Type CommandType { get; } = typeof(Command_MeleeAttackEachHostile);
    }
}
