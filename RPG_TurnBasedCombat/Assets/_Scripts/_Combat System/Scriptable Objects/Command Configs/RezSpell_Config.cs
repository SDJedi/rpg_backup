using System;
using UnityEngine;

//Last Reviewed: 1/21/22
namespace RPG.CombatSystem
{
    //same configuration as HealSpell_Config, except for the CommandType.
    //Note: the different command type is needed for when we add a new Command at runtime.
    [CreateAssetMenu(menuName = "Spell / New Rez Spell")]
    public class RezSpell_Config : HealSpell_Config
    {
        public override Type CommandType { get; } = typeof(Command_RezPlayer);
    }
}