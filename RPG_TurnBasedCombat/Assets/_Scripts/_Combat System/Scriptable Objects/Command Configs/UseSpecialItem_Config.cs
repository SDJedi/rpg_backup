﻿using System;
using RPG.Inventory;
using UnityEngine;

namespace RPG.CombatSystem
{
    //Instantiate and use a special item in combat (i.e. Lance exploding chicken)
    [CreateAssetMenu(menuName = "Use Special Item Config")]
    public class UseSpecialItem_Config : BaseCommand_Config
    {
        [SerializeField] protected float moveToPosSpeed = 5;
        [SerializeField] protected float animationDelay = 1.5f; //pause this long right after animation is triggered
        [SerializeField] protected UseableItem itemToCreateAndUse;

        public float MoveToPosSpeed { get { return moveToPosSpeed; } }
        public float AnimationDelay { get { return animationDelay; } }
        public UseableItem ItemToCreateAndUse { get { return itemToCreateAndUse; } }

        public override Type CommandType { get; } = typeof(Command_UseSpecialItem);
    }
}
