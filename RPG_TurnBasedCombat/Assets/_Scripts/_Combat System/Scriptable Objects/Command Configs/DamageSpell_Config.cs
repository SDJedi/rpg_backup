using System;
using System.Collections;
using UnityEngine;

//Last Reviewed: 2/29/20
namespace RPG.CombatSystem
{
    //Note: a PerEnemy AoE spell where the damage config deals 40 damage will deal 40 damage to each enemy regardless of how many enemies there are.
    //a DividedAmongAllEnemies AoE where the damage config deals 400 damage will deal 400 damage if there is one enemy, 200 damage each to two enemies, 100 damage each to 4 enemies, etc.
    public enum AllocateDamage { PerEnemy, DividedAmongAllEnemies }

    //allows for creation of spells that deal damage.
    //extends BaseSpell_Config.
    //can handle single target spells as well as AoE.
    //can handle direct damage as well as damage over time.
    //AoE spells can be set to deal damage on a per enemy basis OR set to divide a damage total among all targets.
    //Note: if the AoE uses a blanket particle effect instead of spawning an effect on each target, see
    //Spell_BlanketParticleEffect_Config.cs
    [CreateAssetMenu(menuName = "Spell / New Damage Spell")]
    public class DamageSpell_Config : BaseSpell_Config
    {
        [Header("Damage Spell Specific")]
        [SerializeField] protected AllocateDamage dealDamage = AllocateDamage.PerEnemy;
        [SerializeField] protected Damage_Config attackConfig_OnApply;
        [SerializeField] protected Damage_Config attackConfig_OnTick;


        public Damage_Config AttackConfig_OnApply { get { return attackConfig_OnApply; } }
        public Damage_Config AttackConfig_OnTick { get { return attackConfig_OnTick; } }


        Base_Stats_Config attackerStats; //important for DoT spells

        public override Type CommandType { get; } = typeof(Command_SpellEffect);


        public override void OnApplication(Character attacker, Character effectedTarget)
        {
            //if there's no attackConfig_OnTick, this is a direct damage spell, set turnsDuration to 0;
            if (attackConfig_OnTick == null)
            {
                turnDuration = 0;
                turnsRemaining = 0;
            }
            else
            {
                turnsRemaining = turnDuration;
            }

            //grab a copy of attacker stats for tick damage later in case attacker dies
            attackerStats = Instantiate(attacker.Stats_Config);
            attackerStats.SetCScript(attacker.Stats_Config.CScript); //added 5/2/20

            //single-target hostile spell, there is no reason to split the damage
            if (_TargetType == TargetType.Hostile)
            {
                dealDamage = AllocateDamage.PerEnemy;
            }

            //if targetType == AllHostile, do we want to take the damage potential of the spell
            //and split amongst all enemies? If so...
            if (dealDamage == AllocateDamage.DividedAmongAllEnemies)
            {
                Damage_Config newCnfg = Instantiate(AttackConfig_OnApply);
                newCnfg.MinDamage = newCnfg.MinDamage / (float)EnemyManager.Instance.EnemiesInEncounter.Count;
                newCnfg.MaxDamage = newCnfg.MaxDamage / (float)EnemyManager.Instance.EnemiesInEncounter.Count;
                GenerateAttack.ExecuteAttack(newCnfg, attackerStats, effectedTarget.gameObject, ThisCommandTriggersProcs);
            }
            //deal full damage from AttackConfig_OnApply per enemy...
            else
            {
                GenerateAttack.ExecuteAttack(AttackConfig_OnApply, attackerStats, effectedTarget.gameObject, ThisCommandTriggersProcs);
            }
        }

        public override void OnTermination(Character effectedTarget)
        {
            if (attackerStats != null)
            {
                Destroy(attackerStats);
            }
        }

        public override IEnumerator OnTick(Character effectedTarget)
        {
            if (AttackConfig_OnTick == null)
            {
                yield break;
            }
            GenerateAttack.ExecuteAttack(AttackConfig_OnTick, attackerStats, effectedTarget.gameObject, false);
        }
    }
}