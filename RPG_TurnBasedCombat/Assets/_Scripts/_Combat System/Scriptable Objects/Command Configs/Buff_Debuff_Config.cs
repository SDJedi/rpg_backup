using System;
using System.Collections;
using UnityEngine;

//Last Reviewed: 2/29/20
namespace RPG.CombatSystem
{
    //allows for the creation of spells that temporarily modify stats.
    //extends BaseSpell_Config 
    [CreateAssetMenu(menuName = "Spell / New Buff or Debuff")]
    public class Buff_Debuff_Config : BaseSpell_Config
    {
        [Header("Match each stat with the amount it is to be modified")]
        [SerializeField] StatAbbreviation[] statToModify = new StatAbbreviation[1];
        [SerializeField] int[] amountToChange = new int[1];

        public StatAbbreviation[] StatToModify { get { return statToModify; } }
        public int[] AmountToChange { get { return amountToChange; } }

        protected bool abortApplication = false;

        override public Type CommandType { get; } = typeof(Command_SpellEffect);


        //modify stats and add to ActiveSpellList of effected target
        public override void OnApplication(Character caster, Character effectedTarget)
        {
            if (StatToModify.Length != AmountToChange.Length)
            {
                Debug.LogWarning("ERROR! StatToModify[] must have same number of entries as AmountToChange[]");
                turnDuration = 0; //Note: prevents spell from being added to Character --> ActiveSpellEffects List
                abortApplication = true;
                return;
            }

            turnsRemaining = TurnDuration;

            var stats = effectedTarget.Stats_Config;
            for (int i = 0; i < StatToModify.Length; i++)
            {
                stats.ModifyStat(StatToModify[i], AmountToChange[i]);
            }
        }

        //reverse what was done on application
        public override void OnTermination(Character effectedTarget)
        {
            var stats = effectedTarget.Stats_Config;
            for (int i = 0; i < StatToModify.Length; i++)
            {
                stats.ModifyStat(StatToModify[i], -AmountToChange[i]);
            }
        }

        //no implementation of OnTick for basic stat buff
        public override IEnumerator OnTick(Character effectedTarget) { yield return null; }
    }
}
