using System;
using UnityEngine;

//Last Reviewed: 2/29/20
namespace RPG.CombatSystem
{
    public enum TypeToRemove { Beneficial, Detrimental, Any }

    //allows for the creation of spells that can strip active spell effects off a character.
    //used by Command_Dispel_Cure.cs
    [CreateAssetMenu(menuName = "Spell / New Dispel or Cure")]
    public class Dispel_Cure_Config : BaseCommand_Config
    {
        [SerializeField] protected TypeToRemove typeToRemove;
        [SerializeField] protected int maxNumOfEffectsToClear = 1;

        public TypeToRemove TypeToRemove { get { return typeToRemove; } }
        public int MaxNumOfEffectsToClear { get { return maxNumOfEffectsToClear; } }


        public override Type CommandType { get; } = typeof(Command_Dispel_Cure);
    }
}