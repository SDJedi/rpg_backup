using System;
using System.Collections.Generic;
using UnityEngine;

//Last Reviewed: 2/29/20
namespace RPG.CombatSystem
{
    //allows for the creation of spells that when cast can add procs and/or damage shields to a character.
    //extends Buff_Debuff_Config.
    [CreateAssetMenu(menuName = "Spell / New Add Proc or Damage Shield")]
    public class AddProcOrDamageShield_Config : Buff_Debuff_Config
    {
        [SerializeField] List<Proc> procsToAdd = null;
        [SerializeField] List<DamageShield> damageShieldsToAdd = null;

        public List<Proc> ProcsToAdd { get { return procsToAdd; } }
        public List<DamageShield> DamageShieldsToAdd { get { return damageShieldsToAdd; } }

        public override Type CommandType { get; } = typeof(Command_SpellEffect);


        public override void OnApplication(Character caster, Character effectedTarget)
        {
            //base.OnApplication allows for stats to be modified as part of the buff
            base.OnApplication(caster, effectedTarget);
            //if there was a problem with the base application, abort.
            if (abortApplication)return;

            //Procs: target of spell is a PC...
            if (effectedTarget.Is_A_PC)
            {
                foreach (Proc p in procsToAdd)
                {
                    PC_ProcManager.Instance.AddProcToPC(effectedTarget.Stats_Config.CharName, p);
                }
            }
            //Procs: target of spell is an enemy...
            else
            {
                var pCont = ((EnemyCharacter)effectedTarget).procContainer;
                foreach (Proc p in procsToAdd)
                {
                    pCont.AddProcToList(p);
                }
            }

            //DamageShields: any character...
            var ada = effectedTarget.GetComponent<Attacked_DamageAttacker>();
            foreach (DamageShield ds in damageShieldsToAdd)
            {
                ada.AddDamageShieldToIAttackable(ds);
            }
        }

        public override void OnTermination(Character effectedTarget)
        {
            base.OnTermination(effectedTarget);

            //Remove Procs: PC
            if (effectedTarget.Is_A_PC)
            {
                foreach (Proc p in procsToAdd)
                {
                    PC_ProcManager.Instance.RemoveProcFromPC(effectedTarget.Stats_Config.CharName, p);
                }
            }
            //Remove Procs: Enemy
            else
            {
                var pCont = ((EnemyCharacter)effectedTarget).procContainer;
                foreach (Proc p in procsToAdd)
                {
                    pCont.RemoveProcFromList(p);
                }
            }

            //Remove DamageShields: any character
            var ada = effectedTarget.GetComponent<Attacked_DamageAttacker>();
            foreach (DamageShield ds in damageShieldsToAdd)
            {
                ada.RemoveADamageShieldFromIAttackable(ds);
            }
        }
    }
}
