using System;
using System.Collections;
using UnityEngine;

//Last Reviewed: 3/1/20
namespace RPG.CombatSystem
{
    //a special type of spell that requires the player to select an option after choosing to use this spell
    [CreateAssetMenu(menuName = "Spell / Choice On Cast")]
    public class Spell_ChoicesOnCast_Config : BaseSpell_Config
    {
        [Header("Choices are available when command is selected:")][Space(5)]
        public GameObject ChoiceBoxPrefab;
        [Header("Top-Of-Box Mssg and Button Labels")]
        public string TopOfBoxMssg;
        public string Btn0_Lbl;
        public string Btn1_Lbl;
        [Header("Spell Options")]
        public BaseSpell_Config Option0 = null;
        public BaseSpell_Config Option1 = null;

        public override Type CommandType { get; } = typeof(Command_SpellEffect_ChoicesOnCast);

        public override void OnApplication(Character caster, Character effectedTarget)
        {
            throw new System.NotImplementedException();
        }

        public override void OnTermination(Character effectedTarget)
        {
            throw new System.NotImplementedException();
        }

        public override IEnumerator OnTick(Character effectedTarget)
        {
            throw new System.NotImplementedException();
        }
    }
}
