using System;
using UnityEngine;

//Last Reviewed: 2/29/20
namespace RPG.CombatSystem
{
    //set move-speed, position, and damage for a melee attack.
    //extends BaseCommand_Config.
    [CreateAssetMenu(menuName = "Attack Config / New Melee Attack Config")]
    public class MeleeAttack_Config : BaseCommand_Config
    {
        [SerializeField] protected float moveToTargetSpeed = 15;
        [SerializeField] protected float hostileFrontZOffset = 0; //adjust where target front is for larger enemy or special animation
        [SerializeField] protected Damage_Config damageConfig;

        public float MoveToTargetSpeed { get { return moveToTargetSpeed; } }
        public float HostileFrontZOffset { get { return hostileFrontZOffset; } }
        public Damage_Config DamageConfig { get { return damageConfig; } }

        public override Type CommandType { get; } = typeof(Command_MeleeAttackSingleTarget);
    }
}
