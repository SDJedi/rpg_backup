using System;
using UnityEngine;

//Last Reviewed: 3/1/20
namespace RPG.CombatSystem
{
    //configure a new ranged attack
    [CreateAssetMenu(menuName = "Attack Config / New Ranged Attack Config")]
    public class RangedAttack_Config : BaseCommand_Config
    {
        [SerializeField] protected float moveToPosSpeed = 5;
        [SerializeField] protected float animationDelay = 1.5f; //pause this long right after animation is triggered
        [Header("Optional: search for a child or grandchild object by name to use as the projectile spawn point")]
        [SerializeField] protected string projectileSpawnPointName = "--";
        [SerializeField] protected bool moveBeforeFire = true;
        [SerializeField] protected bool faceTargetBeforeFiring = false;
        [SerializeField] protected GameObject projectileToFire;

        public float MoveToPosSpeed { get { return moveToPosSpeed; } }
        public float AnimationDelay { get { return animationDelay; } }
        public string ProjectileSpawnPointName { get { return projectileSpawnPointName; } }
        public bool MoveBeforeFire { get { return moveBeforeFire; } }
        public bool FaceTargetBeforeFiring { get { return faceTargetBeforeFiring; } }
        public GameObject ProjectileToFire { get { return projectileToFire; } }

        public override Type CommandType { get; } = typeof(Command_RangedAttack);
    }
}