﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.CombatSystem
{
    [CreateAssetMenu(menuName = "Summon Reinforcements Config / New Summon Reinforcements Config")]
    public class SummonReinforcements_Config : BaseCommand_Config
    {
        [SerializeField] EnemyEncounter reinforcements = null;
        [Header("Ensure 1 destination per enemy prefab in Reinforcements")]
        [SerializeField] List<TransformDetails> destinations = null;

        public EnemyEncounter Reinforcements { get { return reinforcements; } }
        public List<TransformDetails> Destinations { get { return destinations; } }

        public override Type CommandType { get; } = typeof(Command_SummonReinforcements);
    }

    [System.Serializable]
    public class TransformDetails
    {
        public Vector3 position;
        public Vector3 eulerRotation;
    }
}
