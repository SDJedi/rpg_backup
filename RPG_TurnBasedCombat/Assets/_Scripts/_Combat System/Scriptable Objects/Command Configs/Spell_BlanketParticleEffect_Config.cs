using System;
using UnityEngine;

//Last Reviewed: 3/1/20
namespace RPG.CombatSystem
{
    //configure an AoE spell with a single particle effect, as opposed to an AoE
    //spell where an instance of the particle effect is spawned on each target.
    [CreateAssetMenu(menuName = "Spell / New Blanket AoE Spell")]
    public class Spell_BlanketParticleEffect_Config : DamageSpell_Config
    {
        public Vector3 ParticleEffectLocation_CastByPC = Vector3.zero;
        public Vector3 ParticleEffectLocation_CastByEnemy = Vector3.zero;

        public override Type CommandType { get; } = typeof(Command_SpellEffect_BlanketParticleEffect);
    }
}
