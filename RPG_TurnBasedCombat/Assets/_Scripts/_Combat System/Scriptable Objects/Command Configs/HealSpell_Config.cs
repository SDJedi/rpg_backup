using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Last Reviewed: 2/29/20
namespace RPG.CombatSystem
{
    public enum HealableStats { HP, MP, TP }

    //create spells that replenish resources (HP, MP, TP)
    [CreateAssetMenu(menuName = "Spell / New Heal Spell")]
    public class HealSpell_Config : BaseSpell_Config
    {
        [Header("Stat(s) to heal:")]
        [SerializeField] List<HealableStats> statsToHeal = new List<HealableStats>() { HealableStats.HP };

        [Header("Heal Spell Specific; Flat Amounts:")]
        [SerializeField] int minAmount_OnApply = 0;
        [SerializeField] int maxAmount_OnApply = 0;
        [SerializeField] int minAmount_PerTick = 0;
        [SerializeField] int maxAmount_PerTick = 0;

        [Header("Override Flat Amounts with Percentages")]
        [Range(0, 100)]
        [SerializeField] int percentageOfMaxRegainedOnInitialHit = 0;
        [Range(0, 100)]
        [SerializeField] int percentageOfMaxRegainedPerTick = 0;

        public int MinAmount_OnApply { get { return minAmount_OnApply; } }
        public int MaxAmount_OnApply { get { return maxAmount_OnApply; } }
        public int MinAmount_PerTick { get { return minAmount_PerTick; } }
        public int MaxAmount_PerTick { get { return maxAmount_PerTick; } }

        public int PercentageOfMaxRegainedOnInitialHit { get { return percentageOfMaxRegainedOnInitialHit; } }
        public int PercentageOfMaxRegainedPerTick { get { return percentageOfMaxRegainedPerTick; } }

        public override Type CommandType { get; } = typeof(Command_SpellEffect);


        public override void OnApplication(Character caster, Character effectedTarget)
        {
            turnsRemaining = TurnDuration;
            effectedTarget.StartCoroutine(ApplyHeals(effectedTarget));
        }

        IEnumerator ApplyHeals(Character effectedTarget)
        {
            if (!effectedTarget.IsAlive && this.GetType() != typeof(RezSpell_Config)) yield break;

            Base_Stats_Config stats = effectedTarget.Stats_Config;
            float Amnt;

            if (statsToHeal.Contains(HealableStats.HP))
            {
                if (percentageOfMaxRegainedOnInitialHit > 0)
                {
                    Amnt = (float)stats.MaxHP * ((float)percentageOfMaxRegainedOnInitialHit / 100);
                }
                else
                {
                    Amnt = UnityEngine.Random.Range(MinAmount_OnApply, MaxAmount_OnApply);
                }

                HealStatAndDisplayMessages(effectedTarget, HealableStats.HP, Amnt);
            }

            yield return new WaitForSeconds(0.25f);

            if (statsToHeal.Contains(HealableStats.MP) && effectedTarget.HasMP)
            {
                if (percentageOfMaxRegainedOnInitialHit > 0)
                {
                    Amnt = (float)stats.MaxMP * ((float)percentageOfMaxRegainedOnInitialHit / 100);
                }
                else
                {
                    Amnt = UnityEngine.Random.Range(MinAmount_OnApply, MaxAmount_OnApply);
                }

                HealStatAndDisplayMessages(effectedTarget, HealableStats.MP, Amnt);
            }

            yield return new WaitForSeconds(0.25f);

            if (statsToHeal.Contains(HealableStats.TP) && effectedTarget.HasTP)
            {
                if (percentageOfMaxRegainedOnInitialHit > 0)
                {
                    Amnt = (float)stats.MaxTP * ((float)percentageOfMaxRegainedOnInitialHit / 100);
                }
                else
                {
                    Amnt = UnityEngine.Random.Range(MinAmount_OnApply, MaxAmount_OnApply);
                }

                HealStatAndDisplayMessages(effectedTarget, HealableStats.TP, Amnt);
            }
        }

        //no implementation of OnTermination
        public override void OnTermination(Character effectedTarget) { }

        public override IEnumerator OnTick(Character effectedTarget)
        {
            yield return ApplyTick(effectedTarget);
        }

        IEnumerator ApplyTick(Character effectedTarget)
        {
            Base_Stats_Config stats = effectedTarget.Stats_Config;
            float Amnt;

            if (statsToHeal.Contains(HealableStats.HP))
            {
                if (percentageOfMaxRegainedPerTick > 0)
                {
                    Amnt = (float)stats.MaxHP * ((float)percentageOfMaxRegainedPerTick / 100);
                }
                else
                {
                    Amnt = UnityEngine.Random.Range(MinAmount_PerTick, MaxAmount_PerTick);
                }

                HealStatAndDisplayMessages(effectedTarget, HealableStats.HP, Amnt);
            }

            yield return new WaitForSeconds(0.25f);

            if (statsToHeal.Contains(HealableStats.MP) && effectedTarget.HasMP)
            {
                if (percentageOfMaxRegainedPerTick > 0)
                {
                    Amnt = (float)stats.MaxMP * ((float)percentageOfMaxRegainedPerTick / 100);
                }
                else
                {
                    Amnt = UnityEngine.Random.Range(MinAmount_PerTick, MaxAmount_PerTick);
                }

                HealStatAndDisplayMessages(effectedTarget, HealableStats.MP, Amnt);
            }

            yield return new WaitForSeconds(0.25f);

            if (statsToHeal.Contains(HealableStats.TP) && effectedTarget.HasTP)
            {
                if (percentageOfMaxRegainedPerTick > 0)
                {
                    Amnt = (float)stats.MaxTP * ((float)percentageOfMaxRegainedPerTick / 100);
                }
                else
                {
                    Amnt = UnityEngine.Random.Range(MinAmount_PerTick, MaxAmount_PerTick);
                }

                HealStatAndDisplayMessages(effectedTarget, HealableStats.TP, Amnt);
            }
        }

        void HealStatAndDisplayMessages(Character effectedTarget, HealableStats statType, float amnt)
        {
            var stats = effectedTarget.Stats_Config;
            switch (statType)
            {
                case HealableStats.HP:
                    if (stats.CurHP + amnt > stats.MaxHP)
                    {
                        amnt = (stats.MaxHP - stats.CurHP);
                    }

                    stats.ModifyCurrentHP((int)amnt);
                    //create HP modify scroll text for heal
                    if (amnt > 0)
                    {
                        GenerateSpecialScrollingText.GenerateScrollingText(effectedTarget.transform, ((int)amnt).ToString(), Color.green);
                        CombatLogManager.Instance.AddToCombatLog(effectedTarget.Stats_Config.CharName + " regained " + ((int)amnt).ToString() + " HP.", new Color32(0, 255, 0, 255));
                    }
                    break;

                case HealableStats.MP:
                    if (stats.CurMP + amnt > stats.MaxMP)
                    {
                        amnt = (stats.MaxMP - stats.CurMP);
                    }

                    stats.ModifyCurrentMP((int)amnt);
                    //create MP modify scroll text for heal
                    if (amnt > 0)
                    {
                        GenerateSpecialScrollingText.GenerateScrollingText(effectedTarget.transform, ((int)amnt).ToString(), Color.blue);
                        CombatLogManager.Instance.AddToCombatLog(effectedTarget.Stats_Config.CharName + " regained " + ((int)amnt).ToString() + " MP.", new Color32(0, 0, 255, 255));
                    }
                    break;

                case HealableStats.TP:
                    if (stats.CurTP + amnt > stats.MaxTP)
                    {
                        amnt = (stats.MaxTP - stats.CurTP);
                    }

                    stats.ModifyCurrentTP((int)amnt);
                    //create TP modify scroll text for heal
                    if (amnt > 0)
                    {
                        GenerateSpecialScrollingText.GenerateScrollingText(effectedTarget.transform, ((int)amnt).ToString(), Color.yellow);
                        CombatLogManager.Instance.AddToCombatLog(effectedTarget.Stats_Config.CharName + " regained " + ((int)amnt).ToString() + " TP.", new Color32(255, 255, 0, 255));
                    }
                    break;
            }
        }
    }
}
