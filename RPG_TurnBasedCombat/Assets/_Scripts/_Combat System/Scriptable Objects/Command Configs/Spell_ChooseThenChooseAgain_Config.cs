using System;
using UnityEngine;

//Last Reviewed: 3/1/20
namespace RPG.CombatSystem
{
    //extends the concept of making choices after choosing a Command, providing a "choice tree" based on
    //previous choices.
    [CreateAssetMenu(menuName = "Spell / Choose Then Choose Again")]
    public class Spell_ChooseThenChooseAgain_Config : Spell_ChoicesOnCast_Config
    {
        [Header("Top-Of-Box Mssg and Button Labels")]
        public string TopOfBoxMssg_2ndSet_0ChosenFirst;
        public string Btn0_Lbl_2ndSet_0ChosenFirst;
        public string Btn1_Lbl_2ndSet_0ChosenFirst;
        [Space(20)]
        [Header("Top-Of-Box Mssg and Button Labels")]
        public string TopOfBoxMssg_2ndSet_1ChosenFirst;
        public string Btn0_Lbl_2ndSet_1ChosenFirst;
        public string Btn1_Lbl_2ndSet_1ChosenFirst;
        [Space(20)]

        public BaseSpell_Config Option0_Option0 = null;
        public BaseSpell_Config Option0_Option1 = null;
        public BaseSpell_Config Option1_Option0 = null;
        public BaseSpell_Config Option1_Option1 = null;

        public override Type CommandType { get; } = typeof(Command_SpellEffect_Choose_ThenChooseAgain);
    }
}
