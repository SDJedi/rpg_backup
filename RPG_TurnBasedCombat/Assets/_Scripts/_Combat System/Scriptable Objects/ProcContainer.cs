using System.Collections.Generic;
using UnityEngine;

//Last Reviewed: 3/1/20
namespace RPG.CombatSystem
{
    //Proc: a spell with a % chance to fire when a condition is met (usually an attack hit)
    [System.Serializable]
    public class Proc
    {
        public string nameOfProc;
        public BaseSpell_Config spell_Config;
        [Range(0, 1)]
        public float procChance;

        public Proc(string name, BaseSpell_Config spell, float chance)
        {
            nameOfProc = name;
            spell_Config = spell;
            procChance = chance;
        }
    }

    //ProcContainer contains a List of active procs for a given character, as well as methods for 
    //adding and removing procs from the List.
    //ProcContainers are created at runtime, not from the asset menu ... no need to assign 
    //these in inspector for any character (see PC_ProcManager.cs and EnemyCharacter.cs)
    public class ProcContainer : ScriptableObject
    {
        [SerializeField] List<Proc> activeProcs = new List<Proc>();

        public List<Proc> ActiveProcs { get { return activeProcs; } }
        public string UniqueID { get; set; } = ""; //set to the CharName


        public void AddProcToList(Proc proc)
        {
            ActiveProcs.Add(new Proc(proc.nameOfProc, proc.spell_Config, proc.procChance));
        }

        public void RemoveProcFromList(Proc proc)
        {
            for (int i = activeProcs.Count - 1; i >= 0; i--)
            {
                if (activeProcs[i].spell_Config.CmndName == proc.spell_Config.CmndName)
                {
                    activeProcs.Remove(activeProcs[i]);
                    break;
                }
            }

            activeProcs.RemoveAll(n => n == null);
        }

        public void ClearAllProcs()
        {
            activeProcs.Clear();
            activeProcs.RemoveAll(n => n == null);
            activeProcs.Capacity = 0;
        }
    }
}
