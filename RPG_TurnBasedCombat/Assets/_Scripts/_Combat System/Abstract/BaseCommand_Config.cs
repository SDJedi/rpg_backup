using System;
using System.Collections.Generic;
using UnityEngine;

//Last Reviewed: 2/17/20
namespace RPG.CombatSystem
{
    //Note: Commands that are part of the "Main Menu" (e.g. Attack(0), Defend(1), Special(2), Magic(3), Items(4))
    //are attached to PC prefabs directly and given a buttonIndex between 0 - 8.
    //all other commands have a buttonIndex of -1.
    public enum CommandMenus { Special, Magic }
    public enum TargetType { Hostile, Friendly, Self, All, AllHostile, AllFriendly, DeadFriendly, NONE, Any, Friendly_NotSelf }

    //contains the things that ALL commands have in common.
    //basic building block for making commands that are useable by PCs and Enemies.
    public abstract class BaseCommand_Config : ScriptableObject
    {
        [Header("Command Name:")]
        [SerializeField] protected string cmndName;

        [Header("Icon")]
        [SerializeField] protected Sprite icon;

        [Header("Command Description:")]
        [TextArea]
        [SerializeField] protected string description;

        [Header("Message to show at top of the screen when cast:")]
        [SerializeField] protected string onCastMessage;

        [Header("Message to show in the CombatLog:")]
        [SerializeField] protected string combatLogMessage;
        [SerializeField] protected Color32 combatLogMssgColor = new Color32(255, 255, 255, 255);

        [Header("Animation trigger:")]
        [Space(5)]
        [SerializeField] protected string animatorTriggerName;

        [Header("Resource Costs:")]
        [Space(5)]
        [SerializeField] protected int hPCost;
        [SerializeField] protected int mPCost;
        [SerializeField] protected int tPCost;

        [Header("Target Type:")]
        [Space(5)]
        [SerializeField] TargetType targetType = TargetType.NONE;

        [Header("ParticleEffect spawns on target of spell/attack after the animation plays")]
        [Space(5)]
        [SerializeField] protected GameObject particleEffect;
        [Header("OnCast_SFX is played at the start of a cast/attack animation")]
        [Space(5)]
        [SerializeField] protected AudioClip onCastSoundEffect;
        [Header("OnHit_SFX is called by AttackFromAnimation() Animation Event (melee only)")]
        [Space(5)]
        [SerializeField] protected AudioClip onHitSoundEffect;

        [Header("Menu to appear in (irrelevant to MainMenu commands)")]
        [Space(5)]
        [SerializeField] protected CommandMenus menuToAppearIn = CommandMenus.Special;

        [Header("Procs built-in to this command. Can go off when this command hits")]
        [Space(5)]
        [SerializeField] protected List<Proc> additionalOnHitEffects;

        [Header("Can this command trigger procs from other sources?")]
        [SerializeField] protected bool thisCommandTriggersProcs;


        public TargetType _TargetType { get { return targetType; } }
        public List<Proc> AdditionalOnHitEffects { get { return additionalOnHitEffects; } }
        public GameObject ParticleEffect { get { return particleEffect; } }
        public AudioClip OnCastSoundEffect { get { return onCastSoundEffect; } }
        public AudioClip OnHitSoundEffect { get { return onHitSoundEffect; } }
        public string CmndName { get { return cmndName; } }
        public Sprite Icon { get { return icon; } }
        public string Description { get { return description; } }
        public string OnCastMessage { get { return onCastMessage; } }
        public string CombatLogMessage { get { return combatLogMessage; } }
        public Color32 CombatLogMssgColor { get { return combatLogMssgColor; } }
        public bool ThisCommandTriggersProcs { get { return thisCommandTriggersProcs; } set { thisCommandTriggersProcs = value; } }
        public bool AddToSpecialMenu { get { return menuToAppearIn == CommandMenus.Special; } }
        public bool AddToMagicMenu { get { return menuToAppearIn == CommandMenus.Magic; } }
        public int HPCost { get { return hPCost; } }
        public int MPCost { get { return mPCost; } }
        public int TPCost { get { return tPCost; } }
        public string AnimatorTriggerName { get { return animatorTriggerName; } }

        //supply a getter that returns the Type of Command that uses the Config
        public abstract Type CommandType { get; }
    }
}
