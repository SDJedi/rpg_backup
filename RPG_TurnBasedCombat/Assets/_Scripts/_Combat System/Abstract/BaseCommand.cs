using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Last Reviewed: 2/18/20
namespace RPG.CombatSystem
{
    //Note: Most of the variables are set by a ScriptableObject asset that inherits from BaseCommand_Config.cs
    //      This allows Commands to be built using the asset menu, and hides most of the variables on the Command components
    //      attached to characters

    //the basic building block for all Commands. used by both PCs and enemies
    public abstract class BaseCommand : MonoBehaviour
    {
        //Note: descendents of BaseCommand will require specific descendents of BaseCommand_Config
        //see Assertions used in Start() of derived classes (e.g. Assert.IsTrue(command_Config.GetType() == typeof(MeleeAttack_Config));)
        [Header("Note: config Type must match command Type")]
        [SerializeField] protected BaseCommand_Config command_Config;

        [Header("Leave as (-1) unless this is a PC MainMenu Command: Attack(0), Defend(1), Special(2), Magic(3), Items(4)")]
        [SerializeField] protected int buttonIndex = -1;
        public int ButtonIndex { get { return buttonIndex; } set { buttonIndex = value; } }

        //components attached to this gameObject (initialized in Start())
        protected Character CScript = null;
        protected Base_Stats_Config stats = null;
        protected Animator anim = null;

        //command_Config can be set in inspector (e.g. when placing commands on enemy prefabs, or setting up main-menu Attack(0)
        //and Defend(1) on PC prefabs), but it also can be set at runtime (see PCManager.cs --> AddACommandToAPC())
        public BaseCommand_Config Command_Config { get { return command_Config; } set { command_Config = value; } }

        //Info that gets set by the command_Config
        public TargetType _TargetType { get; protected set; }
        public List<Proc> AdditionalOnHitEffects { get; protected set; }
        public GameObject ParticleEffect { get; protected set; }
        public AudioClip OnCastSoundEffect { get; protected set; } //played at start of animation (e.g. Cast / Attack)
        public AudioClip OnHitSoundEffect { get; protected set; } //played when an animation event generates an attack that hits
        public string CommandName { get; protected set; }
        public string CommandDescription { get; protected set; }
        public string OnCastMessage { get; protected set; }
        public string CombatLogMessage { get; protected set; }
        public Color32 CombatLogMssgColor { get; protected set; }
        public bool ThisCommandTriggersProcs { get; protected set; }
        public bool AddToSpecialMenu { get; protected set; }
        public bool AddToMagicMenu { get; protected set; }
        public int HPCost { get; protected set; }
        public int MPCost { get; protected set; }
        public int TPCost { get; protected set; }
        public string AnimatorTriggerName { get; protected set; }
        public bool CanAffordCosts { get { return CanAfford(); } }

        protected virtual void Start()
        {
            CScript = GetComponent<Character>();
            anim = GetComponentInChildren<Animator>();
            stats = CScript.Stats_Config; //Note: This MUST point to the cloned (instantiated) SO that was made in Awake() in PlayerCharacter.cs && EnemyCharacter.cs... if it references the assest itself instead of the clone, data gets corrupted

            InitializeValuesFromScriptableObject();
        }

        //applies to PCs only... assigned as a listener to a specific UI button (see ButtonCommandManager.cs)
        public virtual void OnButtonClick()
        {
            if (!TurnManager.Instance.PlayerInputAllowed)
            {
                return;
            }

            //alert interested parties that a CommandButton has been pressed
            if (ButtonCommandManager.CommandButtonPressed != null)
            {
                ButtonCommandManager.CommandButtonPressed(this);
            }

            //if we have a command in CommandsToQueue, but this isn't it, clear the queue and targets for this PC
            if (CScript.CommandsToQueue.Count > 0 && CScript.CommandsToQueue[0] != this)
            {
                CScript.ClearAllTargets();
                CScript.ClearCommandsToQueueList();
            }

            //if we can't afford the cost, display a message, make sure TurnStep is reset (in case we were selecting a target for a different
            //command when we pressed this button), refresh the UI, clear the queue and targets for this PC
            if (!CanAfford())
            {
                TurnManager.Instance.SetCurrentTurnStep(TurnStep.SelectSubMenuCommand);
                SoundManager.Instance.PlayBuzzer();
                Combat_UI_Manager.Instance.SetMsg_TopOfScreen("Can't afford cost of " + CommandName, Color.white, 0, false);
                Combat_UI_Manager.Instance.Un_OutlineAllCharacters();
                Combat_UI_Manager.Instance.OutlineCharacter(gameObject);
                CScript.ClearAllTargets();
                CScript.ClearCommandsToQueueList();
                return;
            }

            //highlight just this PC
            Combat_UI_Manager.Instance.Un_OutlineAllCharacters();
            Combat_UI_Manager.Instance.OutlineCharacter(gameObject);

            AllowPlayerToChooseTargetOrQueueCommandIfAlreadyHaveTarget(CScript);
        }

        //if this is our first time clicking this command button: 
        //    a) if command requires a target, set a potential target and move to targeting step
        //    b) else, queue this command and move to next pc.
        //if this is our second time clicking this command button:
        //    we already have a target (one we selected, or the potential target that was set on first click),
        //    so queue this command and move to next pc:
        protected virtual void AllowPlayerToChooseTargetOrQueueCommandIfAlreadyHaveTarget(Character character)
        {
            //no choice here, Command can only target self
            if (_TargetType == TargetType.Self)
            {
                character.AddCommandForQueuing(this);
                character.SetFriendlyTarget(character);
                TurnManager.Instance.MoveToNextPC(gameObject);
            }

            //player needs to choose a living, friendly target
            if (_TargetType == TargetType.Friendly)
            {
                if (character.FriendlyTarget != null && !character.FriendlyTarget.IsAlive)
                {
                    Combat_UI_Manager.Instance.SetMsg_TopOfScreen("Can't cast " + CommandName + " on a dead character", Color.white, 0, false);
                    return;
                }
                //first click; set a default target and wait for player to confirm or change (see TargetManager.cs; mouse-over/tab changes target)
                if (character.FriendlyTarget == null || (character.CommandsToQueue.Count > 0 && character.CommandsToQueue[0] != this))
                {
                    character.AddCommandForQueuing(this);
                    TurnManager.Instance.SetCurrentTurnStep(TurnStep.SelectFriendlyTarget);
                    TargetManager.Instance.SelectFirstLivingPCIn_PCsInParty_AndSetItAsActivePCsFriendlyTarget();
                }
                //second click; we have a target already, so move on to next PC
                else if (character.CommandsToQueue.Count > 0 && character.CommandsToQueue[0] == this && character.FriendlyTarget != null)
                {
                    TurnManager.Instance.MoveToNextPC(gameObject);
                }
            }

            //player needs to choose a living, friendly target (not self)
            if (_TargetType == TargetType.Friendly_NotSelf)
            {
                if (character.FriendlyTarget != null && !character.FriendlyTarget.IsAlive)
                {
                    Combat_UI_Manager.Instance.SetMsg_TopOfScreen("Can't cast " + CommandName + " on a dead character", Color.white, 0, false);
                    return;
                }

                if (character.FriendlyTarget == CScript)
                {
                    Combat_UI_Manager.Instance.SetMsg_TopOfScreen("Can't cast " + CommandName + " on self", Color.white, 0, false);
                    return;
                }
                //first click...
                if (character.FriendlyTarget == null || (character.CommandsToQueue.Count > 0 && character.CommandsToQueue[0] != this))
                {
                    character.AddCommandForQueuing(this);
                    TurnManager.Instance.SetCurrentTurnStep(TurnStep.SelectFriendlyTarget);
                    TargetManager.Instance.SelectFirstLivingPCIn_PCsInPartyThatIsNotSelf_AndSetItAsActivePCsFriendlyTarget(CScript);
                }
                //second click...
                else if (character.CommandsToQueue.Count > 0 && character.CommandsToQueue[0] == this && character.FriendlyTarget != null)
                {
                    TurnManager.Instance.MoveToNextPC(gameObject);
                }
            }

            //player needs to choose an enemy target
            if (_TargetType == TargetType.Hostile)
            {
                if (character.HostileTarget != null && !character.HostileTarget.IsAlive)
                {
                    Combat_UI_Manager.Instance.SetMsg_TopOfScreen("Can't cast " + CommandName + " on a dead character", Color.white, 0, false);
                    return;
                }
                //first click...
                if (character.HostileTarget == null || (character.CommandsToQueue.Count > 0 && character.CommandsToQueue[0] != this))
                {
                    character.AddCommandForQueuing(this);
                    TurnManager.Instance.SetCurrentTurnStep(TurnStep.SelectHostileTarget);
                    TargetManager.Instance.SelectFirstLivingEnemyIn_EnemiesInParty_AndSetItAsActivePCsHostileTarget();
                }
                //second click...
                else if (character.CommandsToQueue.Count > 0 && character.CommandsToQueue[0] == this && character.HostileTarget != null)
                {
                    TurnManager.Instance.MoveToNextPC(gameObject);
                }
            }

            //player needs to choose a dead, friendly target
            if (_TargetType == TargetType.DeadFriendly)
            {
                if (character.FriendlyTarget != null && character.FriendlyTarget.IsAlive)
                {
                    SoundManager.Instance.PlayBuzzer();
                    Combat_UI_Manager.Instance.SetMsg_TopOfScreen("Must cast " + CommandName + " on a dead ally.", Color.white, 0, false);
                    return;
                }
                //first click...
                if (character.FriendlyTarget == null || (character.CommandsToQueue.Count > 0 && character.CommandsToQueue[0] != this))
                {
                    TurnManager.Instance.SetCurrentTurnStep(TurnStep.SelectFriendlyTarget);
                    TargetManager.Instance.SetFirstDeadPCAsFriendlyTarget(character);
                    //if there are no dead friendlies...
                    if (character.FriendlyTarget == null)
                    {
                        //display error and reset UI
                        Combat_UI_Manager.Instance.SetMsg_TopOfScreen("There are no dead allies to use that on.", Color.white, 0, false);
                        SoundManager.Instance.PlayBuzzer();
                        TurnManager.Instance.SetCurrentTurnStep(TurnStep.SelectSubMenuCommand);
                        Combat_UI_Manager.Instance.UpdateUI_SingleFriendlyTarget();
                        return;
                    }
                    character.AddCommandForQueuing(this);
                }
                //second click...
                else if (character.CommandsToQueue.Count > 0 && character.CommandsToQueue[0] == this && character.FriendlyTarget != null)
                {
                    TurnManager.Instance.MoveToNextPC(gameObject);
                }
            }

            //this Command does not require a target to be chosen by player
            if (_TargetType == TargetType.All || _TargetType == TargetType.AllFriendly || _TargetType == TargetType.AllHostile)
            {
                character.ClearAllTargets();
                character.AddCommandForQueuing(this);
                TurnManager.Instance.MoveToNextPC(gameObject);
            }
        }

        //called by TurnManager.cs when this Command is at the top of the CommandQueue
        //does the following:
        //1) checks for sufficient resources to cast.
        //2) checks that the target of the command is valid, and if not attempts to secure a new valid target.
        //3) if checks 1 and 2 pass, calls DeductCost()
        //4) finally calls DoCommand()
        public virtual IEnumerator CommandInstructions()
        {
            //make sure the character can still afford to cast the ability at time of execution
            if (!CanAfford())
            {
                yield return NotEnoughResources();
                yield break;
            }
            //make sure target is still valid at time of execution
            if (!TargetConfirmedValid())
            {
                SoundManager.Instance.PlayBuzzer();
                Combat_UI_Manager.Instance.SetMsg_TopOfScreen(CScript.Stats_Config.CharName + " could not find a valid target for " + CommandName + ".", Color.white);
                yield return new WaitForSeconds(2f);
                yield break;
            }

            DeductCost();
            yield return DoCommand(); //do stuff specific to this command
        }

        //will check if target is valid, and if not, attempt to set a valid target
        bool TargetConfirmedValid()
        {
            if (_TargetType == TargetType.Friendly)
            {
                if (!TargetIsValid())
                {
                    //print("Friendly target is null or dead...looking for replacement");
                    TargetManager.Instance.SetARandomLivingFriendlyCharacterAsFriendlyTarget(CScript);
                }

                if (!TargetIsValid())
                {
                    Debug.Log("No valid target: Could not execute command");
                    return false;
                }
            }
            else if (_TargetType == TargetType.Friendly_NotSelf)
            {
                if (!TargetIsValid())
                {
                    print("Friendly target is null or dead...looking for replacement (self not valid)");
                    TargetManager.Instance.SetARandomLivingFriendlyCharacterAsFriendlyTarget(CScript, false);
                }

                if (!TargetIsValid())
                {
                    Debug.Log("No valid target: Could not execute command");
                    return false;
                }
            }
            else if (_TargetType == TargetType.Hostile)
            {
                if (!TargetIsValid())
                {
                    //print("Hostile target is null or dead...looking for replacement");
                    TargetManager.Instance.SetARandomLivingHostileCharacterAsHostileTarget(CScript);
                }

                if (!TargetIsValid())
                {
                    Debug.Log("No valid target: Could not execute command");
                    return false;
                }
            }
            else if (_TargetType == TargetType.DeadFriendly)
            {
                if (!TargetIsValid())
                {
                    //print("Friendly target is invalid...looking for replacement");
                    TargetManager.Instance.SetFirstDeadPCAsFriendlyTarget(CScript);
                }

                if (!TargetIsValid())
                {
                    Debug.Log("No valid target: Could not execute command");
                    return false;
                }
            }

            if (!TargetIsValid())
            {
                Debug.Log("No valid target: Could not execute command");
                return false;
            }

            return true;
        }

        protected virtual void DeductCost()
        {
            if (HPCost > 0)
            {
                stats.ModifyCurrentHP(-HPCost);
            }
            if (MPCost > 0)
            {
                stats.ModifyCurrentMP(-MPCost);
            }
            if (TPCost > 0)
            {
                stats.ModifyCurrentTP(-TPCost);
            }
        }

        protected virtual bool CanAfford()
        {
            return (stats.CurHP > HPCost && stats.CurMP >= MPCost && stats.CurTP >= TPCost);
        }

        protected virtual IEnumerator NotEnoughResources()
        {
            Combat_UI_Manager.Instance.SetMsg_TopOfScreen(CScript.Stats_Config.CharName + " tried to cast " + CommandName + " but lacked the resources.", Color.white);
            yield return new WaitForSeconds(2);
        }

        protected string ShowCastMessage()
        {
            string mssg;

            if (OnCastMessage == "")
            {
                mssg = CScript.Stats_Config.CharName + " casts " + CommandName;
            }
            else
            {
                mssg = ReplaceTagsInMessage(OnCastMessage, CScript);
            }

            Combat_UI_Manager.Instance.SetMsg_TopOfScreen(mssg, Color.white);
            return mssg;
        }

        protected void AddToCombatLog(Color32 txtColor)
        {
            if (CombatLogMessage == null || CombatLogMessage == "")
            {
                string genericMssg = "<name> casts <command>.";
                CombatLogManager.Instance.AddToCombatLog(ReplaceTagsInMessage(genericMssg, CScript), txtColor);
                return;
            }
            else
            {
                CombatLogManager.Instance.AddToCombatLog(ReplaceTagsInMessage(CombatLogMessage, CScript), txtColor);
            }
        }

        //<name> == name of the caster
        //<command> == name of Command
        //<fTarget> == caster's friendly target
        //<hTarget> == caster's hostile target
        string ReplaceTagsInMessage(string mssg, Character CScript)
        {
            string newStr;

            newStr = mssg.Replace("<name>", CScript.Stats_Config.CharName);
            newStr = newStr.Replace("<command>", CommandName);
            if (CScript.FriendlyTarget != null)
            {
                newStr = newStr.Replace("<fTarget>", CScript.FriendlyTarget.Stats_Config.CharName);
            }

            if (CScript.HostileTarget != null)
            {
                newStr = newStr.Replace("<hTarget>", CScript.HostileTarget.Stats_Config.CharName);
            }

            return newStr;
        }

        //use the config ScriptableObject to populate Command values
        public virtual void InitializeValuesFromScriptableObject()
        {
            if (command_Config == null)
            {
                Debug.LogWarning("command_Config for " + this.name + " is null. Could not initialize Command values.");
                return;
            }

            _TargetType = command_Config._TargetType;
            AdditionalOnHitEffects = command_Config.AdditionalOnHitEffects;
            ParticleEffect = command_Config.ParticleEffect;
            OnCastSoundEffect = command_Config.OnCastSoundEffect;
            OnHitSoundEffect = command_Config.OnHitSoundEffect;
            CommandName = command_Config.CmndName;
            CommandDescription = command_Config.Description;
            OnCastMessage = command_Config.OnCastMessage;
            CombatLogMessage = command_Config.CombatLogMessage;
            CombatLogMssgColor = command_Config.CombatLogMssgColor;
            ThisCommandTriggersProcs = command_Config.ThisCommandTriggersProcs;
            AddToSpecialMenu = command_Config.AddToSpecialMenu;
            AddToMagicMenu = command_Config.AddToMagicMenu;
            HPCost = command_Config.HPCost;
            MPCost = command_Config.MPCost;
            TPCost = command_Config.TPCost;
            AnimatorTriggerName = command_Config.AnimatorTriggerName;
        }

        protected abstract IEnumerator DoCommand();
        public abstract bool TargetIsValid();
    }
}