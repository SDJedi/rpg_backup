using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

//Last Reviewed: 2/18/20
namespace RPG.CombatSystem
{
    //base class for both player characters and non-player characters.
    public abstract class Character : MonoBehaviour
    {
        [SerializeField] protected Base_Stats_Config stats_Config;
        [SerializeField] protected Transform character_Front;
        [SerializeField] protected bool hasMP = false;
        [SerializeField] protected bool hasTP = false;

        float timeSinceLastProc = Mathf.Infinity;

        public Base_Stats_Config Stats_Config { get { return stats_Config; } }
        public Transform Character_Front { get { return character_Front; } }
        public bool HasMP { get { return hasMP; } }
        public bool HasTP { get { return hasTP; } }

        public Character HostileTarget { get; protected set; }
        public Character FriendlyTarget { get; protected set; }
        public List<BaseSpell_Config> ActiveSpellEffects { get; protected set; } = new List<BaseSpell_Config>();
        public List<BaseCommand> CommandsToQueue { get; protected set; } = new List<BaseCommand>(); //The Commands that will be added to: TurnManager.cs, <List>CommandQueue
        public int InitiativeTotal { get; protected set; } //see RollForInitiative() below 

        public bool Is_A_PC { get; protected set; } //Note: Gets set to true in Awake() of PlayerCharacter.cs
        public bool IsAlive { get { return stats_Config.IsAlive(); } }


        protected virtual void Awake()
        {
            stats_Config.SetCScript(this);
        }

        void Update()
        {
            timeSinceLastProc += Time.deltaTime;
        }

        void OnEnable()
        {
            GenerateAttack.AttackLanded += AttackHit;
        }

        void OnDisable()
        {
            GenerateAttack.AttackLanded -= AttackHit;
        }

        //called whenever any attack lands
        void AttackHit(Damage_Config dc, Attack attack, Base_Stats_Config attacker, Base_Stats_Config defender, bool triggerProcs)
        {
            //if this char is the attacker, and the attack that just landed triggers procs...
            if (stats_Config == attacker && triggerProcs && timeSinceLastProc > SpellAndProcManager.minTimeBetweenProcs)
            {
                //...do procs
                SpellAndProcManager.Instance.FireProcs(this);
                timeSinceLastProc = 0;
            }
        }

        //add a Command to the List of Commands that will be added to the TurnManager Queue
        public void AddCommandForQueuing(BaseCommand cmnd)
        {
            CommandsToQueue.Add(cmnd);
            CommandsToQueue.RemoveAll(n => n == null);
        }

        public void ClearCommandsToQueueList()
        {
            CommandsToQueue.Clear();
        }

        public void SetHostileTarget(Character target)
        {
            HostileTarget = target;
        }

        public void SetFriendlyTarget(Character target)
        {
            FriendlyTarget = target;
        }

        public void ClearAllTargets()
        {
            HostileTarget = null;
            FriendlyTarget = null;
        }

        public void Set_Stats_Config(Base_Stats_Config stats)
        {
            stats_Config = stats;
        }

        //method used to effect a character with a spell. handles resistance checks and re-application of existing effects
        //Note: effect passed in here should ALWAYS be an instance, not the actual asset
        public void ApplySpellEffect(Character caster, BaseSpell_Config instantiatedSpellConfig)
        {
            bool resisted = false;

            if (instantiatedSpellConfig.IsResistable)
            {
                resisted = CheckIfResisted(caster, instantiatedSpellConfig);
            }

            if (!resisted)
            {
                switch (instantiatedSpellConfig.OnReApply)
                {
                    case ReapplicationType.Refresh:
                        if (ActiveSpellEffects.Any(e => e.CmndName == instantiatedSpellConfig.CmndName))
                        {
                            //print(gameObject.name + " ASE List already contains an instance of " + effect + ". RefreshDuration()");
                            foreach (BaseSpell_Config e in ActiveSpellEffects)
                            {
                                if (e.CmndName == instantiatedSpellConfig.CmndName)
                                {
                                    e.RefreshDuration();
                                    Destroy(instantiatedSpellConfig);
                                    break;
                                }
                            }
                        }
                        else
                        {
                            ApplyEffect(caster, instantiatedSpellConfig);
                        }
                        break;

                    case ReapplicationType.Stack:
                        //print(gameObject.name + " ASE List contains an instance of " + effect + ", but effect is stackable. Adding a new instance to the list");
                        ApplyEffect(caster, instantiatedSpellConfig);
                        break;

                    case ReapplicationType.FreshApplication:
                        if (ActiveSpellEffects.Any(e => e.CmndName == instantiatedSpellConfig.CmndName))
                        {
                            //print(gameObject.name + " ASE List already contains an instance of " + effect + ". Removing and re-applying");
                            foreach (BaseSpell_Config e in ActiveSpellEffects)
                            {
                                if (e.CmndName == instantiatedSpellConfig.CmndName)
                                {
                                    RemoveOneSpellEffect(e);
                                    ApplyEffect(caster, instantiatedSpellConfig);
                                    break;
                                }
                            }
                        }
                        else
                        {
                            ApplyEffect(caster, instantiatedSpellConfig);
                        }
                        break;

                    case ReapplicationType.NONE:
                        if (ActiveSpellEffects.Any(e => e.CmndName == instantiatedSpellConfig.CmndName))
                        {
                            break;
                        }
                        else
                        {
                            ApplyEffect(caster, instantiatedSpellConfig);
                        }
                        break;
                }
            }
        }

        //called by TurnManager.cs at the start of a character's turn [see ExecuteAllCommands()]
        public IEnumerator TickSpellEffects()
        {
            foreach (BaseSpell_Config effect in ActiveSpellEffects)
            {
                if (IsAlive && effect != null && (effect.turnsRemaining > 0 || effect.EffectRemainsUntilCured))
                {
                    yield return effect.OnTick(this);

                    if (IsAlive)
                    {
                        effect.ReduceRemainingTurns();
                        yield return new WaitForSeconds(0.25f);
                    }
                    else
                    {
                        break;
                    }
                    //print(effect.EffectName + " on " + charName +  ": turns remaining == " + effect.turnsRemaining);                    
                }
                else if (!IsAlive)
                {
                    yield break;
                }
            }
        }

        //called by TurnManager.cs at the end of a character's turn [see ExecuteAllCommands()]
        public void RemoveExpiredEffects()
        {
            for (int i = ActiveSpellEffects.Count; i > 0; i--)
            {
                if (ActiveSpellEffects[i - 1].turnsRemaining <= 0 && !ActiveSpellEffects[i - 1].EffectRemainsUntilCured)
                {
                    ActiveSpellEffects[i - 1].OnTermination(this);
                    var e = ActiveSpellEffects[i - 1];
                    ActiveSpellEffects.Remove(ActiveSpellEffects[i - 1]);
                    Destroy(e);
                    //print("Terminating " + ActiveSpellEffects[i] + " on " + name + " because turnsRemaining == 0");
                }
            }
            ActiveSpellEffects.RemoveAll(n => (n == null));
            //print(name + " ActiveSpellEffects.Count after removing all effects with 0 turns remaining == " + ActiveSpellEffects.Count);
        }

        public void RemoveOneSpellEffect(BaseSpell_Config effect)
        {
            if (ActiveSpellEffects.Contains(effect))
            {
                effect.OnTermination(this);
                ActiveSpellEffects.Remove(effect);
                Destroy(effect);
            }
        }

        public void ClearAllSpellEffects()
        {
            for (int i = ActiveSpellEffects.Count - 1; i >= 0; i--)
            {
                if (ActiveSpellEffects[i] != null)
                {
                    ActiveSpellEffects[i].OnTermination(this);
                    Destroy(ActiveSpellEffects[i]);
                }
            }
            ActiveSpellEffects.Clear();
        }

        public bool CheckForActiveSpellEffect(string spellName)
        {
            for (int i = 0; i < ActiveSpellEffects.Count; i++)
            {
                if (ActiveSpellEffects[i].CmndName == spellName)
                {
                    //Debug.Log(stats_Config.CharName + " has active spell: " + spellName);
                    return true;
                }
            }
            return false;
        }

        //InitTotal == Random(1 - 20) +  AGI_Mod + Init_Mod
        public void RollForInitiative()
        {
            InitiativeTotal = DiceRolls.D20() + Stats_Config.GetStatModifier(StatAbbreviation.AGI) + Stats_Config.InitiativeMod;
        }

        bool CheckIfResisted(Character caster, BaseSpell_Config instantiatedEffect)
        {
            DamageSpell_Config dmgSpell = (DamageSpell_Config)instantiatedEffect;
            //can't resist a spell you are weak against
            if (dmgSpell != null && (dmgSpell.AttackConfig_OnApply != null && stats_Config.WeakAgainst.Contains(dmgSpell.AttackConfig_OnApply.DmgType)) || dmgSpell.AttackConfig_OnTick != null && stats_Config.WeakAgainst.Contains(dmgSpell.AttackConfig_OnTick.DmgType))
            {
                Debug.Log(this.stats_Config.CharName + " can't resist " + dmgSpell.CmndName + " because they are weak against the dmg type of " + dmgSpell.AttackConfig_OnApply.DmgType.ToString());
                return false;
            }
            //standard resist check
            if (Random.value <= Stats_Config.MagRes)
            {
                GenerateSpecialScrollingText.GenerateScrollingText(transform, "RESIST", new Color(1, 1, 1, 1));
                CombatLogManager.Instance.AddToCombatLog(stats_Config.CharName + " RESISTED " + instantiatedEffect.CmndName + ".", new Color32(255, 255, 255, 255));
                return true;
            }
            return false;
        }

        void ApplyEffect(Character caster, BaseSpell_Config instantiatedEffect)
        {
            instantiatedEffect.OnApplication(caster, this);
            if (instantiatedEffect.TurnDuration > 0 && IsAlive)
            {
                ActiveSpellEffects.Add(instantiatedEffect);
            }
            else
            {
                Destroy(instantiatedEffect);
            }
        }
    }
}
