using System.Collections;
using UnityEngine;

//Last Reviewed: 2/17/20
namespace RPG.CombatSystem
{
    //Note: "Refresh" will refresh the duration of the spell WITHOUT triggering it's OnApply() or OnTermination(),
    //whereas FreshApplication WILL trigger OnTermination() of the existing spell, and then re-apply. 
    //See Character.cs ApplySpellEffect()
    public enum ReapplicationType { Refresh, Stack, FreshApplication, NONE }

    //contains the things that ALL spells have in common.
    //basic building block for making spells that are useable by PCs and Enemies.
    public abstract class BaseSpell_Config : BaseCommand_Config
    {
        [Header("Spell details:")]
        [Space(5)]
        [SerializeField] protected ReapplicationType onReApply = ReapplicationType.FreshApplication;
        [SerializeField] protected float animationDelay = 1.5f; //used just after casting animation is triggered
        [SerializeField] protected float delayAfterParticleEffect = 0; //if we want to pause before dealing damage after particle effect spawns
        [SerializeField] protected bool isBeneficial = false;
        [SerializeField] protected bool isDetrimental = false;
        [SerializeField] protected bool isResistable = true;
        [SerializeField] protected bool isCureable_Dispelable = true;
        [SerializeField] protected bool effectRemainsUntilCured = false;
        [SerializeField] protected int turnDuration = 1;

        public int turnsRemaining { get; protected set; } //Note: need to set this = turnDuration in OnApplication()

        public ReapplicationType OnReApply { get { return onReApply; } }
        public float AnimationDelay { get { return animationDelay; } }
        public float DelayAfterParticleEffect { get { return delayAfterParticleEffect; } }
        public bool IsBeneficial { get { return isBeneficial; } }
        public bool IsDetrimental { get { return isDetrimental; } }
        public bool IsResistable { get { return isResistable; } }
        public bool IsCureable_Dispelable { get { return isCureable_Dispelable; } }
        public bool EffectRemainsUntilCured { get { return effectRemainsUntilCured; } }
        public int TurnDuration { get { return turnDuration; } }


        public void RefreshDuration()
        {
            turnsRemaining = turnDuration;
        }

        public void ReduceRemainingTurns()
        {
            turnsRemaining--;
        }


        public abstract void OnApplication(Character caster, Character effectedTarget); //do this when effect is applied
        public abstract IEnumerator OnTick(Character effectedTarget); //do this each Tick;
        public abstract void OnTermination(Character effectedTarget); //do this when effect is removed
    }
}