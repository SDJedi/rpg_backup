using UnityEngine;

//Last Reviewed: 2/18/20
namespace RPG.CombatSystem
{
    //Allow for more complex enemy AI
    public abstract class EnemyBrain : MonoBehaviour
    {
        public abstract BaseCommand DetermineCommandToUse();
    }
}
