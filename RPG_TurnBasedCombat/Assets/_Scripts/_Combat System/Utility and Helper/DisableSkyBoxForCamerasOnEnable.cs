using UnityEngine;

//Last Reviewed: 3/1/20
namespace RPG.CombatSystem
{
    //used on battlefield environments where we don't want a skybox (OrcFortress for example)
    public class DisableSkyBoxForCamerasOnEnable : MonoBehaviour
    {
        [SerializeField] Color backgroundColor = Color.black;


        void OnEnable()
        {
            CameraManager.Instance.UseSolidColorBackground(backgroundColor);
        }

        void OnDisable()
        {
            if (CameraManager.InstanceExists)
            {
                CameraManager.Instance.UseSkyBoxBackground();
            }
        }
    }
}
