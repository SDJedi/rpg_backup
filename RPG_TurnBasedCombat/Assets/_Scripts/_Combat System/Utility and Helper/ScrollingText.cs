using TMPro;
using UnityEngine;

//Last Reviewed: 3/1/20
namespace RPG.CombatSystem
{
    //makes a textMesh face the camera and moves it upward at a configurable speed
    //for a configurable amount of time, and then destroys the gameObject.
    public class ScrollingText : MonoBehaviour
    {
        public float Duration = 1.5f;
        public float Speed = 12;
        public bool VariableSpeed = true;

        TextMeshPro textMesh;
        float startTime;
        float spdVariation;


        void Awake()
        {
            textMesh = GetComponentInChildren<TextMeshPro>();
            startTime = Time.time;
        }

        void Update()
        {
            transform.forward = CameraManager.Instance.ActiveCam.transform.forward;

            if (Time.time - startTime < Duration)
            {
                if (VariableSpeed)
                {
                    spdVariation = (Random.Range(Speed * 0.5f, Speed * 1.5f));
                }
                else
                {
                    spdVariation = Speed;
                }
                transform.Translate(Vector3.up * spdVariation * Time.deltaTime);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public void SetText(string text)
        {
            textMesh.text = text;
        }

        public void SetColor(Color color)
        {
            textMesh.color = color;
        }
    }
}
