using TMPro;
using UnityEngine;
using UnityEngine.UI;

//Last Reviewed: 3/1/20
namespace RPG.CombatSystem
{
    //used for spells that require an additional choice to be made after they are selected.
    //see Command_SpellEffect_ChoicesOnCast.cs
    public class ChoiceBoxConfig : MonoBehaviour
    {
        public TextMeshProUGUI TopOfBoxMessage;
        public Button btn0;
        public Button btn1;
    }
}
