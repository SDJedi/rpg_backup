using UnityEngine;

//Last Reviewed: 3/1/20
namespace RPG.CombatSystem
{
    //for large creatures, scale the size of spell particle effects that hit them
    public class ParticleEffectScaler : MonoBehaviour
    {
        [SerializeField] float desiredSize = 1;

        public void ScaleParticleEffect(GameObject particleEffect)
        {
            particleEffect.transform.localScale = new Vector3(desiredSize, desiredSize, desiredSize);
        }
    }
}
