using UnityEngine;

//Last Reviewed: 3/1/20
namespace RPG.CombatSystem
{
    //force this object to face the active camera
    public class AutoFaceCamera : MonoBehaviour
    {
        Camera mainCam; //cache Camera.main for performance reasons


        void Start()
        {
            mainCam = Camera.main;
        }

        void Update()
        {
            if (CameraManager.InstanceExists)
            {
                transform.forward = CameraManager.Instance.ActiveCam.transform.forward;
            }
            else
            {
                transform.forward = mainCam.transform.forward;
            }
        }
    }
}
