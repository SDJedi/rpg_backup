using UnityEngine;

//Last Reviewed: 3/1/20
namespace RPG.CombatSystem
{
    //works with ParticleEffectScaler to enlarge particle effects that hit large creatures
    public static class ScaleObjectWithModel
    {
        public static void MatchScale(GameObject model, GameObject obj)
        {
            var scaleScript = model.GetComponentInChildren<ParticleEffectScaler>();
            if (scaleScript != null)
            {
                scaleScript.ScaleParticleEffect(obj);
            }
        }
    }
}
