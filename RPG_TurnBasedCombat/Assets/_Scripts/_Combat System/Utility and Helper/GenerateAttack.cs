using System;
using UnityEngine;

//Last Reviewed: 3/1/20
namespace RPG.CombatSystem
{
    //this class does the heavy lifting when it comes to figuring out if an attack hits or misses and how much damage
    //it should do. it's called on by damage spells, animation events, and projectiles, and is responsible
    //for triggering all IAttackable.OnHit() when a defender is hit and all ITriggerOnMissedAttack.OnMiss() when the
    //attack doesn't land.
    public static class GenerateAttack
    {
        //base defense amount
        const int baseDef = 5;

        //Character.cs listens to this event to know when to fire procs
        public static event Action<Damage_Config, Attack, Base_Stats_Config, Base_Stats_Config, bool> AttackLanded;

        //returns true if attack hit
        public static bool ExecuteAttack(Damage_Config dc, Base_Stats_Config attackerStats, GameObject defender, bool triggerProcs)
        {
            bool attackHit = false;
            if (dc != null && attackerStats != null && defender != null)
            {
                var _defender = defender.GetComponent<Character>();
                var defenderStats = _defender.Stats_Config;

                if (AttackHit(dc, attackerStats, defenderStats) && _defender.IsAlive)
                {
                    var attack = CreateAttack(dc, attackerStats, defenderStats);
                    var attackables = defender.GetComponentsInChildren(typeof(IAttackable));

                    if (AttackLanded != null)
                    {
                        AttackLanded(dc, attack, attackerStats, defenderStats, triggerProcs);
                    }

                    foreach (IAttackable iAttackable in attackables)
                    {
                        iAttackable.OnHit(dc, attack, attackerStats, defenderStats, triggerProcs);
                    }

                    attackHit = true;
                }
                //attack missed...
                else if (_defender.IsAlive)
                {
                    var attack = CreateAttack(dc, attackerStats, defenderStats);
                    var trigOnMiss = defender.GetComponentsInChildren(typeof(ITriggerOnMissedAttack));

                    foreach (ITriggerOnMissedAttack iMissed in trigOnMiss)
                    {
                        iMissed.OnMiss(dc, attack, attackerStats, defenderStats, triggerProcs);
                    }

                    CombatLogManager.Instance.AddToCombatLog(attackerStats.CharName + " tried to attack " + defenderStats.CharName + " but MISSED!", new Color32(255, 255, 255, 255));
                    GenerateSpecialScrollingText.GenerateScrollingText(defender.transform, "MISS!", new Color(1, 1, 1, 1));
                    attackHit = false;
                }
            }
            return attackHit;
        }

        //determine if an attack hits
        static bool AttackHit(Damage_Config dc, Base_Stats_Config attackerStats, Base_Stats_Config defenderStats)
        {
            bool hit;
            var roll = DiceRolls.D20();
            //Debug.Log("Roll to hit == " + roll);
            if (!dc.AttackCanMiss || roll == 20)
            {
                hit = true;
            }
            else if (roll == 1)
            {
                hit = false;
            }
            else
            {
                int defenderTotalDefence = baseDef + defenderStats.DEF + defenderStats.GetStatModifier(StatAbbreviation.AGI);
                //Debug.Log("DefenseTotal: " + defenderTotalDefence);
                switch (dc.AtkType)
                {
                    case AttackType.Melee:
                        hit = (roll + attackerStats.BaseAtkBonus + attackerStats.GetStatModifier(StatAbbreviation.STR) > defenderTotalDefence);
                        //Debug.Log("(M)AttackTotal: " + (roll + attackerStats.baseAtkBonus + attackerStats.GetStatModifier(StatAbbreviation.STR)));
                        break;
                    case AttackType.Ranged:
                        hit = (roll + attackerStats.BaseAtkBonus + attackerStats.GetStatModifier(StatAbbreviation.AGI) > defenderTotalDefence);
                        //Debug.Log("(R)AttackTotal: " + (roll + attackerStats.baseAtkBonus + attackerStats.GetStatModifier(StatAbbreviation.AGI)));
                        break;
                    case AttackType.Spell:
                        hit = (roll + attackerStats.BaseAtkBonus + attackerStats.GetStatModifier(StatAbbreviation.INT) > defenderTotalDefence);
                        //Debug.Log("(S)AttackTotal: " + (roll + attackerStats.baseAtkBonus + attackerStats.GetStatModifier(StatAbbreviation.INT)));
                        break;
                    default:
                        hit = false;
                        break;
                }
            }
            return hit;
        }

        static Attack CreateAttack(Damage_Config dc, Base_Stats_Config attackerStats, Base_Stats_Config defenderStats)
        {
            float coreDamage = 0;
            //roll for damage based on DamageConfig
            coreDamage += UnityEngine.Random.Range(dc.MinDamage, dc.MaxDamage);

            //roll for crit
            bool isCrit = UnityEngine.Random.value < dc.CritChance;
            if (isCrit)
            {
                coreDamage += UnityEngine.Random.Range(dc.MinDamage, dc.MaxDamage);
            }

            //add modifier based on AttackType
            switch (dc.AtkType)
            {
                case AttackType.Melee:
                    coreDamage += attackerStats.GetStatModifier(StatAbbreviation.STR);
                    break;
                case AttackType.Ranged:
                    coreDamage += attackerStats.GetStatModifier(StatAbbreviation.AGI);
                    break;
                case AttackType.Spell:
                    coreDamage += attackerStats.GetStatModifier(StatAbbreviation.INT);
                    break;
                default:
                    break;
            }

            //add attacker's damage bonus
            if (dc.AtkType != AttackType.DamageShield)
            {
                coreDamage += attackerStats.DamageBonus;
            }

            // no negative values for damage at this point ; 1 is minimum dmg
            if (coreDamage < 1)
            {
                coreDamage = 1;
            }

            if (defenderStats != null)
            {
                //weakAgainst == double dmg :: strongAgainst == half dmg :: attunedTo == dmg heals
                coreDamage = ModifyDamageBasedOnStrengthsAndWeaknesses(coreDamage, dc.DmgType, defenderStats);

                //if coreDamage is a positive number && more than the defender's DamageReduction...
                if (coreDamage > 0 && coreDamage > defenderStats.DmgReduction)
                {
                    //subtract DamageReduction for final value
                    coreDamage = (coreDamage - defenderStats.DmgReduction);
                }
                //if coreDamage is positive number BUT is less than the defender's DamageReduction...
                else if (coreDamage > 0 && coreDamage < defenderStats.DmgReduction)
                {
                    //set coreDamage to 0
                    coreDamage = 0;
                }
            }

            return new Attack((int)coreDamage, isCrit);
        }

        static float ModifyDamageBasedOnStrengthsAndWeaknesses(float dmg, DamageType dt, Base_Stats_Config defenderStats)
        {
            //check for weakness...
            foreach (DamageType d in defenderStats.WeakAgainst)
            {
                if (dt == d)
                {
                    //weakness found
                    return dmg * 2;
                }
            }
            //check for "strong against"
            foreach (DamageType d in defenderStats.StrongAgainst)
            {
                if (dt == d)
                {
                    //strength found
                    return dmg / 2;
                }
            }
            //check for "attuned"
            foreach (DamageType d in defenderStats.AttunedTo)
            {
                if (dt == d)
                {
                    //attuned
                    //Debug.Log(defenderStats.name + " is attuned. Dmg value = " + dmg);
                    return dmg * -1;
                }
            }
            //no modifications
            return dmg;
        }
    }
}