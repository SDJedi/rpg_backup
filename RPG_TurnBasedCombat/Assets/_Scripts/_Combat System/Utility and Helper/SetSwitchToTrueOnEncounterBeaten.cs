using RPG.InteractableSystem;
using UnityEngine;

//Last Reviewed: 3/1/20
namespace RPG.CombatSystem
{
    //several of these are attached to a gameObject in the CombatScene called OnEncounterBeaten.
    //primary use is to set a game switch when a boss is beaten.
    public class SetSwitchToTrueOnEncounterBeaten : MonoBehaviour
    {
        [SerializeField] EnemyEncounter encounter = null;
        [SerializeField] string switchName = null;


        void Start()
        {
            PostBattleManager.Instance.EncounterBeaten += OnEncounterBeaten;
        }

        void OnDestroy()
        {
            if (PostBattleManager.InstanceExists)
            {
                PostBattleManager.Instance.EncounterBeaten -= OnEncounterBeaten;
            }
        }

        void OnEncounterBeaten(EnemyEncounter enc)
        {
            if (enc == encounter)
            {
                AllGameSwitches.TrySetCondition(switchName, true);
            }
        }
    }
}
