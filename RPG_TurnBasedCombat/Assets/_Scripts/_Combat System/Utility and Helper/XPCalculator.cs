using System.Collections.Generic;
using RPG.ProgressionSystem;
using UnityEngine;

//Last Reviewed: 3/1/20
namespace RPG.CombatSystem
{
    //class used to calculate PC levels
    public static class XPCalculator
    {
        //public static List<int> XPCHart = new List<int>() { 0, 300, 900, 2700, 6500, 14000, 23000, 34000, 48000, 64000, 85000, 100000, 120000, 140000, 165000, 195000, 225000, 265000, 305000, 355000 };
        //sped up leveling ... above is original chart
        public static List<int> XPCHart = new List<int>() { 0, 100, 300, 700, 1500, 3100, 6300, 12700, 48000, 64000, 85000, 100000, 120000, 140000, 165000, 195000, 225000, 265000, 305000, 355000 };

        public static int MaxLevel { get { return XPCHart.Count; } }

        public static int GetLvlBasedOnCurrentXP(int curXP)
        {
            if (curXP < 0)
            {
                Debug.LogWarning("Can't have negative curXP");
                return 1;
            }

            for (int i = XPCHart.Count - 1; i >= 0; i--)
            {
                if (curXP >= XPCHart[i])
                {
                    return i + 1;
                }
            }

            return 1;
        }

        public static void AwardXPAndCP(int totalXPToAward)
        {
            List<PC_Stats_Config> statsOfLivingPCsInParty = PCManager.Instance.GetStatsForPCsInParty(true);
            int xpPerChar = totalXPToAward / statsOfLivingPCsInParty.Count;

            //add xp to all living PCs in the party
            for (int i = 0; i < statsOfLivingPCsInParty.Count; i++)
            {
                statsOfLivingPCsInParty[i].ModifyCurrentXP(xpPerChar);
                int lvlAfterAllXPAwarded = XPCalculator.GetLvlBasedOnCurrentXP(statsOfLivingPCsInParty[i].CurXP);
                while (statsOfLivingPCsInParty[i].CurLvl < lvlAfterAllXPAwarded)
                {
                    statsOfLivingPCsInParty[i].IncreasePCLvl();
                }
            }
            AwardCharPoints(totalXPToAward);
        }

        public static void AwardCharPoints(int totalXPToAward)
        {
            var XP = CharacterPoints_Tracker.Instance.XPTowardsNextPoint;
            XP += totalXPToAward;

            while (XP >= CharacterPoints_Tracker.Instance.XPRequiredPerCP)
            {
                CharacterPoints_Tracker.Instance.ModifyCharacterPoints(1);
                XP -= CharacterPoints_Tracker.Instance.XPRequiredPerCP;
            }

            CharacterPoints_Tracker.Instance.XPTowardsNextPoint = XP;
        }
    }
}
