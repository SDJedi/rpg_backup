using TMPro;
using UnityEngine;

//Last Reviewed: 3/1/20
namespace RPG.CombatSystem
{
    //set a message and watch it scroll...
    public static class GenerateSpecialScrollingText
    {
        public static void GenerateScrollingText(Transform location, string msg, Color clr)
        {
            GameObject S_Text = new GameObject("S_Text");
            GameObject sTxt_inner = new GameObject("sTxt_inner", typeof(RectTransform));

            sTxt_inner.transform.SetParent(S_Text.transform);
            S_Text.transform.position = location.position;

            var tmComp = sTxt_inner.AddComponent<TextMeshPro>();
            RectTransform rt = sTxt_inner.GetComponent<RectTransform>();
            tmComp.fontSize = 10;
            tmComp.fontSizeMin = 5;
            tmComp.fontSizeMax = 10;
            tmComp.alignment = TextAlignmentOptions.Center;
            rt.sizeDelta = new Vector2(20, 5);
            rt.rotation = Quaternion.Euler(0, 0, 0);

            var t = S_Text.AddComponent<ScrollingText>();

            t.SetColor(clr);
            t.SetText(msg);
        }
    }
}
