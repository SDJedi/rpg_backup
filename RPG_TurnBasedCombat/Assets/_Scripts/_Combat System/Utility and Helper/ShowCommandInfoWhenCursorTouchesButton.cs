using UnityEngine;
using UnityEngine.EventSystems;

//Last Reviewed: 3/1/20
namespace RPG.CombatSystem
{
    //shows / hides UI related to a Command when mouse is over a Command button
    public class ShowCommandInfoWhenCursorTouchesButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        BaseCommand cmndAssignedToButton; //set in ButtonCommandManager.cs


        public void StoreReferenceToCommandCurrentlyOnThisButton(BaseCommand cmnd)
        {
            cmndAssignedToButton = cmnd;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            Combat_UI_Manager.Instance.ShowCommandInfoPanel();
            Combat_UI_Manager.Instance.PopulateCommandInfoPanel(cmndAssignedToButton);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            Combat_UI_Manager.Instance.HideCommandInfoPanel();
        }
    }
}
