using RPG.InteractableSystem;
using RPG.QuestSystem;
using RPG.Saving;
using UnityEngine;
using UnityEngine.AI;
//Last Reviewed: 8/30/19

public class WerepupScript : MonoBehaviour
{
    public Quest Quest;
    public GameObject WerepupDestination;
    public Condition rescuedCondition;

    NavMeshAgent nav;
    Animator anim;
    GameObjectFollowPC followScript;
    readonly int hash_Speed_AnimPara = Animator.StringToHash("Speed");


    void Start()
    {
        nav = GetComponent<NavMeshAgent>();
        anim = GetComponentInChildren<Animator>();
        followScript = GetComponent<GameObjectFollowPC>();
    }

    void Update()
    {
        if (!followScript.enabled)
        {
            float speed = nav.desiredVelocity.magnitude;

            nav.SetDestination(WerepupDestination.transform.position);
            anim.SetFloat(hash_Speed_AnimPara, speed, 0.1f, Time.deltaTime);
        }
    }

    void OnEnable()
    {
        QuestManager.QuestObjectiveUpdated += ParentsFound;
    }

    void OnDisable()
    {
        QuestManager.QuestObjectiveUpdated -= ParentsFound;
    }

    void ParentsFound(Quest quest, int index)
    {
        if (quest.Title == Quest.Title && index == 0)
        {
            nav.stoppingDistance = 0;
            nav.speed = 12;
            AllGameSwitches.TrySetCondition(rescuedCondition, true);
            //rescuedCondition.satisfied = true;
            followScript.enabled = false;
            GameObject.FindObjectOfType<SavingWrapper>().SaveOneGameObject(gameObject);
        }
    }
}
