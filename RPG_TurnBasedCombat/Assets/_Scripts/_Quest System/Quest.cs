using System;
using System.Collections.Generic;
using UnityEngine;

//Last Reviewed: 2/5/20
namespace RPG.QuestSystem
{
    public enum QuestState { Active, Complete, NotAcquired }

    //create a new quest
    [CreateAssetMenu(fileName = "New Quest", menuName = "Quest")]
    public class Quest : ScriptableObject
    {
        public const int MaxNumOfObjectives = 5; //UI has spots for 5 objectives per quest

        public string Title; //quest name
        [TextArea]
        public string Description; //quest description
        [TextArea]
        public string CompletedDescription; //text to show in journal when quest is completed
        public int Level; //difficulty lvl ... not in use as of 2/5/20
        public QuestState State = QuestState.NotAcquired;
        public bool IsRepeatable;
        public bool RewardClaimed;
        [Space(20)]
        public List<QuestObjective> Objectives; //All objectives that must be completed to finish this quest
        [Space(20)]
        public QuestReward Reward; //the reward for quest completion

        public DateTime DateAcquired;
        public DateTime DateCompleted;


        //check to see if all required objectives are complete, called each time an objective is updated ... see QuestManager.cs, UpdateQuestObjective()
        public bool AllRequiredObjectivesComplete()
        {
            for (int i = Objectives.Count - 1; i >= 0; i--)
            {
                if (!Objectives[i].ObjectiveComplete && !Objectives[i].IsOptional)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
