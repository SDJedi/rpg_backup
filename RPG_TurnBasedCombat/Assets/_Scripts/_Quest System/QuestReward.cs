using System.Collections.Generic;
using RPG.Inventory;
using UnityEngine;

//Last Reviewed: 2/5/20
namespace RPG.QuestSystem
{
    //create a new reward for a quest
    [CreateAssetMenu(fileName = "New Quest Reward", menuName = "Quest Reward")]
    public class QuestReward : ScriptableObject
    {
        [Header("Note: Max items per reward == 8")]
        public List<Item> ItemRewards;
        public int GPReward;
        public int XPReward;
    }
}
