using UnityEngine;

//Last Reviewed: 2/5/20
namespace RPG.QuestSystem
{
    //this script is attached to all active and completed quest buttons
    public class QuestAttachedToButton : MonoBehaviour
    {
        public Quest quest = null;

        //set the QuestOnDisplay and update the UI
        public void OnQuestClick()
        {
            QuestJournal.Instance.UpdateQuestOnDisplayDetails(quest);
        }
    }
}
