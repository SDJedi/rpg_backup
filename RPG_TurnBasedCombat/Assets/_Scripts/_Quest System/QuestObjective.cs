using System.Collections.Generic;
using RPG.InteractableSystem;
using UnityEngine;

//Last Reviewed: 2/5/20
namespace RPG.QuestSystem
{
    //represents a task to be completed as part of a quest
    //a quest can have up to 5 objectives
    [System.Serializable]
    public class QuestObjective
    {
        [TextArea]
        public string Description;
        [TextArea]
        public string ObjectiveUpdatedDescription;

        [SerializeField] int counter = 0;
        [SerializeField] int counterTarget = 0; //Note: for an objective to be marked complete, the "counter" must be >= "counterTarget"
        [SerializeField] bool isOptional = false; //quest can be completed without completing this objective
        [SerializeField] bool isHidden = false;
        [SerializeField] List<int> objectivesToShowWhenThisObjectiveIsComplete = null;
        [SerializeField] List<Condition> conditionsToSetTrueOnCompletion = null;

        //defaults; needed for repeatable quests
        int defaultCounter;
        int defaultCounterTarget;
        string defaultDescription;
        bool defaultIsHidden;

        public int Counter { get { return counter; } }
        public int CounterTarget { get { return counterTarget; } }
        public List<int> ObjectivesToShowWhenThisObjectiveIsComplete { get { return objectivesToShowWhenThisObjectiveIsComplete; } }
        public List<Condition> ConditionsToSetTrueOnCompletion { get { return conditionsToSetTrueOnCompletion; } }
        public bool IsOptional { get { return isOptional; } }
        public bool IsHidden { get { return isHidden; } }
        public bool ObjectiveComplete { get; private set; }


        //change the description of this objective 
        void UpdateDescription(string newDescription)
        {
            newDescription = newDescription.Replace("<counter>", counter.ToString());
            Description = newDescription;
        }

        //should only be called by QuestManager.cs --> UpdateQuestObjective()
        //returns true if objective completed...
        public bool UpdateQuestObjective(int amnt)
        {
            counter += amnt;

            if (counter >= counterTarget)
            {
                ObjectiveComplete = true;
                UpdateDescription(ObjectiveUpdatedDescription);
                return true;
            }
            
            UpdateDescription(ObjectiveUpdatedDescription);
            return false;
        }

        public void ShowObjective(bool show)
        {
            isHidden = !show;
        }

        public void SetObjectiveDefaults()
        {
            defaultCounter = counter;
            defaultCounterTarget = counterTarget;
            defaultDescription = Description;
            defaultIsHidden = isHidden;
        }

        public void ResetObjective()
        {
            counter = defaultCounter;
            counterTarget = defaultCounterTarget;
            Description = defaultDescription;
            isHidden = defaultIsHidden;
            ObjectiveComplete = false;
        }

        //used by save system to restore objective from save file
        public void SetCounter(int counter)
        {
            this.counter = counter;
        }
        //used by save system to restore objective from save file
        public void SetDescription(string description)
        {
            Description = description;
        }
        //used by save system to restore objective from save file
        public void SetCompletionState(bool state)
        {
            ObjectiveComplete = state;
        }
    }
}
