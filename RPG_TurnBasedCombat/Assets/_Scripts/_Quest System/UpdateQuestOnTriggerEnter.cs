using UnityEngine;

//Last Reviewed: 2/5/20
namespace RPG.QuestSystem
{
    //update a quest when the OverWorldPC enters a trigger
    public class UpdateQuestOnTriggerEnter : MonoBehaviour
    {
        public Quest Quest;
        public int ObjectiveIndex;
        public int[] ObjectivesThatMustBeCompletedFirst;
        public GameObject ObjectToDisable; //optional object that gets disabled when objective is updated


        void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "OverWorldPC")
            {
                UpdateQuestObjective();
            }
        }

        void UpdateQuestObjective()
        {
            if (this.Quest == null)
            {
                Debug.LogWarning("Missing quest to update on trigger enter");
                return;
            }

            Quest questInstance = QuestManager.Instance.GetQuestByName(Quest.Title);

            if (questInstance == null || questInstance.State != QuestState.Active)
            {
                Debug.Log("Quest \"" + questInstance.Title + "\" does not exist or is not active. Cannot update objective");
                return;
            }

            for (int i = 0; i < ObjectivesThatMustBeCompletedFirst.Length; i++)
            {
                if (!QuestManager.Instance.ObjectiveIsComplete(questInstance, ObjectivesThatMustBeCompletedFirst[i]))
                {
                    Debug.LogWarning("a prerequisite objective is not yet complete");
                    return;
                }
            }

            QuestManager.Instance.UpdateQuestObjective(questInstance.Title, ObjectiveIndex);

            if (ObjectToDisable != null)
            {
                ObjectToDisable.SetActive(false);
            }
        }
    }
}
