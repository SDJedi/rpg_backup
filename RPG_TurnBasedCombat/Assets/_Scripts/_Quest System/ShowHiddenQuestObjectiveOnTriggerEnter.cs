using UnityEngine;

//Last Reviewed: 2/5/20
namespace RPG.QuestSystem
{
    //Show a hidden Objective when OverWorldPC hits a trigger collider
    public class ShowHiddenQuestObjectiveOnTriggerEnter : MonoBehaviour
    {
        public Quest Quest;

        [SerializeField] int ObjectiveIndex = -1;


        void OnTriggerEnter(Collider other)
        {
            Quest q = QuestManager.Instance.GetQuestByName(Quest.Title);

            if (ObjectiveIndex < 0 || ObjectiveIndex >= q.Objectives.Count)
            {
                Debug.Log("invalid objective index");
                return;
            }

            //OverWorldPC is the trigger, show objective
            if (other.gameObject.tag == "OverWorldPC")
            {
                q.Objectives[ObjectiveIndex].ShowObjective(true);
            }
        }
    }
}
