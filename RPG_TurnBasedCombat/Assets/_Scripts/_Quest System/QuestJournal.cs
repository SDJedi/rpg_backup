using System;
using System.Collections.Generic;
using System.Linq;
using DialogueSystem;
using RPG.Inventory;
using RPG.SceneControl;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//Last Reviewed: 2/5/20
namespace RPG.QuestSystem
{
    //This class handles the UI for the Quest System / lives in the BootScene
    public class QuestJournal : Singleton<QuestJournal>
    {
        public event Action QuestJournalJustOpened; //fires whenever the QuestJournal is opened

        [SerializeField] GameObject ButtonControlPanel = null; //the options panel in the top-left of the screen

        [Header("Main Panel")]
        public GameObject QuestJournal_Panel;
        [Header("Active Quests")]
        public GameObject ActiveQuests_Panel;
        public Button[] ActiveQuestButtons;
        [Header("Completed Quests")]
        public GameObject CompletedQuests_Panel;
        public Button[] CompletedQuestButtons;

        //QuestDetailsPanel
        public TextMeshProUGUI QuestDetails_Title;
        public TextMeshProUGUI QuestDetails_DateReceived;
        public TextMeshProUGUI QuestDetails_DateCompleted;
        public TextMeshProUGUI QuestDetails_Description;
        public GameObject[] QuestDetails_Objectives;
        public Toggle[] QuestDetails_ObjectiveToggles;
        public TextMeshProUGUI[] QuestDetails_ObjectiveDescriptions;

        //QuestRewardsPanel
        public GameObject QuestRewards_Panel;
        public TextMeshProUGUI[] QuestRewardItems_Txt;
        public TextMeshProUGUI XPReward_Txt;
        public TextMeshProUGUI GPReward_Txt;
        public Button ClaimReward_Button;
        public TextMeshProUGUI RewardClaimed_Lbl;

        public Quest QuestOnDisplay { get; private set; } //we show details for one quest at a time ... this quest


        void Start()
        {
            CloseQuestJournal();
            SceneManager.activeSceneChanged += OnSceneChanged;
            QuestManager.QuestObjectiveUpdated += UpdateQuestJournal;
            QuestManager.QuestCompleted += UpdateQuestJournal;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            SceneManager.activeSceneChanged -= OnSceneChanged;
            QuestManager.QuestObjectiveUpdated -= UpdateQuestJournal;
            QuestManager.QuestCompleted -= UpdateQuestJournal;
        }

        void OnSceneChanged(Scene x, Scene newScene)
        {
            CloseQuestJournal();
        }

        void Update()
        {
            if ((Input.GetKeyDown(KeyCode.J) || Input.GetKeyDown(KeyCode.Escape)) && QuestJournalIsOpen())
            {
                CloseQuestJournal();
            }
            else if (Input.GetKeyDown(KeyCode.J) && OkToOpenQuestJournal())
            {
                OpenQuestJournal();
            }
        }

        public bool QuestJournalIsOpen()
        {
            return QuestJournal_Panel.activeSelf;
        }

        void ShowActiveQuests()
        {
            ActiveQuests_Panel.SetActive(true);
            CompletedQuests_Panel.SetActive(false);
            UpdateQuestJournal(); //Note: updates before and after are necessary to prevent issues that arise when switching saved game files
            SetQuestOnDisplayToFirstQuest();
            UpdateQuestJournal();
        }

        void ShowCompletedQuests()
        {
            CompletedQuests_Panel.SetActive(true);
            ActiveQuests_Panel.SetActive(false);
            UpdateQuestJournal();
            SetQuestOnDisplayToFirstQuest();
            UpdateQuestJournal();
        }

        void OpenQuestJournal()
        {
            QuestJournal_Panel.SetActive(true);
            Inventory_UI.Instance.CloseInvWindow();
            ShowActiveQuests();

            if (QuestJournalJustOpened != null)
            {
                QuestJournalJustOpened();
            }
        }

        public void CloseQuestJournal()
        {
            QuestJournal_Panel.SetActive(false);
        }

        //assigned to UI button OnClick
        public void ToggleQuestJournal()
        {
            if (!QuestJournalIsOpen() && OkToOpenQuestJournal())
            {
                OpenQuestJournal();
            }
            else
            {
                CloseQuestJournal();
            }
        }

        //Note: the Events in QuestManager have a signature that requires these parameters
        public void UpdateQuestJournal(Quest quest = null, int objIndex = 0)
        {
            UpdateActiveQuests();
            UpdateCompletedQuests();
            UpdateQuestOnDisplayDetails();
        }

        void EraseQuestOnDisplayDetails()
        {
            QuestDetails_Title.text = "";
            QuestDetails_DateReceived.text = "";
            QuestDetails_Description.text = "";
            QuestDetails_DateCompleted.text = "";
            ClaimReward_Button.gameObject.SetActive(false);
            RewardClaimed_Lbl.gameObject.SetActive(false);
            HideQuestRewardsPanel();

            for (int i = 0; i < QuestDetails_Objectives.Length; i++)
            {
                QuestDetails_Objectives[i].SetActive(false);
            }
        }

        //collect all active quests, sort them by DateReceived, then display them in the ActiveQuests Panel
        void UpdateActiveQuests()
        {
            ClearActiveQuestsPanel();

            List<Quest> QuestsSortedByReceivedDate = new List<Quest>();

            for (int i = 0; i < QuestManager.Instance.AllQuestsInGame.Count; i++)
            {
                if (QuestManager.Instance.AllQuestsInGame[i].State == QuestState.Active)
                {
                    QuestsSortedByReceivedDate.Add(QuestManager.Instance.AllQuestsInGame[i]);
                }
            }

            QuestsSortedByReceivedDate = QuestsSortedByReceivedDate.OrderBy(q => q.DateAcquired).ToList();

            for (int i = 0; i < ActiveQuestButtons.Length; i++)
            {
                if (i < QuestsSortedByReceivedDate.Count)
                {
                    var btn = ActiveQuestButtons[i];
                    btn.transform.gameObject.SetActive(true);
                    btn.GetComponent<QuestAttachedToButton>().quest = QuestsSortedByReceivedDate[i];
                    btn.GetComponentInChildren<TextMeshProUGUI>().text = QuestsSortedByReceivedDate[i].Title;
                }
                else
                {
                    ActiveQuestButtons[i].transform.gameObject.SetActive(false);
                }
            }
        }

        //collect all completed quests, sort them by DateCompleted, then display them in the CompletedQuests Panel
        void UpdateCompletedQuests()
        {
            ClearCompletedQuestsPanel();

            List<Quest> QuestsSortedByCompletionDate = new List<Quest>();

            for (int i = 0; i < QuestManager.Instance.AllQuestsInGame.Count; i++)
            {
                if (QuestManager.Instance.AllQuestsInGame[i].State == QuestState.Complete)
                {
                    QuestsSortedByCompletionDate.Add(QuestManager.Instance.AllQuestsInGame[i]);
                }
            }

            QuestsSortedByCompletionDate = QuestsSortedByCompletionDate.OrderBy(q => q.DateCompleted).ToList();

            for (int i = 0; i < CompletedQuestButtons.Length; i++)
            {
                if (i < QuestsSortedByCompletionDate.Count)
                {
                    var btn = CompletedQuestButtons[i];
                    //set btn color to green if quest reward is unclaimed, white if reward has been claimed
                    if (!QuestsSortedByCompletionDate[i].RewardClaimed)
                    {
                        var colors = btn.colors;
                        colors.normalColor = Color.green;
                        btn.colors = colors;
                    }
                    else
                    {
                        var colors = btn.colors;
                        colors.normalColor = Color.white;
                        btn.colors = colors;
                    }

                    btn.transform.gameObject.SetActive(true);
                    btn.GetComponent<QuestAttachedToButton>().quest = QuestsSortedByCompletionDate[i];
                    btn.GetComponentInChildren<TextMeshProUGUI>().text = QuestsSortedByCompletionDate[i].Title;
                }
                else
                {
                    CompletedQuestButtons[i].transform.gameObject.SetActive(false);
                }
            }
        }

        void ClearActiveQuestsPanel()
        {
            for (int i = ActiveQuestButtons.Length - 1; i >= 0; i--)
            {
                ActiveQuestButtons[i].GetComponentInChildren<TextMeshProUGUI>().text = "";
                ActiveQuestButtons[i].GetComponent<QuestAttachedToButton>().quest = null;
            }
        }

        void ClearCompletedQuestsPanel()
        {
            for (int i = CompletedQuestButtons.Length - 1; i >= 0; i--)
            {
                CompletedQuestButtons[i].GetComponentInChildren<TextMeshProUGUI>().text = "";
                CompletedQuestButtons[i].GetComponent<QuestAttachedToButton>().quest = null;
            }
        }

        public void UpdateQuestOnDisplayDetails(Quest quest = null)
        {
            //start with fresh UI
            EraseQuestOnDisplayDetails();

            //if we passed in a quest when method was called, make that the new QuestOnDisplay
            if (quest != null)
            {
                QuestOnDisplay = quest;
            }

            //if we did NOT pass in a quest when method was called, check to make sure that QuestOnDisplay != null
            if (QuestOnDisplay == null)
            {
                return;
            }

            //if it's a completed quest...
            if (QuestOnDisplay.State == QuestState.Complete)
            {
                QuestDetails_Title.text = QuestOnDisplay.Title;
                QuestDetails_DateReceived.text = "Received On: " + QuestOnDisplay.DateAcquired.ToString();
                QuestDetails_Description.text = QuestOnDisplay.CompletedDescription;
                QuestDetails_DateCompleted.text = "Completed On: " + QuestOnDisplay.DateCompleted.ToString();
                DisplayQuestRewardsForCompletedQuest(QuestOnDisplay);

                if (QuestOnDisplay.RewardClaimed || QuestOnDisplay.Reward == null)
                {
                    ClaimReward_Button.gameObject.SetActive(false);
                    RewardClaimed_Lbl.gameObject.SetActive(true);
                }
                else
                {
                    ClaimReward_Button.gameObject.SetActive(true);
                    RewardClaimed_Lbl.gameObject.SetActive(false);
                }
            }

            //if it's an active quest...
            if (QuestOnDisplay.State == QuestState.Active)
            {
                QuestDetails_Title.text = QuestOnDisplay.Title;
                QuestDetails_DateReceived.text = "Received On: " + QuestOnDisplay.DateAcquired.ToString();
                QuestDetails_Description.text = QuestOnDisplay.Description;
                HideQuestRewardsPanel();

                for (int i = 0; i < QuestOnDisplay.Objectives.Count; i++)
                {
                    if (!QuestOnDisplay.Objectives[i].IsHidden)
                    {
                        QuestDetails_Objectives[i].SetActive(true);
                        QuestDetails_ObjectiveToggles[i].isOn = QuestOnDisplay.Objectives[i].ObjectiveComplete;
                        QuestDetails_ObjectiveDescriptions[i].text = QuestOnDisplay.Objectives[i].Description;
                    }
                }
            }
        }

        void DisplayQuestRewardsForCompletedQuest(Quest quest)
        {
            if (quest.Reward == null)
            {
                Debug.LogWarning("the quest " + quest.Title + " does not have a reward");
                quest.RewardClaimed = true;
                return;
            }

            ShowQuestRewardsPanel();
            //set the text for all reward items...
            for (int i = 0; i < QuestRewardItems_Txt.Length; i++)
            {
                if (quest.Reward.ItemRewards.Count > 8)
                {
                    Debug.Log("reward for quest " + quest.Title + " has more than 8 items, reduce to 8 or less");
                }

                quest.Reward.ItemRewards.RemoveAll(n => n == null); //Note: affects the QuestReward asset
                if (i < quest.Reward.ItemRewards.Count)
                {
                    QuestRewardItems_Txt[i].transform.gameObject.SetActive(true);
                    QuestRewardItems_Txt[i].enabled = true;
                    QuestRewardItems_Txt[i].text = quest.Reward.ItemRewards[i].ItemName;
                    QuestRewardItems_Txt[i].GetComponentInChildren<Image>().sprite = quest.Reward.ItemRewards[i].Icon;
                    QuestRewardItems_Txt[i].color = quest.Reward.ItemRewards[i].GetItemTextColor();
                }
                else
                {
                    QuestRewardItems_Txt[i].transform.gameObject.SetActive(false);
                    QuestRewardItems_Txt[i].enabled = false;
                }
            }
            //set the text for XP and GP rewards
            XPReward_Txt.text = quest.Reward.XPReward.ToString();
            GPReward_Txt.text = quest.Reward.GPReward.ToString();
        }

        void ShowQuestRewardsPanel()
        {
            QuestRewards_Panel.SetActive(true);
        }

        void HideQuestRewardsPanel()
        {
            QuestRewards_Panel.SetActive(false);
        }

        //when journal is first opened, or active / completed tabs are pressed, set the QuestOnDisplay to the
        //first quest on the list and show details for that quest
        void SetQuestOnDisplayToFirstQuest()
        {
            if (ActiveQuests_Panel.activeSelf)
            {
                if (ActiveQuestButtons[0].transform.gameObject.activeSelf)
                {
                    QuestOnDisplay = ActiveQuestButtons[0].GetComponent<QuestAttachedToButton>().quest;
                }
                else
                {
                    QuestOnDisplay = null;
                }
            }
            else if (CompletedQuests_Panel.activeSelf)
            {
                if (CompletedQuestButtons[0].transform.gameObject.activeSelf)
                {
                    QuestOnDisplay = CompletedQuestButtons[0].GetComponent<QuestAttachedToButton>().quest;
                }
                else
                {
                    QuestOnDisplay = null;
                }
            }
        }

        bool OkToOpenQuestJournal()
        {
            return (ButtonControlPanel.activeSelf && //if the button control panel is hidden, don't open window
                !SceneController.Instance.sceneChangeInProgress &&
                !SceneController.Instance.isFading &&
                !ItemShopManager.Instance.IsShopping &&
                !DialogueManager.Instance.InConversation);
        }
    }
}
