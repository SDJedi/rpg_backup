using System;
using System.Collections.Generic;
using System.Linq;
using RPG.InteractableSystem;
using RPG.Inventory;
using RPG.Messaging;
using RPG.Saving;
using UnityEngine;

//Last Reviewed: 2/5/20
namespace RPG.QuestSystem
{
    //class to handle Quests ... activating, updating, and completing
    public class QuestManager : Singleton<QuestManager>, ISaveable
    {
        public const int MaxNumOfActiveQuests = 16;

        public static event Action<Quest, int> QuestObjectiveUpdated; //int == the index of the completed objective
        public static event Action<Quest, int> QuestCompleted; //int does not matter, but I need the Action to have the same signature as the Action above

        public List<Quest> AllQuestsInGame; //The master list of all quests

        List<Quest> QuestAssets = new List<Quest>(); //don't modify these at run-time
        int numOfActiveQuests; //track how many quests are currently active

        public bool PlayerHasUnclaimedRewards
        {
            get
            {
                foreach (Quest q in AllQuestsInGame)
                {
                    if (q.State == QuestState.Complete && !q.RewardClaimed)
                    {
                        return true;
                    }
                }
                return false;
            }
        }


        protected override void Awake()
        {
            base.Awake();
            //store a reference to the all of the Quest ScObj assets
            for (int i = 0; i < AllQuestsInGame.Count; i++)
            {
                QuestAssets.Add(AllQuestsInGame[i]);
            }

            //clone all quest objects so that changes we make at runtime
            //are applied to instances and not the actual quest assets. also removes any duplicates in the master list
            InstantiateQuests();
        }

        void Start()
        {
            SavingWrapper.Instance.StartingNewGame += InstantiateQuests;
        }

        void OnDisable()
        {
            if (SavingWrapper.InstanceExists)
            {
                SavingWrapper.Instance.StartingNewGame -= InstantiateQuests;
            }
        }

        //Adds a quest to the QuestJournal under Active Quests
        public void SetQuestAsActive(string nameOfQuest)
        {
            if (numOfActiveQuests >= MaxNumOfActiveQuests)
            {
                SoundManager.Instance.PlayBuzzer();
                NotificationWindowManager.Instance.DisplayNotification("You have reached the maximum number of active quests: [" + MaxNumOfActiveQuests + "]\nCould not add the Quest: \n\"" + nameOfQuest + "\"", Color.black);
                return;
            }

            //loop through AllQuestsInGame
            for (int i = 0; i < AllQuestsInGame.Count; i++)
            {
                //find a quest with a matching title
                if (AllQuestsInGame[i].Title == nameOfQuest)
                {
                    //make sure it's a quest we can activate (i.e. one that is flagged as NotAcquired, or a completed quest that is repeatable)
                    if (AllQuestsInGame[i].State == QuestState.NotAcquired || (AllQuestsInGame[i].State == QuestState.Complete && AllQuestsInGame[i].IsRepeatable))
                    {
                        //if it happens to be a completed, but repeatable quest...
                        if (AllQuestsInGame[i].State == QuestState.Complete)
                        {
                            //has the reward been claimed yet?
                            if (!AllQuestsInGame[i].RewardClaimed)
                            {
                                //if not, display a message and exit method...
                                SoundManager.Instance.PlayBuzzer();
                                NotificationWindowManager.Instance.DisplayNotification("The quest \"" + AllQuestsInGame[i].Title + "\" is repeatable, but you must claim the reward before you can get it again.", Color.black);
                                return;
                            }
                        }

                        //we're good to activate the quest...
                        AllQuestsInGame[i].State = QuestState.Active;
                        AllQuestsInGame[i].DateAcquired = System.DateTime.Now;
                        AllQuestsInGame[i].RewardClaimed = false; //Note: false by default, but reset to false in case this is a repeatable quest
                        QuestJournal.Instance.UpdateQuestJournal();
                        NotificationWindowManager.Instance.DisplayNotification("Quest Received:\n \"" + AllQuestsInGame[i].Title + "\"", Color.black);
                        numOfActiveQuests++;

                        return;
                    }
                    //quest is already active...
                    if (AllQuestsInGame[i].State == QuestState.Active)
                    {
                        NotificationWindowManager.Instance.DisplayNotification("Quest: \"" + nameOfQuest + "\" is already active", Color.black);
                        return;
                    }
                    //quest is already completed and not repeatable...
                    if (AllQuestsInGame[i].State == QuestState.Complete)
                    {
                        Debug.Log("Quest: " + nameOfQuest + " has already been completed");
                        return;
                    }
                }
            }
            //nameOfQuest does not matche any quest title in AllQuestsInGame
            Debug.LogWarning("Quest: " + nameOfQuest + " does not exist in AllQuestsInGame List");
        }

        //gets called when an objective is updated && the check for AllObjectivesComplete returns true
        void CompleteQuest(string nameOfQuest)
        {
            Quest quest = GetQuestByName(nameOfQuest); //grabs the quest instance from AllQuestsInGame List

            if (quest != null)
            {
                if (quest.State != QuestState.Active)
                {
                    Debug.Log("Quest: " + nameOfQuest + " is not active; cannot set as complete");
                    return;
                }

                quest.State = QuestState.Complete;
                quest.DateCompleted = System.DateTime.Now;
                numOfActiveQuests--;

                if (quest.IsRepeatable)
                {
                    for (int j = 0; j < quest.Objectives.Count; j++)
                    {
                        quest.Objectives[j].ResetObjective();
                    }
                }

                NotificationWindowManager.Instance.DisplayNotification("Quest Complete!\n" + "\"" + quest.Title + "\"\n[Open quest journal to claim reward]", Color.black);

                if (QuestCompleted != null)
                {
                    QuestCompleted(quest, -999); //number does not matter
                }

                if (quest.Reward == null)
                {
                    Debug.LogWarning("The quest " + quest.Title + " does not have a reward");
                    quest.RewardClaimed = true;
                }
            }
        }

        //called by button press
        public void GrantQuestRewards()
        {
            Quest quest = QuestJournal.Instance.QuestOnDisplay;

            if (quest.Reward == null)
            {
                Debug.LogWarning("The quest " + quest.Title + " does not have a reward");
                quest.RewardClaimed = true;
                return;
            }

            //award items...
            for (int i = 0; i < quest.Reward.ItemRewards.Count; i++)
            {
                InventoryManager.Instance.AddItemToInventory(Instantiate(quest.Reward.ItemRewards[i]));
            }

            //award GP...
            InventoryManager.Instance.ModifyGPAmnt(quest.Reward.GPReward);

            //award XP / CP
            RPG.CombatSystem.XPCalculator.AwardXPAndCP(quest.Reward.XPReward);

            quest.RewardClaimed = true;
            QuestJournal.Instance.UpdateQuestJournal();
        }

        //all quest objective updates should go through this method...
        public void UpdateQuestObjective(string questName, int objectiveIndex, int counterMod = 1 /*, string newDescription = ""*/ )
        {
            Quest quest = GetQuestByName(questName); //make sure we're updating the instance and not the actual asset

            if (quest == null)
            {
                Debug.LogWarning("Quest " + questName + " does not exist ... check spelling of quest name");
                return;
            }

            //if quest is not active, do nothing...
            if (quest.State != QuestState.Active)
            {
                Debug.LogWarning("Player is not on quest " + quest.Title);
                return;
            }

            //if the index is invalid, do nothing...
            if (objectiveIndex < 0 || objectiveIndex >= quest.Objectives.Count)
            {
                Debug.LogWarning("No objective exists for " + quest.Title + " at index " + objectiveIndex);
                return;
            }

            //if the objective is complete, do nothing...
            if (quest.Objectives[objectiveIndex].ObjectiveComplete)
            {
                Debug.Log("Objective: " + quest.Objectives[objectiveIndex].Description + " is already complete");
                return;
            }

            //update the objective
            if (quest.Objectives[objectiveIndex].UpdateQuestObjective(counterMod)) //true if updated objective is complete
            {
                //if the updated objective is now complete...
                //show any hidden objectives we need to show
                foreach (int index in quest.Objectives[objectiveIndex].ObjectivesToShowWhenThisObjectiveIsComplete)
                {
                    quest.Objectives[index].ShowObjective(true);
                }
                //set any game switches we need to set to true
                foreach (Condition cond in quest.Objectives[objectiveIndex].ConditionsToSetTrueOnCompletion)
                {
                    AllGameSwitches.TrySetCondition(cond, true);
                }
            }

            NotificationWindowManager.Instance.DisplayNotification("\"" + quest.Title + "\"\n" + quest.Objectives[objectiveIndex].Description, Color.black);

            //broadcast event to interested parties
            if (QuestObjectiveUpdated != null)
            {
                QuestObjectiveUpdated(quest, objectiveIndex);
            }

            //now check to see if all required objectives for this quest are complete
            if (quest.AllRequiredObjectivesComplete())
            {
                CompleteQuest(quest.Title);
            }
        }

        //make clones of all quest assets and work with those clones at runtime,
        //this preserves the original assets in their default state
        void InstantiateQuests()
        {
            List<Quest> noDuplicateQuestsList = QuestAssets.Distinct().ToList();
            noDuplicateQuestsList.RemoveAll(n => n == null);

            AllQuestsInGame.Clear();

            for (int i = 0; i < noDuplicateQuestsList.Count; i++)
            {
                Quest q = Instantiate(noDuplicateQuestsList[i]);
                AllQuestsInGame.Add(q);

                //save defaults for repeatable quests
                if (q.IsRepeatable)
                {
                    for (int j = 0; j < q.Objectives.Count; j++)
                    {
                        q.Objectives[j].SetObjectiveDefaults();
                    }
                }
            }
            //print("Quests have been set to default state");
        }

        //check to see if a specific quest objective is complete
        public bool ObjectiveIsComplete(Quest quest, int indexOfObjective)
        {
            return GetQuestByName(quest.Title).Objectives[indexOfObjective].ObjectiveComplete;
        }

        //returns the instantiated quest in AllQuestsInGame[] that matches the given questName
        public Quest GetQuestByName(string questName)
        {
            for (int i = 0; i < AllQuestsInGame.Count; i++)
            {
                if (AllQuestsInGame[i].Title == questName)
                {
                    return AllQuestsInGame[i];
                }
            }

            Debug.LogWarning("No quest titled " + questName + " exists in AllQuestsInGame");
            return null;
        }

        #region SaveData

        [System.Serializable]
        class QuestsSaveData
        {
            //number of active quests
            public int numOfActiveQuests;

            //state of all game switches
            public List<bool> AllGameSwitches_States = new List<bool>();

            //state of all Quests
            public List<QuestState> AllQuests_States = new List<QuestState>();
            public List<bool> AllQuests_RewardClaimed = new List<bool>();
            public List<DateTime> AllQuests_DateAcquired = new List<DateTime>();
            public List<DateTime> AllQuests_DateCompleted = new List<DateTime>();

            //state of each objective for each Quest
            public List<List<bool>> AllQuests_ObjectiveState = new List<List<bool>>();
            public List<List<bool>> AllQuests_ObjectiveHiddenStatus = new List<List<bool>>();
            public List<List<int>> AllQuests_ObjectiveCounters = new List<List<int>>();
            public List<List<string>> AllQuests_ObjectiveDescriptions = new List<List<string>>();
        }

        public object CaptureState()
        {
            //print("saving quest data and AllGameSwitches");
            QuestsSaveData saveData = new QuestsSaveData();

            //save number of active quests
            saveData.numOfActiveQuests = numOfActiveQuests;

            //save the state of all game switches
            AllGameSwitches AGS = AllGameSwitches.Instance;
            foreach (Condition condition in AGS.conditions)
            {
                saveData.AllGameSwitches_States.Add(condition.Satisfied);
            }

            //make sure there are no null entries in the quest list
            AllQuestsInGame.RemoveAll(n => n == null);

            //loop through all quests...
            for (int i = 0; i < AllQuestsInGame.Count; i++)
            {
                //save whether or not the quest is complete
                saveData.AllQuests_States.Add(AllQuestsInGame[i].State);
                //save whether or not the reward has been claimed
                saveData.AllQuests_RewardClaimed.Add(AllQuestsInGame[i].RewardClaimed);
                //save date acquired and date completed
                saveData.AllQuests_DateAcquired.Add(AllQuestsInGame[i].DateAcquired);
                saveData.AllQuests_DateCompleted.Add(AllQuestsInGame[i].DateCompleted);

                //for each objective, save its completed state
                List<bool> objectivesStates = new List<bool>();
                foreach (QuestObjective obj in AllQuestsInGame[i].Objectives)
                {
                    objectivesStates.Add(obj.ObjectiveComplete);
                }
                saveData.AllQuests_ObjectiveState.Add(objectivesStates);

                //for each objective, save its "counter" value
                List<int> objectivesCounters = new List<int>();
                foreach (QuestObjective obj in AllQuestsInGame[i].Objectives)
                {
                    objectivesCounters.Add(obj.Counter);
                }
                saveData.AllQuests_ObjectiveCounters.Add(objectivesCounters);

                //for each objective, save the current description
                List<string> objectivesDescriptions = new List<string>();
                foreach (QuestObjective obj in AllQuestsInGame[i].Objectives)
                {
                    objectivesDescriptions.Add(obj.Description);
                }
                saveData.AllQuests_ObjectiveDescriptions.Add(objectivesDescriptions);

                //for each objective, save whether or not it is visible in the quest journal
                List<bool> objectivesHiddenStates = new List<bool>();
                foreach (QuestObjective obj in AllQuestsInGame[i].Objectives)
                {
                    objectivesHiddenStates.Add(obj.IsHidden);
                }
                saveData.AllQuests_ObjectiveHiddenStatus.Add(objectivesHiddenStates);
            }

            return saveData;
        }

        public void RestoreState(object state)
        {
            QuestsSaveData saveData = (QuestsSaveData)state;

            //restore num of active quests from saveData
            numOfActiveQuests = saveData.numOfActiveQuests;

            AllGameSwitches AGS = AllGameSwitches.Instance;
            //reset all game switches to default (false)
            AGS.Reset();
            //print("restoring game switches");
            //restore all game switches from saveData
            for (int i = 0; i < AGS.conditions.Length; i++)
            {
                if (i < saveData.AllGameSwitches_States.Count)
                {
                    AGS.conditions[i].SetConditionSatisfied(saveData.AllGameSwitches_States[i]);
                }
                else
                {
                    Debug.LogWarning("Error restoring a game switch value. Was a new switch recently added?");
                }
            }

            //restore each quest from saveData
            for (int i = 0; i < AllQuestsInGame.Count; i++)
            {
                if (i < saveData.AllQuests_States.Count)
                {
                    AllQuestsInGame[i].State = saveData.AllQuests_States[i]; //completed state
                    AllQuestsInGame[i].RewardClaimed = saveData.AllQuests_RewardClaimed[i]; //reward claimed state
                    AllQuestsInGame[i].DateAcquired = saveData.AllQuests_DateAcquired[i];
                    AllQuestsInGame[i].DateCompleted = saveData.AllQuests_DateCompleted[i];
                    for (int j = 0; j < AllQuestsInGame[i].Objectives.Count; j++)
                    {
                        AllQuestsInGame[i].Objectives[j].SetCompletionState(saveData.AllQuests_ObjectiveState[i][j]);
                        AllQuestsInGame[i].Objectives[j].SetCounter(saveData.AllQuests_ObjectiveCounters[i][j]);
                        AllQuestsInGame[i].Objectives[j].SetDescription(saveData.AllQuests_ObjectiveDescriptions[i][j]);
                        AllQuestsInGame[i].Objectives[j].ShowObjective(!saveData.AllQuests_ObjectiveHiddenStatus[i][j]);
                    }
                }
                else
                {
                    Debug.LogWarning("saveData for at least 1 quest was not loaded... new quest(s) added recently?");
                }
            }
        }
        #endregion
    }
}
