﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using RPG.SceneControl;
using RPG.InteractableSystem;

public class MiniGame_ArmWrestle : MonoBehaviour
{
    const float OpponentVictoryAngle = -81;
    const float PlayerVictoryAngle = 81;
    const float playerSTR_gainPerPress = 0.35f;
    const float FatiguePerSecond = 0.95f;
    const float MaxPlayerSTR = 25;
    const float MaxOpponentSTR = 20;

    //opponent STR-per-second by difficulty lvl
    const float EasySTR = 1.25f;
    const float NormalSTR = 1.75f;
    const float HardSTR = 7f;
    const float InsaneSTR = 10f;

    [SerializeField] bool loadFromTavernScene = true;
    [SerializeField] Animator anim;
    [SerializeField] Transform pivot = null;
    [SerializeField] Image blueBattleBar_Player = null;
    [SerializeField] TextMeshProUGUI playerSTRIndicator = null;
    [SerializeField] TextMeshProUGUI opponentSTRIndicator = null;
    [SerializeField] TextMeshProUGUI notificationText = null;
    [SerializeField] TextMeshProUGUI countdownText = null;
    [SerializeField] GameObject allDifficultyButtons = null;
    [SerializeField] GameObject instructionPanel = null;
    [SerializeField] Button toMainMenuButton = null;
    [SerializeField] Button readyButton;

    int difficultyLvl; //0 == easy / 1 == normal / 2 == hard / 3 == insane
    float opponentSTR_gainPerSecond;
    float maxPlayerSTR;
    float maxOpponentSTR;
    float curOpponentSTR;
    float curPlayerSTR;
    float pivotZValue;
    float barFillAmount;
    float advantageTimer;
    bool opponentBoosted;
    bool playerBoosted;
    bool waitingToStart;

    public bool GameOver { get { return pivotZValue <= OpponentVictoryAngle || pivotZValue >= PlayerVictoryAngle; } }
    public bool PlayerHasAdvantage { get { return advantageTimer > 10; } }


    void Start()
    {
        ResetVariables();
        instructionPanel.SetActive(loadFromTavernScene);
        allDifficultyButtons.SetActive(!loadFromTavernScene);
        if (loadFromTavernScene)
        {
            AllGameSwitches.TrySetCondition("ArmwrestledGriswold", true);
            SetListenerOnReadyButton();
        }
        waitingToStart = true;
    }

    //set difficulty ... hard if Griswold has been beaten once
    void SetListenerOnReadyButton()
    {
        if (!AllGameSwitches.CheckIfConditionIsTrue("BeatGriswoldArmwrestling_Once"))
        {
            readyButton.onClick.AddListener(() => StartGame(1));
        }
        else
        {
            readyButton.onClick.AddListener(() => StartGame(2));
        }
    }

    //game loop
    void Update()
    {
        //get the current value of our pivot's z rotation
        pivotZValue = pivot.localEulerAngles.z;

        //when z value in inspector is negative currentAngle ends up set to a 
        //positive value(e.g. -15 == +345). We subtract 360 to get the negative value we want.
        if (pivotZValue > PlayerVictoryAngle)
        {
            pivotZValue -= 360;
        }

        //while game is in progress...
        if (!GameOver && !waitingToStart)
        {
            //player gains advantage if winning for 10 seconds...
            UpdatePlayerAdvantageTimer();
            //adjust player STR (auto decay; player input)
            GetPlayerSTR();
            //adjust opponent STR
            GetOpponentSTR();
            //calculate a new z value for the pivot:
            UpdatePivotZValue();
        }
        //GameOver ... reset after a few seconds
        else if (GameOver && !waitingToStart)
        {
            waitingToStart = true;
            StartCoroutine(HandleGameOver());
        }

        //keep UI up-to-date :: adjusts the STR bar, displays STR numbers, displays Win/Loss mssg
        UpdateUI();
    }

    //player must stay in the sweet-spot for 10 seconds to gain advantage and have a chance to win
    void UpdatePlayerAdvantageTimer()
    {
        if (pivotZValue < 60 && !PlayerHasAdvantage && notificationText.text == "Sweet Spot! Hold em here!")
        {
            advantageTimer = 0;
            ShowNotification("", false);
        }

        if (pivotZValue > 62 && !PlayerHasAdvantage && !opponentBoosted)
        {
            advantageTimer = 0;
            ShowNotification("Careful!");
        }

        if (pivotZValue > 60 && pivotZValue < 62 && !PlayerHasAdvantage && !opponentBoosted)
        {
            advantageTimer += Time.deltaTime;
            ShowNotification("Sweet Spot! Hold em here!");
        }

        if (pivotZValue > 60 && PlayerHasAdvantage && !playerBoosted)
        {
            maxPlayerSTR *= 2;
            maxOpponentSTR *= 2;
            curOpponentSTR = curPlayerSTR;
            playerBoosted = true;
            ShowNotification("Finish Him!");
        }
    }

    //called every frame...
    void GetPlayerSTR()
    {
        //decay playerSTR by fatigue amount
        if (curPlayerSTR > 0)
        {
            curPlayerSTR -= FatiguePerSecond * Time.deltaTime;
        }

        //increase STR when player presses X
        if (Input.GetKeyDown(KeyCode.X))
        {
            if (pivotZValue < -55) //player is losing, slightly harder to gain STR
            {
                ShowNotification("", false);
                curPlayerSTR += playerSTR_gainPerPress * 0.9f;
            }
            else if (pivotZValue > 55) //player is winning, slightly easier to gain STR
            {
                curPlayerSTR += playerSTR_gainPerPress * 1.1f;
            }
            else //default STR gain
            {
                curPlayerSTR += playerSTR_gainPerPress;
            }
        }

        //cap STR
        if (curPlayerSTR > maxPlayerSTR)
        {
            curPlayerSTR = maxPlayerSTR;
        }
    }

    //called every frame...
    void GetOpponentSTR()
    {
        //cap STR
        if (curOpponentSTR < maxOpponentSTR)
        {
            curOpponentSTR += opponentSTR_gainPerSecond * Time.deltaTime;
        }

        //if player tries to win without advantage...
        if (pivotZValue > 65 && !PlayerHasAdvantage && !opponentBoosted)
        {
            maxOpponentSTR += 2;
            curPlayerSTR = curPlayerSTR * 0.5f;
            opponentBoosted = true;
            advantageTimer = 0;
            ShowNotification("Uh Oh! He's mad!");
        }

        //while boosted, opponent gains STR quickly
        if (opponentBoosted)
        {
            //STR capped at 2 * max when boosted
            if (curOpponentSTR < maxOpponentSTR * 2)
            {
                curOpponentSTR += (curOpponentSTR * 0.05f);
            }
            else
            {
                curOpponentSTR = maxOpponentSTR * 2;
            }
        }

        //remove boost and angry message once opponent gets z rotation to -25
        if (pivotZValue < -25 && opponentBoosted)
        {
            if (curOpponentSTR > maxOpponentSTR)
            {
                curOpponentSTR -= (curOpponentSTR / 10);
            }
            else
            {
                curOpponentSTR = maxOpponentSTR;
                opponentBoosted = false;
                ShowNotification("", false);
            }
        }
    }

    //called every frame...
    void UpdatePivotZValue()
    {
        //subtract for opponent
        float newZValue = pivotZValue - curOpponentSTR * Time.deltaTime;
        //add for player
        newZValue += curPlayerSTR * Time.deltaTime;

        //cap Z value
        if (newZValue < OpponentVictoryAngle)
        {
            newZValue = OpponentVictoryAngle;
        }
        if (newZValue > PlayerVictoryAngle)
        {
            newZValue = PlayerVictoryAngle;
        }

        //adjust the z rotation of the pivot
        pivot.localEulerAngles = new Vector3(0, 0, newZValue);
        //update animation
        anim.SetFloat("Blend", newZValue);
    }

    //keep UI updated
    void UpdateUI()
    {
        if (pivotZValue < 0)
        {
            blueBattleBar_Player.fillAmount = (Mathf.Abs(OpponentVictoryAngle) - Mathf.Abs(pivotZValue)) / (PlayerVictoryAngle + Mathf.Abs(OpponentVictoryAngle));
        }
        else
        {
            blueBattleBar_Player.fillAmount = (pivotZValue + PlayerVictoryAngle) / (PlayerVictoryAngle + Mathf.Abs(OpponentVictoryAngle));
        }

        playerSTRIndicator.text = curPlayerSTR.ToString("N0");
        opponentSTRIndicator.text = curOpponentSTR.ToString("N0");

        if (GameOver)
        {
            if (pivotZValue > 0) //win
            {
                ShowNotification("You Win!");
            }
            else //loss
            {
                ShowNotification("You Lose!");
            }
        }
    }

    //called by button press (Easy, Normal, Hard, Ready, etc...)
    public void StartGame(int difLVL)
    {
        AdjustDifficulty(difLVL);
        StartCoroutine(_Start());
    }

    //countdown and start the battle
    IEnumerator _Start()
    {
        ShowUIButtons(false);

        //do countodown...
        countdownText.gameObject.SetActive(true);
        countdownText.text = "3";
        yield return new WaitForSeconds(1);
        countdownText.text = "2";
        yield return new WaitForSeconds(1);
        countdownText.text = "1";
        yield return new WaitForSeconds(1);
        countdownText.gameObject.SetActive(false);

        //start the battle
        waitingToStart = false;
    }

    //set opponent STR based on difficulty
    void AdjustDifficulty(int difLVL)
    {
        if (difLVL < 0 || difLVL > 3)
        {
            Debug.LogWarning("Invalid difLvl: setting to 1");
            difLVL = 1;
        }

        difficultyLvl = difLVL;

        switch (difLVL)
        {
            case 0:
                opponentSTR_gainPerSecond = EasySTR;
                break;
            case 1:
                opponentSTR_gainPerSecond = NormalSTR;
                break;
            case 2:
                opponentSTR_gainPerSecond = HardSTR;
                break;
            case 3:
                opponentSTR_gainPerSecond = InsaneSTR;
                break;
        }
    }

    //reset after GameOver
    IEnumerator HandleGameOver()
    {
        yield return new WaitForSeconds(3);

        if (loadFromTavernScene)
        {
            if (!AllGameSwitches.CheckIfConditionIsTrue("BeatGriswoldArmwrestling_Once"))
            {
                AllGameSwitches.TrySetCondition("BeatGriswoldArmwrestling_Once", pivotZValue >= PlayerVictoryAngle);
            }
            else if (!AllGameSwitches.CheckIfConditionIsTrue("BeatGriswoldArmwrestling_Twice"))
            {
                AllGameSwitches.TrySetCondition("BeatGriswoldArmwrestling_Twice", pivotZValue >= PlayerVictoryAngle);
            }

            SceneController.Instance.FadeAndLoadScene(SceneController.Instance.nameOfMostRecentlyUnloadedScene);
        }
        else
        {
            ResetVariables();
            ShowUIButtons(true);
            ShowNotification("", false);
        }
    }

    void ResetVariables()
    {
        opponentBoosted = false;
        playerBoosted = false;
        advantageTimer = 0;
        curPlayerSTR = 0;
        curOpponentSTR = 0;
        maxPlayerSTR = MaxPlayerSTR;
        maxOpponentSTR = MaxOpponentSTR;
        pivot.localEulerAngles = Vector3.zero;
        blueBattleBar_Player.fillAmount = 0.5f;
        playerSTRIndicator.text = curPlayerSTR.ToString("N0");
        opponentSTRIndicator.text = curOpponentSTR.ToString("N0");
    }

    //show or hide the UI buttons
    void ShowUIButtons(bool show)
    {
        if (!loadFromTavernScene)
        {
            allDifficultyButtons.SetActive(show);
            toMainMenuButton.gameObject.SetActive(show);
        }
        else
        {
            instructionPanel.SetActive(show);
        }
    }

    //show or hide notifications
    void ShowNotification(string mssg, bool show = true)
    {
        if (!show)
        {
            notificationText.enabled = false;
        }
        else
        {
            notificationText.text = mssg;
            notificationText.enabled = true;
        }
    }
}
