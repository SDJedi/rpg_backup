﻿using RPG.SceneControl;
using UnityEngine;

public class LoadArmWrestleMiniGameScene : MonoBehaviour
{
    public void LoadMiniGame()
    {
        SceneController.Instance.FadeAndLoadScene("MiniGame_ArmWrestle", false);
    }
}
