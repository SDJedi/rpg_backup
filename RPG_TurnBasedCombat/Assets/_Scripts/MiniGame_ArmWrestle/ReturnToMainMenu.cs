﻿using System.Collections;
using System.Collections.Generic;
using RPG.SceneControl;
using UnityEngine;

public class ReturnToMainMenu : MonoBehaviour
{
    public void LoadMainMenu()
    {
        SceneController.Instance.LoadMainMenu(0);
    }
}
