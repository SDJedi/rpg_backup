using DialogueSystem;
using RPG.Inventory;

//Last Reviewed: 1/23/20
namespace RPG.InteractableSystem
{
    //Reaction used to open an item shop
    public class ShowItemShopPanelReaction : DelayedReaction
    {
        public ItemShop itemShop;
        public ReactionCollection onExitReactions = null;

        protected override void ImmediateReaction()
        {
            DialogueManager.Instance.EndConversation();
            ItemShopManager.Instance.SetUpNewItemShop(itemShop);
            ItemShopManager.Instance.SetOnExitReactions(onExitReactions);
        }
    }
}
