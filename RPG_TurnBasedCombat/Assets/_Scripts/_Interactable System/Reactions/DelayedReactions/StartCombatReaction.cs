using System.Collections.Generic;
using RPG.CombatSystem;

//Last Reviewed: 1/23/20
namespace RPG.InteractableSystem
{
    //set the possible encounters list to a specific encounter, then load the CombatScene as a reaction
    public class StartCombatReaction : DelayedReaction
    {
        public List<EncounterAndSpawnChance> newEncounterList = null;
        public bool playStartBattleSFX = true;


        protected override void ImmediateReaction()
        {
            RandomEncounterManager.Instance.SetPossibleEncountersList(newEncounterList);
            RandomEncounterManager.Instance.LoadCombatScene(playStartBattleSFX);
        }
    }
}
