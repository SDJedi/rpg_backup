﻿using System.Collections.Generic;
using RPG.Inventory;
using RPG.Messaging;
using UnityEngine;

namespace RPG.InteractableSystem
{
    public class RemoveItemsFromInventoryReaction : DelayedReaction
    {
        [SerializeField] List<Item> itemsToRemove = null;


        protected override void ImmediateReaction()
        {
            //strings to help with formatting the notification
            string leftBracket = "<color=\"black\">[</color>";
            string rightBracket = "<color=\"black\">]</color>";
            string endColorAndBold = "</b></color>";

            foreach (Item itm in itemsToRemove)
            {
                if (itm != null && InventoryManager.Instance.ItemExistsInInventory(itm))
                {
                    string startColorAndBold = "<color=" + itm.GetItemTextColorAsHexString() + "><b>";
                    string itemName = leftBracket + startColorAndBold + itm.ItemName + endColorAndBold + rightBracket;
                    InventoryManager.Instance.SubtractItemFromInventory(itm);
                    NotificationWindowManager.Instance.DisplayNotification(itemName + " was removed from inventory.", Color.green);
                }
            }
        }
    }
}
