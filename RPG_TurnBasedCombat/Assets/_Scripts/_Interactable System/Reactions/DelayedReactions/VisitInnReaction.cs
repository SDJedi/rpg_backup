//Last Reviewed: 1/23/20
namespace RPG.InteractableSystem
{
    //visit an Inn as a reaction
    public class VisitInnReaction : DelayedReaction
    {
        protected override void ImmediateReaction()
        {
            VistInn.RestoreAllPCs();
        }
    }
}
