using System.Collections.Generic;

//Last Reviewed: 1/23/20
namespace RPG.InteractableSystem
{
    //allows us to check for certain conditions, then react
    public class CheckConditionThenReactReaction : DelayedReaction
    {
        public List<Condition> conditionsToCheck;
        public int requiredGP = 0;
        public ReactionCollection reactionsIfTrue;
        public ReactionCollection reactionsIfFalse;


        protected override void ImmediateReaction()
        {
            bool allCondMet = true;

            //check current GP...
            if (RPG.Inventory.InventoryManager.Instance.CurrentGP < requiredGP)
            {
                //failed GP requirement, so conditions are not met
                allCondMet = false;
            }
            else
            {
                //check each condition to see if it is satisfied.
                //if ANY condition is NOT satisfied, allCondMet is set to false
                foreach (Condition c in conditionsToCheck)
                {
                    if (c.Satisfied == false)
                    {
                        allCondMet = false;
                        break;
                    }
                }
            }

            //fire off one of the two reaction collections based on allCondMet
            if (allCondMet == true)
            {
                if (reactionsIfTrue != null)
                {
                    reactionsIfTrue.React();
                }
            }
            else
            {
                if (reactionsIfFalse != null)
                {
                    reactionsIfFalse.React();
                }
            }
        }
    }
}
