using UnityEngine;

//Last Reviewed: 1/23/20
namespace RPG.InteractableSystem
{
    //enable or disable a component as a reaction
    public class BehaviourReaction : DelayedReaction
    {
        public Behaviour behaviour;
        public bool enabledState;


        protected override void ImmediateReaction()
        {
            behaviour.enabled = enabledState;
        }
    }
}
