using System.Collections.Generic;
using RPG.Inventory;
using RPG.Messaging;
using UnityEngine;

//Last Reviewed: 1/23/20
namespace RPG.InteractableSystem
{
    //add items to inventory as a reaction
    public class AddItemsToInventoryReaction : DelayedReaction
    {
        [SerializeField] List<Item> itemsToAdd = null;


        protected override void ImmediateReaction()
        {
            //strings to help with formatting the notification
            string leftBracket = "<color=\"black\">[</color>";
            string rightBracket = "<color=\"black\">]</color>";
            string endColorAndBold = "</b></color>";

            foreach (Item itm in itemsToAdd)
            {
                if (itm != null)
                {
                    string startColorAndBold = "<color=" + itm.GetItemTextColorAsHexString() + "><b>";
                    string itemName = leftBracket + startColorAndBold + itm.ItemName + endColorAndBold + rightBracket;
                    InventoryManager.Instance.AddItemToInventory(Instantiate(itm));
                    NotificationWindowManager.Instance.DisplayNotification(itemName + " was added to your inventory.", Color.green);
                }
            }
        }
    }
}
