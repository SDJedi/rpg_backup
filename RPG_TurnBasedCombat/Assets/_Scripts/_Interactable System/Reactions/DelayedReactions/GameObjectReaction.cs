using UnityEngine;

//Last Reviewed: 1/23/20
namespace RPG.InteractableSystem
{
    //activate or deactivate a GameObject as a reaction
    public class GameObjectReaction : DelayedReaction
    {
        public GameObject gameObject;
        public bool activeState;


        protected override void ImmediateReaction()
        {
            gameObject.SetActive(activeState);
        }
    }
}
