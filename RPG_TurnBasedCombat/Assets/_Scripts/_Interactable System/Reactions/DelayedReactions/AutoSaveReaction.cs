using RPG.Saving;
using UnityEngine;

//Last Reviewed: 1/23/20
namespace RPG.InteractableSystem
{
    //overwrite the autoSaveFile
    public class AutoSaveReaction : DelayedReaction
    {
        protected override void ImmediateReaction()
        {
            GameObject.FindObjectOfType<SavingWrapper>().Save(SavingWrapper.autoSaveFile);
        }
    }
}
