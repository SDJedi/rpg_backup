﻿using RPG.Messaging;
using RPG.QuestSystem;
using UnityEngine;

//Last Reviewed: 3/5/20
namespace RPG.InteractableSystem
{
    public class ChangeQuestObjectiveDescriptionReaction : DelayedReaction
    {
        public Quest Quest; //quest to update
        public int ObjectiveIndex; //index of the objective that is to be updated
        [TextArea]
        [SerializeField] string newDescription = "";


        protected override void ImmediateReaction()
        {
            //get the instantiated quest in AllQuestsInGame[] that matches the given questName
            Quest questInstance = QuestManager.Instance.GetQuestByName(Quest.Title);

            //ensure the quest exists and is active
            if (questInstance == null || questInstance.State != QuestState.Active)
            {
                Debug.Log("Quest \"" + questInstance.Title + "\" does not exist or is not active. Cannot update objective");
                return;
            }

            if (ObjectiveIndex >= questInstance.Objectives.Count)
            {
                Debug.Log("Quest \"" + questInstance.Title + "\" does not have an objective at index " + ObjectiveIndex);
                return;
            }

            questInstance.Objectives[ObjectiveIndex].SetDescription(newDescription);
            NotificationWindowManager.Instance.DisplayNotification("\"" + questInstance.Title + "\"\n" + questInstance.Objectives[ObjectiveIndex].Description, Color.black);
        }
    }
}
