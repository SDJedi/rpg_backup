using UnityEngine;

//Last Reviewed: 1/23/20
namespace RPG.InteractableSystem
{
    //play an animation as a reaction
    public class AnimationReaction : DelayedReaction
    {
        public Animator animator;
        public string trigger;

        int triggerHash;


        protected override void SpecificInit()
        {
            triggerHash = Animator.StringToHash(trigger);
        }

        protected override void ImmediateReaction()
        {
            animator.SetTrigger(triggerHash);
        }
    }
}
