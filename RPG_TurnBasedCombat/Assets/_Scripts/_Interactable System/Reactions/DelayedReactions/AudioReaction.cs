using UnityEngine;

//Last Reviewed: 1/23/20
namespace RPG.InteractableSystem
{
    //play a sound as a reaction
    public class AudioReaction : DelayedReaction
    {
        public AudioSource audioSource;
        public AudioClip audioClip;


        protected override void ImmediateReaction()
        {
            audioSource.clip = audioClip;
            audioSource.Play();
        }
    }
}
