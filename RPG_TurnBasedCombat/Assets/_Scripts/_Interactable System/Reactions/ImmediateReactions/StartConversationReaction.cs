﻿using DialogueSystem;
using UnityEngine;

//Last Reviewed: 11/18/20
namespace RPG.InteractableSystem
{
    //Start a conversation as a reaction
    public class StartConversationReaction : DelayedReaction
    {
        public GameObject NPC;
        public string nodeTag; //leave blank if conversation starts from root node
        public bool allowPlayerToEndConversation = false;
        public bool useConversationCam = true;

        protected override void ImmediateReaction()
        {
            DialogueManager.Instance.StartConversation(NPC, nodeTag, allowPlayerToEndConversation, useConversationCam);
        }
    }
}
