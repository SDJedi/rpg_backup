using RPG.QuestSystem;
using UnityEngine;

//Last Reviewed: 1/23/20
namespace RPG.InteractableSystem
{
    //update a Quest Objective as a Reaction
    public class UpdateQuestReaction : Reaction
    {
        public Quest Quest; //quest to update
        public int ObjectiveIndex; //index of the objective that is to be updated
        public int[] ObjectivesThatMustBeCompletedFirst; //a list of indices of prerequisite objectives
        public GameObject ObjectToDisable; //optional .. disable a GameObject as part of the objective update


        protected override void ImmediateReaction()
        {
            //get the instantiated quest in AllQuestsInGame[] that matches the given questName
            Quest questInstance = QuestManager.Instance.GetQuestByName(Quest.Title);

            //ensure the quest exists and is active
            if (questInstance == null || questInstance.State != QuestState.Active)
            {
                Debug.Log("Quest \"" + questInstance.Title + "\" does not exist or is not active. Cannot update objective");
                return;
            }

            //check that all prerequisite objectives are complete 
            for (int i = 0; i < ObjectivesThatMustBeCompletedFirst.Length; i++)
            {
                if (!QuestManager.Instance.ObjectiveIsComplete(questInstance, ObjectivesThatMustBeCompletedFirst[i]))
                {
                    Debug.LogWarning("a prerequisite objective is not yet complete");
                    return;
                }
            }

            //update the objective...
            QuestManager.Instance.UpdateQuestObjective(questInstance.Title, ObjectiveIndex);
            
            //if there is an ObjectToDisable, disable it...
            if (ObjectToDisable != null)
            {
                ObjectToDisable.SetActive(false);
            }
        }
    }
}
