using RPG.Messaging;
using UnityEngine;

//Last Reviewed: 1/23/20
namespace RPG.InteractableSystem
{
    //show text at the bottom of the screen as a reaction
    public class TextReaction : Reaction
    {
        public string message;
        public Color textColor = Color.white;
        public float delay;
        public bool overrideExistingText = false;

        TextManager textManager; //Note: each OverWorld scene should contain a gameObject called "TextManager" with a TextManager component


        protected override void SpecificInit()
        {
            var tm = GameObject.Find("TextManager");

            if (tm == null)
            {
                Debug.LogWarning("Scene needs a TextManager GameObject and Component");
                return;
            }

            textManager = tm.GetComponent<TextManager>();
        }

        protected override void ImmediateReaction()
        {
            textManager.DisplayMessage(message, textColor, delay, 0, overrideExistingText);
        }
    }
}
