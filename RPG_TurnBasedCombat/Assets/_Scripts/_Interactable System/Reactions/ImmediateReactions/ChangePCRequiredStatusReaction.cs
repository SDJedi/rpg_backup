using RPG.CombatSystem;
using UnityEngine;

//Last Reviewed: 1/23/20
namespace RPG.InteractableSystem
{
    //Reaction that changes if a PC is required to be in party or not
    public class ChangePCRequiredStatusReaction : Reaction
    {
        public GameObject PC_Prefab;
        public bool IsRequired;


        protected override void ImmediateReaction()
        {
            //Set PC status to IsRequired
            for (int i = 0; i < PCManager.Instance.AcquiredTeamMembers.Count; i++)
            {
                if (PCManager.Instance.AcquiredTeamMembers[i].PC_Prefab == PC_Prefab)
                {
                    PCManager.Instance.AcquiredTeamMembers[i].RequiredPartyMember = IsRequired;
                    break;
                }
            }
        }
    }
}
