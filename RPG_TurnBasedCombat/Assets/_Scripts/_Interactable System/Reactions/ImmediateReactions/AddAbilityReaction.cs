using RPG.CombatSystem;
using UnityEngine;

//Last Reviewed: 1/23/20
namespace RPG.InteractableSystem
{
    //Add an ability to a PC as a reaction. can optionaly remove an ability at the same time,
    //e.g. when the new ability replaces an old one.
    public class AddAbilityReaction : Reaction
    {
        public string CharName;
        public BaseCommand_Config AbilityToAdd;
        [Header("Optional")]
        public BaseCommand_Config AbilityToRemove;


        protected override void ImmediateReaction()
        {
            PC_Stats_Config stats = PCManager.Instance.GetStatsForPC(CharName);
            if (stats != null)
            {
                stats.AddAbility(AbilityToRemove, AbilityToAdd);
                return;
            }

            Debug.LogWarning("Could not find PC " + CharName + " to add ability to");
        }
    }
}