using RPG.Inventory;

//Last Reviewed: 1/23/20
namespace RPG.InteractableSystem
{
    //Increase / decrease GP as a reaction
    public class ModifyGPReaction : Reaction
    {
        public int Amnt;


        protected override void ImmediateReaction()
        {
            InventoryManager.Instance.ModifyGPAmnt(Amnt);
        }
    }
}
