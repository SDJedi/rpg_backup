//Last Reviewed: 1/21/22
namespace RPG.InteractableSystem
{
    //change the "Satisfied" property of a condition as a reaction
    public class ConditionReaction : Reaction
    {
        public Condition condition;
        public bool satisfied = true;


        protected override void ImmediateReaction()
        {
            AllGameSwitches.TrySetCondition(condition, satisfied);
        }
    }
}