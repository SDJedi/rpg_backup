using RPG.SceneControl;
using UnityEngine;

//Last Reviewed: 1/23/20
namespace RPG.InteractableSystem
{
    //Load a new scene as a reaction
    public class SceneReaction : DelayedReaction
    {
        [SerializeField] string sceneName = "";
        [SerializeField] string nameOfSpawnPoint = "";


        protected override void ImmediateReaction()
        {
            SceneController.Instance.NameOfMostRecentSpawnPoint = nameOfSpawnPoint;
            SceneController.Instance.FadeAndLoadScene(sceneName);
        }
    }
}