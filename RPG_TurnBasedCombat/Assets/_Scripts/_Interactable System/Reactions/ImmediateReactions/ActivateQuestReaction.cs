using RPG.QuestSystem;
using UnityEngine;

//Last Reviewed: 1/13/22
namespace RPG.InteractableSystem
{
    //Reaction used to start a quest ... can also activate / deactivate gameObjects at the same time
    public class ActivateQuestReaction : DelayedReaction
    {
        public Quest Quest;
        public GameObject[] GameObjectsToActivate = new GameObject[0];
        public GameObject[] GameObjectsToDeactivate = new GameObject[0];


        protected override void ImmediateReaction()
        {
            //activate the quest...
            QuestManager.Instance.SetQuestAsActive(Quest.Title);
            //if quest is activated, enable and disable gameobjects in the arrays
            if (QuestManager.Instance.GetQuestByName(Quest.Title).State == QuestState.Active)
            {
                for (int i = 0; i < GameObjectsToActivate.Length; i++)
                {
                    if (GameObjectsToActivate[i] != null)
                    {
                        GameObjectsToActivate[i].SetActive(true);
                    }
                }
                for (int i = 0; i < GameObjectsToDeactivate.Length; i++)
                {
                    if (GameObjectsToDeactivate[i] != null)
                    {
                        GameObjectsToDeactivate[i].SetActive(false);
                    }
                }
            }
            else
            {
                Debug.LogWarning("Could not activate Quest: \"" + Quest.Title + "\". Did not activate or deactivate related GameObjects");
            }
        }
    }
}