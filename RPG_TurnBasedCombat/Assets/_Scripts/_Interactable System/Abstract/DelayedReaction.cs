using System.Collections;
using UnityEngine;

//Last Reviewed: 1/22/20
namespace RPG.InteractableSystem
{
    //Allows for the addition of a time-delay to a Reaction
    public abstract class DelayedReaction : Reaction
    {
        public float delay = 0;

        protected WaitForSeconds wait;


        //set the value of wait, run any special initialization code for this Reaction
        public new void Init()
        {
            wait = new WaitForSeconds(delay);
            SpecificInit();
        }

        public new void React(MonoBehaviour monoBehaviour)
        {
            monoBehaviour.StartCoroutine(ReactCoroutine());
        }

        protected IEnumerator ReactCoroutine()
        {
            yield return wait;

            ImmediateReaction();
        }
    }
}