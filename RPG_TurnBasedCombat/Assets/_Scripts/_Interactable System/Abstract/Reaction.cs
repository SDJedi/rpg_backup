using UnityEngine;

//Last Reviewed: 1/22/20
namespace RPG.InteractableSystem
{
    //The framework of a Reaction
    public abstract class Reaction : ScriptableObject
    {
        //This gets called in Start() of ReactionCollection.cs
        public void Init()
        {
            SpecificInit(); //often this does nothing...
        }

        //wrapper method for ImmediateReaction()
        public void React(MonoBehaviour monoBehaviour)
        {
            ImmediateReaction();
        }

        protected abstract void ImmediateReaction(); //each reaction defines what happens when React() fires off
        protected virtual void SpecificInit() { } //each reaction can define its own initialization ... most do not...
        //see TextReaction.cs and AnimationReaction.cs for examples of how it is used
    }
}