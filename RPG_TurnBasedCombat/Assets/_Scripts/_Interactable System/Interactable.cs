using UnityEngine;

//Last Reviewed: 1/23/20
namespace RPG.InteractableSystem
{
    //The heart of the Interactable System, add this to a GameObject, and decide what happens under what conditions
    //Note: The GameObject will also need an EventTrigger component, with a Pointer Click event (PointerClick parameters:
    //the Overworld PC, PC_MoveOrStartInteraction.OnInteractableClick(), and this Interactable script)
    public class Interactable : MonoBehaviour
    {
        [System.Serializable]
        public struct CursorDetails
        {
            public Texture2D cTexture;
            public Vector2 cHotSpot;
        }

        //public bool showDialogueCursorOnHover = false;
        public Transform interactionLocation; //the location the PC should move to
        public ConditionCollection[] conditionCollections = new ConditionCollection[0]; // an array of ConditionCollections, each of which has an array of Conditions
        public ReactionCollection defaultReactionCollection; //the reactions that happen when none of the ConditionCollections have thier conditions met
        public CursorDetails cursorInfo;


        public void Interact() //Called when the PC reaches the interactionLocation and stops
        {
            if (!gameObject.activeSelf)
            {
                Debug.LogWarning("GameObject with interactable is not active.");
                return;
            }

            for (int i = 0; i < conditionCollections.Length; i++)
            {
                if (conditionCollections[i].CheckAndReact())
                {
                    return;
                }
            }

            if (defaultReactionCollection != null)
            {
                defaultReactionCollection.React();
            }
            else if (this != null)
            {
                Debug.Log(gameObject + " has no default reaction collection for its Interactable script");
            }
        }
    }
}
