using System;
using UnityEngine;

//Last Reviewed: 1/23/20
namespace RPG.InteractableSystem
{
    //One or more Reactions that get called to React(), usually in response to an Interactable when conditions are met
    public class ReactionCollection : MonoBehaviour
    {
        public Reaction[] reactions = new Reaction[0]; //an array of all reactions in the collection
        public static Action<GameObject> reactionTriggered;


        //initialize all reactions in the collection
        void Start()
        {
            for (int i = 0; i < reactions.Length; i++)
            {
                DelayedReaction delayedReaction = reactions[i] as DelayedReaction;

                if (delayedReaction)
                {
                    delayedReaction.Init(); //sets a WaitForSeconds == delay; then runs any SpecificInit()
                }
                else
                {
                    reactions[i].Init(); //runs any SpecificInit()
                }
            }
        }

        //call React() on each reaction in the collection 
        public void React()
        {
            if (reactionTriggered != null)
            {
                reactionTriggered(gameObject);
            }
            
            for (int i = 0; i < reactions.Length; i++)
            {
                DelayedReaction delayedReaction = reactions[i] as DelayedReaction;

                if (delayedReaction)
                {
                    delayedReaction.React(this);
                }
                else
                {
                    reactions[i].React(this);
                }
            }
        }
    }
}
