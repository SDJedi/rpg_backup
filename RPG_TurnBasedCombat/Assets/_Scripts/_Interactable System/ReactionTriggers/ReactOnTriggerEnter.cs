using System.Collections;
using UnityEngine;

//Last Reviewed: 1/23/20
namespace RPG.InteractableSystem
{
    //Used to set off a Reaction when the OverWorldPC hits a collider
    public class ReactOnTriggerEnter : MonoBehaviour
    {
        public float delay = 0;
        public Condition[] requiredTrueConditions = new Condition[0]; //list of conditions that must be true for reactions to fire
        public Condition[] requiredFalseConditions = new Condition[0]; //list of conditions that must be false for reactions to fire
        public ReactionCollection reactionCollection; //reactions to fire OnTriggerEnter()


        void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "OverWorldPC")
            {
                StartCoroutine(React(delay));
            }
        }

        IEnumerator React(float delay)
        {
            //check that all requiredTrueConditions are == true
            for (int i = 0; i < requiredTrueConditions.Length; i++)
            {

                if (!(requiredTrueConditions[i]).Satisfied)
                {
                    yield break;
                }
            }

            //check that all requiredFalseConditions are == false
            for (int i = 0; i < requiredFalseConditions.Length; i++)
            {

                if ((requiredFalseConditions[i]).Satisfied)
                {
                    yield break;
                }
            }

            //wait for the time delay
            yield return new WaitForSeconds(delay);
            //fire off each reaction in the reactionCollection
            reactionCollection.React();
        }
    }
}
