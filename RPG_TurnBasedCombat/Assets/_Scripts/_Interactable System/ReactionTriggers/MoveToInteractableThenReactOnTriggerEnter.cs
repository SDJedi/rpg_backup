using UnityEngine;

//Last Reviewed: 1/23/20
namespace RPG.InteractableSystem
{
    //simulates the player clicking on a particular interactable when OverWorldPC enters a trigger
    public class MoveToInteractableThenReactOnTriggerEnter : MonoBehaviour
    {
        [SerializeField] Interactable interactable = null;


        void OnTriggerEnter(Collider other)
        {
            if (other.tag == "OverWorldPC")
            {
                var mover = other.gameObject.GetComponent<PC_MoveOrStartInteraction>();
                mover.OnInteractableClick(interactable);
            }
        }
    }
}
