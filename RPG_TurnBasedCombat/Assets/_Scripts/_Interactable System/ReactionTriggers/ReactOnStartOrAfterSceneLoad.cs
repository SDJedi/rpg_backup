using RPG.SceneControl;
using UnityEngine;

//Last Reviewed: 1/23/20
namespace RPG.InteractableSystem
{
    //This script can be used to fire of reactions either on Start(),
    //OR on a scene change AFTER all save data for the scene has been loaded
    public class ReactOnStartOrAfterSceneLoad : MonoBehaviour
    {
        [SerializeField] Condition[] requiredTrueConditions = new Condition[0]; //list of conditions that must be true for reactions to fire
        [SerializeField] Condition[] requiredFalseConditions = new Condition[0]; //list of conditions that must be false for reactions to fire
        [SerializeField] ReactionCollection reactionCollection = null;
        [SerializeField] bool waitForSavedDataToLoad = true; //leave as true if we need to make sure save data for the scene is current


        void Start()
        {
            if (!waitForSavedDataToLoad && CheckConditions())
            {
                reactionCollection.React();
            }
        }

        void OnEnable()
        {
            SceneController.Instance.AfterSceneLoad += ReactAfterDataLoad;
        }

        void OnDisable()
        {
            if (SceneController.InstanceExists)
            {
                SceneController.Instance.AfterSceneLoad -= ReactAfterDataLoad;
            }
        }

        void ReactAfterDataLoad()
        {
            if (waitForSavedDataToLoad && CheckConditions())
            {
                reactionCollection.React();
            }
        }

        bool CheckConditions()
        {
            //check that all requiredTrueConditions are == true
            for (int i = 0; i < requiredTrueConditions.Length; i++)
            {
                if (!(requiredTrueConditions[i]).Satisfied)
                {
                    return false;
                }
            }

            //check that all requiredFalseConditions are == false
            for (int i = 0; i < requiredFalseConditions.Length; i++)
            {

                if ((requiredFalseConditions[i]).Satisfied)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
