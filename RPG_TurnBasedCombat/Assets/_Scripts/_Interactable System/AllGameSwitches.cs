using System;
using UnityEngine;

//Last Reviewed: 1/22/20
namespace RPG.InteractableSystem
{
    //A master list of "Conditions" for the game ... essentially a list of booleans (false by default) that can be set dynamically
    //during gameplay. We can then check at any time to see if a particular condition has been met and run game logic based on the 
    //result of the check. 

    //Note: This data is saved and loaded by QuestManager.cs
    public class AllGameSwitches : ResettableScriptableObject
    {
        const string loadPath = "AllGameSwitches";

        public Condition[] conditions; //all of our game switches

        static AllGameSwitches instance = null;

        //getter|setter for instance
        public static AllGameSwitches Instance
        {
            get
            {
                if (!instance)
                {
                    instance = FindObjectOfType<AllGameSwitches>();
                }

                if (!instance)
                {
                    instance = Resources.Load<AllGameSwitches>(loadPath);
                }

                if (!instance)
                {
                    Debug.LogError("An \"AllGameSwitches\" asset has not been created yet. Go to Assets > Create > AllGameSwitches. (Asset will be placed in Resources folder automatically).");
                }

                return instance;
            }
            set
            {
                if (instance == null)
                {
                    instance = value;
                }
            }
        }

        //set all conditions to false
        public override void Reset()
        {
            if (conditions == null)
            {
                return;
            }

            for (int i = 0; i < conditions.Length; i++)
            {
                // if (conditions[i] == null)
                // {
                //     Debug.Log("null condition at[" + i + "].");
                //     List<Condition> cList = conditions.ToList();
                //     cList.RemoveAt(i);
                //     conditions = cList.ToArray();
                // }
                conditions[i].SetConditionSatisfied(false);
            }
        }

        public static void SortConditionsAlphabetically()
        {
            Array.Sort(instance.conditions, (x, y) => String.Compare(x.description, y.description));
        }

        //check to see if the necessary STATE of requiredCondition (true or false), matches the state of the matching condition in AllGameSwitches
        //used by Interactable --> ConditionCollection ... Don't call this method from anywhere else.
        public static bool CheckCondition(Condition requiredCondition)
        {
            //Note: requiredCondition is created in the Editor when a ConditionCollection is added to an interactable.
            //it shares the same name as a condition in AllGameSwitches, but is NOT the same Condition
            Condition[] allGameSwitches = Instance.conditions; //array of all conditions in game 
            Condition globalCondition = null;

            if (allGameSwitches != null && allGameSwitches[0] != null)
            {
                for (int i = 0; i < allGameSwitches.Length; i++)
                {
                    if (allGameSwitches[i].hash == requiredCondition.hash)
                    {
                        globalCondition = allGameSwitches[i]; //we found the condition we need to look at
                    }
                }
            }

            if (!globalCondition)
            {
                return false;
            }
            //returns true if our condition parameters are a match (either both false or both true)
            return globalCondition.Satisfied == requiredCondition.Satisfied;
        }

        public static bool CheckIfConditionIsTrue(string nameOfCondition)
        {
            //Debug.Log("Checking if condition [" + nameOfCondition + "] is true");
            Condition[] allGameSwitches = Instance.conditions; //array of all conditions in game 
            if (allGameSwitches != null && allGameSwitches[0] != null)
            {
                for (int i = 0; i < allGameSwitches.Length; i++)
                {
                    if (allGameSwitches[i].description == nameOfCondition)
                    {
                        return allGameSwitches[i].Satisfied;
                    }
                }
                Debug.LogWarning("Could not find a condition named " + nameOfCondition);
                return false;
            }

            Debug.LogWarning("allGameSwitches is null or has no entries");
            return false;
        }

        //set the value of a specific condition
        public static bool TrySetCondition(Condition condition, bool value)
        {
            foreach (Condition cond in Instance.conditions)
            {
                if (cond == condition)
                {
                    cond.SetConditionSatisfied(value);
                    return true;
                }
            }

            Debug.LogWarning("Could not find condition \"" + condition.description + "\" in AllGameSwitches");
            return false;
        }

        //overload for above using the name of the condition
        public static bool TrySetCondition(string condName, bool value)
        {
            foreach (Condition cond in Instance.conditions)
            {
                if (cond.description == condName)
                {
                    cond.SetConditionSatisfied(value);
                    return true;
                }
            }

            Debug.LogWarning("Could not find condition \"" + condName + "\" in AllGameSwitches");
            return false;
        }
    }
}