using UnityEngine;

//Last Reviewed: 1/21/22
namespace RPG.InteractableSystem
{
    //Conditions are stored in AllGameSwitches and toggled to true / false during gameplay
    [System.Serializable]
    public class Condition : ScriptableObject
    {
        public string description;
        public int hash; //set and used by Editor scripts

        [SerializeField] bool satisfied;

        public bool Satisfied { get { return satisfied; } }


        public void SetConditionSatisfied(bool value)
        {
            satisfied = value;
        }
    }
}