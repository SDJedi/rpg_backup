using UnityEngine;

//Last Reviewed: 1/22/20
namespace RPG.InteractableSystem
{
    //Interactable.cs is a MonoBehaviour that uses this class. Each ConditionCollection holds one or more Conditions,
    //and if each of those Conditions is met, a ReactionCollection is triggered, which holds one or more Reactions
    public class ConditionCollection : ScriptableObject
    {
        public string description;
        public Condition[] requiredConditions = new Condition[0];
        public ReactionCollection reactionCollection;

        //Check each Condition in the array, and if all Conditions are met && we have a ReactionCollection 
        //then, React();
        public bool CheckAndReact()
        {
            for (int i = 0; i < requiredConditions.Length; i++)
            {
                if (!AllGameSwitches.CheckCondition(requiredConditions[i]))
                {
                    //at least one requiredCondition is not met
                    return false;
                }
            }

            //all conditions have been met, if we have a reactionCollection, React()
            if (reactionCollection)
            {
                reactionCollection.React();
            }
            else
            {
                Debug.LogWarning(this + " does not have a ReactionCollection");
            }

            return true;
        }
    }
}
