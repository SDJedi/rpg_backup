using System.Collections;
using RPG.SceneControl;
using UnityEngine;

//Last Reviewed: 11/15/19

public class MusicClass : MonoBehaviour
{
    [SerializeField] AudioSource[] _audioSources = null;
    public float fadeSpeed = 0.5f;
    public bool isPlaying = true;

    public string playingTrackName = "Nothing";
    public int playingTrackIndex = 0;
    public float playingTrackVolume = 0.000f;

    public string lastTrackName = "Nothing";
    public int lastTrackIndex = 0;
    public float lastTrackVolume = 0.000f;


    void Start()
    {
        SceneController.Instance.AfterSceneLoad += OnSceneChanged;
    }

    void OnDestroy()
    {
        if (SceneController.InstanceExists)
        {
            SceneController.Instance.AfterSceneLoad -= OnSceneChanged;
        }
    }

    void OnSceneChanged()
    {
        var sm = FindObjectOfType<SceneMusic>();
        if (sm != null)
        {
            PlayMusic(sm.NameOfMusic, sm.Vol);
        }
        else
        {
            PlayMusic("ThemeMusic");
        }
    }

    public void PlayMusic(string transformName, float vol = 0.2f, bool fadeIn = true)
    {
        StopAllCoroutines();
        KillAnyDuplicateTracks();

        for (int a = 0; a < _audioSources.Length; a++)
        {
            if (_audioSources[a].name == transformName)
            {
                //Debug.Log("Found Track Name (" + transformName + ") at Index(" + a.ToString() + ")");
                playingTrackIndex = a;
                playingTrackName = transformName;
                break;
            }
        }

        if (!fadeIn)
        {
            _audioSources[lastTrackIndex].Stop();
            lastTrackIndex = playingTrackIndex;
            lastTrackName = playingTrackName;
            _audioSources[playingTrackIndex].Play();
            _audioSources[playingTrackIndex].volume = vol;
            return;
        }

        if (playingTrackIndex == lastTrackIndex)
        {
            //Debug.Log("Same Track Selected");
            if (_audioSources[lastTrackIndex].isPlaying)
            {
                return;
            }
            _audioSources[lastTrackIndex].Play();
        }
        else
        {
            if (isPlaying)
            {
                //Debug.Log("Fading in new music - Fading out old music");
                StartCoroutine(FadeOutOldMusic_FadeInNewMusic(vol));
            }
            else
            {
                //Debug.Log("Fading in new music");
                StartCoroutine(FadeInNewMusic());
            }
        }
    }

    public void FadeOutMusic()
    {
        PlayMusic("NoMusic");
    }

    public void AdjustVolume(float newVol)
    {
        _audioSources[playingTrackIndex].volume = newVol;
    }

    IEnumerator FadeMusic()
    {
        while (_audioSources[playingTrackIndex].volume > 0)
        {
            _audioSources[playingTrackIndex].volume -= fadeSpeed;
            yield return new WaitForSeconds(0.001f);
        }
    }

    IEnumerator FadeOutOldMusic_FadeInNewMusic(float vol)
    {
        _audioSources[playingTrackIndex].volume = 0.000f;
        _audioSources[playingTrackIndex].Play();
        while (_audioSources[playingTrackIndex].volume < vol)
        {
            _audioSources[lastTrackIndex].volume -= fadeSpeed;
            _audioSources[playingTrackIndex].volume += fadeSpeed;
            //Debug.Log("Fade: " + lastTrackName + " " + _audioSources[lastTrackIndex].volume.ToString() + " Rise: " + playingTrackName + " " + _audioSources[playingTrackIndex].volume.ToString());
            yield return new WaitForSeconds(0.001f);
            lastTrackVolume = _audioSources[lastTrackIndex].volume;
            playingTrackVolume = _audioSources[playingTrackIndex].volume;
        }
        _audioSources[lastTrackIndex].volume = 0.000f; // Just In Case....
        _audioSources[lastTrackIndex].Stop();

        lastTrackIndex = playingTrackIndex;
        lastTrackName = playingTrackName;
        isPlaying = true;
        KillAnyDuplicateTracks();
    }

    void KillAnyDuplicateTracks()
    {
        foreach (AudioSource src in _audioSources)
        {
            if (src.isPlaying && src.gameObject.name != playingTrackName)
            {
                src.Stop();
            }
        }
    }

    IEnumerator FadeInNewMusic()
    {
        _audioSources[playingTrackIndex].volume = 0.000f;
        _audioSources[playingTrackIndex].Play();
        while (_audioSources[playingTrackIndex].volume < 0.15f)
        {
            _audioSources[playingTrackIndex].volume += fadeSpeed * 2;
            //Debug.Log("Fading In: " + _audioSources[track_index].volume.ToString());
            yield return new WaitForSeconds(0.001f);
            playingTrackVolume = _audioSources[playingTrackIndex].volume;
        }

        lastTrackIndex = playingTrackIndex;
        lastTrackName = playingTrackName;
        isPlaying = true;
    }
}
