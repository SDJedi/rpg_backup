using RPG.Saving;
using UnityEngine;

//Last Reviewed: 11/15/19

public class SceneMusic : Singleton<SceneMusic>, ISaveable
{
    [SerializeField] string nameOfMusic = "";
    [Range(0, 1)]
    [SerializeField] float volume = 0.2f;

    public string NameOfMusic { get { return nameOfMusic; } }
    public float Vol { get { return volume; } }


    public void ChangeSceneMusic(string newMusic, float vol = 0.2f)
    {
        nameOfMusic = newMusic;
        volume = vol;
        SoundManager.Instance.StartMusic(newMusic, vol);
    }

    [System.Serializable]
    public class SceneMusicSaveData
    {
        public string nameOfMusic;
        public float vol;
    }

    public object CaptureState()
    {
        SceneMusicSaveData saveData = new SceneMusicSaveData();
        saveData.nameOfMusic = this.nameOfMusic;
        saveData.vol = this.volume;

        return saveData;
    }

    public void RestoreState(object state)
    {
        nameOfMusic = ((SceneMusicSaveData)state).nameOfMusic;
        volume = ((SceneMusicSaveData)state).vol;
    }
}