using UnityEngine;

public class StartMusicOnEnable : MonoBehaviour
{
    [SerializeField] string musicTransformName = null;
    [Range(0, 1)]
    [SerializeField] float volume = 0.2f;
    [SerializeField] bool fadeIn = true;


    void OnEnable()
    {
        if (SoundManager.InstanceExists)
        {
            SoundManager.Instance.StartMusic(musicTransformName, volume, fadeIn);
        }
    }
}
