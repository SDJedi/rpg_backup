using System.Collections;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleSoundSystem : MonoBehaviour
{
    ParticleSystem mainSystem;
    int currentNumOfParticles = 0;

    [SerializeField] AudioClip[] onBirthSounds = null;
    [SerializeField] AudioClip[] onDeathSounds = null;


    void Start()
    {
        mainSystem = GetComponent<ParticleSystem>();
    }


    void Update()
    {
        var count = mainSystem.particleCount;

        if (count < currentNumOfParticles && onDeathSounds.Length > 0)
        {
            SoundManager.Instance.PlayAudioClip(onDeathSounds[Random.Range(0, onDeathSounds.Length)]);
        }

        if (count > currentNumOfParticles && onBirthSounds.Length > 0)
        {
            SoundManager.Instance.PlayAudioClip(onBirthSounds[Random.Range(0, onDeathSounds.Length)]);
        }

        currentNumOfParticles = count;
    }

}
