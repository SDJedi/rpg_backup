using System.Collections;
using UnityEngine;

//Last Reviewed: 11/15/19

public class SoundManager : Singleton<SoundManager>
{
    [SerializeField] AudioSource audioSource = null;
    [SerializeField] AudioClip buzzer = null;
    [SerializeField] AudioClip battleStart = null;


    public void PlayAudioClip(AudioClip clip, float volume = 1, float delay = 0)
    {
        StartCoroutine(PlayClip(clip, volume, delay));
    }

    public void StartMusic(string transformName, float vol = 0.2f, bool fadeIn = true)
    {
        GetComponent<MusicClass>().PlayMusic(transformName, vol, fadeIn);
    }

    public void FadeOutCurrentMusic()
    {
        GetComponent<MusicClass>().FadeOutMusic();
    }

    public void PlayBuzzer()
    {
        PlayAudioClip(buzzer, 1, 0);
    }

    public void PlayBattleStart()
    {
        PlayAudioClip(battleStart, 0.6f, 0);
    }

    public void AdjustMusicVolume(float vol)
    {
        GetComponent<MusicClass>().AdjustVolume(vol);
    }

    IEnumerator PlayClip(AudioClip clip, float volume = 1, float delay = 0)
    {
        yield return new WaitForSeconds(delay);
        audioSource.PlayOneShot(clip, volume);
    }
}
