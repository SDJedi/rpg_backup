using UnityEngine;

public class FadeOutMusicOnEnable : MonoBehaviour
{
    void OnEnable()
    {
        if (SoundManager.InstanceExists)
        {
            SoundManager.Instance.FadeOutCurrentMusic();
        }
    }
}
