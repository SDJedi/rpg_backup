using UnityEngine;

//Last Reviewed: 11/15/19

public class ChangeSceneMusicOnTriggerEnter : MonoBehaviour
{
    [SerializeField] string newMusic = "";
    [Range(0, 1)]
    [SerializeField] float volume = 0.2f;


    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "OverWorldPC")
        {
            //Debug.Log(gameObject.name);
            SceneMusic.Instance.ChangeSceneMusic(newMusic, volume);
        }
    }
}
