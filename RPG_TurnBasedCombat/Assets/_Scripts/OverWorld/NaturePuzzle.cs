﻿using System.Collections;
using Cinemachine;
using RPG.InteractableSystem;
using UnityEngine;

public class NaturePuzzle : MonoBehaviour
{
    const float rotationSpeed = 40;

    [SerializeField] GameObject mainPuzzleObject = null;
    [SerializeField] GameObject puzzleStarter = null;
    [SerializeField] GameObject outterRing = null;
    [SerializeField] GameObject middleRing = null;
    [SerializeField] GameObject innerRing = null;
    [SerializeField] GameObject sunParticleEffect = null;
    [SerializeField] GameObject reward = null;
    [SerializeField] Condition naturePuzzleSwitch = null;
    [SerializeField] Canvas puzzleCanvas = null;
    [SerializeField] Animator anim = null;
    [SerializeField] PC_MoveOrStartInteraction mover = null;
    [SerializeField] CinemachineVirtualCamera puzCam = null;

    GameObject ringToRotate;
    ShowHideButtonControlPanel controlPanel;
    bool doingPuzzle;


    void Start()
    {
        ringToRotate = outterRing;
        controlPanel = GameObject.FindObjectOfType<ShowHideButtonControlPanel>();
    }

    void Update()
    {
        if (!doingPuzzle)return;

        if (!AllGameSwitches.CheckIfConditionIsTrue("OverWorld_2_NaturePuzzleSolved"))
        {
            if (Input.GetKey(KeyCode.RightArrow))
            {
                ringToRotate.transform.Rotate(new Vector3(0, rotationSpeed * Time.deltaTime, 0));
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                ringToRotate.transform.Rotate(new Vector3(0, -rotationSpeed * Time.deltaTime, 0));
            }

            if (Input.GetKeyDown(KeyCode.Tab))
            {
                ChangeRingToRotate();
            }

            CheckForVictory();
        }
    }

    //puzzleStarter calls this method when enabled
    public void StartOrStopNaturePuzzle()
    {
        //reset the switch that starts the puzzle (puzzleStarter is enabled via an interactable)
        puzzleStarter.SetActive(false);
        //start the puzzle...
        if (!doingPuzzle && !AllGameSwitches.CheckIfConditionIsTrue("OverWorld_2_NaturePuzzleSolved"))
        {
            StartCoroutine(StartPuzzle());
        }
        //stop the puzzle
        else if (doingPuzzle)
        {
            doingPuzzle = false;
            if (!AllGameSwitches.CheckIfConditionIsTrue("OverWorld_2_NaturePuzzleSolved"))
            {
                //hide puzzle
                mainPuzzleObject.SetActive(false);
                //enable movement/input
                mover.EnableInputHandling();
                //show UI buttons
                controlPanel.ShowPanel();
                //swap cameras
                puzCam.Priority = 0;
            }
            else
            {
                StartCoroutine(PuzzleSolved());
            }
        }
    }

    IEnumerator StartPuzzle()
    {
        doingPuzzle = true;
        //swap cameras
        puzCam.Priority = 15;
        //disable movement/input
        mover.DisableInputHandling();
        //hide UI buttons
        controlPanel.HidePanel();
        //pause
        yield return new WaitForSeconds(0.25f);
        //show puzzle
        mainPuzzleObject.SetActive(true);
    }

    IEnumerator PuzzleSolved()
    {
        //showParticleEffect
        sunParticleEffect.SetActive(true);
        //play animation
        anim.enabled = true;
        //hide canvas
        puzzleCanvas.enabled = false;
        //wait
        yield return new WaitForSeconds(6);
        //enable movement/input
        mover.EnableInputHandling();
        //show UI buttons
        controlPanel.ShowPanel();
        //swap cameras
        puzCam.Priority = 0;
        //hide puzzle
        mainPuzzleObject.SetActive(false);
        yield return new WaitForSeconds(1);
        reward.SetActive(true);
    }

    void ChangeRingToRotate()
    {
        if (ringToRotate == outterRing)
        {
            ringToRotate = middleRing;
        }
        else if (ringToRotate == middleRing)
        {
            ringToRotate = innerRing;
        }
        else
        {
            ringToRotate = outterRing;
        }
    }

    void CheckForVictory()
    {
        if (AngleIsAcceptable(outterRing.transform.localEulerAngles.y) &&
            AngleIsAcceptable(middleRing.transform.localEulerAngles.y) &&
            AngleIsAcceptable(innerRing.transform.localEulerAngles.y))
        {
            //set the game switch
            AllGameSwitches.TrySetCondition(naturePuzzleSwitch, true);
            StartOrStopNaturePuzzle();
        }
    }

    bool AngleIsAcceptable(float y)
    {
        if (y >= 0 && y <= 2)
        {
            return true;
        }
        if (y <= 360 && y > 358)
        {
            return true;
        }

        return false;
    }
}
