using System.Collections;
using Cinemachine;
using RPG.DialogueSystem;
using RPG.Inventory;
using RPG.QuestSystem;
using RPG.Saving;
using UnityEngine;
//Last Reviewed: 9/13/19

//handles the OverWorld camera
public class CameraZoomControls : MonoBehaviour, ISaveable
{
    const float rotationSpeed = 3f;
    const float camJostleSpeed = 30;

    [SerializeField] float minZoom = 5;
    [SerializeField] float maxZoom = 25;
    [SerializeField] float scrollSensitivity = 20;

    CinemachineVirtualCamera vcam;
    CinemachineFramingTransposer cft;
    Quaternion defaultCamRotation;

    float savedCamDistance;
    bool camIsAutoPositioning;


    void Awake()
    {
        vcam = GetComponent<CinemachineVirtualCamera>();
        cft = vcam.GetCinemachineComponent<CinemachineFramingTransposer>();
        defaultCamRotation = transform.rotation;
    }

    void Update()
    {
        //clamp zoom distances...
        cft.m_CameraDistance = CapZoomDistance(cft.m_CameraDistance);

        //allow player to zoom with mouse wheel if ok to zoom
        if (OkToManuallyZoom())
        {
            if (Input.GetAxis("Mouse ScrollWheel") != 0)
            {
                if (cft.m_CameraDistance - (Input.GetAxis("Mouse ScrollWheel") * scrollSensitivity) > maxZoom)
                {
                    cft.m_CameraDistance = maxZoom;
                }
                else if (cft.m_CameraDistance - (Input.GetAxis("Mouse ScrollWheel") * scrollSensitivity) < minZoom)
                {
                    cft.m_CameraDistance = minZoom;
                }
                else
                {
                    cft.m_CameraDistance -= (Input.GetAxis("Mouse ScrollWheel") * scrollSensitivity);
                }
            }
        }
    }

    //called by DialogueManager.cs at the start of a conversation
    public void MoveCamToConversationPosition()
    {
        savedCamDistance = cft.m_CameraDistance;

        float targetX = 25;
        float targetY = GameObject.FindGameObjectWithTag("OverWorldPC").transform.rotation.eulerAngles.y + 60;
        float targetZ = 0;

        Quaternion targetRot = Quaternion.Euler(new Vector3(targetX, targetY, targetZ));
        float targetZoom = minZoom;

        StopAllCoroutines();
        StartCoroutine(AutoRotateAndZoomCamera(targetRot, targetZoom, rotationSpeed));
    }

    //called by DialogueManager.cs at the end of a conversation
    public void RestoreCamToPreviousPosition()
    {
        StopAllCoroutines();
        StartCoroutine(AutoRotateAndZoomCamera(defaultCamRotation, savedCamDistance, rotationSpeed));
    }

    //smoothly rotate and zoom camera to a desired spot
    IEnumerator AutoRotateAndZoomCamera(Quaternion targetRot, float targetZoom, float transitionSpeed)
    {
        camIsAutoPositioning = true;

        //ensure targetZoom is within min/max bounds
        targetZoom = CapZoomDistance(targetZoom);

        //set an alternate condition for exiting the while loop ... i.e. if it takes more than 3 seconds 
        //to get the camera in place, something went wrong and we should abort
        float maxTime = 3;

        //move the camera a little each frame until it reaches its destination or 3 seconds have passed
        while (!Mathf.Approximately(cft.m_CameraDistance, targetZoom) || !QuaternionsAreEqual(transform.rotation, targetRot))
        {
            cft.m_CameraDistance = Mathf.MoveTowards(cft.m_CameraDistance, targetZoom, (scrollSensitivity) * Time.deltaTime);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, transitionSpeed * Time.deltaTime);
            maxTime -= Time.deltaTime;
            if (maxTime > 0)
            {
                yield return new WaitForEndOfFrame();
            }
            else
            {
                camIsAutoPositioning = false;
                yield break;
            }
        }

        camIsAutoPositioning = false;
    }

    float CapZoomDistance(float targetZoom)
    {
        if (targetZoom > maxZoom)
        {
            targetZoom = maxZoom;
        }

        if (targetZoom < minZoom)
        {
            targetZoom = minZoom;
        }

        return targetZoom;
    }

    bool QuaternionsAreEqual(Quaternion q1, Quaternion q2)
    {
        return Mathf.Abs(Quaternion.Dot(q1, q2)) >= 0.999999f;
    }

    bool OkToManuallyZoom()
    {
        return (!Inventory_UI.Instance.InvWindowIsOpen() &&
            !QuestJournal.Instance.QuestJournalIsOpen() &&
            !ItemShopManager.Instance.IsShopping &&
            !DialogueSystem.DialogueManager.Instance.InConversation &&
            !camIsAutoPositioning);
    }

    public IEnumerator StartBattle_CamInAndOut()
    {
        float timer = 2;
        bool moveIn = true;

        while (timer > 0)
        {
            timer -= Time.deltaTime;

            if (vcam.m_Lens.FieldOfView > 55 && moveIn)
            {
                vcam.m_Lens.FieldOfView -= camJostleSpeed * Time.deltaTime;
                if (vcam.m_Lens.FieldOfView <= 55)
                {
                    moveIn = false;
                }
            }
            else if (vcam.m_Lens.FieldOfView < 65 && !moveIn)
            {
                vcam.m_Lens.FieldOfView += camJostleSpeed * Time.deltaTime;
                if (vcam.m_Lens.FieldOfView >= 65)
                {
                    moveIn = true;
                }
            }

            yield return new WaitForEndOfFrame();
        }
    }

#region Save Data
    public object CaptureState()
    {
        return cft.m_CameraDistance;
    }

    public void RestoreState(object state)
    {
        cft.m_CameraDistance = (float)state;
    }
#endregion
}
