using RPG.CombatSystem;
using RPG.SceneControl;

public static class VistInn
{
    static float fadeTime = 3;


    public static void RestoreAllPCs()
    {
        SceneController.Instance.FadeOutAndBackInAfterXSeconds(fadeTime);
        foreach (Base_Stats_Config stats in PCManager.Instance.PC_StatConfigs)
        {
            stats.FullyRestoreCharacter();
        }
    }
}
