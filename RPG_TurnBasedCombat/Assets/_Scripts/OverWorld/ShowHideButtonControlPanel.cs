using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Last Reviewed: 11/15/19

//The main button control panel is visible most of the time and gets hidden when certain scenes are active,
//or when cinematics are playing.
public class ShowHideButtonControlPanel : MonoBehaviour
{
    public GameObject panel = null;

    List<string> HidePanelSceneNames = new List<string> { "CharacterSelectScene", "CombatScene", "MainMenu", "CharacterPointsScene", "MiniGame_ArmWrestle" };


    void Start()
    {
        SceneManager.sceneLoaded += ShowOrHidePanelBasedOnScene;
    }

    void OnDestroy()
    {
        SceneManager.sceneLoaded -= ShowOrHidePanelBasedOnScene;
    }

    void ShowOrHidePanelBasedOnScene(Scene scene, LoadSceneMode mode)
    {
        for (int i = 0; i < HidePanelSceneNames.Count; i++)
        {
            if (scene.name == HidePanelSceneNames[i])
            {
                HidePanel();
                return;
            }
        }

        ShowPanel();
    }

    public void HidePanel()
    {
        panel.SetActive(false);
    }

    public void ShowPanel()
    {
        panel.SetActive(true);
    }
}