using UnityEngine;
using UnityEngine.AI;

public class GameObjectFollowPC : MonoBehaviour
{
    GameObject PC = null;
    NavMeshAgent nav;
    Animator anim;
    readonly int hash_Speed_AnimPara = Animator.StringToHash("Speed");


    void Start()
    {
        PC = GameObject.FindGameObjectWithTag("OverWorldPC");
        nav = GetComponent<NavMeshAgent>();
        anim = GetComponentInChildren<Animator>();
    }

    void Update()
    {
        if (nav != null && nav.isActiveAndEnabled)
        {
            float speed = nav.desiredVelocity.magnitude;
            nav.SetDestination(PC.transform.position);
            if (anim != null)
            {
                anim.SetFloat(hash_Speed_AnimPara, speed, 0.1f, Time.deltaTime);
            }
        }
    }
}
