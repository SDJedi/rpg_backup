using RPG.CombatSystem;
using UnityEngine;

public class RezSaulIfDeadAfterCombat : MonoBehaviour
{
    void Start()
    {
        Base_Stats_Config SaulStats = PCManager.Instance.GetStatsForPC("Saul");
        if (!SaulStats.IsAlive())
        {
            SaulStats.ModifyCurrentHP(1);
        }
    }
}
