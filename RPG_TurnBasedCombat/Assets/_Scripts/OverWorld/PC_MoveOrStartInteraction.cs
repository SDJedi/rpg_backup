using System.Collections;
using RPG.CombatSystem;
using RPG.DialogueSystem;
using RPG.InteractableSystem;
using RPG.Inventory;
using RPG.QuestSystem;
using RPG.SceneControl;
using RPG.Utility;
using UnityEngine;
using UnityEngine.AI;

//Last Reviewed: 3/20/20
public class PC_MoveOrStartInteraction : MonoBehaviour
{
    public readonly float MaxNavPathLength = 45;
    const float stopDistanceProportion = 0.1f;
    const float inputHoldDelay = 0.5f;

    readonly int hash_Speed_AnimPara = Animator.StringToHash("Speed");
    readonly int hash_Locomotion_AnimTag = Animator.StringToHash("Locomotion");

    [SerializeField] Animator anim = null;
    [SerializeField] NavMeshAgent navAgent = null;
    [SerializeField] float speedDampTime = 0.1f;
    [SerializeField] float slowingSpeed = 0.175f;
    [SerializeField] float turnSpeedThreshold = 0.5f;
    [SerializeField] float turnSmoothing = 15;

    WaitForSeconds inputHoldWait;
    Vector3 destinationPosition;
    Interactable currentInteractable;
    NavMeshPath path;
    Collider col;
    Camera mainCam;
    bool handleInput = true;

    public bool IsMoving { get { return navAgent.velocity.magnitude > 0.01f; } }
    public bool HasInteractable { get { return currentInteractable != null; } }
    public Vector3 DestinationPosition { get { return destinationPosition; } }


    void Start()
    {
        inputHoldWait = new WaitForSeconds(inputHoldDelay);
        navAgent.updateRotation = false;
        destinationPosition = transform.position;
        path = new NavMeshPath();
        col = GetComponent<Collider>();
        mainCam = Camera.main;
    }

    //built-in Unity method
    void OnAnimatorMove()
    {
        navAgent.velocity = anim.deltaPosition / Time.deltaTime;
    }

    void Update()
    {
        //handle click on interactable
        if (navAgent.isActiveAndEnabled && Input.GetMouseButtonDown(0))
        {
            OnInteractableClick(PrimaryRaycaster.Instance.InteractableMouseIsOver);
        }

        //handle click / hold on terrain
        if (navAgent.isActiveAndEnabled && Input.GetMouseButton(0))
        {
            OnGroundClick();
        }

        if (navAgent.pathPending) return;

        //handle stopping/slowing/rotating
        float speed = navAgent.desiredVelocity.magnitude;

        if (navAgent.isActiveAndEnabled && navAgent.remainingDistance <= navAgent.stoppingDistance * stopDistanceProportion)
        {
            Stopping(out speed);
        }
        else if (navAgent.isActiveAndEnabled && navAgent.remainingDistance <= navAgent.stoppingDistance)
        {
            Slowing(out speed, navAgent.remainingDistance);
        }
        else if (speed > turnSpeedThreshold)
        {
            LookWhereYoureGoing();
        }

        //play walk/run/idle animation based on speed
        anim.SetFloat(hash_Speed_AnimPara, speed, speedDampTime, Time.deltaTime);
    }

    void OnEnable()
    {
        RandomEncounterManager.Instance.SetMover(this);
        PrimaryRaycaster.Instance.SetOverWorldPC(gameObject);
        Inventory_UI.Instance.InvWindowJustOpened += StopMoving;
        QuestJournal.Instance.QuestJournalJustOpened += StopMoving;
        SceneController.Instance.SceneChangeInitiated += StopMovementAndInputHandling;
    }

    void OnDisable()
    {
        if (PrimaryRaycaster.InstanceExists)
        {
            PrimaryRaycaster.Instance.SetOverWorldPC(null);
        }

        if (Inventory_UI.InstanceExists)
        {
            Inventory_UI.Instance.InvWindowJustOpened -= StopMoving;
        }

        if (QuestJournal.InstanceExists)
        {
            QuestJournal.Instance.QuestJournalJustOpened -= StopMoving;
        }

        if (SceneController.InstanceExists)
        {
            SceneController.Instance.SceneChangeInitiated -= StopMovementAndInputHandling;
        }

        if (RandomEncounterManager.InstanceExists)
        {
            RandomEncounterManager.Instance.SetMover(null);
        }
    }

    void Stopping(out float speed)
    {
        //print("stopping");
        navAgent.isStopped = true;
        //transform.position = destinationPosition;
        speed = 0f;

        if (currentInteractable)
        {
            transform.rotation = currentInteractable.interactionLocation.rotation;
            currentInteractable.Interact();
            currentInteractable = null;
            StartCoroutine(WaitForInteraction());
        }
    }

    void Slowing(out float speed, float distanceToDestination)
    {
        //print("slowing");
        navAgent.isStopped = true;
        float proportionalDistance = 1f - (distanceToDestination / navAgent.stoppingDistance);
        Quaternion targetRotation = currentInteractable != null ? currentInteractable.interactionLocation.rotation : transform.rotation;
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, proportionalDistance);
        transform.position = Vector3.MoveTowards(transform.position, destinationPosition, slowingSpeed * Time.deltaTime);
        speed = Mathf.Lerp(slowingSpeed, 0f, proportionalDistance);
    }

    void LookWhereYoureGoing()
    {
        //print("rotating");
        Quaternion lookRot = Quaternion.LookRotation(navAgent.desiredVelocity);
        Quaternion targetRot = Quaternion.Lerp(transform.rotation, lookRot, turnSmoothing * Time.deltaTime);
        //only rotate on the y-axis
        Vector3 v = new Vector3(transform.rotation.eulerAngles.x, targetRot.eulerAngles.y, transform.rotation.eulerAngles.z);
        transform.rotation = Quaternion.Euler(v);
    }

    public void StopMovementAndInputHandling()
    {
        DisableInputHandling();
        StopMoving();
    }

    public void EnableInputHandling()
    {
        handleInput = true;
    }

    public void DisableInputHandling()
    {
        handleInput = false;
    }

    void StopMoving()
    {
        if (navAgent.isActiveAndEnabled)
        {
            navAgent.velocity = Vector3.zero;
            navAgent.isStopped = true;
        }
    }

    void OnGroundClick()
    {
        //ensure collider is on
        col.enabled = true;

        if (!OkToMoveAndInteract())
        {
            return;
        }

        if (!PrimaryRaycaster.Instance.MouseOverReachableTerrain)
        {
            if (currentInteractable == null)
            {
                StopMoving();
            }
            return;
        }

        if (navAgent.pathPending) return;

        Vector3 targetPos = transform.position;
        RaycastToMousePos(out targetPos);

        float dist = Vector3.Distance(transform.position, targetPos);
        if (dist < 0.5f)
        {
            //Debug.Log("travel distance too small");
            return;
        }

        currentInteractable = null;
        destinationPosition = targetPos;
        navAgent.SetDestination(destinationPosition);
        navAgent.isStopped = false;
    }

    public void OnInteractableClick(Interactable interactable)
    {
        //ensure collider is on
        col.enabled = true;

        if (!OkToMoveAndInteract())
        {
            return;
        }

        if (interactable == null)
        {
            return;
        }

        if (!NavMeshUtility.NavPathIsValid(navAgent, interactable.interactionLocation.position, MaxNavPathLength))
        {
            destinationPosition = transform.position;
            currentInteractable = null;
            return;
        }

        currentInteractable = interactable;
        destinationPosition = currentInteractable.interactionLocation.position;

        if (Vector3.Distance(destinationPosition, transform.position) <= navAgent.stoppingDistance)
        {
            transform.rotation = currentInteractable.interactionLocation.rotation;
            currentInteractable.Interact();
            currentInteractable = null;
            StartCoroutine(WaitForInteraction());
        }
        else
        {
            navAgent.SetDestination(destinationPosition);
            navAgent.isStopped = false;
        }
    }

    void RaycastToMousePos(out Vector3 targetPos)
    {
        RaycastHit hit;
        if (Physics.Raycast(NavMeshUtility.GetMouseRay(mainCam), out hit))
        {
            //set position to move to
            targetPos = hit.point;
        }
        else
        {
            targetPos = transform.position;
        }
    }

    public void OverrideDestinationPos(Vector3 pos)
    {
        destinationPosition = pos;
    }

    IEnumerator WaitForInteraction()
    {
        handleInput = false;

        yield return inputHoldWait;

        while (anim.GetCurrentAnimatorStateInfo(0).tagHash != hash_Locomotion_AnimTag)
        {
            yield return null;
        }

        handleInput = true;
    }

    bool OkToMoveAndInteract()
    {
        return (handleInput &&
            !DialogueSystem.DialogueManager.Instance.InConversation && //newer system
            !DialogueSystem.DialogueManager.Instance.InConversation &&
            !Inventory_UI.Instance.InvWindowIsOpen() &&
            !QuestJournal.Instance.QuestJournalIsOpen() &&
            !SceneController.Instance.sceneChangeInProgress &&
            !ItemShopManager.Instance.IsShopping);
    }
}