﻿using UnityEngine;

public class BeginNaturePuzzleOnEnable : MonoBehaviour
{
    [SerializeField] NaturePuzzle naturePuzzle = null;


    void OnEnable()
    {
        naturePuzzle.StartOrStopNaturePuzzle();
    }
}
