using System.Collections.Generic;
using RPG.CombatSystem;
using UnityEngine;

//Last Reviewed:11/1/19

public class ChangeRandomEncountersOnTriggerEnter : MonoBehaviour
{
    [Header("Empty List will reset to DefaultEncountersForScene if one exists")]
    [SerializeField] List<EncounterAndSpawnChance> newEncounterList = null;


    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "OverWorldPC")
        {
            if ((newEncounterList == null || newEncounterList.Count < 1) && DefaultEncountersForScene.InstanceExists)
            {
                newEncounterList = DefaultEncountersForScene.Instance.encountersForThisScene;
            }
            RandomEncounterManager.Instance.SetPossibleEncountersList(newEncounterList);
        }
    }
}
