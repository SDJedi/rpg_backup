using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PatrolRoute : MonoBehaviour
{
    [SerializeField] List<Transform> waypoints = new List<Transform>();
    [SerializeField] Animator anim = null;
    [SerializeField] float lingerTime = 1f;
    [SerializeField] bool matchWaypointRotation = true;
    [SerializeField] float rotSpeed = 35;

    NavMeshAgent nav;
    int lastIndex;
    int currentIndex;
    float timer = 0;
    bool waiting = false;


    void Start()
    {
        nav = GetComponent<NavMeshAgent>();
        nav.autoBraking = true; //need to have autoBraking or the nav can get stuck trying to hit a waypoint
        currentIndex = 0;
        nav.SetDestination(waypoints[currentIndex].position);
    }

    void Update()
    {
        timer += Time.deltaTime;

        //If we're not waiting at a waypoint and we have no pathPending, check to see if we have reached the current waypoint...
        if (!nav.pathPending && !waiting)
        {
            //if we're not yet at the waypoint, set anim bool so we play the walking animation
            if (anim != null)
            {
                anim.SetBool("IsMoving_Fwd", true);
            }

            //check distance from waypoint...
            if (Mathf.Approximately(nav.remainingDistance, 0f))
            {
                //one last check to make sure we have stopped at the waypoint...
                if (!nav.hasPath || nav.velocity.sqrMagnitude == 0)
                {
                    //we have reached our current destination; save a reference to the currentIndex before we update it
                    lastIndex = currentIndex;

                    //if we're not at the last waypoint, increment the index
                    if (currentIndex < waypoints.Count - 1)
                    {
                        currentIndex++;
                    }
                    //if we ARE at the last waypoint, set the index to 0
                    else
                    {
                        currentIndex = 0;
                    }

                    //set animator walking bool to false...
                    if (anim != null)
                    {
                        anim.SetBool("IsMoving_Fwd", false);
                    }

                    //set linger timer to 0; set waiting = true 
                    timer = 0;
                    waiting = true;
                }
            }
        }

        //Optional: rotate to match waypoint rotation while lingering
        if (waiting && matchWaypointRotation && timer < lingerTime)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, waypoints[lastIndex].rotation, rotSpeed * Time.deltaTime);
        }

        //wait until we've been at the waypoint for lingerTime
        if (waiting && timer >= lingerTime)
        {
            //set a new destination and set waiting to false
            nav.SetDestination(waypoints[currentIndex].position);
            waiting = false;
        }
    }
}
