using RPG.CombatSystem;
using UnityEngine;

//Last Reviewed: 11/14/19

public class EnableOrDisableRandomEncountersOnTriggerEnter : MonoBehaviour
{
    [System.Serializable]
    public enum RandomEncounterState { Enable, Disable }

    [SerializeField] RandomEncounterState randomEncounters = RandomEncounterState.Enable;


    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "OverWorldPC")
        {
            switch (randomEncounters)
            {
                case RandomEncounterState.Enable:
                    RandomEncounterManager.Instance.RandomEncountersDisabled = false;
                    break;
                case RandomEncounterState.Disable:
                    RandomEncounterManager.Instance.RandomEncountersDisabled = true;
                    break;
            }
        }
    }
}
