using System.Collections;
using UnityEngine;

public class NPC_RotateTowardsPlayer : MonoBehaviour
{
    public float distanceFromPlayerToStartRotating = 7;
    public float rotateSpeed = 200;
    public float lingerTime = 2;

    Vector3 dir;
    Quaternion lookRot;
    Quaternion defaultRotation;
    GameObject player;
    bool okToRotate = true;
    float timeSinceLookedAtPC;


    void Start()
    {
        player = GameObject.FindGameObjectWithTag("OverWorldPC");
        defaultRotation = transform.rotation;
    }

    void Update()
    {
        timeSinceLookedAtPC += Time.deltaTime;

        if (okToRotate && Vector3.Distance(player.transform.position, transform.position) <= distanceFromPlayerToStartRotating)
        {
            timeSinceLookedAtPC = 0;
            dir = (player.transform.position - transform.position).normalized;
            lookRot = Quaternion.LookRotation(dir);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, lookRot, rotateSpeed * Time.deltaTime);
        }

        if (okToRotate && timeSinceLookedAtPC > lingerTime)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, defaultRotation, rotateSpeed * Time.deltaTime);
        }

        //rotate on y-axis only
        transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, 0);
    }

    public void StopRotatingForXSeconds(float seconds)
    {
        okToRotate = false;
        StopAllCoroutines();
        StartCoroutine(StopRot(seconds));
    }

    IEnumerator StopRot(float sec)
    {
        yield return new WaitForSeconds(sec);
        okToRotate = true;
    }
}
