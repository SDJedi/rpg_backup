using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using RPG.SceneControl;
using UnityEngine;
using UnityEngine.SceneManagement;

//Last Reviewed: 2/16/20
namespace RPG.Saving
{
    public class SavingSystem : MonoBehaviour
    {
        const string lastSceneName = "LSN";

        public bool Loading { get; private set; }

        #region Public Interface
        //Create a new saveFile or overwrite an existing one
        public void Save(string saveFile)
        {
            //print("Save() was called... " + saveFile);
            //Create a Dictionary. If saveFile already exists, LoadFile() returns an existing Dictionary from saveFile,
            //and if saveFile does NOT exist, LoadFile() returns a new, empty Dictionary
            Dictionary<string, object> state = LoadFile(saveFile);

            //CaptureGameState() stores the current state of all, active SaveableEntity into the Dictionary[state], 
            //and also saves a reference to the active scene buildIndex
            CaptureGameState(state);

            //Serialize the Dictionary, then write the info into the saveFile
            SaveFile(saveFile, state);
        }

        public void SaveSingleObject(string saveFile, GameObject gameObj)
        {
            //Create a Dictionary. If saveFile already exists, LoadFile() returns an existing Dictionary from saveFile,
            //and if saveFile does NOT exist, LoadFile() returns a new, empty Dictionary
            Dictionary<string, object> state = LoadFile(saveFile);

            SaveableEntity saveable = gameObj.GetComponent<SaveableEntity>();
            //capture the state of the object (will capture ALL ISaveable on the SaveableEntity[saveable])
            state[saveable.GetUniqueID()] = saveable.CaptureState();

            //Serialize the Dictionary, then write the info into the saveFile
            SaveFile(saveFile, state);
        }

        public void LoadSingleObject(string saveFile, GameObject gameObj)
        {
            var state = LoadFile(saveFile);

            var saveable = gameObj.GetComponent<SaveableEntity>();

            //get a reference to the saveable's key
            string id = saveable.GetUniqueID();
            //if the key exists...
            if (state.ContainsKey(id))
            {
                //run the RestoreState() that is unique to this object
                saveable.RestoreState(state[saveable.GetUniqueID()]);
            }
        }

        //Load an existing saveFile
        public void Load(string saveFile)
        {
            //print("Load() was called... " + saveFile);
            Loading = true;
            RestoreGameState(LoadFile(saveFile));
        }

        //called when loading from a saveFile ... see SavingWrapper.cs : TryLoadSavedGame()
        //Loads the saveFile && then, if the current scene is different
        //from the last scene stored in the save file, loads the last scene in the saveFile 
        public IEnumerator LoadLastScene(string saveFile)
        {
            //create a Dictionary, if saveFile exists, LoadFile() returns a Dictionary from saveFile,
            //and if saveFile does NOT exist, LoadFile() returns a new, empty Dictionary
            Dictionary<string, object> state = LoadFile(saveFile);

            //If the Dictionary has a key, "LSN"...
            if (state.ContainsKey(lastSceneName))
            {
                //grab the string, scene.name at key "LSN"
                string nameOfScene = (string)state[lastSceneName];

                //if our current scene != last saved scene...
                if (nameOfScene != SceneManager.GetActiveScene().name)
                {
                    //print("loading: " + saveFile);
                    //load the last saved scene, DON'T autoSave here, we want the save file to load completely first
                    SceneController.Instance.FadeAndLoadScene(nameOfScene, false);
                    while (SceneController.Instance.sceneChangeInProgress)
                    {
                        yield return null;
                    }
                }
            }
            else
            {
                Debug.LogWarning("Error loading new scene: save file, " + saveFile + ", does not contain a lastSceneName entry");
            }
        }

        //Delete an existing saveFile
        public void Delete(string saveFile)
        {
            string path = GetPathFromSaveFile(saveFile);
            File.Delete(path);
            print("Deleted save file at: " + path);
        }
        #endregion

        #region Private Methods
        //Serialize our Dictionary[state] containing all of our SavableEntity objects with their uniqueIDs as keys
        void SaveFile(string saveFile, Dictionary<string, object> state)
        {
            string path = GetPathFromSaveFile(saveFile);

            //Note: "stream.Close()" is one way to manually close an open FileStream.
            //It is important to ensure all open FileStreams get closed. In this case, however,
            //the using statement is taking care of this for us. 
            //From the MS Docs: (using) "Provides a convenient syntax that ensures the correct use of IDisposable objects."
            using (FileStream stream = File.Open(path, FileMode.Create))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, state);
                //print("File saved to: " + path);
            }
        }

        //Returns a Deserialized Dictionary<string, object> from a saveFile if one exists, 
        //or returns a new Dictionary if there is no saveFile
        Dictionary<string, object> LoadFile(string saveFile)
        {
            string path = GetPathFromSaveFile(saveFile);

            //if no file exists at the given location...
            if (!File.Exists(path))
            {
                return new Dictionary<string, object>();
            }

            //Open the file, Deserialize it, return it as a Dictionary<string, object>
            using (FileStream stream = File.Open(path, FileMode.Open))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                return (Dictionary<string, object>)formatter.Deserialize(stream);
            }
        }

        //Called by Save()
        void CaptureGameState(Dictionary<string, object> state)
        {
            //loop through all active, loaded objects of Type SaveableEntity...
            foreach (SaveableEntity saveable in FindObjectsOfType(typeof(SaveableEntity)))
            {
                //capture the current state of each object; store that object in the dictionary
                state[saveable.GetUniqueID()] = saveable.CaptureState();
            }
            //every time we capture state, we also save a reference to the current scene (with some exceptions)
            if (SceneManager.GetActiveScene().name != "CombatScene" &&
                SceneManager.GetActiveScene().name != "CharacterSelectScene" &&
                SceneManager.GetActiveScene().name != "CharacterPointsScene" &&
                SceneManager.GetActiveScene().name != "MainMenu" &&
                SceneManager.GetActiveScene().name != "BootScene")
            {
                state[lastSceneName] = SceneManager.GetActiveScene().name;
            }
        }

        //Called by Load()
        void RestoreGameState(Dictionary<string, object> state)
        {
            //print("Restore Start...");
            //loop through all active, loaded objects of Type SaveableEntity...
            foreach (SaveableEntity saveable in FindObjectsOfType<SaveableEntity>())
            {
                //get a reference to the saveable's key
                //print("restoring...");
                string id = saveable.GetUniqueID();
                //if the key exists...
                if (state.ContainsKey(id))
                {
                    //run the RestoreState() that is unique to this object
                    saveable.RestoreState(state[saveable.GetUniqueID()]);
                }
            }
            //print("Restore End...");
            Loading = false;
        }

        //returns the the full path where our saveFile lives
        string GetPathFromSaveFile(string saveFile)
        {
            return Path.Combine(Application.persistentDataPath, saveFile + ".sav");
        }
    }
    #endregion
}
