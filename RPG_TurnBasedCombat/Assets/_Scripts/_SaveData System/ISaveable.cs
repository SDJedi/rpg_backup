//Last Reviewed: 2/16/20
namespace RPG.Saving
{
    //implement in any class that has state that needs to be saved to disc
    public interface ISaveable
    {
        object CaptureState();
        void RestoreState(object state);
    }
}
