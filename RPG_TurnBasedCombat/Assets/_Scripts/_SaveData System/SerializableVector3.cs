using UnityEngine;

//Last Reviewed: 2/16/20
namespace RPG.Saving
{
    [System.Serializable]
    public class SerializableVector3
    {
        public float X, Y, Z;

#region Constructors
        public SerializableVector3(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public SerializableVector3(Vector3 v3)
        {
            X = v3.x;
            Y = v3.y;
            Z = v3.z;
        }
#endregion


        public Vector3 ToVector3()
        {
            return new Vector3(X, Y, Z);
        }
    }
}
