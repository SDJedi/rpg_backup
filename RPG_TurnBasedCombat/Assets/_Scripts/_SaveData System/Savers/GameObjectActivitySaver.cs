using System.Collections.Generic;
using UnityEngine;

//Last Reviewed: 2/16/20
namespace RPG.Saving
{
    //save whether objects are active or inactive ... one per scene, store all objects that can change active state at runtime
    public class GameObjectActivitySaver : MonoBehaviour, ISaveable
    {
        //list of objects that we need to track and save their active status
        public GameObject[] gameObjectsToSave;

        #region SaveData
        //the list of active states that we return in CaptureState() for saving
        [System.Serializable]
        struct GameObjectActiveState_SaveData
        {
            public List<bool> activeStates;
        }

        //get the active state of all objects in the array, send that info to the save system
        public object CaptureState()
        {
            var saveData = new GameObjectActiveState_SaveData();
            saveData.activeStates = new List<bool>();

            for (int i = 0; i < gameObjectsToSave.Length; i++)
            {
                saveData.activeStates.Add(gameObjectsToSave[i].activeSelf);
                //Debug.Log("saving " + gameObjectsToSave[i].name + " : active == " + gameObjectsToSave[i].activeSelf);
            }

            return saveData;
        }

        //restore all objects in the array to their active state
        public void RestoreState(object state)
        {
            var saveData = (GameObjectActiveState_SaveData)state;
            //Debug.Log("Loading " + saveData.activeStates.Count + " saved gameObjects from GameObjectActivitySaver");
            for (int i = 0; i < gameObjectsToSave.Length; i++)
            {
                //Debug.Log("restore state " + gameObjectsToSave[i]);
                if (i < saveData.activeStates.Count)
                {
                    //check to see if the default state of the object at the start of scene load is active
                    bool activeAtStart = gameObjectsToSave[i].activeSelf;

                    //activate or deactivate gameObject based on save info
                    gameObjectsToSave[i].SetActive(saveData.activeStates[i]);

                    //If the object was NOT active at start, but it WAS active on last save, we have an issue...
                    //other info about this object that was saved will not have been restored, because the object
                    //was in its default state (inactive) when the SaveSystem tried to RestoreState() to all objects
                    //and so, now that the object has been set to active, we call on the SaveSystem to try again
                    //to restore any other state we may have saved on this gameObj...
                    if (!activeAtStart && gameObjectsToSave[i].activeSelf && gameObjectsToSave[i].GetComponent<SaveableEntity>() != null)
                    {
                        GameObject.FindObjectOfType<SavingWrapper>().LoadOneGameObject(gameObjectsToSave[i]);
                    }
                }
                else
                {
                    Debug.LogWarning("Was a new object added to GameObjectActivitySaver?");
                }
            }
        }
        #endregion
    }
}