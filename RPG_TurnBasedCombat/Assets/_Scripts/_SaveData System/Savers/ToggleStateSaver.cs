using UnityEngine;
using UnityEngine.UI;

//Last Reviewed: 3/1/20
namespace RPG.Saving
{
    public class ToggleStateSaver : MonoBehaviour, ISaveable
    {
        [SerializeField] Toggle toggleToSave;


        public object CaptureState()
        {
            if (toggleToSave == null)
            {
                toggleToSave = GetComponentInChildren<Toggle>(true);
            }
            return toggleToSave.isOn;
        }

        public void RestoreState(object state)
        {
            if (toggleToSave == null)
            {
                toggleToSave = GetComponentInChildren<Toggle>(true);
            }
            toggleToSave.isOn = (bool)state;
        }
    }
}
