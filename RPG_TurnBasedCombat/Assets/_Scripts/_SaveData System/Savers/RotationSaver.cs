using UnityEngine;

//Last Reviewed: 2/16/20
namespace RPG.Saving
{
    public class RotationSaver : MonoBehaviour, ISaveable
    {
        public object CaptureState()
        {
            return new SerializableVector3(transform.rotation.eulerAngles);
        }

        public void RestoreState(object state)
        {
            transform.eulerAngles = ((SerializableVector3)state).ToVector3();
        }
    }
}
