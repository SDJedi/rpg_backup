using UnityEngine;
using UnityEngine.AI;

//Last Reviewed: 2/16/20
namespace RPG.Saving
{
    public class PositionSaver : MonoBehaviour, ISaveable
    {
        public object CaptureState()
        {
            SerializableVector3 savePos = new SerializableVector3(transform.position);
            //Debug.Log("save pos of " + gameObject.name + " " + savePos.X + "/" + savePos.Y + "/" + savePos.Z);
            return savePos;
        }

        public void RestoreState(object state)
        {
            //Debug.Log("restore pos of " + gameObject.name + " " + ((SerializableVector3)state).X + "/" + ((SerializableVector3)state).Y + "/" + ((SerializableVector3)state).Z);
            NavMeshAgent nav = GetComponent<NavMeshAgent>();
            if (nav != null && nav.enabled)
            {
                nav.Warp(((SerializableVector3)state).ToVector3());
            }
            else
            {
                transform.position = ((SerializableVector3)state).ToVector3();
            }
        }
    }
}
