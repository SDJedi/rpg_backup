using UnityEngine;

//Last Reviewed: 2/16/20
namespace RPG.Saving
{
    //save whether a component is enabled or disabled
    public class BehaviourEnableStateSaver : MonoBehaviour, ISaveable
    {
        public Behaviour behaviourToSave; // Reference to the Behaviour that will have its enabled state saved from and loaded to.


        public object CaptureState()
        {
            return behaviourToSave.enabled;
        }

        public void RestoreState(object state)
        {
            behaviourToSave.enabled = (bool)state;
        }
    }
}
