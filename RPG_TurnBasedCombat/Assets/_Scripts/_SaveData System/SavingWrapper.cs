using System;
using System.Collections;
using System.IO;
using RPG.InteractableSystem;
using RPG.SceneControl;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

//Last Reviewed: 2/16/20
namespace RPG.Saving
{
    //the game-specific interface to SavingSystem.cs
    //NOTE: If I need to make sure save data is fully loaded before checking a value,
    //SceneController.cs / AfterSceneLoad event should only fire AFTER all relevant save data 
    //is loaded.
    public class SavingWrapper : Singleton<SavingWrapper>
    {
        public event Action StartingNewGame;

        public const string runtimeSaveFile = "RSF";
        public const string autoSaveFile = "AutoSave";
        public const string saveSlotOne = "Save_1";
        public const string saveSlotTwo = "Save_2";

        [SerializeField] TextMeshProUGUI debugTxt = null;

        SavingSystem savingSystem;

        public bool savingEnabled { get; private set; } = true;


        protected override void Awake()
        {
            base.Awake();
            //reset game switches before we load from file
            AllGameSwitches AGS = AllGameSwitches.Instance;
            AGS.Reset();

            savingSystem = GetComponent<SavingSystem>();
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.F1) && OkToSave())
            {
                print("Manual Save to Slot 1...");
                Save(saveSlotOne);
            }
            else if (Input.GetKeyDown(KeyCode.F1) && !OkToSave())
            {
                print("Save failed");
            }

            if (Input.GetKeyDown(KeyCode.F2) && OkToSave())
            {
                print("Manual Save to Slot 2...");
                Save(saveSlotTwo);
            }
            else if (Input.GetKeyDown(KeyCode.F2) && !OkToSave())
            {
                print("Save failed");
            }

            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Delete))
            {
                DeleteAllSaveFiles();
            }
        }

        //called from MainMenuButtons_LoadSavedOrStartNewGame.cs, which is attached to the MainMenu button
        public void StartNewGame()
        {
            //reset AGS
            AllGameSwitches.Instance.Reset();

            //Let interested parties know we are starting a new game (a lot of 
            //data in the BootScene needs to be reset)
            if (StartingNewGame != null)
            {
                StartingNewGame();
            }

            //load the starting scene
            File.Delete(GetPathFromSaveFile(runtimeSaveFile));

            string startScene = Enum.GetName(typeof(StartSceneOptions), SceneController.Instance.StartScene);
            SceneController.Instance.FadeAndLoadScene(startScene);
        }

        //called from MainMenuButtons_LoadSavedOrStartNewGame.cs, which is attached to the MainMenu buttons
        public void TryLoadSavedGame(int index)
        {
            switch (index)
            {
                case 0:
                    string path = GetPathFromSaveFile(autoSaveFile);

                    //if no file exists at the given location...
                    if (!File.Exists(path))
                    {
                        print("no autosave");
                    }
                    else
                    {
                        StartCoroutine(LoadLastScene(autoSaveFile));
                        File.Copy(path, GetPathFromSaveFile(runtimeSaveFile), true);
                    }
                    break;
                case 1:
                    path = GetPathFromSaveFile(saveSlotOne);

                    //if no file exists at the given location...
                    if (!File.Exists(path))
                    {
                        print("no file in slot 1");
                    }
                    else
                    {
                        StartCoroutine(LoadLastScene(saveSlotOne));
                        File.Copy(path, GetPathFromSaveFile(runtimeSaveFile), true);
                    }
                    break;
                case 2:
                    path = GetPathFromSaveFile(saveSlotTwo);

                    //if no file exists at the given location...
                    if (!File.Exists(path))
                    {
                        print("no file in slot 2");
                    }
                    else
                    {
                        StartCoroutine(LoadLastScene(saveSlotTwo));
                        File.Copy(path, GetPathFromSaveFile(runtimeSaveFile), true);
                    }
                    break;
                default:
                    print("invalid index");
                    break;
            }
        }

        //called when loading from a saveFile ... see TryLoadSavedGame()
        IEnumerator LoadLastScene(string saveFile)
        {
            if (SceneManager.GetActiveScene().name == "MainMenu")
            {
                yield return savingSystem.LoadLastScene(saveFile);
            }
        }

        //called often on scenechanges
        public void RuntimeSave()
        {
            //print("RuntimeSave() was called");
            if (debugTxt != null)
            {
                debugTxt.text = "Game saved at: " + Path.Combine(Application.persistentDataPath, runtimeSaveFile + ".sav");
            }
            savingSystem.Save(runtimeSaveFile);
        }

        //saves to RSF, then creates or overwrites a save file for fileName
        public void Save(string fileName)
        {
            if (debugTxt != null)
            {
                debugTxt.text = "Game saved at: " + Path.Combine(Application.persistentDataPath, fileName + ".sav");
            }
            savingSystem.Save(runtimeSaveFile);
            File.Copy(GetPathFromSaveFile(runtimeSaveFile), GetPathFromSaveFile(fileName), true);
            /////////////
            if (fileName == autoSaveFile)
            {
                Debug.Log("AutoSaving");
            }
            /////////////
        }

        public void LoadRuntimeSave()
        {
            savingSystem.Load(runtimeSaveFile);
        }

        //temporary way to quickly delete save files, including the newGameFile when necessary
        void DeleteAllSaveFiles()
        {
            savingSystem.Delete(autoSaveFile);
            savingSystem.Delete(runtimeSaveFile);
            savingSystem.Delete(saveSlotOne);
            savingSystem.Delete(saveSlotTwo);
        }

        bool OkToSave()
        {
            return (SceneManager.GetActiveScene().name != "MainMenu" &&
                SceneManager.GetActiveScene().name != "CombatScene" &&
                savingEnabled);
        }

        public void SetSavingEnabled(bool enabled)
        {
            savingEnabled = enabled;
        }

        //returns the the full path where our saveFile lives
        string GetPathFromSaveFile(string saveFile)
        {
            return Path.Combine(Application.persistentDataPath, saveFile + ".sav");
        }

        public void SaveOneGameObject(GameObject go)
        {
            savingSystem.SaveSingleObject(runtimeSaveFile, go);
        }

        public void LoadOneGameObject(GameObject go)
        {
            savingSystem.LoadSingleObject(runtimeSaveFile, go);
        }
    }
}
