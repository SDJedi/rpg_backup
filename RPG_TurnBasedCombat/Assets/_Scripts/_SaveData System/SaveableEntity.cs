using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

//Last Reviewed: 2/16/20
namespace RPG.Saving
{
    [ExecuteAlways]
    public class SaveableEntity : MonoBehaviour
    {
        //globalLookup is used to help ensure each SaveableEntity has a unique "uniqueID"
        static Dictionary<string, SaveableEntity> globalLookup = new Dictionary<string, SaveableEntity>();

        [SerializeField] string uniqueID = "";


        public string GetUniqueID()
        {
            return uniqueID;
        }

        //loop through all ISaveable on gameObject; on each ISaveable, call CaptureState() and store that data
        //in a Dictionary; return the Dictionary
        public object CaptureState()
        {
            Dictionary<string, object> state = new Dictionary<string, object>();
            foreach (ISaveable saveable in GetComponents<ISaveable>())
            {
                state[saveable.GetType().ToString()] = saveable.CaptureState();
            }
            return state;
        }

        //take in a Dictionary<string, object> containing saveData; loop through each ISaveable on gameObject;
        //look up the Run-Time-Type of the ISaveable in the Dictionary<string, object>; if the 
        //ISaveable exists in the Dictionary, call RestoreState()
        public void RestoreState(object state)
        {
            //Debug.Log("SaveableEntity: " + GetUniqueID() + " is running RestoreState()");
            Dictionary<string, object> stateDict = (Dictionary<string, object>)state;
            foreach (ISaveable saveable in GetComponents<ISaveable>())
            {
                string typeString = saveable.GetType().ToString();
                if (stateDict.ContainsKey(typeString))
                {
                    saveable.RestoreState(stateDict[typeString]);
                }
            }
        }

        //set uniqueID automatically
        #if UNITY_EDITOR
        void Update()
        {
            if (Application.IsPlaying(gameObject) || //we are in play mode ... don't generate uniqueID
                string.IsNullOrEmpty(gameObject.scene.path)) //we are editing a prefab ... don't generate uniqueID
            {
                return;
            }
    
            SerializedObject obj = new SerializedObject(this); //serialize the monobehaviour
            SerializedProperty property = obj.FindProperty("uniqueID"); //get a reference to the uniqueID property

            if (string.IsNullOrEmpty(property.stringValue) || !IsUnique(property.stringValue)) //make sure the uniqueID has not already been set
            {
                property.stringValue = System.Guid.NewGuid().ToString(); //set the uniqueID
                obj.ApplyModifiedProperties(); //apply the change
            }

            globalLookup[property.stringValue] = this; //add this SaveableEntity to the globalLookup under the key: uniqueID
        }

        bool IsUnique(string candidate)
        {
            //ignore "player"
            if (candidate == "player")
            {
                return true;
            }

            if (!globalLookup.ContainsKey(candidate))
            {
                return true;
            }

            if (globalLookup[candidate] == this)
            {
                return true;
            }

            if (globalLookup[candidate] == null)
            {
                globalLookup.Remove(candidate);
                return true;
            }

            if (globalLookup[candidate].GetUniqueID() != candidate)
            {
                globalLookup.Remove(candidate);
                return true;
            }

            return false;
        }
        #endif
    }
}
