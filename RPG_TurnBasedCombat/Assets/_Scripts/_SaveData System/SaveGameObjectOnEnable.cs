using UnityEngine;

//Last Reviewed: 2/16/20
namespace RPG.Saving
{
    //save the state of a single GameObject to the runtimeSaveFile whenever this gameObject is enabled
    public class SaveGameObjectOnEnable : MonoBehaviour
    {
        [SerializeField] GameObject objToSave = null;


        void OnEnable()
        {
            if (objToSave.GetComponent<SaveableEntity>() == null)
            {
                Debug.LogWarning(objToSave.name + " does not have a SaveableEntity... cannot save it to runtimeSaveFile");
                return;
            }
            FindObjectOfType<SavingSystem>().SaveSingleObject(SavingWrapper.runtimeSaveFile, objToSave);
        }
    }
}
