﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;


namespace DialogueSystem
{
    public class DialogueEditor : EditorWindow
    {
        static Dialogue selectedDialogue = null;

        [NonSerialized] DialogueNode draggingNode = null;
        [NonSerialized] DialogueNode creatingNode = null;
        [NonSerialized] DialogueNode deletingNode = null;
        [NonSerialized] DialogueNode linkingParentNode = null;
        [NonSerialized] Vector2 draggingOffset;
        [NonSerialized] GUIStyle nodeStyle;
        [NonSerialized] GUIStyle playerNodeStyle;
        [NonSerialized] GUIStyle labelStyle;
        [NonSerialized] GUIStyle textFieldStyle;
        [NonSerialized] bool draggingCanvas = false;
        [NonSerialized] Vector2 draggingCanvasOffset;

        GUIStyle greenTxt;
        GUIStyle redTxt;
        Vector2 scrollPos;
        Vector2 dialogueCanvasSize = new Vector2(6000, 4000);

        const float backgroundImgSize = 50;
        const int nodePadding = 20;
        const int nodeBorder = 20;


        [MenuItem("Window/Dialogue")]
        public static void ShowEditorWindow()
        {
            GetWindow<DialogueEditor>(false, "Dialogue");
        }

        //open the dialogue window when a dialogue asset is opened
        [OnOpenAsset(0)]
        static bool OnOpenAsset(int ID, int line)
        {
            selectedDialogue = EditorUtility.InstanceIDToObject(ID)as Dialogue;
            if (selectedDialogue != null)
            {
                ShowEditorWindow();
                return true;
            }
            return false;
        }

        void OnEnable()
        {
            Selection.selectionChanged += OnSelectionChanged;

            nodeStyle = new GUIStyle();
            nodeStyle.wordWrap = true;
            nodeStyle.normal.background = EditorGUIUtility.Load("node0")as Texture2D; //node0 is a built in Unity 2d texture
            nodeStyle.padding = new RectOffset(nodePadding, nodePadding, 20, nodePadding);
            nodeStyle.border = new RectOffset(nodeBorder, nodeBorder, nodeBorder, nodeBorder);

            playerNodeStyle = new GUIStyle();
            playerNodeStyle.wordWrap = true;
            playerNodeStyle.normal.background = EditorGUIUtility.Load("node1")as Texture2D; //node1 is a built in Unity 2d texture
            playerNodeStyle.padding = new RectOffset(nodePadding, nodePadding, 20, nodePadding);
            playerNodeStyle.border = new RectOffset(nodeBorder, nodeBorder, nodeBorder, nodeBorder);

            labelStyle = new GUIStyle();
            labelStyle.normal.textColor = Color.white;
            labelStyle.alignment = TextAnchor.LowerLeft;
            labelStyle.richText = true;
        }

        void SetGreenTxt()
        {
            greenTxt = new GUIStyle(GUI.skin.button);
            Color32 green = new Color32(1, 100, 32, 255);
            greenTxt.fontStyle = FontStyle.Bold;
            greenTxt.normal.textColor = green;
            greenTxt.hover.textColor = green;
            greenTxt.active.textColor = green;
            greenTxt.focused.textColor = green;
        }

        void SetRedTxt()
        {
            redTxt = new GUIStyle(GUI.skin.button);
            redTxt.normal.textColor = Color.red;
            redTxt.hover.textColor = Color.red;
            redTxt.active.textColor = Color.red;
            redTxt.focused.textColor = Color.red;
        }

        void OnSelectionChanged()
        {
            if (Selection.activeObject != null && Selection.activeObject.GetType() == typeof(Dialogue))
            {
                selectedDialogue = Selection.activeObject as Dialogue;
                ShowEditorWindow();
                Repaint();
            }
        }

        //auto called by unity. updates the UI
        void OnGUI()
        {
            if (selectedDialogue)
            {
                ProcessEvents(); //move nodes or canvas if user is dragging with mouse

                scrollPos = EditorGUILayout.BeginScrollView(scrollPos);

                //paint background for dialogue window
                Rect canvas = GUILayoutUtility.GetRect(dialogueCanvasSize.x, dialogueCanvasSize.y);
                Texture2D tex = Resources.Load("background")as Texture2D;
                GUI.DrawTextureWithTexCoords(canvas, tex, new Rect(0, 0, dialogueCanvasSize.x / backgroundImgSize, dialogueCanvasSize.y / backgroundImgSize));

                //draw nodes and connections
                foreach (DialogueNode node in selectedDialogue.GetAllNodes())
                {
                    DrawConnections(node);
                }
                foreach (DialogueNode node in selectedDialogue.GetAllNodes())
                {
                    DrawNode(node);
                }

                //handle deletion of existing nodes and adding of child nodes
                if (creatingNode != null)
                {
                    selectedDialogue.CreateNode(creatingNode);
                    creatingNode = null;
                }

                if (deletingNode != null)
                {
                    selectedDialogue.DeleteNode(deletingNode);
                    deletingNode = null;
                }
                EditorGUILayout.EndScrollView();
            }
            else
            {
                EditorGUILayout.LabelField("No Dialogue Selected");
            }
        }

        //handles drag-and-drop of nodes in inspector
        void ProcessEvents()
        {
            if (Event.current.type == EventType.MouseUp)
            {
                draggingNode = null;
                draggingCanvas = false;
            }
            else if (Event.current.type == EventType.MouseDown && draggingNode == null)
            {
                draggingNode = GetNodeAtPoint(Event.current.mousePosition + scrollPos);
                if (draggingNode != null)
                {
                    draggingOffset = draggingNode.GetRect().position - Event.current.mousePosition;
                    Selection.activeObject = draggingNode;
                }
                else
                {
                    draggingCanvas = true;
                    draggingCanvasOffset = Event.current.mousePosition + scrollPos;
                    Selection.activeObject = selectedDialogue;
                }
            }
            else if (Event.current.type == EventType.MouseDrag && draggingNode != null)
            {
                draggingNode.SetRectPos(Event.current.mousePosition + draggingOffset);
                GUI.changed = true;
            }
            else if (Event.current.type == EventType.MouseDrag && draggingCanvas)
            {
                scrollPos = draggingCanvasOffset - Event.current.mousePosition;
                GUI.changed = true;
            }
        }

        DialogueNode GetNodeAtPoint(Vector2 point)
        {
            DialogueNode nodeToReturn = null;

            foreach (DialogueNode node in selectedDialogue.GetAllNodes())
            {
                if (node.GetRect().Contains(point))
                {
                    nodeToReturn = node;
                }
            }
            return nodeToReturn;
        }

        void DrawNode(DialogueNode node)
        {
            MaintainMinWidthAndHeight(node);

            if (node.GetNodeIsPlayerChoice())
            {
                GUILayout.BeginArea(node.GetRect(), playerNodeStyle);
                GUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("<size=15><b>Player Choice:</b></size>", labelStyle, GUILayout.Width(70), GUILayout.Height(30));
                GUILayout.EndHorizontal();
                EditorGUILayout.LabelField("text:", labelStyle);
                textFieldStyle = new GUIStyle(GUI.skin.textField);
                textFieldStyle.wordWrap = true;
                string newText = EditorGUILayout.TextField(node.GetText(), textFieldStyle, GUILayout.Width(node.GetRect().width - 40), GUILayout.Height(node.GetRect().height - 150));
                node.SetText(newText);
            }
            else
            {
                GUILayout.BeginArea(node.GetRect(), nodeStyle);
                GUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("<size=15><b>Speaker: </b></size>", labelStyle, GUILayout.Width(70), GUILayout.Height(30));
                textFieldStyle = new GUIStyle(GUI.skin.textField);
                textFieldStyle.alignment = TextAnchor.LowerLeft;
                textFieldStyle.wordWrap = true;
                string newName = EditorGUILayout.TextField(node.GetSpeakerName(), textFieldStyle, GUILayout.Width(135), GUILayout.Height(30));
                node.SetSpeakerName(newName);
                GUILayout.EndHorizontal();
                EditorGUILayout.LabelField("text:", labelStyle);
                textFieldStyle.alignment = TextAnchor.UpperLeft;
                string newText = EditorGUILayout.TextField(node.GetText(), textFieldStyle, GUILayout.Width(node.GetRect().width - 40), GUILayout.Height(node.GetRect().height - 150));
                node.SetText(newText);
            }

            GUILayout.BeginHorizontal();

            //draw delete button
            SetRedTxt();
            if (GUILayout.Button("delete", redTxt))
            {
                deletingNode = node;
            }

            //draw button that handles linking and unlinking nodes
            Draw_Link_Unlink_Button(node);

            //draw add child button
            if (GUILayout.Button("+"))
            {
                creatingNode = node;
                linkingParentNode = null;
            }

            GUILayout.EndHorizontal();

            GUILayout.Space(20);
            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("<size=12><b>Ref.Tag: </b></size>", labelStyle, GUILayout.Width(52), GUILayout.Height(20));
            string newTag = EditorGUILayout.TextField(node.GetTag(), textFieldStyle, GUILayout.Width(node.GetRect().width - 95), GUILayout.Height(20));
            node.SetTag(newTag);
            GUILayout.EndHorizontal();
            GUILayout.EndArea();
        }

        void Draw_Link_Unlink_Button(DialogueNode node)
        {
            if (linkingParentNode == null)
            {
                if (GUILayout.Button("link"))
                {
                    linkingParentNode = node;
                }
            }
            else if (linkingParentNode == node)
            {
                if (GUILayout.Button("cancel"))
                {
                    linkingParentNode = null;
                }
            }
            else if (!linkingParentNode.GetChildIDs().Contains(node.GetID()) && !node.GetChildIDs().Contains(linkingParentNode.GetID()))
            {
                SetGreenTxt();
                if (GUILayout.Button("add child", greenTxt))
                {
                    linkingParentNode.AddChildToList(node.GetID());
                    linkingParentNode = null;
                }
            }
            else if (linkingParentNode.GetChildIDs().Contains(node.GetID()))
            {
                if (GUILayout.Button("unlink"))
                {
                    linkingParentNode.RemoveChildFromList(node.GetID());
                    linkingParentNode = null;
                }
            }
        }

        void MaintainMinWidthAndHeight(DialogueNode node)
        {
            if (node.GetRect().width < DialogueNode.minWidth)
            {
                node.SetRectSize(DialogueNode.minWidth, node.GetRect().height);
            }
            if (node.GetRect().height < DialogueNode.minHeight)
            {
                node.SetRectSize(node.GetRect().height, DialogueNode.minHeight);
            }
        }

        void DrawConnections(DialogueNode node)
        {
            Vector2 startPos = new Vector2(node.GetRect().xMax - 8, node.GetRect().yMax - 20);
            foreach (DialogueNode childNode in selectedDialogue.GetAllChildren(node))
            {
                Vector2 endPos = new Vector2(childNode.GetRect().xMin + 8, childNode.GetRect().center.y);
                Vector2 controlPointOffset = new Vector2(endPos.x - startPos.x, 0);
                controlPointOffset.x *= 0.75f;
                Handles.DrawBezier(startPos, endPos, startPos + controlPointOffset, endPos - controlPointOffset, Color.white, null, 4);
            }
        }
    }
}
