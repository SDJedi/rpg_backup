using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GameSwitchRenamer))]
public class GameSwitchRenamerEditor : Editor
{
    const string creationPath = "Assets/Resources/GameSwitchRenamer.asset";

    GameSwitchRenamer gameSwitchRenamer;
    SerializedProperty newDescriptionProperty;

    void OnEnable()
    {
        gameSwitchRenamer = (GameSwitchRenamer)target;
    }

    [MenuItem("Assets/Create/GameSwitchRenamer")]
    static void CreateRenameSwitchAsset()
    {
        if (GameSwitchRenamer.Instance)
        {
            Debug.Log("GameSwitchRenamer asset already exists");
            return;
        }


        GameSwitchRenamer instance = CreateInstance<GameSwitchRenamer>();
        AssetDatabase.CreateAsset(instance, creationPath);

        GameSwitchRenamer.Instance = instance;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        EditorGUILayout.Space();
        EditorGUILayout.Space();

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Rename Switch", GUILayout.Width(120)))
        {
            gameSwitchRenamer.RenameSwitch();
            GUI.FocusControl("");
        }
        EditorGUILayout.EndHorizontal();
    }
}
