using RPG.InteractableSystem;
using UnityEditor;

[CustomEditor(typeof(ActivateQuestReaction))]
public class ActivateQuestReactionEditor : ReactionEditor
{
    protected override string GetFoldoutLabel()
    {
        return "Activate Quest Reaction";
    }
}
