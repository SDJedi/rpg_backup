﻿using RPG.InteractableSystem;
using UnityEditor;

[CustomEditor(typeof(RemoveItemsFromInventoryReaction))]
public class RemoveItemsFromInventoryReactionEditor : ReactionEditor
{
    protected override string GetFoldoutLabel()
    {
        return "Remove Items from Inventory Reaction";
    }
}
