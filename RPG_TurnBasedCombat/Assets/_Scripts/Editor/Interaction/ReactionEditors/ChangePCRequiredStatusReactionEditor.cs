using RPG.InteractableSystem;
using UnityEditor;

[CustomEditor(typeof(ChangePCRequiredStatusReaction))]
public class ChangePCRequiredStatusReactionEditor : ReactionEditor
{
    protected override string GetFoldoutLabel()
    {
        return "Change PC Required Status Reaction";
    }
}
