﻿using RPG.InteractableSystem;
using UnityEditor;

[CustomEditor(typeof(StartConversationReaction))]
public class StartConversationReactionEditor : ReactionEditor
{
    protected override string GetFoldoutLabel()
    {
        return "Start Conversation Reaction";
    }
}
