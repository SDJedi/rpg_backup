using RPG.InteractableSystem;
using UnityEditor;

[CustomEditor(typeof(ModifyGPReaction))]
public class ModifyGPReactionEditor : ReactionEditor
{
    protected override string GetFoldoutLabel()
    {
        return "Modify GP Reaction";
    }
}
