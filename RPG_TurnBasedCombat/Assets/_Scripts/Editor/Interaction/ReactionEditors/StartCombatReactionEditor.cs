using RPG.InteractableSystem;
using UnityEditor;

[CustomEditor(typeof(StartCombatReaction))]
public class StartCombatReactionEditor : ReactionEditor
{
    protected override string GetFoldoutLabel()
    {
        return "Start Combat Reaction";
    }
}
