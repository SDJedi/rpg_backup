using RPG.InteractableSystem;
using UnityEditor;

[CustomEditor(typeof(UpdateQuestReaction))]
public class UpdateQuestReactionEditor : ReactionEditor
{
    protected override string GetFoldoutLabel()
    {
        return "Update Quest Reaction";
    }
}
