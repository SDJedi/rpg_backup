using RPG.InteractableSystem;
using UnityEditor;

[CustomEditor(typeof(CheckConditionThenReactReaction))]
public class CheckConditionThenReactReactionEditor : ReactionEditor
{
    protected override string GetFoldoutLabel()
    {
        return "Check Condition Then React Reaction";
    }
}
