using RPG.InteractableSystem;
using UnityEditor;

[CustomEditor(typeof(AutoSaveReaction))]
public class AutoSaveReactionEditor : ReactionEditor
{
    protected override string GetFoldoutLabel()
    {
        return "AutoSave Reaction";
    }
}
