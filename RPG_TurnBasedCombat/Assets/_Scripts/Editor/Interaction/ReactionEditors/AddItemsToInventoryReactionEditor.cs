using RPG.InteractableSystem;
using UnityEditor;

[CustomEditor(typeof(AddItemsToInventoryReaction))]
public class AddItemsToInventoryReactionEditor : ReactionEditor
{
    protected override string GetFoldoutLabel()
    {
        return "Add Items to Inventory Reaction";
    }
}
