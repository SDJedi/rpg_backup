using RPG.InteractableSystem;
using UnityEditor;

[CustomEditor(typeof(ShowItemShopPanelReaction))]
public class ShowItemShopPanelReactionEditor : ReactionEditor
{
    protected override string GetFoldoutLabel()
    {
        return "Show Item Shop Panel Reaction";
    }
}
