﻿using RPG.InteractableSystem;
using UnityEditor;

[CustomEditor(typeof(ChangeQuestObjectiveDescriptionReaction))]
public class ChangeQuestObjectiveDescriptionReactionEditor : ReactionEditor
{
    protected override string GetFoldoutLabel()
    {
        return "Update Objective Description Reaction";
    }
}
