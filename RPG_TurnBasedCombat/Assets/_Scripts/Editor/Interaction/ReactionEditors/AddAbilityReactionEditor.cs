using RPG.InteractableSystem;
using UnityEditor;

[CustomEditor(typeof(AddAbilityReaction))]
public class AddAbilityReactionEditor : ReactionEditor
{
    protected override string GetFoldoutLabel()
    {
        return "Add Ability Reaction";
    }
}
