using RPG.InteractableSystem;
using UnityEditor;

[CustomEditor(typeof(VisitInnReaction))]
public class VisitInnReactionEditor : ReactionEditor
{
    protected override string GetFoldoutLabel()
    {
        return "Visit Inn Reaction";
    }
}
