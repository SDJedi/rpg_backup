using RPG.InteractableSystem;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Condition))]
public class ConditionEditor : Editor
{
    public enum EditorType
    {
        ConditionAsset,
        AllGameSwitchesAsset,
        ConditionCollection
    }


    public EditorType editorType;
    public SerializedProperty conditionsProperty;


    private SerializedProperty descriptionProperty;
    private SerializedProperty satisfiedProperty;
    private SerializedProperty hashProperty;
    public Condition condition { get; private set; }


    private const float conditionButtonWidth = 30f;
    private const float toggleOffset = 30f;
    private const string conditionPropDescriptionName = "description";
    private const string conditionPropSatisfiedName = "satisfied";
    private const string conditionPropHashName = "hash";
    private const string blankDescription = "No conditions set.";


    private void OnEnable()
    {
        condition = (Condition)target;

        if (target == null)
        {
            DestroyImmediate(this);
            return;
        }

        descriptionProperty = serializedObject.FindProperty(conditionPropDescriptionName);
        satisfiedProperty = serializedObject.FindProperty(conditionPropSatisfiedName);
        hashProperty = serializedObject.FindProperty(conditionPropHashName);
    }


    public override void OnInspectorGUI()
    {
        switch (editorType)
        {
            case EditorType.AllGameSwitchesAsset:
                AllGameSwitchessAssetGUI();
                break;
            case EditorType.ConditionAsset:
                ConditionAssetGUI();
                break;
            case EditorType.ConditionCollection:
                InteractableGUI();
                break;
            default:
                throw new UnityException("Unknown ConditionEditor.EditorType.");
        }
    }

    void AllGameSwitchessAssetGUI()
    {
        Rect myBox = EditorGUILayout.BeginHorizontal(GUI.skin.box, GUILayout.Height(40));
        EditorGUI.indentLevel++;

        EditorGUILayout.LabelField("[" + System.Array.IndexOf(AllGameSwitches.Instance.conditions, condition) + "] " + condition.description);

        if (GUILayout.Button("-", GUILayout.Width(conditionButtonWidth)))
        {
            AllGameSwitchesEditor.RemoveCondition(condition);
        }
        //TODO: find a way to position the check-box in the inspector, keep it from moving, and have it update in inspector whenever changed
        Rect boolBox = new Rect(new Vector2(myBox.position.x + 10, myBox.position.y + 20), new Vector2(myBox.size.x - 10, myBox.size.y - 20));


        condition.SetConditionSatisfied(EditorGUI.Toggle(boolBox, "condition satisfied?", condition.Satisfied));

        EditorGUI.indentLevel--;
        EditorGUILayout.EndHorizontal();
    }

    void ConditionAssetGUI()
    {
        EditorGUILayout.BeginHorizontal(GUI.skin.box);
        EditorGUI.indentLevel++;

        EditorGUILayout.LabelField(condition.description);

        EditorGUI.indentLevel--;
        EditorGUILayout.EndHorizontal();
    }


    void InteractableGUI()
    {
        serializedObject.Update();

        float width = EditorGUIUtility.currentViewWidth / 3f;

        EditorGUILayout.BeginHorizontal();

        int conditionIndex = AllGameSwitchesEditor.TryGetConditionIndex(condition);

        if (conditionIndex == -1)
            conditionIndex = 0;

        conditionIndex = EditorGUILayout.Popup(conditionIndex, AllGameSwitchesEditor.AllGameSwitchesDescriptions,
            GUILayout.Width(width));
        Condition globalCondition = AllGameSwitchesEditor.TryGetConditionAt(conditionIndex);
        descriptionProperty.stringValue = globalCondition != null ? globalCondition.description : blankDescription;
        //Debug.Log("here ... problem with hash?");
        //hashProperty.intValue = Animator.StringToHash (descriptionProperty.stringValue); //Old code
        hashProperty.intValue = globalCondition != null ? globalCondition.hash : -1; //My version
        EditorGUILayout.PropertyField(satisfiedProperty, GUIContent.none, GUILayout.Width(width + toggleOffset));

        if (GUILayout.Button("-", GUILayout.Width(conditionButtonWidth)))
        {
            conditionsProperty.RemoveFromObjectArray(condition);
        }

        EditorGUILayout.EndHorizontal();

        serializedObject.ApplyModifiedProperties();
    }


    public static Condition CreateCondition()
    {
        Condition newCondition = CreateInstance<Condition>();
        string blankDescription = "No conditions set.";
        Condition globalCondition = AllGameSwitchesEditor.TryGetConditionAt(0);
        newCondition.description = globalCondition != null ? globalCondition.description : blankDescription;
        ///////////Dangerous if() Test Code???/////////////////
        if (globalCondition == null) /// <=== my addition : only set hash if condition doesn't already exist
            SetHash(newCondition);
        ///////////////////////////////////////////////
        return newCondition;
    }


    public static Condition CreateCondition(string description)
    {
        Condition newCondition = CreateInstance<Condition>();
        newCondition.description = description;
        if (!ConditionHasUniqueDescription(newCondition))
        {
            DestroyImmediate(newCondition);
            return null;
        }

        if (!SetHash(newCondition))
        {
            DestroyImmediate(newCondition);
            return null;
        }
        return newCondition;
    }


    private static bool SetHash(Condition condition)
    {
        int posHash = Animator.StringToHash(condition.description);
        foreach (Condition c in AllGameSwitches.Instance.conditions)
        {
            if (c.hash == posHash)
            {
                Debug.Log("Possible duplicate hash problem! " + c.description + " was renamed and is using the generated hash");
                return false;
            }
        }

        condition.hash = Animator.StringToHash(condition.description);
        return true;
    }

    public static bool ConditionHasUniqueDescription(Condition condition)
    {
        foreach (Condition c in AllGameSwitches.Instance.conditions)
        {
            if (c.description == condition.description)
            {
                Debug.Log("duplicate description problem!!!");
                return false;
            }
        }
        return true;
    }
}
