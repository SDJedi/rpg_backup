using System;
using System.Linq;
using RPG.InteractableSystem;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AllGameSwitches))]
public class AllGameSwitchesEditor : Editor
{
    public static string[] AllGameSwitchesDescriptions
    {
        get
        {
            if (allGameSwitchesDescriptions == null)
            {
                SetAllGameSwitchesDescriptions();
            }
            return allGameSwitchesDescriptions;
        }
        private set { allGameSwitchesDescriptions = value; }
    }


    private static string[] allGameSwitchesDescriptions;


    private ConditionEditor[] conditionEditors;
    private AllGameSwitches allGameSwitchess;
    private string newConditionDescription = "New Switch";


    private const string creationPath = "Assets/Resources/AllGameSwitches.asset";
    private const float buttonWidth = 30f;


    private void OnEnable()
    {
        allGameSwitchess = (AllGameSwitches)target;

        if (allGameSwitchess.conditions == null)
        {
            allGameSwitchess.conditions = new Condition[0];
        }

        if (conditionEditors == null)
        {
            CreateEditors();
        }
    }

    private void OnDisable()
    {
        for (int i = 0; i < conditionEditors.Length; i++)
        {
            DestroyImmediate(conditionEditors[i]);
        }

        conditionEditors = null;
    }

    private static void SetAllGameSwitchesDescriptions()
    {
        AllGameSwitchesDescriptions = new string[TryGetConditionsLength()];

        for (int i = 0; i < AllGameSwitchesDescriptions.Length; i++)
        {
            AllGameSwitchesDescriptions[i] = TryGetConditionAt(i).description;
        }
    }


    public override void OnInspectorGUI()
    {
        if (conditionEditors.Length != TryGetConditionsLength())
        {
            for (int i = 0; i < conditionEditors.Length; i++)
            {
                DestroyImmediate(conditionEditors[i]);
            }

            CreateEditors();
        }

        for (int i = 0; i < conditionEditors.Length; i++)
        {
            if (conditionEditors[i] != null)
                conditionEditors[i].OnInspectorGUI();
        }

        if (TryGetConditionsLength() > 0)
        {
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();
        }

        EditorGUILayout.BeginHorizontal();
        newConditionDescription = EditorGUILayout.TextField(GUIContent.none, newConditionDescription);
        if (GUILayout.Button("+", GUILayout.Width(buttonWidth)) && newConditionDescription != "" && newConditionDescription != "New Switch")
        {
            AddCondition(newConditionDescription);
        }
        EditorGUILayout.EndHorizontal();
    }


    private void CreateEditors()
    {
        conditionEditors = new ConditionEditor[allGameSwitchess.conditions.Length];

        for (int i = 0; i < conditionEditors.Length; i++)
        {
            conditionEditors[i] = CreateEditor(TryGetConditionAt(i)) as ConditionEditor;
            if (conditionEditors[i] != null)
            {
                conditionEditors[i].editorType = ConditionEditor.EditorType.AllGameSwitchesAsset;
            }
        }
        conditionEditors = conditionEditors.Where(c => c != null).ToArray();
        Array.Sort(conditionEditors, delegate (ConditionEditor ce1, ConditionEditor ce2) { return ce1.condition.description.CompareTo(ce2.condition.description); });
    }

    [MenuItem("Assets/Create/AllGameSwitches")]
    private static void CreateAllGameSwitchessAsset()
    {
        if (AllGameSwitches.Instance)
        {
            Debug.Log("AllGameSwitches asset already exists");
            return;
        }


        AllGameSwitches instance = CreateInstance<AllGameSwitches>();
        AssetDatabase.CreateAsset(instance, creationPath);

        AllGameSwitches.Instance = instance;

        instance.conditions = new Condition[0];
    }


    private void AddCondition(string description)
    {
        if (!AllGameSwitches.Instance)
        {
            Debug.LogError("AllGameSwitches has not been created yet.");
            return;
        }

        Condition newCondition = ConditionEditor.CreateCondition(description);

        if (newCondition != null)
        {
            newCondition.name = description;

            Undo.RecordObject(newCondition, "Created new Condition");

            AssetDatabase.AddObjectToAsset(newCondition, AllGameSwitches.Instance);
            AssetDatabase.SaveAssets();
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(newCondition));

            ArrayUtility.Add(ref AllGameSwitches.Instance.conditions, newCondition);

            EditorUtility.SetDirty(AllGameSwitches.Instance);
            newConditionDescription = "New Switch";
            SetAllGameSwitchesDescriptions();
            AllGameSwitches.SortConditionsAlphabetically();
        }
    }


    public static void RemoveCondition(Condition condition)
    {
        if (!AllGameSwitches.Instance)
        {
            Debug.LogError("AllGameSwitches has not been created yet.");
            return;
        }

        Undo.RecordObject(AllGameSwitches.Instance, "Removing condition");

        ArrayUtility.Remove(ref AllGameSwitches.Instance.conditions, condition);

        DestroyImmediate(condition, true);
        AssetDatabase.SaveAssets();

        EditorUtility.SetDirty(AllGameSwitches.Instance);

        SetAllGameSwitchesDescriptions();
        AllGameSwitches.SortConditionsAlphabetically();
    }


    public static int TryGetConditionIndex(Condition condition)
    {
        for (int i = 0; i < TryGetConditionsLength(); i++)
        {
            if (TryGetConditionAt(i).hash == condition.hash)
                return i;
        }

        return -1;
    }


    public static Condition TryGetConditionAt(int index)
    {
        Condition[] allGameSwitchess = AllGameSwitches.Instance.conditions;

        if (allGameSwitchess == null || allGameSwitchess[0] == null)
            return null;

        if (index >= allGameSwitchess.Length)
            return allGameSwitchess[0];

        return allGameSwitchess[index];
    }


    public static int TryGetConditionsLength()
    {
        if (AllGameSwitches.Instance.conditions == null)
            return 0;

        return AllGameSwitches.Instance.conditions.Length;
    }
}