using RPG.InteractableSystem;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Interactable))]
public class InteractableEditor : EditorWithSubEditors<ConditionCollectionEditor, ConditionCollection>
{
    private Interactable interactable;
    //private SerializedProperty cursorProperty;
    private SerializedProperty cursorInfo;
    private SerializedProperty interactionLocationProperty;
    private SerializedProperty collectionsProperty;
    private SerializedProperty defaultReactionCollectionProperty;


    private const float collectionButtonWidth = 150f;
    //private const string interactablePropDialogueCursorName = "showDialogueCursorOnHover";
    private const string interactablePropCursorInfo = "cursorInfo";
    private const string interactablePropInteractionLocationName = "interactionLocation";
    private const string interactablePropConditionCollectionsName = "conditionCollections";
    private const string interactablePropDefaultReactionCollectionName = "defaultReactionCollection";


    private void OnEnable()
    {
        //cursorProperty = serializedObject.FindProperty(interactablePropDialogueCursorName);
        cursorInfo = serializedObject.FindProperty(interactablePropCursorInfo);
        collectionsProperty = serializedObject.FindProperty(interactablePropConditionCollectionsName);
        interactionLocationProperty = serializedObject.FindProperty(interactablePropInteractionLocationName);
        defaultReactionCollectionProperty = serializedObject.FindProperty(interactablePropDefaultReactionCollectionName);

        interactable = (Interactable)target;

        CheckAndCreateSubEditors(interactable.conditionCollections);
    }

    private void OnDisable()
    {
        CleanupEditors();
    }

    protected override void SubEditorSetup(ConditionCollectionEditor editor)
    {
        if (editor != null)
            editor.collectionsProperty = collectionsProperty;
    }


    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        CheckAndCreateSubEditors(interactable.conditionCollections);

        //EditorGUILayout.PropertyField(cursorProperty);
        EditorGUILayout.PropertyField(cursorInfo, true);
        EditorGUILayout.PropertyField(interactionLocationProperty);

        for (int i = 0; i < subEditors.Length; i++)
        {
            subEditors[i].OnInspectorGUI();
            EditorGUILayout.Space();
        }

        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("Add Condition Collection", GUILayout.Width(collectionButtonWidth)))
        {
            ConditionCollection newCollection = ConditionCollectionEditor.CreateConditionCollection();
            collectionsProperty.AddToObjectArray(newCollection);
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();

        EditorGUILayout.PropertyField(defaultReactionCollectionProperty);

        serializedObject.ApplyModifiedProperties();
    }
}
