using System.Collections.Generic;
using TMPro;
using UnityEngine;

//Last Reviewed: 1/30/20
namespace RPG.Messaging
{
    //Used to display timed messages
    public class TextManager : MonoBehaviour
    {
        const float displayTimePerCharacter = 0.08f;

        public class Instruction
        {
            public Color textColor;
            public string message;
            public float startTime;
            public float duration;
        }

        public TextMeshProUGUI text;
        public TextMeshProUGUI mssgCounterText;
        public float additionalDisplayTime = 0.8f;

        List<Instruction> instructions = new List<Instruction>();

        public bool IsDisplayingText { get; private set; }


        void Start()
        {
            text.richText = true;
        }

        void Update()
        {
            if (instructions.Count > 0 && Time.time >= instructions[0].startTime)
            {
                IsDisplayingText = true;
                text.text = instructions[0].message;
                text.color = instructions[0].textColor;

                instructions[0].duration -= Time.deltaTime;

                if (instructions[0].duration <= 0)
                {
                    instructions.RemoveAt(0);
                    UpdateMssgCounter();
                }
                //print("num of instructions left: " + instructions.Count);
            }
            else if (instructions.Count == 0)
            {
                text.text = string.Empty;
                IsDisplayingText = false;
            }
        }

        //called by "OK" button
        public void SkipToNextMsg()
        {
            if (instructions.Count > 0)
            {
                float remainingDuration = instructions[0].duration;

                for (int i = 0; i < instructions.Count; i++)
                {
                    instructions[i].startTime -= remainingDuration;
                }

                instructions[0].duration = 0;
            }
        }

        //called by "X" button on main message window
        public void DeleteAllMessages()
        {
            IsDisplayingText = false;
            instructions.Clear();
            instructions.Capacity = 0;
        }

        public void DisplayMessage(string message, Color textColor, float delay = 0, float minDisplayTime = 0, bool eraseCurrentMssg = false)
        {
            float startTime = Time.time + delay + TotalDisplayTimeOfAllQueuedMessages();
            float displayDuration = Mathf.Max((message.Length * displayTimePerCharacter + additionalDisplayTime), minDisplayTime);

            Instruction newInstruction = new Instruction
            {
                message = message,
                textColor = textColor,
                startTime = startTime,
                duration = displayDuration
            };

            if (eraseCurrentMssg)
            {
                DeleteAllMessages();
                newInstruction.startTime = Time.time;
            }

            instructions.Add(newInstruction);
            UpdateMssgCounter();
        }

        float TotalDisplayTimeOfAllQueuedMessages()
        {
            float tot = 0;
            for (int i = instructions.Count - 1; i >= 0; i--)
            {
                tot += instructions[i].duration;
            }
            return tot;
        }

        void UpdateMssgCounter()
        {
            if (mssgCounterText != null && instructions.Count > 0)
            {
                mssgCounterText.text = instructions.Count.ToString();
            }
        }
    }
}
