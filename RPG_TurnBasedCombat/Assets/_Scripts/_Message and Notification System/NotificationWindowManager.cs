using RPG.SceneControl;
using UnityEngine;

//Last Reviewed: 1/30/20
namespace RPG.Messaging
{
    //Uses a TextManager to display notifications and error messages to the player
    //in a primary notification window that exists in the BootScene.
    public class NotificationWindowManager : Singleton<NotificationWindowManager>
    {
        [SerializeField] TextManager Notification_TextManager = null;
        [SerializeField] GameObject NotificationWindow_Img = null;

        public Vector2 defaultPos { get { return new Vector2(0, -50); } }


        void Start()
        {
            SceneController.Instance.SceneChangeInitiated += EraseMessagesAndSetInDefaultPosition;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (SceneController.InstanceExists)
            {
                SceneController.Instance.SceneChangeInitiated -= EraseMessagesAndSetInDefaultPosition;
            }
        }

        //show / hide window based on whether there is text being displayed
        void Update()
        {
            if (!Notification_TextManager.IsDisplayingText)
            {
                NotificationWindow_Img.SetActive(false);
            }
            else if (!NotificationWindow_Img.activeSelf)
            {
                NotificationWindow_Img.SetActive(true);
            }
        }

        //plays buzzer sound and displays a red mssg at a desired location
        public void DisplayErrorMessage(string mssg, Vector2 pos)
        {
            SoundManager.Instance.PlayBuzzer();
            RepositionWindow(pos);
            Notification_TextManager.DisplayMessage(mssg, Color.red);
        }

        //display notification at the default pos
        public void DisplayNotification(string message, Color textColor, float delay = 0)
        {
            SetWindowToDefaultPos();
            Notification_TextManager.DisplayMessage(message, textColor, delay);
        }

        //position the notification window, then display notification
        public void DisplayNotification(string message, Color textColor, Vector2 newPos, float delay = 0)
        {
            RepositionWindow(newPos);
            Notification_TextManager.DisplayMessage(message, textColor, delay);
        }

        //allows us to place the notification window in a different location ... default is near the top of the screen
        void RepositionWindow(Vector2 newPos)
        {
            GetComponent<RectTransform>().anchoredPosition = newPos;
        }

        void SetWindowToDefaultPos()
        {
            GetComponent<RectTransform>().anchoredPosition = defaultPos;
        }

        //on scene change...
        void EraseMessagesAndSetInDefaultPosition()
        {
            NotificationWindow_Img.SetActive(false);
            Notification_TextManager.DeleteAllMessages();
            SetWindowToDefaultPos();
        }
    }
}
