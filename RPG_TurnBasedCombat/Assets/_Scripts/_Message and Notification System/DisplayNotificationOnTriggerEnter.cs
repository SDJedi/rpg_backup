using RPG.Saving;
using UnityEngine;

//Last Reviewed: 1/30/20
namespace RPG.Messaging
{
    //display a notification when the OverWorldPC enters a trigger.
    //can be set to display only once, or each time the PC enters the trigger.
    //Note: in order to save the "displayed" state, the gameObject this is 
    //attached to will also need to have a SaveableEntity script attached,
    //as is the case with any script implementing ISaveable
    public class DisplayNotificationOnTriggerEnter : MonoBehaviour, ISaveable
    {
        [SerializeField] string message = "";
        [SerializeField] Color color = Color.white;
        [SerializeField] float delay = 0;
        [SerializeField] bool displayOnlyOnce = true;

        bool displayed = false;


        void OnTriggerEnter(Collider other)
        {
            if (other.tag == "OverWorldPC" && NotificationWindowManager.InstanceExists)
            {
                if (displayOnlyOnce && displayed)
                {
                    return;
                }
                NotificationWindowManager.Instance.DisplayNotification(message, color, delay);
                displayed = true;
            }
        }

        public object CaptureState()
        {
            return displayed;
        }

        public void RestoreState(object state)
        {
            displayed = (bool)state;
        }
    }
}
