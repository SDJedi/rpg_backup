using UnityEngine;
using UnityEngine.Playables;

//Last Reviewed: 11/15/19
public class PlayCinematicOnEnable : MonoBehaviour
{
    [Header("Playable Director to start")]
    [SerializeField] PlayableDirector pd = null;

    [Header("While Cinematic plays...")]
    [SerializeField] bool deactivatePlayerGameObject = true;
    [SerializeField] bool disablePCMovement = true;
    [SerializeField] bool hideButtonPanel = true;

    [Header("After Cinematic plays...")]
    [SerializeField] bool activatePlayerGameObject = true;
    [SerializeField] bool enablePCMovement = true;
    [SerializeField] bool showButtonPanel = true;

    GameObject player;
    PC_MoveOrStartInteraction playerMovement;


    void OnEnable()
    {
        pd.stopped += RunAfterPlayActions;

        player = GameObject.FindGameObjectWithTag("OverWorldPC");
        playerMovement = player.GetComponent<PC_MoveOrStartInteraction>();

        RunBeforePlayActions();
        pd.Play();
    }

    void OnDisable()
    {
        pd.stopped -= RunAfterPlayActions;
    }

    void RunBeforePlayActions()
    {
        if (deactivatePlayerGameObject)
        {
            player.SetActive(false);
        }

        if (disablePCMovement)
        {
            playerMovement.StopMovementAndInputHandling();
        }

        if (hideButtonPanel)
        {
            FindObjectOfType<ShowHideButtonControlPanel>().HidePanel();
        }
    }

    void RunAfterPlayActions(PlayableDirector pd)
    {
        if (activatePlayerGameObject)
        {
            player.SetActive(true);
            GameObject.FindObjectOfType<CameraTeleport>().PlaceCamera();
        }

        if (enablePCMovement)
        {
            playerMovement.EnableInputHandling();
        }

        if (showButtonPanel)
        {
            FindObjectOfType<ShowHideButtonControlPanel>().ShowPanel();
        }
    }
}
