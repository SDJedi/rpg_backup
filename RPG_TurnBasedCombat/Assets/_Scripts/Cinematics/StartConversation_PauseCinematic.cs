using System.Collections;
using UnityEngine;
using UnityEngine.Playables;

//Last Reviewed: 1/13/22

public class StartConversation_PauseCinematic : MonoBehaviour
{
    [SerializeField] GameObject NPC = null;
    [SerializeField] string nodeTag = "";
    [SerializeField] bool playerCanEndConvo = true;
    [SerializeField] bool useConvoCam = false;
    [SerializeField] PlayableDirector playableDirector = null;


    void OnEnable()
    {
        StartCoroutine(StartConvoAndPause());
    }

    IEnumerator StartConvoAndPause()
    {
        DialogueSystem.DialogueManager.Instance.StartConversation(NPC, nodeTag, playerCanEndConvo, useConvoCam);
        playableDirector.playableGraph.GetRootPlayable(0).SetSpeed(0);
        while (DialogueSystem.DialogueManager.Instance.InConversation)
        {
            yield return null;
        }
        playableDirector.playableGraph.GetRootPlayable(0).SetSpeed(1);
    }
}