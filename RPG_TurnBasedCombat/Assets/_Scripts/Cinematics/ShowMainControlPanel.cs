using UnityEngine;

public class ShowMainControlPanel : MonoBehaviour
{
    void OnEnable()
    {
        FindObjectOfType<ShowHideButtonControlPanel>().ShowPanel();
    }
}
