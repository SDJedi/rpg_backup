using UnityEngine;
using UnityEngine.AI;

//Last Reviewed: 3/3/20
public class RepositionOverWorldPCOnEnable : MonoBehaviour
{
    [SerializeField] GameObject OverWorldPC = null;
    [SerializeField] GameObject DesiredPCPos = null;
    [SerializeField] bool setOverWorldPCActiveAfterMove = true;
    [SerializeField] bool WarpMainCamToOverWorldPC = true;


    void OnEnable()
    {
        OverWorldPC.SetActive(true);
        var nav = OverWorldPC.GetComponent<NavMeshAgent>();
        var movement = OverWorldPC.GetComponent<PC_MoveOrStartInteraction>();

        //important to set this, or the pc warps back to original pos when nav agent is reenabled
        movement.OverrideDestinationPos(DesiredPCPos.transform.position);
        
        //turn off nav agent / position PC / turn on nav agent
        nav.enabled = false;
        OverWorldPC.transform.position = DesiredPCPos.transform.position;
        nav.enabled = true;

        //update OWPC rotation to match the pos of DesiredPCPos
        OverWorldPC.transform.rotation = DesiredPCPos.transform.localRotation;
        
        //Activate the OWPC if needed 
        OverWorldPC.SetActive(setOverWorldPCActiveAfterMove);

        //disable / enable main camera to prevent it from lerping into place
        if (WarpMainCamToOverWorldPC)
        {
            GameObject.FindObjectOfType<CameraTeleport>().PlaceCamera();
        }
    }
}
