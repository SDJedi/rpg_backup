using UnityEngine;

public class ToggleAnimatorBoolWithEnable_Disable : MonoBehaviour
{
    [SerializeField] Animator animator = null;
    [SerializeField] string boolToToggle = "";


    void OnEnable()
    {
        animator.SetBool(boolToToggle, true);
    }

    void OnDisable()
    {
        animator.SetBool(boolToToggle, false);
    }
}
