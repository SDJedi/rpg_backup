using RPG.InteractableSystem;
using UnityEngine;
using UnityEngine.Playables;

public class OverWorld_0_BabzIntro : MonoBehaviour
{
    [SerializeField] PlayableDirector pd = null;

    PC_MoveOrStartInteraction playerMover;


    void OnTriggerEnter(Collider other)
    {
        //check if BabzIntroHasAlreadyPlayed...
        if (other.tag == ("OverWorldPC") && !AllGameSwitches.CheckIfConditionIsTrue("Babz Joined Party"))
        {
            pd.Play();
            AllGameSwitches.TrySetCondition("Babz Joined Party", true);
            SoundManager.Instance.StartMusic("TenseEncounter", 0.75f);
            playerMover = other.gameObject.GetComponent<PC_MoveOrStartInteraction>();
            playerMover.StopMovementAndInputHandling();
            FindObjectOfType<ShowHideButtonControlPanel>().HidePanel();
        }
        else
        {
            Debug.Log("intro already played");
        }
    }
}
