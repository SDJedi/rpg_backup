using DialogueSystem;
using RPG.DialogueSystem;
using UnityEngine;
using UnityEngine.AI;

public class PostConversationWithBridgeGuard_NoCombat : MonoBehaviour
{
    [SerializeField] GameObject primaryVirtCam = null;
    [SerializeField] GameObject OverWorldPC = null;
    [SerializeField] GameObject SaulPuppet = null;


    void OnEnable()
    {
        var nav = OverWorldPC.GetComponent<NavMeshAgent>();
        var movement = OverWorldPC.GetComponent<PC_MoveOrStartInteraction>();

        //important to set this, or the pc warps back to original pos when nav agent is reenabled
        movement.OverrideDestinationPos(SaulPuppet.transform.position);
        //turn off nav agent / position PC / turn on nav agent
        nav.enabled = false;
        OverWorldPC.transform.position = SaulPuppet.transform.position;
        nav.enabled = true;
        //re-enable input handling
        movement.EnableInputHandling();

        //Activate the PC and update its rotation to match the puppet
        OverWorldPC.SetActive(true);
        OverWorldPC.transform.rotation = SaulPuppet.transform.localRotation;

        //reactivate the button panel and the primary virtual camera
        FindObjectOfType<ShowHideButtonControlPanel>().ShowPanel();
        primaryVirtCam.SetActive(true);

        //reset the music
        SoundManager.Instance.StartMusic("ThemeMusic", 0.2f, false);
        //end the conversation
        DialogueManager.Instance.EndConversation();
    }
}
