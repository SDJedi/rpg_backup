using RPG.DialogueSystem;
using RPG.InteractableSystem;
using UnityEngine;
using UnityEngine.Playables;

public class OverWorld_0_BridgeScene : MonoBehaviour
{
    public int BridgeToll = 50;

    [SerializeField] PlayableDirector pd = null;
    [SerializeField] GameObject talkingGuard = null;
    [SerializeField] GameObject[] guards = null;

    GameObject player;
    PC_MoveOrStartInteraction playerMovement;


    void Start()
    {
        ShowDeadBodiesIfGuardsAreDead();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "OverWorldPC" && !AllGameSwitches.CheckIfConditionIsTrue("OverWorld_0_BridgeCinematicTriggered")) //if intro has not yet played...
        {
            SoundManager.Instance.StartMusic("PlaceHolderMusic", 1);
            SetGameSwitchForTollAmount();
            pd.stopped += AfterCinematicPlays;
            AllGameSwitches.TrySetCondition("OverWorld_0_BridgeCinematicTriggered", true);
            player = other.gameObject;
            playerMovement = player.GetComponent<PC_MoveOrStartInteraction>();
            playerMovement.StopMovementAndInputHandling();
            FindObjectOfType<ShowHideButtonControlPanel>().HidePanel();
            player.SetActive(false);
            pd.Play();
        }
    }

    void SetGameSwitchForTollAmount()
    {
        AllGameSwitches.TrySetCondition("OverWorld_0_HasTollAmountWhenBridgeApproached", RPG.Inventory.InventoryManager.Instance.CurrentGP >= BridgeToll);
    }

    void AfterCinematicPlays(PlayableDirector pd)
    {
        DialogueSystem.DialogueManager.Instance.StartConversation(talkingGuard, "0", false, false);
        pd.stopped -= AfterCinematicPlays;
    }

    void ShowDeadBodiesIfGuardsAreDead()
    {
        //check if guards have been killed
        if (AllGameSwitches.CheckIfConditionIsTrue("OverWorld_0_BridgeGuardsKilled"))
        {
            foreach (GameObject guard in guards)
            {
                Animator anim = guard.GetComponentInChildren<Animator>();
                anim.Play("Die", 0, 1);
            }
        }
    }
}
