using System.Collections;
using UnityEngine;

public class PostBabzConvo : MonoBehaviour
{
    [SerializeField] GameObject babzPuppet = null;
    [SerializeField] GameObject overWorldSaul = null;


    void Start()
    {
        overWorldSaul.GetComponent<PC_MoveOrStartInteraction>().DisableInputHandling();
        StartCoroutine(MoveBabz());
    }

    IEnumerator MoveBabz()
    {
        float elapsedTime = 0;
        Vector3 startingPos = babzPuppet.transform.position;
        Animator pAnim = babzPuppet.GetComponentInChildren<Animator>();
        pAnim.SetBool("IsMoving_Fwd", true);

        while (elapsedTime < 0.75f)
        {
            babzPuppet.transform.Translate(Vector3.forward * Time.deltaTime * 4);
            yield return new WaitForEndOfFrame();
            elapsedTime += Time.deltaTime;
        }
        babzPuppet.SetActive(false);
        overWorldSaul.GetComponent<PC_MoveOrStartInteraction>().EnableInputHandling();
    }
}
