using System.Collections;
using RPG.InteractableSystem;
using RPG.SceneControl;
using UnityEngine;

public class OverWorld_0_FirstConvoWithBabz : MonoBehaviour
{
    [SerializeField] GameObject BabzPuppet = null;

    GameObject Saul;
    PC_MoveOrStartInteraction pm;

    void Awake()
    {
        Saul = GameObject.FindGameObjectWithTag("OverWorldPC");
        pm = Saul.GetComponent<PC_MoveOrStartInteraction>();
        SceneController.Instance.AfterSceneLoad += ConverseWithBabz;
    }

    void OnDestroy()
    {
        if (SceneController.InstanceExists)
        {
            SceneController.Instance.AfterSceneLoad -= ConverseWithBabz;
        }
    }

    void ConverseWithBabz()
    {
        if (AllGameSwitches.CheckIfConditionIsTrue("OverWorld_0_DefeatedBigRed") && !AllGameSwitches.CheckIfConditionIsTrue("OverWorld_0_HadFirstConvoWithBabz"))
        {
            PositionSaul();
            BabzPuppet.SetActive(true);
            StartCoroutine(StartConvo());
        }
    }

    void PositionSaul()
    {
        pm.DisableInputHandling();
        Saul.transform.position = new Vector3(-296, 100, 421);
        Saul.transform.localRotation = Quaternion.Euler(0, -98, 0);
        pm.OverrideDestinationPos(new Vector3(-296, 100, 421));
    }

    IEnumerator StartConvo()
    {
        yield return new WaitForSeconds(0.5f);
        var babzInt = BabzPuppet.GetComponent<Interactable>();
        babzInt.Interact();
        while (DialogueSystem.DialogueManager.Instance.InConversation)
        {
            yield return null;
        }
        pm.EnableInputHandling();
    }
}
