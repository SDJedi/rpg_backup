using RPG.InteractableSystem;
using RPG.QuestSystem;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Playables;

public class KarlsHut_KarlMakesPotions : MonoBehaviour
{
    [SerializeField] GameObject Karl_NPC = null;

    GameObject player;
    PC_MoveOrStartInteraction playerMovement;
    PlayableDirector pd;
    NavMeshAgent nav;


    void Start()
    {
        player = GameObject.FindGameObjectWithTag("OverWorldPC");
        playerMovement = player.GetComponent<PC_MoveOrStartInteraction>();
        nav = player.GetComponent<NavMeshAgent>();
        pd = GetComponent<PlayableDirector>();

        pd.stopped += CinematicEnded;

        if (AllGameSwitches.CheckIfConditionIsTrue("OverWorld_0_GatheredPurplePlants") && !AllGameSwitches.CheckIfConditionIsTrue("KarlsHut_MadePotionsFromPlants")) //if 5 purple plants gathered, but hasn't made potions...
        {
            //play "make potions" cinematic...
            FindObjectOfType<ShowHideButtonControlPanel>().HidePanel();
            playerMovement.StopMovementAndInputHandling();
            //set player position...
            nav.enabled = false;
            Vector3 saulNewPos = new Vector3(-8.36f, 100, 2.6f);
            playerMovement.OverrideDestinationPos(saulNewPos);
            player.transform.position = saulNewPos;
            player.transform.localEulerAngles = new Vector3(0, 237.7f, 0);
            nav.enabled = true;
            player.SetActive(false);
            //set Karl_NPC position...
            Karl_NPC.transform.position = new Vector3(-10.436f, 100.31f, 0.615f);
            Karl_NPC.transform.localEulerAngles = new Vector3(0, 42.75f, 0);
            //start the cinematic...
            pd.Play();
        }
    }

    void OnDestroy()
    {
        pd.stopped -= CinematicEnded;
    }

    void CinematicEnded(PlayableDirector pd)
    {
        playerMovement.EnableInputHandling();
        FindObjectOfType<ShowHideButtonControlPanel>().ShowPanel();
        QuestManager.Instance.UpdateQuestObjective("Gather \"Purple-looking\" Plants", 1);
    }
}
