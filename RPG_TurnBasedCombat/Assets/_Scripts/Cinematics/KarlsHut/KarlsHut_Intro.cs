using RPG.InteractableSystem;
using RPG.SceneControl;
using UnityEngine;
using UnityEngine.Playables;

//Last Reviewed: 10/8/19
public class KarlsHut_Intro : MonoBehaviour
{
    [Header("Stuff we need to deal with if we exit cutscene early:")]
    [SerializeField] GameObject Karl_NPC_GO = null;
    [SerializeField] GameObject Saul_OverWorld_PC = null;
    [SerializeField] GameObject CauldronAudio = null;

    [Space(20)]
    [SerializeField] Interactable Karl_NPC = null;

    GameObject player;
    PC_MoveOrStartInteraction playerMovement;
    PlayableDirector pd;


    void Start()
    {
        pd = GetComponent<PlayableDirector>();
        pd.stopped += StartConvoWithKarl;

        if (!AllGameSwitches.CheckIfConditionIsTrue("KarlsHut_IntroCinematicHasPlayed")) //if intro has not yet played...
        {
            player = GameObject.FindGameObjectWithTag("OverWorldPC");
            playerMovement = player.GetComponent<PC_MoveOrStartInteraction>();
            FindObjectOfType<ShowHideButtonControlPanel>().HidePanel();
            playerMovement.StopMovementAndInputHandling();
            player.SetActive(false);
            player.GetComponent<DeactivateAnotherGameObjectWhenEnabled>().enabled = true;
            pd.Play();
        }
    }

    void Update()
    {
        //skip intro sequence with [esc]
        if (Input.GetKeyDown(KeyCode.Escape) && pd.state == PlayState.Playing && !SceneController.Instance.sceneChangeInProgress)
        {
            Karl_NPC_GO.SetActive(true);
            Saul_OverWorld_PC.SetActive(true);
            CauldronAudio.SetActive(true);
            pd.Stop();
        }
    }

    void OnDestroy()
    {
        pd.stopped -= StartConvoWithKarl;
    }

    void StartConvoWithKarl(PlayableDirector pd)
    {
        Karl_NPC.Interact();
        playerMovement.EnableInputHandling();
    }
}
