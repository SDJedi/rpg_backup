using System.Collections;
using System.Collections.Generic;
using DialogueSystem;
using RPG.InteractableSystem;
using UnityEngine;

public class SpawnPartyMemberAndStartDialogue : MonoBehaviour
{
    [SerializeField] List<Condition> conditionsThatMustBeTrue = null;
    [SerializeField] List<Condition> conditionsThatMustBeFalse = null;
    [SerializeField] bool triggerInStart = false;
    [SerializeField] GameObject overWorldSaul = null;
    [SerializeField] GameObject retreatPoint = null; //Saul will move towards this object to get out of trigger
    [SerializeField] GameObject puppet = null;
    [SerializeField] string dialogueNodeTag = "";
    [SerializeField] bool useConvoCam = false;

    PC_MoveOrStartInteraction mover;
    bool inProgress;
    bool puppetInPos;
    bool clearOfTrigger;


    void Start()
    {
        mover = overWorldSaul.GetComponentInChildren<PC_MoveOrStartInteraction>();

        // if (retreatPoint == null)
        // {
        //     Debug.LogError("Script requires a retreatPoint to function ... add in inspector");
        // }

        if (triggerInStart && CheckConditions())
        {
            StartCoroutine(StartMiniCinematic(overWorldSaul));
        }
    }

    void OnTriggerEnter(Collider other)
    {
        puppetInPos = false;
        clearOfTrigger = false;
        if (other.gameObject == overWorldSaul && !inProgress)
        {
            if (CheckConditions())
            {
                inProgress = true;
                StartCoroutine(StartMiniCinematic(other.gameObject));
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == overWorldSaul)
        {
            clearOfTrigger = true;
            inProgress = false;
        }
    }

    bool CheckConditions()
    {
        foreach (Condition c in conditionsThatMustBeTrue)
        {
            if (!c.Satisfied)
            {
                return false;
            }
        }

        foreach (Condition c in conditionsThatMustBeFalse)
        {
            if (c.Satisfied)
            {
                return false;
            }
        }

        return true;
    }


    IEnumerator StartMiniCinematic(GameObject overWorldPC)
    {
        mover.StopMovementAndInputHandling();
        var puppet = Instantiate(this.puppet, overWorldPC.transform.position, overWorldPC.transform.rotation);

        Animator pAnim = puppet.GetComponentInChildren<Animator>();
        pAnim.SetBool("IsMoving_Fwd", true);
        yield return MoveOverSeconds(puppet, 0.75f, 4);
        yield return RotatePuppetTowardsSaul(puppet, pAnim);
    }

    IEnumerator MoveOverSeconds(GameObject objectToMove, float seconds, float speed)
    {
        float elapsedTime = 0;
        Vector3 startingPos = objectToMove.transform.position;
        while (elapsedTime < seconds)
        {
            objectToMove.transform.Translate(Vector3.forward * Time.deltaTime * speed);
            yield return new WaitForEndOfFrame();
            elapsedTime += Time.deltaTime;
        }
        puppetInPos = true;
    }

    IEnumerator RotatePuppetTowardsSaul(GameObject puppet, Animator a)
    {
        while (!puppetInPos)
        {
            yield return null;
        }
        a.SetBool("IsMoving_Fwd", false);

        var dir = (overWorldSaul.transform.position - puppet.transform.position).normalized;
        var lookRot = Quaternion.LookRotation(dir);

        while (puppet.transform.rotation != lookRot)
        {
            puppet.transform.rotation = Quaternion.RotateTowards(puppet.transform.rotation, lookRot, 200 * Time.deltaTime);
            yield return null;
        }

        DialogueManager.Instance.StartConversation(puppet, dialogueNodeTag, false, useConvoCam);

        while (DialogueSystem.DialogueManager.Instance.InConversation)
        {
            yield return null;
        }

        puppetInPos = false;
        Animator pAnim = puppet.GetComponentInChildren<Animator>();
        pAnim.SetBool("IsMoving_Fwd", true);
        StartCoroutine(MoveOverSeconds(puppet, 0.75f, 4));
        while (!puppetInPos)
        {
            yield return null;
        }
        EndMiniCinematic(puppet);
    }

    void EndMiniCinematic(GameObject puppet)
    {
        Destroy(puppet);
        if (!triggerInStart)
        {
            StartCoroutine(MoveSaulBack());
        }
        else
        {
            mover.EnableInputHandling();
        }
    }

    IEnumerator MoveSaulBack()
    {
        if (retreatPoint != null)
        {
            while (!clearOfTrigger)
            {
                overWorldSaul.transform.position = Vector3.MoveTowards(overWorldSaul.transform.position, retreatPoint.transform.position, Time.deltaTime * 3);
                yield return new WaitForEndOfFrame();
            }

            overWorldSaul.transform.position = Vector3.MoveTowards(overWorldSaul.transform.position, retreatPoint.transform.position, Time.deltaTime * 25);
            yield return new WaitForSeconds(0.5f);
        }

        mover.EnableInputHandling();
    }
}
