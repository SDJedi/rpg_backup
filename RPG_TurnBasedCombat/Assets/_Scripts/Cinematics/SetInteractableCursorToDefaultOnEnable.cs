using RPG.InteractableSystem;
using UnityEngine;

public class SetInteractableCursorToDefaultOnEnable : MonoBehaviour
{
    [SerializeField] Interactable interactable = null;


    void OnEnable()
    {
        interactable.cursorInfo.cTexture = null;
    }
}
