using System.Collections.Generic;
using UnityEngine;
using RPG.CombatSystem;
using RPG.PartySelectSystem;
using RPG.Inventory;
using RPG.ProgressionSystem;
using System;

public class SetPCLevelsAndXP : MonoBehaviour
{
    struct NameAndSlotPos
    {
        public string name;
        public int slotPos;

        public NameAndSlotPos(string n, int p)
        {
            name = n;
            slotPos = p;
        }
    }

    PC_SkillTree SkillTree_Saul;
    PC_SkillTree SkillTree_Karl;
    PC_SkillTree SkillTree_Babz;
    PC_SkillTree SkillTree_Lance;
    PC_SkillTree SkillTree_Billy;
    PC_SkillTree SkillTree_Jimbo;
    PC_SkillTree SkillTree_Ed;
    PC_SkillTree SkillTree_SirJohn;
    PC_SkillTree SkillTree_Lisa;

    public int CurrentActiveLevel { get; private set; }


    void Start()
    {
        SetAllPCsToAvailable();
        SetPCSkillTrees();
        CharacterPoints_Tracker.Instance.ModifyCharacterPoints(-CharacterPoints_Tracker.Instance.CurrentCP);

        Inventory_UI.Instance.OpenInvWindow();
        //first time we open scene, initialize to level 1
        if (!SkillTree_Saul.Level_1_Purchasables[0].IsPurchased)
        {
            SetPCsToDesiredLevel(1);
        }
    }

    void SetAllPCsToAvailable()
    {
        foreach (SelectablePC PC in PCManager.Instance.AcquiredTeamMembers)
        {
            PC.IsAvailable = true;
        }
    }

    void SetPCSkillTrees()
    {
        //Skill trees are in the BootScene under PCManager
        SkillTree_Saul = GameObject.Find("SkillTree_Saul").GetComponent<PC_SkillTree>();
        SkillTree_Karl = GameObject.Find("SkillTree_Karl").GetComponent<PC_SkillTree>();
        SkillTree_Babz = GameObject.Find("SkillTree_Babz").GetComponent<PC_SkillTree>();
        SkillTree_Lance = GameObject.Find("SkillTree_Lance").GetComponent<PC_SkillTree>();
        SkillTree_Billy = GameObject.Find("SkillTree_Billy").GetComponent<PC_SkillTree>();
        SkillTree_Jimbo = GameObject.Find("SkillTree_Jimbo").GetComponent<PC_SkillTree>();
        SkillTree_Ed = GameObject.Find("SkillTree_Ed").GetComponent<PC_SkillTree>();
        SkillTree_SirJohn = GameObject.Find("SkillTree_SirJohn").GetComponent<PC_SkillTree>();
        SkillTree_Lisa = GameObject.Find("SkillTree_Lisa").GetComponent<PC_SkillTree>();
    }

    //called by UI buttons
    public void SetPCsToDesiredLevel(int desiredLvl)
    {
        CurrentActiveLevel = desiredLvl;

        RemoveAllEquipment();
        ResetPurchasableAbilities();
        ResetProcs();
        ResetPCsToLevel1();

        RaisePCsToDesiredLvl(desiredLvl);
        ActivateAllLevelAppropriateAbilities();

        Inventory_UI.Instance.RefreshInvWindowIfOpen();
    }

    void RemoveAllEquipment()
    {
        foreach (PC_Stats_Config PC in PCManager.Instance.PC_StatConfigs)
        {
            InventoryManager.Instance.UnEquipAllItemsOnPC(PC);
        }
    }

    void ResetPurchasableAbilities()
    {
        FindObjectOfType<InitializeAllPurchasables>().ResetAndInitializePurchasables();
    }

    void ResetProcs()
    {
        PC_ProcManager.Instance.ResetPCProcContainers();
    }

    void ResetPCsToLevel1()
    {
        //note the names and slot positions of PCs
        List<NameAndSlotPos> namesAndPosOfPCs = new List<NameAndSlotPos>();

        for (int i = 0; i < PCManager.Instance.PC_StatConfigs.Count; i++)
        {
            string name = PCManager.Instance.PC_StatConfigs[i].CharName;
            namesAndPosOfPCs.Add(new NameAndSlotPos(name, PCManager.Instance.GetStatsForPC(name).PC_Slot_position));
        }

        //start with a clean slate...
        for (int i = PCManager.Instance.PC_StatConfigs.Count - 1; i >= 0; i--)
        {
            Destroy(PCManager.Instance.PC_StatConfigs[i]);//this is a cloned object
        }
        PCManager.Instance.PC_StatConfigs.Clear();

        //instantiate new configs from the originals in _ScriptableObject Assets -> Configs -> Character Configs -> PC_Stat_Configs
        for (int i = 0; i < PCManager.Instance.AcquiredTeamMembers.Count; i++)
        {
            PCManager.Instance.GetInstantiatedStatsConfigForPC(PCManager.Instance.AcquiredTeamMembers[i].PC_Prefab);
        }

        //update the newly instantiated configs with the correct slot position info
        foreach (PC_Stats_Config cnfg in PCManager.Instance.PC_StatConfigs)
        {
            foreach (NameAndSlotPos np in namesAndPosOfPCs)
            {
                if (cnfg.CharName == np.name)
                {
                    cnfg.PC_Slot_position = np.slotPos;
                    break;
                }
            }
        }
    }

    void RaisePCsToDesiredLvl(int desiredLvl)
    {
        foreach (PC_Stats_Config config in PCManager.Instance.PC_StatConfigs)
        {
            config.ResetCurrentXP();
            config.ModifyCurrentXP(XPCalculator.XPCHart[desiredLvl - 1]); //0 based index

            for (int i = desiredLvl; i > 1; i--)
            {
                config.IncreasePCLvl(false);//stats increased with each lvl up
            }
        }
    }

    void ActivateAllLevelAppropriateAbilities()
    {
        foreach (PC_Stats_Config config in PCManager.Instance.PC_StatConfigs)
        {
            switch (config.CharName)
            {
                case "Saul":
                    ActivatePCAbilities(SkillTree_Saul);
                    break;
                case "Karl":
                    ActivatePCAbilities(SkillTree_Karl);
                    break;
                case "Babz":
                    ActivatePCAbilities(SkillTree_Babz);
                    break;
                case "Lance":
                    ActivatePCAbilities(SkillTree_Lance);
                    break;
                case "Billy":
                    ActivatePCAbilities(SkillTree_Billy);
                    break;
                case "Jimbo":
                    ActivatePCAbilities(SkillTree_Jimbo);
                    break;
                case "Ed":
                    ActivatePCAbilities(SkillTree_Ed);
                    break;
                case "Sir John":
                    ActivatePCAbilities(SkillTree_SirJohn);
                    break;
                case "Lisa":
                    ActivatePCAbilities(SkillTree_Lisa);
                    break;
            }
        }
    }

    void ActivatePCAbilities(PC_SkillTree skillTree)
    {
        for (int i = 1; i <= CurrentActiveLevel; i++)
        {
            switch (i)
            {
                case 1:
                    foreach (CP_Purchasable p in skillTree.Level_1_Purchasables)
                    {
                        p.CompletePurchase(true);
                    }
                    break;
                case 2:
                    foreach (CP_Purchasable p in skillTree.Level_2_Purchasables)
                    {
                        p.CompletePurchase(true);
                    }
                    break;
                case 3:
                    foreach (CP_Purchasable p in skillTree.Level_3_Purchasables)
                    {
                        p.CompletePurchase(true);
                    }
                    break;
                case 4:
                    foreach (CP_Purchasable p in skillTree.Level_4_Purchasables)
                    {
                        p.CompletePurchase(true);
                    }
                    break;
                case 5:
                    foreach (CP_Purchasable p in skillTree.Level_5_Purchasables)
                    {
                        p.CompletePurchase(true);
                    }
                    break;
                case 6:
                    foreach (CP_Purchasable p in skillTree.Level_6_Purchasables)
                    {
                        p.CompletePurchase(true);
                    }
                    break;
                case 7:
                    foreach (CP_Purchasable p in skillTree.Level_7_Purchasables)
                    {
                        p.CompletePurchase(true);
                    }
                    break;
                case 8:
                    foreach (CP_Purchasable p in skillTree.Level_8_Purchasables)
                    {
                        p.CompletePurchase(true);
                    }
                    break;
                case 9:
                    foreach (CP_Purchasable p in skillTree.Level_9_Purchasables)
                    {
                        p.CompletePurchase(true);
                    }
                    break;
                case 10:
                    foreach (CP_Purchasable p in skillTree.Level_10_Purchasables)
                    {
                        p.CompletePurchase(true);
                    }
                    break;
                case 11:
                    foreach (CP_Purchasable p in skillTree.Level_11_Purchasables)
                    {
                        p.CompletePurchase(true);
                    }
                    break;
                case 12:
                    foreach (CP_Purchasable p in skillTree.Level_12_Purchasables)
                    {
                        p.CompletePurchase(true);
                    }
                    break;
                case 13:
                    foreach (CP_Purchasable p in skillTree.Level_13_Purchasables)
                    {
                        p.CompletePurchase(true);
                    }
                    break;
                case 14:
                    foreach (CP_Purchasable p in skillTree.Level_14_Purchasables)
                    {
                        p.CompletePurchase(true);
                    }
                    break;
                case 15:
                    foreach (CP_Purchasable p in skillTree.Level_15_Purchasables)
                    {
                        p.CompletePurchase(true);
                    }
                    break;
                case 16:
                    foreach (CP_Purchasable p in skillTree.Level_16_Purchasables)
                    {
                        p.CompletePurchase(true);
                    }
                    break;
                case 17:
                    foreach (CP_Purchasable p in skillTree.Level_17_Purchasables)
                    {
                        p.CompletePurchase(true);
                    }
                    break;
                case 18:
                    foreach (CP_Purchasable p in skillTree.Level_18_Purchasables)
                    {
                        p.CompletePurchase(true);
                    }
                    break;
                case 19:
                    foreach (CP_Purchasable p in skillTree.Level_19_Purchasables)
                    {
                        p.CompletePurchase(true);
                    }
                    break;
                case 20:
                    foreach (CP_Purchasable p in skillTree.Level_20_Purchasables)
                    {
                        p.CompletePurchase(true);
                    }
                    break;
            }
        }
    }
}