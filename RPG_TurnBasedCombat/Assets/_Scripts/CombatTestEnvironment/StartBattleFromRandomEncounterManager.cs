using RPG.CombatSystem;
using UnityEngine;

public class StartBattleFromRandomEncounterManager : MonoBehaviour
{
    public void StartBattle()
    {
        if (RandomEncounterManager.Instance.possibleEncounters.Count > 0)
        {
            RandomEncounterManager.Instance.LoadCombatScene();
        }
    }
}