using UnityEngine;

//Last Reviewed: 9/5/19
namespace RPG.SceneControl
{
    public class LoadAScene : MonoBehaviour
    {
        public string nameOfSpawnPoint;
        public string SceneName;

        //can be attached to and called by a button to load a specific scene
        //e.g. in QuestTestScene, it is used to leave that area
        public void LoadScene()
        {
            Debug.Log(nameOfSpawnPoint + " " + SceneName);
            SceneController.Instance.NameOfMostRecentSpawnPoint = nameOfSpawnPoint;
            SceneController.Instance.FadeAndLoadScene(SceneName);
        }
    }
}