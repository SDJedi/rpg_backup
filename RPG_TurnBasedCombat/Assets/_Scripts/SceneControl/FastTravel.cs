using System.Collections;
using System.Collections.Generic;
using DialogueSystem;
using RPG.CombatSystem;
using RPG.InteractableSystem;
using RPG.Inventory;
using RPG.QuestSystem;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

//Last Reviewed: 1/21/20
namespace RPG.SceneControl
{
    //attached to SceneController in BootScene.
    //move the OverWorldPC to a specific scene / spawn point
    public class FastTravel : Singleton<FastTravel>
    {
        [SerializeField] MyDropdown dropdown = null; //dropdown menu in BootScene_Canvas
        [SerializeField] GameObject controlPanel = null; //the ui panel that contains the main buttons (i.e. Character Progression, Quest Journal, Inventory, etc)
        [SerializeField] AvailableDestination[] availableDestinations = new AvailableDestination[0]; //set these in inspector

        bool positioningPC; //flag is set to true while moving the OverWorldPC within the same scene

        public bool fastTravelAllowed { get; private set; } = true;


        void Start()
        {
            dropdown.onOptionCicked += (i => TeleportOverWorldPC(i));
            //events that close the dropdown
            Inventory_UI.Instance.InvWindowJustOpened += HideFastTravelDropdown;
            QuestJournal.Instance.QuestJournalJustOpened += HideFastTravelDropdown;
            SceneController.Instance.SceneChangeInitiated += HideFastTravelDropdown;
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                ToggleFastTravelDropdown();
            }
        }

        //unsubscribe from events
        protected override void OnDestroy()
        {
            base.OnDestroy();

            dropdown.onOptionCicked -= (i => TeleportOverWorldPC(i));

            if (Inventory_UI.InstanceExists)
            {
                Inventory_UI.Instance.InvWindowJustOpened -= HideFastTravelDropdown;
            }
            if (QuestJournal.InstanceExists)
            {
                QuestJournal.Instance.QuestJournalJustOpened -= HideFastTravelDropdown;
            }
            if (SceneController.InstanceExists)
            {
                SceneController.Instance.SceneChangeInitiated -= HideFastTravelDropdown;
            }
        }

        //called by FastTravel_Btn under ControlPanel
        public void ToggleFastTravelDropdown()
        {
            if (controlPanel.activeInHierarchy && !DialogueManager.Instance.InConversation && !SceneController.Instance.sceneChangeInProgress)
            {
                dropdown.gameObject.SetActive(!dropdown.gameObject.activeSelf);
                PopulateFastTravelDropdown();
                dropdown.Show();
                Destroy(GameObject.Find("Blocker")); //"Blocker" prevents clicking on other UI elements
            }
        }

        public void HideFastTravelDropdown()
        {
            dropdown.gameObject.SetActive(false);
            Destroy(GameObject.Find("Blocker")); //"Blocker" prevents clicking on other UI elements
        }

        //show options in the dropdown based on currently available destinations.
        //destination availability is based on conditions in AllGameSwitches
        void PopulateFastTravelDropdown()
        {
            dropdown.ClearOptions();

            List<string> destNames = new List<string>();
            //add all options that have been unlocked (i.e. IsAvailable == true)
            foreach (AvailableDestination dest in availableDestinations)
            {
                if (dest.IsAvailable)
                {
                    destNames.Add(dest.Destination.Label);
                }
            }
            destNames.Sort();
            dropdown.AddOptions(destNames);
        }

        //called on dropdown option clicked
        void TeleportOverWorldPC(int index)
        {
            //find the correct destination based on the Label...
            AvailableDestination ad = null;
            for (int i = 0; i < availableDestinations.Length; i++)
            {
                if (dropdown.options[index].text == availableDestinations[i].Destination.Label)
                {
                    ad = availableDestinations[i];
                    break;
                }
            }

            //TODO: show confirmation window first? Maybe.

            //move the OverWorldPC if allowed
            if (!FastTravelAllowed())
            {
                Debug.Log("no fast travel allowed");
                return;
            }
            SendOverWorldPCToSpawnPoint(ad);
        }

        //to make fast travel unavailable in any scene, add a gameobject withe the "NoFastTravel" tag
        bool FastTravelAllowed()
        {
            return GameObject.FindWithTag("NoFastTravel") == null && fastTravelAllowed;
        }

        void SendOverWorldPCToSpawnPoint(AvailableDestination availDest)
        {
            /////////////////  Error checking  /////////////////
            if (availDest == null)
            {
                Debug.LogWarning("Invalid destination.");
                return;
            }

            var OverWorldPC = GameObject.FindGameObjectWithTag("OverWorldPC");
            if (OverWorldPC == null)
            {
                Debug.LogWarning("No OverWorldPC to move.");
                return;
            }

            if (SceneController.Instance.sceneChangeInProgress)
            {
                Debug.LogWarning("Can't move OverWorldPC while scene change in progress.");
                return;
            }
            //////////////  End Error checking  //////////////////

            //different methods are used for moving within same scene and moving to a different scene
            if (SceneManager.GetActiveScene().name == availDest.Destination.SceneName)
            {
                if (!positioningPC)
                {
                    RelocateOverWorldPC_WithinCurrentScene(availDest.Destination.NameOfSpawnPoint);
                }
            }
            else
            {
                RelocateOverWorldPC_ToNewScene(availDest.Destination.SceneName, availDest.Destination.NameOfSpawnPoint);
            }

            //enable/disable random encounters after teleport
            RandomEncounterManager.Instance.RandomEncountersDisabled = availDest.Destination.DisableRandomEncountersAfterTravel;
        }

        #region Method Details
        void RelocateOverWorldPC_WithinCurrentScene(string spName)
        {
            var sp = FindSpawnPoint(spName);
            if (sp != null)
            {
                StartCoroutine(RelocateWithinCurrentScene(sp.transform.position));
            }
        }

        PCSpawnPoint FindSpawnPoint(string spName)
        {
            PCSpawnPoint[] SPs = FindObjectsOfType<PCSpawnPoint>();
            for (int i = 0; i < SPs.Length; i++)
            {
                if (spName == SPs[i].NameOfSpawnPoint)
                {
                    return SPs[i];
                }
            }
            Debug.LogWarning("No SpawnPoint named " + spName + " exists");
            return null;
        }

        IEnumerator RelocateWithinCurrentScene(Vector3 newPos)
        {
            HideFastTravelDropdown();
            positioningPC = true;
            var OverWorldPC = GameObject.FindGameObjectWithTag("OverWorldPC");
            var nav = OverWorldPC.GetComponent<NavMeshAgent>();
            var movement = OverWorldPC.GetComponent<PC_MoveOrStartInteraction>();

            movement.StopMovementAndInputHandling();
            //start fading out
            SceneController.Instance.FadeOutAndBackInAfterXSeconds(2);
            yield return new WaitForSeconds(1.5f);
            //important to set this, or the pc warps back to original pos when nav agent is reenabled
            movement.OverrideDestinationPos(newPos);
            //move the OverWorldPC
            nav.enabled = false;
            OverWorldPC.transform.position = newPos;
            nav.enabled = true;
            while (SceneController.Instance.isFading)
            {
                yield return new WaitForEndOfFrame();
            }
            OverWorldPC.GetComponent<PC_MoveOrStartInteraction>().EnableInputHandling();
            positioningPC = false;
        }

        void RelocateOverWorldPC_ToNewScene(string SceneName, string nameOfSpawnPoint)
        {
            SceneController.Instance.NameOfMostRecentSpawnPoint = nameOfSpawnPoint;
            SceneController.Instance.FadeAndLoadScene(SceneName);
        }
        #endregion

        #region Helper Classes
        [System.Serializable]
        public class FastTravelDestination
        {
            [Header("\"Label\" is what shows up in the FastTravel dropdown")]
            [SerializeField] string label = "";
            [SerializeField] string sceneName = "";
            [SerializeField] string nameOfSpawnPoint = "";
            [SerializeField] bool disableRandomEncountersAfterTravel = false;

            public string Label { get { return label; } }
            public string SceneName { get { return sceneName; } }
            public string NameOfSpawnPoint { get { return nameOfSpawnPoint; } }
            public bool DisableRandomEncountersAfterTravel { get { return disableRandomEncountersAfterTravel; } }
        }

        [System.Serializable]
        public class AvailableDestination
        {
            [Header("Condition that must be true for the option to show in the dropdown")]
            [SerializeField] Condition fastTravelCondition = null;
            [SerializeField] FastTravelDestination destination = null;

            public bool IsAvailable { get { return fastTravelCondition.Satisfied; } }
            public FastTravelDestination Destination { get { return destination; } }
        }
        #endregion
    }
}
