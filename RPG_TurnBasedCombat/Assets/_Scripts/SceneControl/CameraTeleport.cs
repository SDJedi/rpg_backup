using System.Collections;
using Cinemachine;
using UnityEngine;


public class CameraTeleport : MonoBehaviour
{
    CinemachineVirtualCamera vCam = null;


    void Awake()
    {
        vCam = GetComponent<CinemachineVirtualCamera>();
    }

    public void PlaceCamera()
    {
        StartCoroutine(DisableThenReenableCamera());
    }

    //prevents camera from lerping to player and instead insta-positions it on follow target
    IEnumerator DisableThenReenableCamera()
    {
        vCam.enabled = false;
        yield return null;
        vCam.enabled = true;
    }
}
