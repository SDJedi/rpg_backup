using System;
using System.Collections;
using DialogueSystem;
using RPG.Inventory;
using RPG.QuestSystem;
using RPG.Saving;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
//Last Reviewed: 9/5/19

// This script exists in the Persistent scene and manages the content
// based scene's loading.  It works on a principle that the
// Persistent scene will be loaded first, then it loads the scenes that
// contain the player and other visual elements when they are needed.
// At the same time it will unload the scenes that are not needed when
// the player leaves them.
namespace RPG.SceneControl
{
    //match the names of the scenes exactly
    public enum StartSceneOptions
    {
        AncientRuins,
        KarlsHut,
        KitsonForest,
        OrcFortress,
        OverWorld_0,
        OverWorld_1,
        OverWorld_2,
        OverWorldSceneTest,
        QuestTestScene,
        CombatTestEnvironment
    }

    public class SceneController : Singleton<SceneController>
    {
        public event Action LeavingCombatScene; // Event delegate that is called just before the combat scene is unloaded.
        public event Action SceneChangeInitiated; // Event delegate that is called as soon as FadeAndLoadScene() is called.
        public event Action BeforeSceneUnload; // Event delegate that is called just before a scene is unloaded.
        public event Action AfterSceneLoad; // Event delegate that is called just after a scene is loaded.

        public CanvasGroup faderCanvasGroup; // The CanvasGroup that controls the Image used for fading to black.
        public float fadeDuration = 0.5f; // How long it should take to fade to and from black.
        public StartSceneOptions StartScene;// The name of the scene that should be loaded when "New Game" is pressed.

        [SerializeField] GameObject ButtonControlPanel = null;

        public bool isFading { get; private set; } // Flag used to determine if the Image is currently fading to or from black.
        public bool sceneChangeInProgress { get; private set; }
        public string nameOfSceneWeAreSwitchingTo { get; private set; }
        public string nameOfMostRecentlyUnloadedScene { get; private set; }
        public string NameOfMostRecentSpawnPoint { get; set; }


        void Start()
        {
            if (SceneManager.GetActiveScene().name != "MainMenu")
            {
                StartCoroutine(LoadSceneAndSetActive("MainMenu", false));
                SoundManager.Instance.StartMusic("MainMenuMusic", 0.2f, false);
            }
        }

        void Update()
        {
            //Open CharacterSelectScene with "P"
            if (Input.GetKeyDown(KeyCode.P))
            {
                LoadCharacterSelectScene();
            }

            //Open CharacterPointsScene with "C"
            if (Input.GetKeyDown(KeyCode.C))
            {
                LoadCharacterPointsScene();
            }
        }

        //transition into the CombatScene
        public void LoadCombatScene(bool playBattleStartSFX = true)
        {
            StartCoroutine(TransitionToCombatScene(playBattleStartSFX));
        }

        IEnumerator TransitionToCombatScene(bool playBattleStartSFX)
        {
            sceneChangeInProgress = true;
            nameOfSceneWeAreSwitchingTo = "CombatScene";
            nameOfMostRecentlyUnloadedScene = SceneManager.GetActiveScene().name;

            // If this event has any subscribers, call it.
            if (SceneChangeInitiated != null)
            {
                SceneChangeInitiated();
            }

            // If this event has any subscribers, call it.
            if (BeforeSceneUnload != null)
            {
                BeforeSceneUnload();
            }

            //save state...
            SavingWrapper saveWrapper = GameObject.FindObjectOfType<SavingWrapper>();
            saveWrapper.RuntimeSave();

            //play battlestart sound
            if (playBattleStartSFX)
            {
                SoundManager.Instance.FadeOutCurrentMusic();
                SoundManager.Instance.PlayBattleStart();
            }

            //camera movement
            if (GameObject.FindObjectOfType<CameraZoomControls>() != null)
            {
                StartCoroutine(GameObject.FindObjectOfType<CameraZoomControls>().StartBattle_CamInAndOut());
            }

            //fade out
            yield return Fade(1);

            //unload current scene
            yield return SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().buildIndex);

            //load CombatScene
            yield return LoadSceneAndSetActive("CombatScene");

            sceneChangeInProgress = false;

            // If this event has any subscribers, call it.
            if (AfterSceneLoad != null)
            {
                AfterSceneLoad();
            }

            //fade in
            yield return Fade(0f);
        }

        // This is the main external point of contact and influence from the rest of the project.
        // This will be called by a SceneReaction when the player wants to switch scenes.
        public void FadeAndLoadScene(string sceneName, bool runtimeSave = true)
        {
            //print("fade and load. runtimeSave? " + runtimeSave); //Note: One instance in which we do NOT want to save when switching scenes
            //is when we are switching FROM the main menu
            nameOfSceneWeAreSwitchingTo = sceneName;
            if (SceneChangeInitiated != null)
            {
                SceneChangeInitiated();
            }

            // If a fade isn't happening then start fading and switching scenes.
            if (!isFading)
            {
                sceneChangeInProgress = true;
                if (SceneManager.GetActiveScene().name == "CombatScene" && LeavingCombatScene != null)
                {
                    LeavingCombatScene();
                }

                StartCoroutine(FadeAndSwitchScenes(sceneName, runtimeSave));
            }
        }

        IEnumerator FadeAndSwitchScenes(string sceneName, bool runtimeSave = true)
        {
            // Start fading to black and wait for it to finish before continuing.
            yield return Fade(1f);

            // If this event has any subscribers, call it.
            if (BeforeSceneUnload != null)
            {
                BeforeSceneUnload();
            }

            //save state...
            SavingWrapper saveWrapper = GameObject.FindObjectOfType<SavingWrapper>();
            if (runtimeSave)
            {
                saveWrapper.RuntimeSave();
            }

            // Unload the current active scene.
            nameOfMostRecentlyUnloadedScene = SceneManager.GetActiveScene().name;
            yield return SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().buildIndex);

            // Start loading the given scene and wait for it to finish.
            yield return LoadSceneAndSetActive(sceneName);

            //print("new scene is loaded, all saved data should be current...");
            MoveOverWorldPCToSpawnPointIfNeededAndSave();

            //save state
            if (runtimeSave && SceneManager.GetActiveScene().name != "MainMenu")
            {
                saveWrapper.RuntimeSave();
            }

            sceneChangeInProgress = false;

            // If this event has any subscribers, call it.
            if (AfterSceneLoad != null)
            {
                //print("AfterSceneLoad ...");
                AfterSceneLoad();
            }

            // Start fading back in and wait for it to finish before exiting the function.
            yield return Fade(0f);
        }

        IEnumerator LoadSceneAndSetActive(string sceneName, bool loadSavedData = true)
        {
            //print("start LoadSceneAndSetActive()");
            nameOfSceneWeAreSwitchingTo = sceneName;
            // Allow the given scene to load over several frames and add it to the already loaded scenes (just the Persistent scene at this point).
            yield return SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);

            // Find the scene that was most recently loaded (the one at the last index of the loaded scenes).
            Scene newlyLoadedScene = SceneManager.GetSceneAt(SceneManager.sceneCount - 1);

            // Set the newly loaded scene as the active scene (this marks it as the one to be unloaded next).
            SceneManager.SetActiveScene(newlyLoadedScene);

            if (loadSavedData)
            {
                //load data for the newly loaded scene from the CSF file
                var saveWrap = GameObject.FindObjectOfType<SavingWrapper>();
                saveWrap.LoadRuntimeSave();
                //yield return new WaitForSeconds(0.1f);
                while (GameObject.FindObjectOfType<SavingSystem>().Loading)
                {
                    print("save data still loading... do we ever reach here?");
                    yield return null;
                }
            }
            //print("end LoadSceneAndSetActive()");
        }

        IEnumerator Fade(float finalAlpha)
        {
            float fadeInDelay = 0.5f;
            // Set the fading flag to true so the FadeAndSwitchScenes coroutine won't be called again.
            isFading = true;

            // Make sure the CanvasGroup blocks raycasts into the scene so no more input can be accepted.
            faderCanvasGroup.blocksRaycasts = true;

            // Calculate how fast the CanvasGroup should fade based on it's current alpha, it's final alpha and how long it has to change between the two.
            float fadeSpeed = Mathf.Abs(faderCanvasGroup.alpha - finalAlpha) / fadeDuration;

            if (finalAlpha == 0)
            {
                while (fadeInDelay > 0)
                {
                    fadeInDelay -= Time.deltaTime;
                    yield return new WaitForEndOfFrame();
                }
            }

            // While the CanvasGroup hasn't reached the final alpha yet...
            while (!Mathf.Approximately(faderCanvasGroup.alpha, finalAlpha))
            {
                // ... move the alpha towards it's target alpha.
                faderCanvasGroup.alpha = Mathf.MoveTowards(faderCanvasGroup.alpha, finalAlpha, fadeSpeed * Time.deltaTime);

                // Wait for a frame then continue.
                yield return null;
            }

            // Set the flag to false since the fade has finished.
            isFading = false;

            // Stop the CanvasGroup from blocking raycasts so input is no longer ignored.
            faderCanvasGroup.blocksRaycasts = false;
        }

        void MoveOverWorldPCToSpawnPointIfNeededAndSave()
        {
            var PC = GameObject.FindGameObjectWithTag("OverWorldPC");

            if (PC != null)
            {
                var pm = PC.GetComponent<PC_MoveOrStartInteraction>();
                var nav = PC.GetComponent<NavMeshAgent>();
                PCSpawnPoint[] SPs = FindObjectsOfType<PCSpawnPoint>();
                for (int i = 0; i < SPs.Length; i++)
                {
                    if (NameOfMostRecentSpawnPoint == SPs[i].NameOfSpawnPoint)
                    {
                        pm.DisableInputHandling();
                        pm.OverrideDestinationPos(SPs[i].gameObject.transform.position);
                        nav.enabled = false;
                        PC.transform.position = SPs[i].gameObject.transform.position;
                        PC.transform.rotation = SPs[i].gameObject.gameObject.transform.rotation;
                        NameOfMostRecentSpawnPoint = "";
                        nav.enabled = true;
                        nav.Warp(pm.DestinationPosition);
                        pm.EnableInputHandling();
                        PC.GetComponent<Collider>().enabled = true;
                        CameraTeleport ct = GameObject.FindObjectOfType<CameraTeleport>();

                        if (ct != null)
                        {
                            ct.PlaceCamera();
                        }

                        break;
                    }
                }

                SavingWrapper saveWrapper = GameObject.FindObjectOfType<SavingWrapper>();
                saveWrapper.SaveOneGameObject(PC);
            }
        }

        public void GameOver_LoadMainMenu(float delay)
        {
            Inventory_UI.Instance.CloseInvWindow();
            QuestJournal.Instance.CloseQuestJournal();
            StartCoroutine(LoadMainMenuScene(delay, false));
        }

        //for Inn
        public void FadeOutAndBackInAfterXSeconds(float seconds)
        {
            StartCoroutine(FadeOutAndIn(seconds));
        }

        IEnumerator FadeOutAndIn(float seconds)
        {
            var PC = GameObject.FindGameObjectWithTag("OverWorldPC");
            PC_MoveOrStartInteraction pm = null;
            if (PC != null)
            {
                pm = PC.GetComponent<PC_MoveOrStartInteraction>();
                pm.DisableInputHandling();
            }
            yield return Fade(1);
            isFading = true;
            yield return new WaitForSeconds(seconds);
            yield return Fade(0);
            if (pm != null)
            {
                pm.EnableInputHandling();
            }
        }

        //for MainMenu Button
        public void LoadMainMenu(float delay)
        {
            if (DialogueManager.Instance.InConversation || sceneChangeInProgress) return;

            Inventory_UI.Instance.CloseInvWindow();
            QuestJournal.Instance.CloseQuestJournal();
            FastTravel.Instance.HideFastTravelDropdown();
            StartCoroutine(LoadMainMenuScene(delay));
        }

        IEnumerator LoadMainMenuScene(float delay, bool autoSave = true)
        {
            yield return new WaitForSeconds(delay);
            Resources.UnloadUnusedAssets();
            FadeAndLoadScene("MainMenu", autoSave);
        }

        public void LoadCharacterSelectScene()
        {
            if (OkToOpenCharacterSelectAndProgressionScenes())
            {
                Inventory_UI.Instance.CloseInvWindow();
                QuestJournal.Instance.CloseQuestJournal();
                FastTravel.Instance.HideFastTravelDropdown();
                FadeAndLoadScene("CharacterSelectScene");
            }
        }

        public void LoadCharacterPointsScene()
        {
            if (OkToOpenCharacterSelectAndProgressionScenes()) //TODO: need to make one of these for the PointScreen
            {
                Inventory_UI.Instance.CloseInvWindow();
                QuestJournal.Instance.CloseQuestJournal();
                FastTravel.Instance.HideFastTravelDropdown();
                FadeAndLoadScene("CharacterPointsScene");
            }
        }

        bool OkToOpenCharacterSelectAndProgressionScenes()
        {
            return (!isFading &&
                !sceneChangeInProgress &&
                SceneManager.GetActiveScene().name != "CombatScene" &&
                SceneManager.GetActiveScene().name != "MainMenu" &&
                ButtonControlPanel.activeSelf && //if the button control panel is hidden, no open window
                !DialogueManager.Instance.InConversation &&
                !ItemShopManager.Instance.IsShopping);
        }
    }
}