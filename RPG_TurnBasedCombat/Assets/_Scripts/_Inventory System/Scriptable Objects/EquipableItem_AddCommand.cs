using RPG.CombatSystem;
using UnityEngine;

//Last Reviewed: 1/29/20
namespace RPG.Inventory
{
    //create a piece of equipment that, when equipped, adds a new ability to a PC
    [CreateAssetMenu(menuName = "Item / Equipment / AddCommand")]
    public class EquipableItem_AddCommand : EquipableItem_ModifyStats
    {
        public BaseCommand_Config Config;
    }
}
