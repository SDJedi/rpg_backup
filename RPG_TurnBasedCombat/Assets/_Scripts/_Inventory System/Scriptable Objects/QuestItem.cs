﻿using UnityEngine;

namespace RPG.Inventory
{
    //create a new quest item (shows in inventory, but can't be sold or equipped)
    [CreateAssetMenu(menuName = "Item / Quest Item")]
    public class QuestItem : Item
    {
        public override bool IsQuestItem { get { return true; } }
        public override ItemRarity Rarity { get { return ItemRarity.Quest; } }
    }
}
