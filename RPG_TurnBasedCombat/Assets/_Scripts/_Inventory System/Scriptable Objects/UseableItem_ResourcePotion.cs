﻿using System.Collections;
using System.Collections.Generic;
using RPG.CombatSystem;
using UnityEngine;
using UnityEngine.SceneManagement;

//Last Reviewed: 3/2/20
namespace RPG.Inventory
{
    //create an item that can restore HP, MP, and/or TP to one or more PCs
    [CreateAssetMenu(menuName = "Item / Resource Potion")]
    public class UseableItem_ResourcePotion : UseableItem
    {
        [Header("HP")]
        [SerializeField] int minHP = 0;
        [SerializeField] int maxHP = 0;

        [Header("MP")]
        [SerializeField] int minMP = 0;
        [SerializeField] int maxMP = 0;

        [Header("TP")]
        [SerializeField] int minTP = 0;
        [SerializeField] int maxTP = 0;


        public override void UseItem(Base_Stats_Config caster, List<Base_Stats_Config> targets)
        {
            if (OnUseSoundEffect != null && SceneManager.GetActiveScene().name != "CombatScene")
            {
                SoundManager.Instance.PlayAudioClip(OnUseSoundEffect);
            }

            SetMaxGreaterThanOrEqualToMin();
            InventoryManager.Instance.StartCoroutine(UseItemCoroutine(caster, targets));
        }

        void SetMaxGreaterThanOrEqualToMin()
        {
            if (maxHP < minHP)
            {
                maxHP = minHP;
            }

            if (maxMP < minMP)
            {
                maxMP = minMP;
            }

            if (maxTP < minTP)
            {
                maxTP = minTP;
            }
        }

        IEnumerator UseItemCoroutine(Base_Stats_Config caster, List<Base_Stats_Config> targets)
        {
            foreach (Base_Stats_Config stats in targets)
            {
                //only heal living targets...
                if (stats.IsAlive())
                {
                    //roll for heal amounts
                    int HPAmnt = UnityEngine.Random.Range(minHP, maxHP + 1);
                    int MPAmnt = UnityEngine.Random.Range(minMP, maxMP + 1);
                    int TPAmnt = UnityEngine.Random.Range(minTP, maxTP + 1);

                    //cap amounts to max values
                    if (stats.CurHP + HPAmnt > stats.MaxHP)
                    {
                        HPAmnt = (stats.MaxHP - stats.CurHP);
                    }
                    if (stats.CurMP + MPAmnt > stats.MaxMP)
                    {
                        MPAmnt = (stats.MaxMP - stats.CurMP);
                    }
                    if (stats.CurTP + TPAmnt > stats.MaxTP)
                    {
                        TPAmnt = (stats.MaxTP - stats.CurTP);
                    }

                    //heal HP
                    stats.ModifyCurrentHP(HPAmnt);

                    //heal MP
                    if (stats.MaxMP > 0)
                    {
                        stats.ModifyCurrentMP(MPAmnt);
                    }
                    else
                    {
                        MPAmnt = 0;
                    }

                    //heal TP
                    if (stats.MaxTP > 0)
                    {
                        stats.ModifyCurrentTP(TPAmnt);
                    }
                    else
                    {
                        TPAmnt = 0;
                    }

                    //if in combat...
                    if (SceneManager.GetActiveScene().name == "CombatScene")
                    {
                        yield return CreateTextAndLogMessages(stats, HPAmnt, MPAmnt, TPAmnt);
                    }
                }
            }
        }
        //show scrolling text of heal amounts
        IEnumerator CreateTextAndLogMessages(Base_Stats_Config stats, int HPAmnt, int MPAmnt, int TPAmnt)
        {
            if (HPAmnt > 0)
            {
                GenerateSpecialScrollingText.GenerateScrollingText(PCManager.Instance.GetPCGameObjectFromStatsConfig(stats).transform, ((int)HPAmnt).ToString(), Color.green);
                CombatLogManager.Instance.AddToCombatLog(stats.CharName + " regained " + ((int)HPAmnt).ToString() + " HP.", new Color32(0, 255, 0, 255));
                yield return new WaitForSeconds(0.1f);
            }
            if (MPAmnt > 0)
            {
                GenerateSpecialScrollingText.GenerateScrollingText(PCManager.Instance.GetPCGameObjectFromStatsConfig(stats).transform, ((int)MPAmnt).ToString(), Color.blue);
                CombatLogManager.Instance.AddToCombatLog(stats.CharName + " regained " + ((int)MPAmnt).ToString() + " MP.", new Color32(0, 0, 255, 255));
                yield return new WaitForSeconds(0.1f);
            }
            if (TPAmnt > 0)
            {
                GenerateSpecialScrollingText.GenerateScrollingText(PCManager.Instance.GetPCGameObjectFromStatsConfig(stats).transform, ((int)TPAmnt).ToString(), Color.yellow);
                CombatLogManager.Instance.AddToCombatLog(stats.CharName + " regained " + ((int)TPAmnt).ToString() + " TP.", new Color32(255, 255, 0, 255));
                yield return new WaitForSeconds(0.1f);
            }
        }
    }
}
