using System.Collections;
using System.Collections.Generic;
using RPG.CombatSystem;
using UnityEngine;

//Last Reviewed: 1/29/20
namespace RPG.Inventory
{
    //create an item that damages all enemies
    [CreateAssetMenu(menuName = "Item / Grenade")]
    public class UseableItem_Grenade : UseableItem
    {
        [SerializeField] bool useDmgConfigParticleEffect = false;
        [SerializeField] DamageSpell_Config DmgSpellConfig = null;
        [SerializeField] GameObject grenadePrefab = null;
        [SerializeField] AudioClip grenadeExplode = null;
        [SerializeField] AudioClip[] additionalOnHitSounds;
        [SerializeField] GameObject[] additionalOnHitParticleEffects;

        public WaitForSeconds pauseAfterThrow { get; private set; } = new WaitForSeconds(2.2f);
        public GameObject GrenadePrefab { get { return grenadePrefab; } }
        public DamageSpell_Config DmgConfig { get { return DmgSpellConfig; } }
        public bool UseDmgConfigParticleEffect { get { return useDmgConfigParticleEffect; } }


        public override void UseItem(Base_Stats_Config caster, List<Base_Stats_Config> targets)
        {
            caster.CScript.StartCoroutine(ThrowGrenade(caster));
        }

        IEnumerator ThrowGrenade(Base_Stats_Config caster)
        {
            GameObject prefab = null;
            if (grenadePrefab != null)
            {
                prefab = Instantiate(grenadePrefab, caster.CScript.gameObject.transform.position, caster.CScript.gameObject.transform.rotation);
                SoundManager.Instance.PlayAudioClip(OnUseSoundEffect);
                yield return SimulateThrow(prefab);
                SoundManager.Instance.PlayAudioClip(grenadeExplode);
                //play any additional effects
                foreach (GameObject pe in additionalOnHitParticleEffects)
                {
                    Instantiate(pe, prefab.transform.position, pe.transform.rotation);
                }
                foreach (AudioClip se in additionalOnHitSounds)
                {
                    SoundManager.Instance.PlayAudioClip(se);
                }

                prefab.SetActive(false);
                Destroy(prefab, 5);
            }

            //apply effect to each enemy
            for (int i = EnemyManager.Instance.EnemiesInEncounter.Count - 1; i >= 0; i--)
            {
                if (EnemyManager.Instance.EnemiesInEncounter[i] == null)
                {
                    continue;
                }

                var c = EnemyManager.Instance.EnemiesInEncounter[i].GetComponent<Character>();
                if (c.IsAlive)
                {
                    c.ApplySpellEffect(caster.CScript, Instantiate(DmgSpellConfig));
                }
            }
            yield return null;
        }

        //move the projectile in an arc over time from the caster to the target.
        IEnumerator SimulateThrow(GameObject projectile)
        {
            Vector3 targetLoc = new Vector3(4, 100, 4); //center of battlefield
            float firingAngle = 65.0f; // default == 65.0f
            float gravity = 12.5f; //default == 12.5f

            // Calculate distance to target
            float target_Distance = Vector3.Distance(projectile.transform.position, targetLoc);

            // Calculate the velocity needed to throw the object to the target at specified angle.
            float projectile_Velocity = target_Distance / (Mathf.Sin(2 * firingAngle * Mathf.Deg2Rad) / gravity);

            // Extract the X  Y componenent of the velocity
            float Vx = Mathf.Sqrt(projectile_Velocity) * Mathf.Cos(firingAngle * Mathf.Deg2Rad);
            float Vy = Mathf.Sqrt(projectile_Velocity) * Mathf.Sin(firingAngle * Mathf.Deg2Rad);

            // Calculate flight time.
            float flightDuration = target_Distance / Vx;

            // Rotate projectile to face the target.
            projectile.transform.rotation = Quaternion.LookRotation(targetLoc - projectile.transform.position);

            float elapse_time = 0;

            while (elapse_time < flightDuration && projectile != null)
            {
                projectile.transform.Translate(0, (Vy - (gravity * elapse_time)) * Time.deltaTime, Vx * Time.deltaTime);

                elapse_time += Time.deltaTime;

                yield return null;
            }
        }
    }
}