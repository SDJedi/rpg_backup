using RPG.CombatSystem;
using UnityEngine;

//Last Reviewed: 1/29/20
namespace RPG.Inventory
{
    //create a piece of equipment that, when equipped, adds a new proc to a PC
    [CreateAssetMenu(menuName = "Item / Equipment / AddProc")]
    public class EquipableItem_AddProc : EquipableItem_ModifyStats
    {
        [SerializeField] Proc proc = null;


        public override void OnEquip(PC_Stats_Config stats)
        {
            base.OnEquip(stats);
            PC_ProcManager.Instance.AddProcToPC(stats.CharName, proc);
        }

        public override void OnUnequip(PC_Stats_Config stats)
        {
            base.OnUnequip(stats);
            PC_ProcManager.Instance.RemoveProcFromPC(stats.CharName, proc);
        }
    }
}
