using RPG.CombatSystem;
using UnityEngine;

//Last Reviewed: 1/29/20
namespace RPG.Inventory
{
    //create a piece of equipment that, when equipped, modifys a PC's stats
    [CreateAssetMenu(menuName = "Item / Equipment / ModifyStats")]
    public class EquipableItem_ModifyStats : EquipableItem
    {
        [Header("Match each stat with the amount it is to be modified")]
        [SerializeField] protected StatAbbreviation[] statToModify = new StatAbbreviation[1];
        [SerializeField] protected int[] amountToChange = new int[1];
        [SerializeField] protected float magicResistance = 0;


        public override void OnEquip(PC_Stats_Config stats)
        {
            if (statToModify.Length != amountToChange.Length)
            {
                Debug.LogWarning("ERROR! StatToModify[] must have same number of entries as AmountToChange[]");
                return;
            }

            for (int i = 0; i < statToModify.Length; i++)
            {
                stats.ModifyStat(statToModify[i], amountToChange[i]);
            }
            stats.AdjustMagicResist(magicResistance);
        }

        public override void OnUnequip(PC_Stats_Config stats)
        {
            if (statToModify.Length != amountToChange.Length)
            {
                Debug.LogWarning("ERROR! StatToModify[] must have same number of entries as AmountToChange[]");
                return;
            }

            for (int i = 0; i < statToModify.Length; i++)
            {
                stats.ModifyStat(statToModify[i], -amountToChange[i]);
            }
            stats.AdjustMagicResist(-magicResistance);
        }
    }
}
