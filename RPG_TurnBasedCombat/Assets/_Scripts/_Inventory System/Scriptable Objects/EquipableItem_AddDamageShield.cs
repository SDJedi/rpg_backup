using RPG.CombatSystem;
using UnityEngine;

//Last Reviewed: 1/29/20
namespace RPG.Inventory
{
    //create a piece of equipment that, when equipped, adds a new damage shield to a PC
    [CreateAssetMenu(menuName = "Item / Equipment / AddDamageShield")]
    public class EquipableItem_AddDamageShield : EquipableItem_ModifyStats
    {
        [SerializeField] DamageShield damageShield = null;


        public override void OnEquip(PC_Stats_Config stats)
        {
            base.OnEquip(stats);
            stats.AddNewDamageShield(damageShield);
        }

        public override void OnUnequip(PC_Stats_Config stats)
        {
            base.OnUnequip(stats);
            stats.RemoveADamageShield(damageShield);
        }
    }
}
