using System.Collections;
using System.Collections.Generic;
using RPG.CombatSystem;
using UnityEngine;
using UnityEngine.SceneManagement;

//Last Reviewed: 1/30/20
namespace RPG.Inventory
{
    //create an item to rez one or more PCs
    [CreateAssetMenu(menuName = "Item / Rez Item")]
    public class UseableItem_RezItem : UseableItem
    {
        [SerializeField] RezSpell_Config rezDetails = null;

        GameObject PCBeingRezed;


        public override void UseItem(Base_Stats_Config caster, List<Base_Stats_Config> targets)
        {
            if (OnUseSoundEffect != null && SceneManager.GetActiveScene().name != "CombatScene")
            {
                SoundManager.Instance.PlayAudioClip(OnUseSoundEffect);
            }

            foreach (Base_Stats_Config stats in targets)
            {
                //only apply rez to dead targets...
                if (stats.IsAlive())
                {
                    continue;
                }

                //if we're in combat...
                if (SceneManager.GetActiveScene().name == "CombatScene")
                {
                    PCBeingRezed = PCManager.Instance.GetPCGameObjectFromStatsConfig(stats);
                    PlayerCharacter PCScript = PCBeingRezed.GetComponent<PlayerCharacter>();
                    CombatLogManager.Instance.AddToCombatLog(stats.CharName + " was REVIVED!", new Color32(255, 255, 255, 255));
                    Animator anim = PCBeingRezed.GetComponentInChildren<Animator>();
                    anim.SetTrigger("Rez");
                    PCScript.ApplySpellEffect(PCManager.Instance.GetPCGameObjectFromStatsConfig(caster).GetComponent<PlayerCharacter>(), Instantiate(rezDetails));
                    PCScript.StartCoroutine(SendRezedPCBackToStartPos());
                }
                //if we're not in combat...
                else
                {
                    float Amnt;

                    if (rezDetails.PercentageOfMaxRegainedOnInitialHit > 0)
                    {
                        Amnt = (float)stats.MaxHP * ((float)rezDetails.PercentageOfMaxRegainedOnInitialHit / 100);
                    }
                    else
                    {
                        Amnt = UnityEngine.Random.Range(rezDetails.MinAmount_OnApply, rezDetails.MaxAmount_OnApply);
                    }

                    stats.ModifyCurrentHP((int)Amnt);
                }
            }
        }

        IEnumerator SendRezedPCBackToStartPos()
        {
            yield return new WaitForSeconds(1.5f);
            yield return MovementManager.Instance.MoveACharacterToItsHomePosition(PCBeingRezed, 20);
        }
    }
}
