using System.Collections.Generic;
using UnityEngine;

//Last Reviewed: 1/29/20
namespace RPG.Inventory
{
    //create a new Item Shop
    [CreateAssetMenu(menuName = "Item Shop")]
    public class ItemShop : ScriptableObject
    {
        public string NameOfShop;
        public string ShopGreeting;
        public List<Item> ItemsInStock;
        public float PriceMarkUp;
    }
}
