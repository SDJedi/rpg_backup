using RPG.CombatSystem;
using UnityEngine;

//Last Reviewed: 1/28/20
namespace RPG.Inventory
{
    //script is attached to each of the 50 COMBAT inventory buttons
    public class Inventory_ItemAttachedToCombatInvButton : MonoBehaviour
    {
        public UseableItem item;

        public void OnButtonClick()
        {
            //when btn is clicked, hand control over to Command_UseItem
            TurnManager.Instance.PCReceivingCommands.GetComponent<Command_UseItem>().SelectTargetsForItem(item);
        }
    }
}
