using TMPro;
using UnityEngine;

//Last Reviewed: 1/29/20
namespace RPG.Inventory
{
    //show details of item rewards
    public class Show_Hide_ItemInfo_CombatReward : Show_Hide_ItemInfo
    {
        readonly Vector2 tooltipPos = new Vector2(691, 135);

        [SerializeField] TextMeshProUGUI textField = null;


        protected override void SetPanelPosition()
        {
            panelPosition = tooltipPos;
        }

        protected override Item GetItemToShow()
        {
            for (int i = 0; i < InventoryManager.Instance.ItemDatabase.Count; i++)
            {
                if (InventoryManager.Instance.ItemDatabase[i].ItemName == textField.text)
                {
                    return InventoryManager.Instance.ItemDatabase[i];
                }
            }
            return null;
        }
    }
}
