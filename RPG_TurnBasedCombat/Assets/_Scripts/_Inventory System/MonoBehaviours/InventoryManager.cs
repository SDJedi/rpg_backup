using System;
using System.Collections.Generic;
using RPG.CombatSystem;
using RPG.Messaging;
using RPG.Saving;
using UnityEngine;

//Last Reviewed: 1/29/20
namespace RPG.Inventory
{
    //This class handles the storage and useage of items and gp
    public class InventoryManager : Singleton<InventoryManager>, ISaveable
    {
        public event Action OnGPChange; //fires whenever ModifyGPAmnt() is called

        public List<Item> ItemDatabase; //All items in the game, cloned/instantiated in Awake() ... qty should stay 1 ... equipped items reference these
        public List<Item> ItemsInInventory; //All items in the party inventory ... qty reflects amount in inventory

        public int CurrentGP; // {get; private set;}


        protected override void Awake()
        {
            base.Awake();

            //create instances of all items in game so that originals don't get changed ... 
            //may not be necessary since we shouldn't be making changes to these at runtime
            for (int i = 0; i < ItemDatabase.Count; i++)
            {
                ItemDatabase[i] = Instantiate(ItemDatabase[i]);
            }
        }

        void Start()
        {
            SavingWrapper.Instance.StartingNewGame += ResetInventoryAndGP;
        }

        void OnDisable()
        {
            if (SavingWrapper.InstanceExists)
            {
                SavingWrapper.Instance.StartingNewGame -= ResetInventoryAndGP;
            }
        }

        //Add an item to Inventory
        public void AddItemToInventory(Item instantiatedItem, int Qty = 1) //Note: "item" passed in should always be a clone of the asset 
        // (i.e. an instance of the scriptable object created via the asset menu, NOT the original asset)
        {
            /////////Error Checking: make sure item passed in is an instance [i.e. (Clone)]////////
            if (instantiatedItem.name.Length >= 7)
            {
                string clone = instantiatedItem.name.Substring(instantiatedItem.name.Length - 7);
                if (clone != "(Clone)")
                {
                    Debug.LogWarning("Could not add " + instantiatedItem.name + " to inventory. Item name suggests it was not an instance of the item.");
                    return;
                }
            }
            else
            {
                Debug.LogWarning("Could not add " + instantiatedItem.name + " to inventory. Item name length suggests it was not an instance of the item.");
                return;
            }
            //////////////////////////////////////////////////////////////////////////////////////////

            //if we don't have an item in inventory that matches the name of "item"...
            if (!ItemExistsInInventory(instantiatedItem))
            {
                //add this item to the inventory list (again, this should always be a clone, not the actual asset)
                //sometimes what gets added is even a clone of a clone, for example, when we un-equip an item.
                ItemsInInventory.Add((instantiatedItem));

                //if we're adding more than 1 of this new item...
                if (Qty > 1)
                {
                    instantiatedItem.ModifyItemQuantity(Qty - 1); //default qty of our instantiated item is 1, so we subtract 1 to compensate
                }
            }
            //we already have at least one of this item in inventory...
            else
            {
                //loop through inventory...
                for (int i = 0; i < ItemsInInventory.Count; i++)
                {
                    if (ItemsInInventory[i].ItemName == instantiatedItem.ItemName) //find a match...
                    {
                        if (Qty > 0) //ensure qty we're adding is not negative
                        {
                            ItemsInInventory[i].ModifyItemQuantity(Qty); //increase the qty of the existing inventory item
                            Destroy(instantiatedItem); //we don't need the instantiated item we passed in since we already had one.
                            break;
                        }
                    }
                }
            }
            //save modified inventory --do we really need to save here?
            //GameObject.FindObjectOfType<SavingWrapper>().SaveOneGameObject(gameObject);
        }

        //Remove some qty of an item from inventory
        public void SubtractItemFromInventory(Item item, int QtyToRemove = 1)
        {
            //if the item we're trying to remove does not exist... do nada
            if (!ItemExistsInInventory(item))
            {
                return;
            }

            //we have at least qty(1) of this item in inventory. Loop backwards through inventory...
            for (int i = ItemsInInventory.Count - 1; i >= 0; i--)
            {
                if (ItemsInInventory[i] != null && ItemsInInventory[i].ItemName == item.ItemName) //find a match...
                {
                    //case: removing QtyToRemove would put ItemQuantity at a negative value... should never happen!
                    if (ItemsInInventory[i].ItemQuantity - QtyToRemove < 0)
                    {
                        Debug.LogWarning("Error: trying to remove more from " + ItemsInInventory[i] + " than qty allows");
                    }

                    //case: we're taking the last of this item from inventory, remove it from the list.
                    if (ItemsInInventory[i].ItemQuantity - QtyToRemove <= 0)
                    {
                        //as a failsafe, make sure that ItemsInInventory[i] is not a reference to a 
                        //database item instance (shouldn't happen)
                        if (!ItemDatabase.Contains(ItemsInInventory[i]))
                        {
                            //we don't need the instance of this item any longer
                            //if we get another one at a later time, a new instance
                            //will be added to ItemsInInventory[]
                            Destroy(ItemsInInventory[i]);
                        }
                        else
                        {
                            Debug.LogWarning("Error: " + ItemsInInventory[i].ItemName + " is a reference to an item in our ItemDatabase.");
                        }
                        //Even though Destroy() has been called, it is my understanding that it doesn't happen
                        //immediately, and so we need to remove the item (or perhaps a null reference) from
                        //ItemsInInventory[]. Then, eleminate any null values, and refresh the inventory UI
                        ItemsInInventory.RemoveAt(i);
                        ItemsInInventory.RemoveAll(n => n == null);
                        Inventory_UI.Instance.RefreshInventoryItemList(Inventory_UI.Instance.CurrentSortMethod);
                    }
                    else
                    {
                        //case: we're not taking the last of this item, simply reduce the quantity in inventory...
                        ItemsInInventory[i].ModifyItemQuantity(-QtyToRemove);
                    }
                }
            }
            //save modified inventory --do we really need to save here?
            //GameObject.FindObjectOfType<SavingWrapper>().SaveOneGameObject(gameObject);
        }

        //called from inventory buttons
        public void EquipOrUseItemOnCurrentlySelectedChar(int btnIndex)
        {
            var item = Inventory_UI.Instance.InvSlots_UI[btnIndex].ItemInSlot; //ItemInSlot is populated in Inventory_UI ... see RefreshInventoryItemList()

            if (item != null)
            {
                //there IS an item associated with the button pressed...

                //check level requirement...
                if (Inventory_UI.Instance.StatsTiedToCurrentCharTab.CurLvl < item.MinLvl)
                {
                    NotificationWindowManager.Instance.DisplayErrorMessage(Inventory_UI.Instance.StatsTiedToCurrentCharTab.CharName + " is too low level to use " + item.ItemName, NotificationWindowManager.Instance.defaultPos);
                    return;
                }

                //equippable?
                if (item.GetType().IsSubclassOf(typeof(EquipableItem)))
                {
                    //can the current PC equip the item?
                    if (item.PCCanUseItem(Inventory_UI.Instance.StatsTiedToCurrentCharTab.CharName))
                    {
                        EquipItem((EquipableItem)item, Inventory_UI.Instance.StatsTiedToCurrentCharTab);
                    }
                    else
                    {
                        NotificationWindowManager.Instance.DisplayErrorMessage(Inventory_UI.Instance.StatsTiedToCurrentCharTab.CharName + " can't equip " + item.ItemName, NotificationWindowManager.Instance.defaultPos);
                    }
                }

                //useable?
                else if (item.GetType().IsSubclassOf(typeof(UseableItem)))
                {
                    if (item.PCCanUseItem(Inventory_UI.Instance.StatsTiedToCurrentCharTab.CharName))
                    {
                        UseItem_OverWorld((UseableItem)item);
                    }
                    else
                    {
                        NotificationWindowManager.Instance.DisplayErrorMessage(Inventory_UI.Instance.StatsTiedToCurrentCharTab.CharName + " can not use " + item.ItemName, NotificationWindowManager.Instance.defaultPos);
                    }
                }
            }
            //after an item is used or equipped, update the relevant UI...
            Inventory_UI.Instance.RefreshInventoryItemList(Inventory_UI.Instance.CurrentSortMethod);
            Inventory_UI.Instance.MatchStatsToOpenCharTab(Inventory_UI.Instance.StatsTiedToCurrentCharTab.UniqueID);
        }

        //called when equipped item is pressed
        public void UnEquipItemOnCurrentlySelectedChar(string slotType)
        {
            //figure out which PC stats we're dealing with (i.e. the currently selected tab)
            var PC = Inventory_UI.Instance.StatsTiedToCurrentCharTab;

            //figure out which item slot we're removing equipment from...
            switch (slotType)
            {
                //Head
                case "Head":
                    UnEquip(PC, PC.EquippedGear.Head); //adds item back into inventory && adjusts PC stats via item.OnUnequip()
                    PC.EquippedGear.Head = null; //stop referencing the item
                    break;
                //Chest
                case "Chest":
                    UnEquip(PC, PC.EquippedGear.Chest);
                    PC.EquippedGear.Chest = null;
                    break;
                //Waist
                case "Waist":
                    UnEquip(PC, PC.EquippedGear.Waist);
                    PC.EquippedGear.Waist = null;
                    break;
                //Legs
                case "Legs":
                    UnEquip(PC, PC.EquippedGear.Legs);
                    PC.EquippedGear.Legs = null;
                    break;
                //Feet
                case "Feet":
                    UnEquip(PC, PC.EquippedGear.Feet);
                    PC.EquippedGear.Feet = null;
                    break;
                //Adorn_1
                case "Adorn_1":
                    UnEquip(PC, PC.EquippedGear.Adorn_1);
                    PC.EquippedGear.Adorn_1 = null;
                    break;
                //Adorn_2
                case "Adorn_2":
                    UnEquip(PC, PC.EquippedGear.Adorn_2);
                    PC.EquippedGear.Adorn_2 = null;
                    break;
                //Weapon
                case "Weapon":
                    UnEquip(PC, PC.EquippedGear.Weapon);
                    PC.EquippedGear.Weapon = null;
                    break;
            }
            //update the UI after unequip to reflect changes in stats and inventory
            Inventory_UI.Instance.RefreshInventoryItemList(Inventory_UI.Instance.CurrentSortMethod);
            Inventory_UI.Instance.MatchStatsToOpenCharTab(Inventory_UI.Instance.StatsTiedToCurrentCharTab.UniqueID);
            //save modified inventory --do we really need to save here?
            //GameObject.FindObjectOfType<SavingWrapper>().SaveOneGameObject(gameObject);
        }

        void UnEquip(PC_Stats_Config PC, EquipableItem item)
        {
            //Note: "item" should always be a reference to an
            //equippable item in the ItemDatabase[]
            if (item == null)
            {
                return;
            }
            AddItemToInventory(Instantiate(item));
            item.OnUnequip(PC);
        }

        public void UnEquipAllItemsOnPC(PC_Stats_Config PC)
        {
            UnEquip(PC, PC.EquippedGear.Head);
            PC.EquippedGear.Head = null;

            UnEquip(PC, PC.EquippedGear.Chest);
            PC.EquippedGear.Chest = null;

            UnEquip(PC, PC.EquippedGear.Waist);
            PC.EquippedGear.Waist = null;

            UnEquip(PC, PC.EquippedGear.Legs);
            PC.EquippedGear.Legs = null;

            UnEquip(PC, PC.EquippedGear.Feet);
            PC.EquippedGear.Feet = null;

            UnEquip(PC, PC.EquippedGear.Adorn_1);
            PC.EquippedGear.Adorn_1 = null;

            UnEquip(PC, PC.EquippedGear.Adorn_2);
            PC.EquippedGear.Adorn_2 = null;

            UnEquip(PC, PC.EquippedGear.Weapon);
            PC.EquippedGear.Weapon = null;
        }

        //returns true if an item with the same name exists in inventory
        public bool ItemExistsInInventory(Item item)
        {
            foreach (Item i in ItemsInInventory)
            {
                if (i != null && i.ItemName == item.ItemName)
                {
                    return true;
                }
            }

            return false;
        }

        //set a PC's worn equipment
        public void EquipItem(EquipableItem item, PC_Stats_Config PC)
        {
            //figure out what equipment slot we're dealing with based on the item's SlotToEquipin
            switch (item.SlotToEquipIn)
            {
                case EquipmentSlot.Head:
                    if (PC.EquippedGear.Head == null)
                    {
                        //no helm in slot ... equip item                 
                        PC.EquippedGear.Head = (EquipableItem)PointToDatabaseInstance(item.ItemName); //Note: multiple characters may be referencing the same instance of a piece of gear
                        PC.EquippedGear.Head.OnEquip(PC); //apply any bonuses...
                        SubtractItemFromInventory(item); //Note: it's possible that the item no longer exists in inventory now [i.e. we equipped the only one we had] this is why equipped gear references items in our ItemDatabase vs. the cloned items in inventory
                    }
                    else if (PC.EquippedGear.Head.ItemName != item.ItemName)
                    {
                        //already a helm in slot, unequip it...
                        AddItemToInventory(Instantiate(PC.EquippedGear.Head));
                        PC.EquippedGear.Head.OnUnequip(PC);
                        PC.EquippedGear.Head = null;

                        //equip item
                        PC.EquippedGear.Head = (EquipableItem)PointToDatabaseInstance(item.ItemName);
                        PC.EquippedGear.Head.OnEquip(PC);
                        SubtractItemFromInventory(item);
                    }
                    break;

                case EquipmentSlot.Chest:
                    if (PC.EquippedGear.Chest == null)
                    {
                        PC.EquippedGear.Chest = (EquipableItem)PointToDatabaseInstance(item.ItemName);
                        PC.EquippedGear.Chest.OnEquip(PC);
                        SubtractItemFromInventory(item);
                    }
                    else if (PC.EquippedGear.Chest.ItemName != item.ItemName)
                    {
                        AddItemToInventory(Instantiate(PC.EquippedGear.Chest));
                        PC.EquippedGear.Chest.OnUnequip(PC);
                        PC.EquippedGear.Chest = null;

                        PC.EquippedGear.Chest = (EquipableItem)PointToDatabaseInstance(item.ItemName);
                        PC.EquippedGear.Chest.OnEquip(PC);
                        SubtractItemFromInventory(item);
                    }
                    break;

                case EquipmentSlot.Waist:
                    if (PC.EquippedGear.Waist == null)
                    {
                        PC.EquippedGear.Waist = (EquipableItem)PointToDatabaseInstance(item.ItemName);
                        PC.EquippedGear.Waist.OnEquip(PC);
                        SubtractItemFromInventory(item);
                    }
                    else if (PC.EquippedGear.Waist.ItemName != item.ItemName)
                    {
                        AddItemToInventory(Instantiate(PC.EquippedGear.Waist));
                        PC.EquippedGear.Waist.OnUnequip(PC);
                        PC.EquippedGear.Waist = null;

                        PC.EquippedGear.Waist = (EquipableItem)PointToDatabaseInstance(item.ItemName);
                        PC.EquippedGear.Waist.OnEquip(PC);
                        SubtractItemFromInventory(item);
                    }
                    break;

                case EquipmentSlot.Legs:
                    if (PC.EquippedGear.Legs == null)
                    {
                        PC.EquippedGear.Legs = (EquipableItem)PointToDatabaseInstance(item.ItemName);
                        PC.EquippedGear.Legs.OnEquip(PC);
                        SubtractItemFromInventory(item);
                    }
                    else if (PC.EquippedGear.Legs.ItemName != item.ItemName)
                    {
                        AddItemToInventory(Instantiate(PC.EquippedGear.Legs));
                        PC.EquippedGear.Legs.OnUnequip(PC);
                        PC.EquippedGear.Legs = null;

                        PC.EquippedGear.Legs = (EquipableItem)PointToDatabaseInstance(item.ItemName);
                        PC.EquippedGear.Legs.OnEquip(PC);
                        SubtractItemFromInventory(item);
                    }
                    break;

                case EquipmentSlot.Feet:
                    if (PC.EquippedGear.Feet == null)
                    {
                        PC.EquippedGear.Feet = (EquipableItem)PointToDatabaseInstance(item.ItemName);
                        PC.EquippedGear.Feet.OnEquip(PC);
                        SubtractItemFromInventory(item);
                    }
                    else if (PC.EquippedGear.Feet.ItemName != item.ItemName)
                    {
                        AddItemToInventory(Instantiate(PC.EquippedGear.Feet));
                        PC.EquippedGear.Feet.OnUnequip(PC);
                        PC.EquippedGear.Feet = null;

                        PC.EquippedGear.Feet = (EquipableItem)PointToDatabaseInstance(item.ItemName);
                        PC.EquippedGear.Feet.OnEquip(PC);
                        SubtractItemFromInventory(item);
                    }
                    break;

                case EquipmentSlot.Adornment:
                    if (PC.EquippedGear.Adorn_1 == null)
                    {
                        PC.EquippedGear.Adorn_1 = (EquipableItem)PointToDatabaseInstance(item.ItemName);
                        PC.EquippedGear.Adorn_1.OnEquip(PC);
                        SubtractItemFromInventory(item);
                    }
                    else if (PC.EquippedGear.Adorn_2 == null)
                    {
                        PC.EquippedGear.Adorn_2 = (EquipableItem)PointToDatabaseInstance(item.ItemName);
                        PC.EquippedGear.Adorn_2.OnEquip(PC);
                        SubtractItemFromInventory(item);
                    }
                    else if (PC.EquippedGear.Adorn_1.ItemName != item.ItemName)
                    {
                        //slots 1 and 2 full ... adorn clicked is not the adorn in slot 1 ... swap out with slot 1 adorn
                        AddItemToInventory(Instantiate(PC.EquippedGear.Adorn_1));
                        PC.EquippedGear.Adorn_1.OnUnequip(PC);
                        PC.EquippedGear.Adorn_1 = null;

                        PC.EquippedGear.Adorn_1 = (EquipableItem)PointToDatabaseInstance(item.ItemName);
                        PC.EquippedGear.Adorn_1.OnEquip(PC);
                        SubtractItemFromInventory(item);
                    }
                    else if (PC.EquippedGear.Adorn_2.ItemName != item.ItemName)
                    {
                        //slots 1 and 2 full ... adorn clicked is already in slot 1, but a different adorn is in slot 2 ... swap out with slot 2 adorn
                        AddItemToInventory(Instantiate(PC.EquippedGear.Adorn_2));
                        PC.EquippedGear.Adorn_2.OnUnequip(PC);
                        PC.EquippedGear.Adorn_2 = null;

                        PC.EquippedGear.Adorn_2 = (EquipableItem)PointToDatabaseInstance(item.ItemName);
                        PC.EquippedGear.Adorn_2.OnEquip(PC);
                        SubtractItemFromInventory(item);
                    }
                    break;

                case EquipmentSlot.Weapon:
                    if (PC.EquippedGear.Weapon == null)
                    {
                        PC.EquippedGear.Weapon = (EquipableItem)PointToDatabaseInstance(item.ItemName);
                        PC.EquippedGear.Weapon.OnEquip(PC);
                        SubtractItemFromInventory(item);
                    }
                    else if (PC.EquippedGear.Weapon.ItemName != item.ItemName)
                    {
                        AddItemToInventory(Instantiate(PC.EquippedGear.Weapon));
                        PC.EquippedGear.Weapon.OnUnequip(PC);
                        PC.EquippedGear.Weapon = null;

                        PC.EquippedGear.Weapon = (EquipableItem)PointToDatabaseInstance(item.ItemName);
                        PC.EquippedGear.Weapon.OnEquip(PC);
                        SubtractItemFromInventory(item);
                    }
                    break;
            }
        }

        //returns the item in the ItemDatabase[] that has an ItemName == itmName. Used for equipable items
        public Item PointToDatabaseInstance(string itmName)
        {
            for (int i = 0; i < ItemDatabase.Count; i++)
            {
                if (itmName == ItemDatabase[i].ItemName)
                {
                    return (ItemDatabase[i]);
                }
            }

            Debug.LogWarning("Could not find item: " + itmName + " in ItemDatabase[]");
            return null;
        }

        //use an item from inventory in the overworld
        void UseItem_OverWorld(UseableItem item)
        {
            if (!ItemExistsInInventory(item))
            {
                return;
            }

            switch (((UseableItem)item).TargetType)
            {
                //use item on the currently selected character
                case TargetType.Friendly:
                case TargetType.Self:
                    List<Base_Stats_Config> targetOfItem = new List<Base_Stats_Config>(1);
                    targetOfItem.Add(Inventory_UI.Instance.StatsTiedToCurrentCharTab);
                    ((UseableItem)item).UseItem(Inventory_UI.Instance.StatsTiedToCurrentCharTab, targetOfItem);
                    SubtractItemFromInventory(item);
                    break;
                //item will effect (ONLY) characters in the party (e.g. will NOT effect any characters the player has collected that are avialable but not currently in the party of (up to) 4)
                case TargetType.All:
                case TargetType.AllFriendly:
                    var stats = new List<Base_Stats_Config>();
                    foreach (PC_Stats_Config statsConfig in PCManager.Instance.GetStatsForPCsInParty())
                    {
                        stats.Add(statsConfig);
                    }
                    ((UseableItem)item).UseItem(Inventory_UI.Instance.StatsTiedToCurrentCharTab, stats);
                    SubtractItemFromInventory(item);
                    break;
                case TargetType.DeadFriendly:
                    if (!Inventory_UI.Instance.StatsTiedToCurrentCharTab.IsAlive())
                    {
                        List<Base_Stats_Config> target = new List<Base_Stats_Config>(1);
                        target.Add(Inventory_UI.Instance.StatsTiedToCurrentCharTab);
                        ((UseableItem)item).UseItem(Inventory_UI.Instance.StatsTiedToCurrentCharTab, target);
                        SubtractItemFromInventory(item);
                    }
                    break;
            }
        }

        //returns the gameobjects that were targets of the item
        public List<GameObject> UseItemInCombat(Character caster, UseableItem item)
        {
            List<Base_Stats_Config> itemTargets = new List<Base_Stats_Config>();
            List<GameObject> charObjectsInScene = new List<GameObject>();

            switch (item.TargetType)
            {
                case TargetType.Self:
                case TargetType.Friendly:
                case TargetType.Friendly_NotSelf:
                case TargetType.DeadFriendly:
                    itemTargets.Add(caster.FriendlyTarget.Stats_Config);
                    charObjectsInScene.Add(caster.FriendlyTarget.gameObject);
                    item.UseItem(caster.Stats_Config, itemTargets);
                    SubtractItemFromInventory(item);
                    return charObjectsInScene;
                case TargetType.Hostile:
                    itemTargets.Add(caster.HostileTarget.Stats_Config);
                    charObjectsInScene.Add(caster.HostileTarget.gameObject);
                    item.UseItem(caster.Stats_Config, itemTargets);
                    SubtractItemFromInventory(item);
                    return charObjectsInScene;
                case TargetType.All:
                    foreach (GameObject PC in PCManager.Instance.PCsInParty)
                    {
                        if (PC != null)
                        {
                            itemTargets.Add(PC.GetComponent<Character>().Stats_Config);
                            charObjectsInScene.Add(PC);
                        }
                    }
                    foreach (GameObject Enemy in EnemyManager.Instance.EnemiesInEncounter)
                    {
                        if (Enemy != null)
                        {
                            itemTargets.Add(Enemy.GetComponent<Character>().Stats_Config);
                            charObjectsInScene.Add(Enemy);
                        }
                    }
                    item.UseItem(caster.Stats_Config, itemTargets);
                    SubtractItemFromInventory(item);
                    return charObjectsInScene;
                case TargetType.AllHostile:
                    foreach (GameObject Enemy in EnemyManager.Instance.EnemiesInEncounter)
                    {
                        if (Enemy != null)
                        {
                            itemTargets.Add(Enemy.GetComponent<Character>().Stats_Config);
                            charObjectsInScene.Add(Enemy);
                        }
                    }
                    item.UseItem(caster.Stats_Config, itemTargets);
                    SubtractItemFromInventory(item);
                    return charObjectsInScene;
                case TargetType.AllFriendly:
                    foreach (GameObject PC in PCManager.Instance.PCsInParty)
                    {
                        if (PC != null)
                        {
                            itemTargets.Add(PC.GetComponent<Character>().Stats_Config);
                            charObjectsInScene.Add(PC);
                        }
                    }
                    item.UseItem(caster.Stats_Config, itemTargets);
                    SubtractItemFromInventory(item);
                    return charObjectsInScene;
                default:
                    Debug.LogWarning("Tried to use Item, but targetType was not valid");
                    return null;
            }
        }

        //returns the item equipped in a specific item slot
        public Item GetItemEquippedOnPC(PC_Stats_Config PC, string equipSlot)
        {
            switch (equipSlot)
            {
                case "Head":
                    return PC.EquippedGear.Head;
                case "Chest":
                    return PC.EquippedGear.Chest;
                case "Waist":
                    return PC.EquippedGear.Waist;
                case "Legs":
                    return PC.EquippedGear.Legs;
                case "Feet":
                    return PC.EquippedGear.Feet;
                case "Adorn_1":
                    return PC.EquippedGear.Adorn_1;
                case "Adorn_2":
                    return PC.EquippedGear.Adorn_2;
                case "Weapon":
                    return PC.EquippedGear.Weapon;
                default:
                    Debug.LogWarning("invalid equip slot");
                    return null;
            }
        }

        //returns the qty owned of a specific item in inventory
        public int GetQtyOfAnItemInInventory(Item itm)
        {
            foreach (Item i in ItemsInInventory)
            {
                if (i != null && i.ItemName == itm.ItemName)
                {
                    return i.ItemQuantity;
                }
            }

            return 0;
        }

        //The method used to add or subtract GP
        public void ModifyGPAmnt(int amnt)
        {
            CurrentGP += amnt;

            if (CurrentGP < 0)
            {
                Debug.LogWarning("Something caused negative GP ... setting CurrentGP to 0");
                CurrentGP = 0;
            }

            if (OnGPChange != null)
            {
                OnGPChange();
            }
        }

        void ResetInventoryAndGP()
        {
            for (int i = ItemsInInventory.Count - 1; i >= 0; i--)
            {
                Destroy(ItemsInInventory[i]);//cloned objects
            }
            ItemsInInventory.Clear();
            CurrentGP = 0;
            //print("Inventory cleared, GP == 0");
        }

        #region SaveData

        [System.Serializable]
        class InventorySaveData
        {
            //save names of all the items in inv
            public List<string> ItemsInInventory_ItemNames = new List<string>();
            //save the amounts of each item
            public List<int> ItemsInInventory_ItemQtys = new List<int>();
            //save the current GP
            public int CurrentGP;
        }

        public object CaptureState()
        {
            InventorySaveData saveData = new InventorySaveData();

            ItemsInInventory.RemoveAll(n => n == null);
            foreach (Item itm in ItemsInInventory)
            {
                saveData.ItemsInInventory_ItemNames.Add(itm.ItemName);
                saveData.ItemsInInventory_ItemQtys.Add(itm.ItemQuantity);
            }

            saveData.CurrentGP = CurrentGP;
            return saveData;
        }

        public void RestoreState(object state)
        {
            //print("restoring inv");
            InventorySaveData saveData = (InventorySaveData)state;

            ResetInventoryAndGP();

            for (int i = 0; i < saveData.ItemsInInventory_ItemNames.Count; i++)
            {
                Item item = PointToDatabaseInstance(saveData.ItemsInInventory_ItemNames[i]);
                int qty = saveData.ItemsInInventory_ItemQtys[i];
                if (item != null)
                {
                    AddItemToInventory(Instantiate(item), qty);
                }
                else
                {
                    Debug.LogWarning("Could not find item named " + saveData.ItemsInInventory_ItemNames[i] + " in ItemDatabse. Was name of item recently changed?");
                }
            }

            CurrentGP = saveData.CurrentGP;

            if (OnGPChange != null)
            {
                OnGPChange();
            }
        }
        #endregion
    }
}