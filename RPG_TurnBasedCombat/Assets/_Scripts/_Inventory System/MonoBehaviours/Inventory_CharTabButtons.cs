using TMPro;
using UnityEngine;
using UnityEngine.UI;

//Last Reviewed: 1/28/20
namespace RPG.Inventory
{
    //this script is attached to the 4 character tabs in the inventory window
    public class Inventory_CharTabButtons : MonoBehaviour
    {
        Button tabBtn;


        void Start()
        {
            tabBtn = GetComponent<Button>();
        }

        public void OnTabClicked()
        {
            //The character's name is also its unique id. 
            //Note: this TextMeshProUGUI text field is populated by Inventory_UI, UpdateTabNames() when the inventory window is opened
            string id = GetComponentInChildren<TextMeshProUGUI>().text;
            Inventory_UI.Instance.MatchStatsToOpenCharTab(id); //populate the stats window with the correct PC stats
        }
    }
}
