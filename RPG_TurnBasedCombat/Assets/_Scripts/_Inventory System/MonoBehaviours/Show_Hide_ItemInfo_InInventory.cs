using UnityEngine;

//Last Reviewed: 1/29/20
namespace RPG.Inventory
{
    //show/hide tooltip for inventory items in inventory window (out of combat)
    public class Show_Hide_ItemInfo_InInventory : Show_Hide_ItemInfo
    {
        readonly Vector2 tooltipPos = new Vector2(709, 189);

        [HideInInspector]
        public int InvSlotIndex; //set by Inventory_InitializeInvSlots


        protected override void SetPanelPosition()
        {
            panelPosition = tooltipPos;
        }

        protected override Item GetItemToShow()
        {
            return Inventory_UI.Instance.InvSlots_UI[InvSlotIndex].ItemInSlot;
        }
    }
}
