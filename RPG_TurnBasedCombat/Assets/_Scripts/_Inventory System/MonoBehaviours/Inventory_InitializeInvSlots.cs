using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

//Last Reviewed: 1/28/20
namespace RPG.Inventory
{
    //attached to Inventory in BootScene. initializes all inventory slots
    //saves me from having to hook up all the fields in [Inventory_UI.cs -> InvSlots_UI] manually in the inspector
    public class Inventory_InitializeInvSlots : MonoBehaviour
    {
        public List<GameObject> InvSlots; //[BootScene -> Inventory_Canvas -> Inventory -> Inventory_Panel -> InvItems_Panel -> ViewPort -> Content]


        void Start()
        {
            for (int i = 0; i < InvSlots.Count; i++)
            {
                var iSlot = InvSlots[i].GetComponent<Show_Hide_ItemInfo_InInventory>();
                iSlot.InvSlotIndex = i;


                InvSlots[i].GetComponent<Button>().onClick.AddListener(() => InventoryManager.Instance.EquipOrUseItemOnCurrentlySelectedChar(iSlot.InvSlotIndex));
                //InvSlots[i].GetComponent<Button>().onClick.AddListener(() => PrintItemInSlot(iSlot.InvSlotIndex));

                Inventory_UI.InvSlot_UI newSlot = new Inventory_UI.InvSlot_UI();
                Inventory_UI.Instance.InvSlots_UI.Add(newSlot);
                newSlot.Slot = InvSlots[i];
                newSlot.ItemName = InvSlots[i].transform.Find("ItemName_Txt").GetComponent<TextMeshProUGUI>();
                newSlot.Qty = InvSlots[i].transform.Find("ItemQty_Txt").GetComponent<TextMeshProUGUI>();
                newSlot.Value = InvSlots[i].transform.Find("ItemValue_Txt").GetComponent<TextMeshProUGUI>();
                newSlot.Icon = InvSlots[i].transform.Find("ItemIcon_Img").GetComponent<Image>();
            }
        }

        // public void PrintItemInSlot(int index)
        // {
        //     print(Inventory_UI.Instance.InvSlots_UI[index].ItemInSlot);
        // }
    }
}
