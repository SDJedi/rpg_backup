using System;
using System.Collections.Generic;
using RPG.CombatSystem;
using RPG.Saving;
using RPG.SceneControl;
using UnityEngine;

//Last Reviewed: 1/28/20
namespace RPG.Inventory
{
    //attached to InitializePCs in KarlsHut scene
    //should only run one time at the start of a new game
    public class InitializeAllPCs : MonoBehaviour, ISaveable
    {
        bool PCsInitialized = false; //this bool gets saved / restored by the save system


        void OnEnable()
        {
            SceneController.Instance.AfterSceneLoad += AfterSaveDataIsLoaded;
        }

        void OnDisable()
        {
            if (SceneController.InstanceExists)
            {
                SceneController.Instance.AfterSceneLoad -= AfterSaveDataIsLoaded;
            }
        }

        //PCsInitialized == false by default, after save data is loaded,
        //run a check to see if starting equipment/level has already been dealt with. if not,
        //add starting equipment/update level and save this gameObject (i.e. save PCsInitialized == true)
        //in the runTimeSaveFile
        void AfterSaveDataIsLoaded()
        {
            if (!PCsInitialized)
            {
                //print("Equipping PCs with starting gear...");
                foreach (PC_Stats_Config sc in PCManager.Instance.PC_StatConfigs)
                {
                    EquipPCStartingGear(sc, sc.PC_StartingEquipment);
                }
                //print("setting starting PC levels");
                foreach (PC_Stats_Config sc in PCManager.Instance.PC_StatConfigs)
                {
                    if (sc.StartingLvl > 1)
                    {
                        UpdatePCStartingLvlAndXP(sc);
                    }
                }
                PCsInitialized = true;
                FindObjectOfType<SavingSystem>().SaveSingleObject(SavingWrapper.runtimeSaveFile, gameObject);
            }
        }

        void UpdatePCStartingLvlAndXP(PC_Stats_Config sc)
        {
            //set XP to min amount required to be at StartingLvl
            sc.ModifyCurrentXP(XPCalculator.XPCHart[sc.StartingLvl - 1]);
            //level up the PC
            for (int i = sc.StartingLvl; i > 1; i--)
            {
                sc.IncreasePCLvl(false);
            }
        }

        void EquipPCStartingGear(PC_Stats_Config stats, List<EquipableItem> eList)
        {
            foreach (EquipableItem e in eList)
            {
                InventoryManager.Instance.AddItemToInventory(Instantiate(e));
                InventoryManager.Instance.EquipItem(e, stats);
            }
        }

        #region SaveData
        public object CaptureState()
        {
            return PCsInitialized;
        }

        public void RestoreState(object state)
        {
            PCsInitialized = (bool)state;
        }
        #endregion
    }
}
