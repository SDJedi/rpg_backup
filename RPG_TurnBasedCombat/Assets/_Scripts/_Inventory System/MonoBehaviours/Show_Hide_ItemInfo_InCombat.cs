using UnityEngine;

//Last Reviewed: 1/29/20
namespace RPG.Inventory
{
    //display info about the combat-useable item that the mouse is over
    public class Show_Hide_ItemInfo_InCombat : Show_Hide_ItemInfo
    {
        readonly Vector2 tooltipPos = new Vector2(540, -100);


        protected override void SetPanelPosition()
        {
            panelPosition = tooltipPos;
        }

        protected override Item GetItemToShow()
        {
            return GetComponent<Inventory_ItemAttachedToCombatInvButton>().item;
        }
    }
}
