using UnityEngine;

//Last Reviewed: 1/29/20
namespace RPG.Inventory
{
    //attached to PC equipment btns ... show/hide tooltip for equipped items
    public class Show_Hide_ItemInfo_Equipped : Show_Hide_ItemInfo
    {
        readonly Vector2 tooltipPos = new Vector2(98, 88);

        public string equipSlot;


        protected override void SetPanelPosition()
        {
            panelPosition = tooltipPos;
        }

        protected override Item GetItemToShow()
        {
            return InventoryManager.Instance.GetItemEquippedOnPC(Inventory_UI.Instance.StatsTiedToCurrentCharTab, equipSlot);
        }
    }
}
