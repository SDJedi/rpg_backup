using System;
using System.Collections.Generic;
using DialogueSystem;
using RPG.CombatSystem;
using RPG.QuestSystem;
using RPG.Saving;
using RPG.SceneControl;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//Last Reviewed: 1/28/20
namespace RPG.Inventory
{
    public enum InvSortMethod { Alphabetical, SellValue, Qty, Rarity }
    public enum InvFilterMethod { All, Equipable, Useable }

    //this class handles displaying all the information that the player needs to see in the inventory window
    public class Inventory_UI : Singleton<Inventory_UI>, ISaveable
    {
        public event Action InvWindowJustOpened; //let other classes know the inv window has been opened (used to stop player movement)

        [SerializeField] GameObject ButtonControlPanel = null;

        [Header("Main Inventory Panel")]
        [SerializeField] GameObject Inventory_Panel = null; //Main panel, holds everything related to Inv Window UI

        [Header("Inventory UI Text Fields")] //All the text fields on the inventory screen 
        #region UI Text Fields
        [SerializeField] TextMeshProUGUI PC_Name = null;
        [SerializeField] TextMeshProUGUI PC_Lvl = null;
        [SerializeField] TextMeshProUGUI PC_HeadSlot = null;
        [SerializeField] TextMeshProUGUI PC_ChestSlot = null;
        [SerializeField] TextMeshProUGUI PC_WaistSlot = null;
        [SerializeField] TextMeshProUGUI PC_LegsSlot = null;
        [SerializeField] TextMeshProUGUI PC_FeetSlot = null;
        [SerializeField] TextMeshProUGUI PC_Adorn_1Slot = null;
        [SerializeField] TextMeshProUGUI PC_Adorn_2Slot = null;
        [SerializeField] TextMeshProUGUI PC_WpnSlot = null;
        [SerializeField] TextMeshProUGUI PC_CurHP = null;
        [SerializeField] TextMeshProUGUI PC_MaxHP = null;
        [SerializeField] TextMeshProUGUI PC_CurMP = null;
        [SerializeField] TextMeshProUGUI PC_MaxMP = null;
        [SerializeField] TextMeshProUGUI PC_CurTP = null;
        [SerializeField] TextMeshProUGUI PC_MaxTP = null;
        [SerializeField] TextMeshProUGUI PC_STR = null;
        [SerializeField] TextMeshProUGUI PC_AGI = null;
        [SerializeField] TextMeshProUGUI PC_INT = null;
        [SerializeField] TextMeshProUGUI PC_DefMod = null;
        [SerializeField] TextMeshProUGUI PC_DmgRed = null;
        [SerializeField] TextMeshProUGUI PC_AtkBonus = null;
        [SerializeField] TextMeshProUGUI PC_DmgBonus = null;
        [SerializeField] TextMeshProUGUI PC_MagRes = null;
        [SerializeField] TextMeshProUGUI PC_InitMod = null;
        [SerializeField] TextMeshProUGUI PC_CurXP = null;
        [SerializeField] TextMeshProUGUI PC_XpToNextLvl = null;

        //character select tabs
        [SerializeField] TextMeshProUGUI TabForPCSlot_0_Txt = null;
        [SerializeField] TextMeshProUGUI TabForPCSlot_1_Txt = null;
        [SerializeField] TextMeshProUGUI TabForPCSlot_2_Txt = null;
        [SerializeField] TextMeshProUGUI TabForPCSlot_3_Txt = null;
        [Header("Active Tab Color")]
        [SerializeField] ColorBlock activeColorBlock = new ColorBlock();
        [Header("Inactive Tab Color")]
        [SerializeField] ColorBlock inactiveColorBlock = new ColorBlock();

        List<TextMeshProUGUI> allPCTabs;
        #endregion

        [Header("TMP Dropdown Menus")]
        [SerializeField] TMP_Dropdown sortOptionsDropdown = null;
        [SerializeField] TMP_Dropdown filterOptionsDropdown = null;

        [Header("150x150 Image for PC")]
        [SerializeField] RawImage PC_Portrait = null;
        [SerializeField] GameObject DeadPCIcon = null;

        Color32 Color32_White = new Color32(255, 255, 255, 255);
        string idOfLastPCDisplayed = "Saul";

        //The list of all (as of 3/2/20, 50) inventory slots. This list is initialized by Inventory_InitializeInvSlots.cs
        public List<InvSlot_UI> InvSlots_UI = new List<InvSlot_UI>();

        public PC_Stats_Config StatsTiedToCurrentCharTab { get; private set; } = null; //references the stats in PCManager.Instance.PC_StatConfigs[] that are currently on display
        public InvSortMethod CurrentSortMethod { get; private set; } = InvSortMethod.Alphabetical;
        public InvFilterMethod CurrentFilterMethod { get; private set; } = InvFilterMethod.All;

        //This class represents a single item in inventory
        [System.Serializable]
        public class InvSlot_UI
        {
            public GameObject Slot; //Contains several components: Image, Button, Inventory_Show_Hide_ItemInfo.cs
            public Item ItemInSlot; //The Item associated with this slot

            public TextMeshProUGUI ItemName; //TextField where the name of the item should be displayed
            public TextMeshProUGUI Qty; //TextField where the qty of the item should be displayed 
            public TextMeshProUGUI Value; //TextField where the sell value of the item should be displayed
            public Image Icon; //img for the item's icon
        }

        void Start()
        {
            CloseInvWindow();
            allPCTabs = new List<TextMeshProUGUI> { TabForPCSlot_0_Txt, TabForPCSlot_1_Txt, TabForPCSlot_2_Txt, TabForPCSlot_3_Txt };
            SceneManager.activeSceneChanged += OnSceneChanged;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            SceneManager.activeSceneChanged -= OnSceneChanged;
        }

        void OnSceneChanged(Scene x, Scene newScene)
        {
            if (newScene.name == "CombatScene")
            {
                CloseInvWindow();
            }

            if (newScene.name == "MainMenu")
            {
                idOfLastPCDisplayed = "Saul";
            }
        }

        void Update()
        {
            //toggle inv window when "I" is pressed / close when [esc] is pressed
            if ((Input.GetKeyDown(KeyCode.I) || Input.GetKeyDown(KeyCode.Escape)) && InvWindowIsOpen())
            {
                CloseInvWindow();
            }
            else if (Input.GetKeyDown(KeyCode.I) && OkToOpenInventoryWindow())
            {
                OpenInvWindow();
            }
        }

        //called whenever inv window is opened...
        void UpdateTabNames()
        {
            //initialize our tab text to blank...
            foreach (TextMeshProUGUI txt in allPCTabs)
            {
                txt.text = "";
            }

            //create and initialize some variables we wil need 
            int numOfPCsInParty = 0;
            int lowestSlotPos = 3;
            string idOfCharInLowestTab = "";
            List<int> availablePCSlots = new List<int> { 0, 1, 2, 3 };

            //eliminate any null entries in our saved StatConfigs
            PCManager.Instance.PC_StatConfigs.RemoveAll(n => n == null);

            //loop through PCsInParty_Prefabs[]...
            for (int i = 0; i < PCManager.Instance.PCsInParty_Prefabs.Length; i++)
            {
                if (PCManager.Instance.PCsInParty_Prefabs[i] != null) //ignore null slots, which we have when the party is less than 4 characters
                {
                    //found a PC Prefab; increment numOfPCsInParty
                    numOfPCsInParty++;
                    //store the name of the PC
                    string nameKey = PCManager.Instance.PCsInParty_Prefabs[i].GetComponent<PlayerCharacter>().CharName;

                    //Get existing or instantiate new stats config
                    PC_Stats_Config stats = PCManager.Instance.GetInstantiatedStatsConfigForPC(PCManager.Instance.PCsInParty_Prefabs[i]);

                    /////////////////////////////////////////////// Failesafe Code //////////////////////////////////////////////////
                    //now we look for PCs that are in the same PC_Slot_Position ... shouldn't happen, but this will fix it if it does
                    bool slotWasAvailable = false;
                    for (int j = availablePCSlots.Count - 1; j >= 0; j--)
                    {
                        if (availablePCSlots[j] == stats.PC_Slot_position)
                        {
                            slotWasAvailable = true;
                            availablePCSlots.RemoveAt(j);
                            break;
                        }
                    }
                    //The slot position assigned to the PC was already taken
                    if (!slotWasAvailable)
                    {
                        Debug.LogWarning("slot pos issue here... running failsafe code");
                        stats.PC_Slot_position = availablePCSlots[0];
                        availablePCSlots.RemoveAt(0);
                    }
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////  

                    //determine if this PC is in a lower slot pos than the current lowestSlotPos
                    if (stats.PC_Slot_position <= lowestSlotPos)
                    {
                        idOfCharInLowestTab = stats.CharName;
                        lowestSlotPos = stats.PC_Slot_position;
                    }

                    //based on pc slot position, set the txt of the correct tab to the name of the character in the matching slot
                    switch (stats.PC_Slot_position)
                    {
                        case 0:
                            TabForPCSlot_0_Txt.text = stats.CharName;
                            TabForPCSlot_0_Txt.gameObject.transform.parent.gameObject.SetActive(true);
                            break;
                        case 1:
                            TabForPCSlot_1_Txt.text = stats.CharName;
                            TabForPCSlot_1_Txt.gameObject.transform.parent.gameObject.SetActive(true);
                            break;
                        case 2:
                            TabForPCSlot_2_Txt.text = stats.CharName;
                            TabForPCSlot_2_Txt.gameObject.transform.parent.gameObject.SetActive(true);
                            break;
                        case 3:
                            TabForPCSlot_3_Txt.text = stats.CharName;
                            TabForPCSlot_3_Txt.gameObject.transform.parent.gameObject.SetActive(true);
                            break;
                    }
                }
            }

            //hide tabs for empty character slots; no need to see tabs if there is only 1 character
            if (TabForPCSlot_0_Txt.text == "" || numOfPCsInParty == 1)
            {
                TabForPCSlot_0_Txt.gameObject.transform.parent.gameObject.SetActive(false);
            }
            if (TabForPCSlot_1_Txt.text == "" || numOfPCsInParty == 1)
            {
                TabForPCSlot_1_Txt.gameObject.transform.parent.gameObject.SetActive(false);
            }
            if (TabForPCSlot_2_Txt.text == "" || numOfPCsInParty == 1)
            {
                TabForPCSlot_2_Txt.gameObject.transform.parent.gameObject.SetActive(false);
            }
            if (TabForPCSlot_3_Txt.text == "" || numOfPCsInParty == 1)
            {
                TabForPCSlot_3_Txt.gameObject.transform.parent.gameObject.SetActive(false);
            }

            //get a list of the names of the PCs in the party
            List<string> curTabNames = new List<string>();
            foreach (TextMeshProUGUI txt in allPCTabs)
            {
                curTabNames.Add(txt.text);
            }

            //check to see if the lastPCDisplayed is still in the party
            if (curTabNames.Contains(idOfLastPCDisplayed))
            {
                //last PC we had displayed IS in the party, display it
                MatchStatsToOpenCharTab(idOfLastPCDisplayed);
            }
            else
            {
                //display the stats / equipment info for the PCInParty in the lowest PC_Slot
                MatchStatsToOpenCharTab(idOfCharInLowestTab);
            }
        }

        //called when a char_tab(btn) is pressed, when inv window is opened, and when an item is equipped / unequipped
        public void MatchStatsToOpenCharTab(string uniqueID)
        {
            idOfLastPCDisplayed = uniqueID;
            for (int i = 0; i < PCManager.Instance.PC_StatConfigs.Count; i++)
            {
                if (PCManager.Instance.PC_StatConfigs[i].UniqueID == uniqueID)
                {
                    StatsTiedToCurrentCharTab = PCManager.Instance.PC_StatConfigs[i];
                    UpdatePCInfo_UI(StatsTiedToCurrentCharTab);
                }
            }

            //set the colors of the selected and non-selected tabs
            foreach (TextMeshProUGUI txt in allPCTabs)
            {
                Button btn = txt.GetComponentInParent<Button>();
                if (btn != null)
                {
                    if (txt.text == uniqueID)
                    {
                        btn.colors = activeColorBlock;
                    }
                    else
                    {
                        btn.colors = inactiveColorBlock;
                    }
                }
            }
        }

        //called when inv window is opened, when an item is equipped / unequipped, and when the sort/filter options are changed
        public void RefreshInventoryItemList(InvSortMethod sortMethod)
        {
            //make sure that we don't have more items in inventory than we have display slots...
            //if (InventoryManager.Instance.ItemsInInventory.Count <= InvSlots_UI.Count)
            //{
            //determine which items to show based on chosen filter
            List<Item> itemsToShow = new List<Item>();
            foreach (Item itm in InventoryManager.Instance.ItemsInInventory)
            {
                if (ItemMeetsFilterRequirements(itm))
                {
                    itemsToShow.Add(itm);
                }
            }

            //Sort the items to show based on chosen sort method
            switch (sortMethod)
            {
                case InvSortMethod.Alphabetical: // a - z
                    itemsToShow.Sort((a, b) => a.ItemName.CompareTo(b.ItemName));
                    break;
                case InvSortMethod.Qty: // lrgst qty - smallest qty
                    itemsToShow.Sort((b, a) => a.ItemQuantity.CompareTo(b.ItemQuantity));
                    break;
                case InvSortMethod.SellValue: // highest value - lowest value
                    itemsToShow.Sort((b, a) => a.SellValue.CompareTo(b.SellValue));
                    break;
                case InvSortMethod.Rarity: //rare - common
                    itemsToShow.Sort(delegate (Item b, Item a)
                    {
                        int xdiff = a.Rarity.CompareTo(b.Rarity); //sort by rarity first
                        if (xdiff != 0) return xdiff;
                        else
                        {
                            return b.ItemName.CompareTo(a.ItemName); //sort each rarity group alphabetically
                        }
                    });
                    break;
            }

            //loop through the list of display slots in the inv window...
            for (int i = 0; i < InvSlots_UI.Count; i++)
            {
                //if there's an item to show...
                if (i < itemsToShow.Count && itemsToShow[i] != null)
                {
                    InvSlots_UI[i].Slot.SetActive(true);
                    InvSlots_UI[i].ItemInSlot = itemsToShow[i];
                    InvSlots_UI[i].ItemName.color = InvSlots_UI[i].ItemInSlot.GetItemTextColor();
                    InvSlots_UI[i].ItemName.text = InvSlots_UI[i].ItemInSlot.ItemName;
                    if (InvSlots_UI[i].ItemInSlot.IsQuestItem)
                    {
                        InvSlots_UI[i].ItemName.text += " [Quest Item]";
                    }
                    InvSlots_UI[i].Qty.text = InvSlots_UI[i].ItemInSlot.ItemQuantity.ToString();
                    if (InvSlots_UI[i].ItemInSlot.IsQuestItem)
                    {
                        InvSlots_UI[i].Value.text = "---";
                    }
                    else
                    {
                        InvSlots_UI[i].Value.text = InvSlots_UI[i].ItemInSlot.SellValue.ToString();
                    }

                    InvSlots_UI[i].Icon.sprite = InvSlots_UI[i].ItemInSlot.Icon;
                }
                //no item to show, so blank out the inv slot...
                else
                {
                    InvSlots_UI[i].ItemInSlot = null;
                    InvSlots_UI[i].ItemName.text = "";
                    InvSlots_UI[i].Qty.text = "";
                    InvSlots_UI[i].Value.text = "";
                    InvSlots_UI[i].Slot.SetActive(false);
                }
            }
            //}
            //else
            //{
            //Debug.LogWarning("More items in inventory than display slots in inv window");
            //}
        }

        //called by dropdown menu in UI
        public void ChangeCurrentSortMethodAndRefresh()
        {
            switch (sortOptionsDropdown.value)
            {
                case 0:
                    CurrentSortMethod = InvSortMethod.Alphabetical;
                    break;
                case 1:
                    CurrentSortMethod = InvSortMethod.SellValue;
                    break;
                case 2:
                    CurrentSortMethod = InvSortMethod.Qty;
                    break;
                case 3:
                    CurrentSortMethod = InvSortMethod.Rarity;
                    break;
            }

            RefreshInventoryItemList(CurrentSortMethod);
        }

        //called by dropdown menu in UI
        public void ChangeCurrentFilterMethodAndRefresh()
        {
            switch (filterOptionsDropdown.value)
            {
                case 0:
                    CurrentFilterMethod = InvFilterMethod.All;
                    break;
                case 1:
                    CurrentFilterMethod = InvFilterMethod.Equipable;
                    break;
                case 2:
                    CurrentFilterMethod = InvFilterMethod.Useable;
                    break;
            }

            RefreshInventoryItemList(CurrentSortMethod);
        }

        public void OpenInvWindow()
        {
            Inventory_Panel.SetActive(true);

            if (InvWindowJustOpened != null)
            {
                InvWindowJustOpened();
            }

            UpdateTabNames();
            RefreshInventoryItemList(CurrentSortMethod);

            QuestJournal.Instance.CloseQuestJournal();
        }

        public void RefreshInvWindowIfOpen()
        {
            if (!InvWindowIsOpen()) return;

            UpdateTabNames();
            RefreshInventoryItemList(CurrentSortMethod);
        }

        public void CloseInvWindow()
        {
            Inventory_Panel.SetActive(false);
        }

        //assigned to UI button OnClick
        public void ToggleInvWindow()
        {
            if (!InvWindowIsOpen() && OkToOpenInventoryWindow())
            {
                OpenInvWindow();
            }
            else
            {
                CloseInvWindow();
            }
        }

        bool ItemMeetsFilterRequirements(Item itm)
        {
            switch (CurrentFilterMethod)
            {
                case InvFilterMethod.Equipable:
                    if (itm.GetType().IsSubclassOf(typeof(EquipableItem)))
                    {
                        return true;
                    }
                    return false;
                case InvFilterMethod.Useable:
                    if (itm.GetType().IsSubclassOf(typeof(UseableItem)))
                    {
                        return true;
                    }
                    return false;
            }
            return true;
        }

        void UpdatePCInfo_UI(PC_Stats_Config stats)
        {
            if (stats != null)
            {
                PC_Name.text = stats.CharName;
                PC_Portrait.texture = stats.PC_Portrait;
                DeadPCIcon.SetActive(false);
                if (!stats.IsAlive())
                {
                    DeadPCIcon.SetActive(true);
                }
                PC_Lvl.text = stats.CurLvl.ToString();
                PC_HeadSlot.text = stats.EquippedGear.Head != null ? stats.EquippedGear.Head.ItemName : "[Empty]";
                PC_HeadSlot.color = stats.EquippedGear.Head != null ? stats.EquippedGear.Head.GetItemTextColor() : Color32_White;
                PC_ChestSlot.text = stats.EquippedGear.Chest != null ? stats.EquippedGear.Chest.ItemName : "[Empty]";
                PC_ChestSlot.color = stats.EquippedGear.Chest != null ? stats.EquippedGear.Chest.GetItemTextColor() : Color32_White;
                PC_WaistSlot.text = stats.EquippedGear.Waist != null ? stats.EquippedGear.Waist.ItemName : "[Empty]";
                PC_WaistSlot.color = stats.EquippedGear.Waist != null ? stats.EquippedGear.Waist.GetItemTextColor() : Color32_White;
                PC_LegsSlot.text = stats.EquippedGear.Legs != null ? stats.EquippedGear.Legs.ItemName : "[Empty]";
                PC_LegsSlot.color = stats.EquippedGear.Legs != null ? stats.EquippedGear.Legs.GetItemTextColor() : Color32_White;
                PC_FeetSlot.text = stats.EquippedGear.Feet != null ? stats.EquippedGear.Feet.ItemName : "[Empty]";
                PC_FeetSlot.color = stats.EquippedGear.Feet != null ? stats.EquippedGear.Feet.GetItemTextColor() : Color32_White;
                PC_Adorn_1Slot.text = stats.EquippedGear.Adorn_1 != null ? stats.EquippedGear.Adorn_1.ItemName : "[Empty]";
                PC_Adorn_1Slot.color = stats.EquippedGear.Adorn_1 != null ? stats.EquippedGear.Adorn_1.GetItemTextColor() : Color32_White;
                PC_Adorn_2Slot.text = stats.EquippedGear.Adorn_2 != null ? stats.EquippedGear.Adorn_2.ItemName : "[Empty]";
                PC_Adorn_2Slot.color = stats.EquippedGear.Adorn_2 != null ? stats.EquippedGear.Adorn_2.GetItemTextColor() : Color32_White;
                PC_WpnSlot.text = stats.EquippedGear.Weapon != null ? stats.EquippedGear.Weapon.ItemName : "[Empty]";
                PC_WpnSlot.color = stats.EquippedGear.Weapon != null ? stats.EquippedGear.Weapon.GetItemTextColor() : Color32_White;
                PC_CurHP.text = stats.CurHP.ToString();
                PC_MaxHP.text = stats.MaxHP.ToString();
                PC_CurMP.text = stats.CurMP.ToString();
                PC_MaxMP.text = stats.MaxMP.ToString();
                PC_CurTP.text = stats.CurTP.ToString();
                PC_MaxTP.text = stats.MaxTP.ToString();
                PC_STR.text = stats.STR >= 30 ? "30 (MAX)" : stats.STR <= 1 ? "1 (MIN)" : stats.STR.ToString();
                PC_AGI.text = stats.AGI >= 30 ? "30 (MAX)" : stats.AGI <= 1 ? "1 (MIN)" : stats.AGI.ToString();
                PC_INT.text = stats.INT >= 30 ? "30 (MAX)" : stats.INT <= 1 ? "1 (MIN)" : stats.INT.ToString();
                PC_DefMod.text = stats.DEF.ToString();
                PC_DmgRed.text = stats.DmgReduction.ToString();
                PC_AtkBonus.text = stats.BaseAtkBonus.ToString();
                PC_DmgBonus.text = stats.DamageBonus.ToString();
                PC_MagRes.text = stats.MagRes >= 1 ? "100%" : stats.MagRes <= 0 ? "0%" : String.Format("{0:0.}%", Mathf.Round(stats.MagRes * 100));
                PC_InitMod.text = stats.InitiativeMod.ToString();
                PC_CurXP.text = stats.CurXP.ToString();
                PC_XpToNextLvl.text = stats.CurLvl == XPCalculator.MaxLevel ? "0" : (XPCalculator.XPCHart[stats.CurLvl] - stats.CurXP).ToString();
            }
        }

        public bool InvWindowIsOpen()
        {
            return Inventory_Panel.activeSelf;
        }

        bool OkToOpenInventoryWindow()
        {
            return (SceneManager.GetActiveScene().name != "CombatScene" && //no inv window in combat
                SceneManager.GetActiveScene().name != "CharacterSelectScene" && //no inv window on char select screen
                SceneManager.GetActiveScene().name != "MainMenu" && //no inv window on main menu
                ButtonControlPanel.activeSelf && //if the button control panel is hidden, no open window
                !SceneController.Instance.isFading && //no inv window when fading NOTE: currently used to prevent the Inv window from opening before restore data is fully loaded
                !SceneController.Instance.sceneChangeInProgress && //no inv window when changing scenes
                !ItemShopManager.Instance.IsShopping && //no inv window while shopping
                !DialogueManager.Instance.InConversation); //no inv window while talking 
        }

        #region SaveData

        [System.Serializable]
        class Inventory_UISaveData
        {
            //save the sort method
            public InvSortMethod sortMethod;
            //save the filter method
            public InvFilterMethod filterMethod;
            //save the name of the last PC displayed in inv window
            public string lastDisplayedPCName;
        }

        public object CaptureState()
        {
            Inventory_UISaveData saveData = new Inventory_UISaveData();
            saveData.sortMethod = CurrentSortMethod;
            saveData.filterMethod = CurrentFilterMethod;
            saveData.lastDisplayedPCName = idOfLastPCDisplayed;

            return saveData;
        }

        public void RestoreState(object state)
        {
            Inventory_UISaveData saveData = (Inventory_UISaveData)state;
            CurrentSortMethod = saveData.sortMethod;
            CurrentFilterMethod = saveData.filterMethod;
            idOfLastPCDisplayed = saveData.lastDisplayedPCName;

            //update the UI dropdown values...
            switch (CurrentSortMethod)
            {
                case InvSortMethod.Alphabetical:
                    sortOptionsDropdown.value = 0;
                    break;
                case InvSortMethod.SellValue:
                    sortOptionsDropdown.value = 1;
                    break;
                case InvSortMethod.Qty:
                    sortOptionsDropdown.value = 2;
                    break;
                case InvSortMethod.Rarity:
                    sortOptionsDropdown.value = 3;
                    break;
            }

            switch (CurrentFilterMethod)
            {
                case InvFilterMethod.All:
                    filterOptionsDropdown.value = 0;
                    break;
                case InvFilterMethod.Equipable:
                    filterOptionsDropdown.value = 1;
                    break;
                case InvFilterMethod.Useable:
                    filterOptionsDropdown.value = 2;
                    break;
            }
        }
        #endregion
    }
}
