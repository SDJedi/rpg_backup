using TMPro;
using UnityEngine;
using UnityEngine.UI;

//Last Reviewed: 1/27/20
namespace RPG.Inventory
{
    //this script is attached to the ui buttons that are used to buy and sell items
    public class ItemShop_ItemForSale_Slot : MonoBehaviour
    {
        Item itm; //Holds a reference to the actual Item asset in the Resources folder
        Button btn; //button component on this gameObject
        int qty; //used to store ItemQuantity

        public Item ItemAttachedToButton { get { return itm; } set { itm = value; } }


        void Start()
        {
            btn = GetComponent<Button>();
            btn.onClick.AddListener(() => TryBuyOrSellItem());
        }

        void OnDestroy()
        {
            btn.onClick.RemoveAllListeners();
        }

        //gets called when a shop button containing an item is pressed
        bool TryBuyOrSellItem()
        {
            if (itm == null)
            {
                Debug.LogWarning("No item associated with shop button");
                return false;
            }

            //get the qty that the player is trying to buy or sell...
            string qtyAsString = ItemShopManager.Instance.QtyInput_Panel.GetComponentInChildren<TMP_InputField>().text;

            if (qtyAsString == "" || qtyAsString == "0" || qtyAsString == "00")
            {
                SoundManager.Instance.PlayBuzzer();
                ItemShopManager.Instance.txtManager.DisplayMessage("Enter a valid quantity", Color.red);
                return false;
            }

            //qty in the input field is valid, convert it into an int...
            qty = int.Parse(qtyAsString);

            //Buy button has been pressed, Buy_Panel is active, we are trying to BUY ItemAttachedToButton
            if (ItemShopManager.Instance.IsBuying)
            {
                //calculate the price based on the SellValue, priceMarkUP, and the qty
                int itmPrice = ((int)((float)itm.SellValue * ItemShopManager.Instance.priceMarkUp)) * qty;

                //make sure the player has enough GP for the purchase...
                if (InventoryManager.Instance.CurrentGP < itmPrice)
                {
                    SoundManager.Instance.PlayBuzzer();
                    ItemShopManager.Instance.txtManager.DisplayMessage("You have insufficient GP", Color.red);
                    return false;
                }

                //make sure we are not trying to buy more than we can hold...
                if (InventoryManager.Instance.GetQtyOfAnItemInInventory(itm) + qty > Item.maxQty)
                {
                    SoundManager.Instance.PlayBuzzer();
                    ItemShopManager.Instance.txtManager.DisplayMessage("That purchase would put you over the maximum quantity allowed", Color.red);

                    //we tried to buy more than we could hold ... for convenience, set the desired qty to the max the player can buy, then exit the method
                    int maxCouldBuy = Item.maxQty - InventoryManager.Instance.GetQtyOfAnItemInInventory(itm);
                    if (maxCouldBuy <= 0)
                    {
                        maxCouldBuy = 1;
                    }
                    ItemShopManager.Instance.QtyInput_Panel.GetComponentInChildren<TMP_InputField>().text = maxCouldBuy.ToString();
                    return false;
                }

                //We have an item, we have the funds, we're not at max : we're good to add an item...
                InventoryManager.Instance.AddItemToInventory(Instantiate(itm), qty);

                //deduct the cost of the item
                InventoryManager.Instance.ModifyGPAmnt(-itmPrice);

                //purchase success...
                SoundManager.Instance.PlayAudioClip(ItemShopManager.Instance.ChaChing);
                return true;
            }

            //Sell button has been pressed, Sell_Panel is active, we are trying to SELL ItemAttachedToButton
            if (ItemShopManager.Instance.IsSelling)
            {
                //we can't sell more of an item than we have... fix qty if necessary
                if (qty > InventoryManager.Instance.GetQtyOfAnItemInInventory(itm))
                {
                    qty = InventoryManager.Instance.GetQtyOfAnItemInInventory(itm);
                    ItemShopManager.Instance.QtyInput_Panel.GetComponentInChildren<TMP_InputField>().text = qty.ToString();
                }

                int GPFromSale = itm.SellValue * qty;

                //remove the proper qty of the item and add GP
                InventoryManager.Instance.SubtractItemFromInventory(itm, qty);
                InventoryManager.Instance.ModifyGPAmnt(GPFromSale);

                //Important: refresh the UI...
                ItemShopManager.Instance.OnSellBtnPressed();

                //sale success...
                SoundManager.Instance.PlayAudioClip(ItemShopManager.Instance.ChaChing);
                return true;
            }

            //Not buying or selling? should never reach here...
            Debug.LogWarning("Check logic ... should always be either buying or selling here...");
            return false;
        }
    }
}
