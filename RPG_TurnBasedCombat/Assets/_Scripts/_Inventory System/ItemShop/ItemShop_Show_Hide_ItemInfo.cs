using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

//Last Reviewed: 1/27/20
namespace RPG.Inventory
{
    //this script is attached to the ui buttons that are used to buy and sell items
    //used to display info about a shop item when the mouse enters the button
    public class ItemShop_Show_Hide_ItemInfo : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public GameObject ItemInfoPanel;
        public TextMeshProUGUI DescriptionText;
        public TextMeshProUGUI QtyInInvText;
        public TextMeshProUGUI TotalCost;
        public TextMeshProUGUI useableBy_Txt;
        public TextMeshProUGUI minLvl_Txt;

        Button btn;


        void Start()
        {
            btn = GetComponent<Button>();
            btn.onClick.AddListener(() => Invoke("OnBtnClick", 0.1f));
        }

        void OnDestroy()
        {
            if (btn)
            {
                btn.onClick.RemoveAllListeners();
            }
        }

        //display item info when mouse enters the button...
        public void OnPointerEnter(PointerEventData eventData)
        {
            Item itm = GetComponent<ItemShop_ItemForSale_Slot>().ItemAttachedToButton;

            if (itm != null)
            {
                ItemInfoPanel.SetActive(true);
                DescriptionText.text = itm.ItemDescription;
                useableBy_Txt.text = itm.GetNamesOfPCsThatAreAvailableAndCanUseOrEquipItem();
                minLvl_Txt.text = itm.MinLvl.ToString();
                QtyInInvText.text = InventoryManager.Instance.GetQtyOfAnItemInInventory(itm).ToString();

                if (ItemShopManager.Instance.IsBuying)
                {
                    int selectedQty = int.Parse(ItemShopManager.Instance.QtyInput_Panel.GetComponentInChildren<TMP_InputField>().text);
                    int pricePerItem = (int)((float)itm.SellValue * ItemShopManager.Instance.priceMarkUp);
                    TotalCost.text = (selectedQty * pricePerItem).ToString();
                }

                if (ItemShopManager.Instance.IsSelling)
                {
                    int selectedQty = int.Parse(ItemShopManager.Instance.QtyInput_Panel.GetComponentInChildren<TMP_InputField>().text);
                    int qtyInInv = InventoryManager.Instance.GetQtyOfAnItemInInventory(itm);
                    if (selectedQty > qtyInInv)
                    {
                        selectedQty = qtyInInv;
                        ItemShopManager.Instance.QtyInput_Panel.GetComponentInChildren<TMP_InputField>().text = selectedQty.ToString();
                    }
                    int pricePerItem = (int)((float)itm.SellValue);
                    TotalCost.text = (selectedQty * pricePerItem).ToString();
                }
            }
        }

        //hide info panel when mouse leaves btn
        public void OnPointerExit(PointerEventData eventData)
        {
            ItemInfoPanel.SetActive(false);
        }

        //when btn is clicked, an item is bought or sold, 
        //when an item is sold, it can change what item is attached to btn (e.g. when the last of an item is sold and the item is removed from inv)
        //aftre a short delay, make sure that the proper info is being displayed
        void OnBtnClick()
        {
            //get the current item...
            Item itm = GetComponent<ItemShop_ItemForSale_Slot>().ItemAttachedToButton;

            //if item isn't null, update the info...
            if (itm != null)
            {
                DescriptionText.text = itm.ItemDescription;
                QtyInInvText.text = InventoryManager.Instance.GetQtyOfAnItemInInventory(itm).ToString();
                minLvl_Txt.text = itm.MinLvl.ToString();
                useableBy_Txt.text = itm.GetNamesOfPCsThatAreAvailableAndCanUseOrEquipItem();

                if (ItemShopManager.Instance.IsSelling)
                {
                    int selectedQty = int.Parse(ItemShopManager.Instance.QtyInput_Panel.GetComponentInChildren<TMP_InputField>().text);
                    int qtyInInv = InventoryManager.Instance.GetQtyOfAnItemInInventory(itm);
                    if (selectedQty > qtyInInv)
                    {
                        selectedQty = qtyInInv;
                        ItemShopManager.Instance.QtyInput_Panel.GetComponentInChildren<TMP_InputField>().text = selectedQty.ToString();
                    }

                    TotalCost.text = (selectedQty * itm.SellValue).ToString();
                }
            }

            if (!gameObject.activeSelf)
            {
                ItemInfoPanel.SetActive(false);
            }
        }
    }
}
