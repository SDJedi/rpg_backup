using System.Collections.Generic;
using RPG.InteractableSystem;
using RPG.Messaging;
using RPG.SceneControl;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

//Last Reviewed: 1/27/20
namespace RPG.Inventory
{
    //Handles opening, displaying, and closing an item shop
    public class ItemShopManager : Singleton<ItemShopManager>
    {
        public GameObject ItemShop_Panel; //main panel
        public GameObject InitialOptions_Panel; //buy, sell, exit
        public GameObject Buy_Panel; //shows when buy button pressed
        public GameObject Sell_Panel; //shows when sell button pressed
        public GameObject QtyInput_Panel; //allows player to set a qty for buying and selling

        //Text fields
        public TextMeshProUGUI ShopName;
        public TextMeshProUGUI ShopGreeting;
        public TextMeshProUGUI MessageTxt;

        public List<Item> AvailableItems; //the items the current shop carries

        public List<Button> ItemsForSale_Btns; //buttons holding shop items [25 buttons]
        public List<Button> ItemsInInventory_Btns; //buttons holding items that the player can sell [50 buttons]

        public AudioClip ChaChing; //Sound effect on purchase / sale
        public float priceMarkUp = 0; //multiplier for the SellValue of an item to determine cost, set by the ItemShop

        public bool IsShopping { get { return ItemShop_Panel.activeSelf; } }
        public bool IsBuying { get { return Buy_Panel.activeSelf; } }
        public bool IsSelling { get { return Sell_Panel.activeSelf; } }

        public TextManager txtManager { get; private set; } //manages the error messages that pop up on buy / sell failures

        ReactionCollection OnExitReactions = null;


        void Start()
        {
            txtManager = GetComponent<TextManager>();
            txtManager.text = MessageTxt;
            //close the shop if scene happens to change while shopping
            SceneController.Instance.BeforeSceneUnload += OnExitBtnPressed;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (SceneController.InstanceExists)
            {
                SceneController.Instance.BeforeSceneUnload -= OnExitBtnPressed;
            }
        }

        //called by a Reaction ... sets up and opens an ItemShop
        public void SetUpNewItemShop(ItemShop newShop)
        {
            AvailableItems = new List<Item>(newShop.ItemsInStock);
            ShopName.text = newShop.NameOfShop;
            ShopGreeting.text = newShop.ShopGreeting;
            priceMarkUp = newShop.PriceMarkUp;
            ShowItemShopPanel();
        }

        //called when "Buy" button of ItemShop is pressed
        public void OnBuyBtnPressed()
        {
            StockTheShelves();
            HideInitialoptionsPanel();
            ShowBuyPanel();
            ShowQtyInputPanel();
            QtyInput_Panel.GetComponentInChildren<TMP_InputField>().text = "1";
        }

        //called when "Sell" button of ItemShop is pressed, AND every time an item is sold to refresh the UI
        public void OnSellBtnPressed()
        {
            LoadInvIntoButtons();
            HideInitialoptionsPanel();
            ShowSellPanel();
            ShowQtyInputPanel();
            QtyInput_Panel.GetComponentInChildren<TMP_InputField>().text = "1";
        }

        //called when "Back" button of ItemShop is pressed
        public void OnBackButtonPressed()
        {
            HideBuyPanel();
            HideSellPanel();
            HideQtyInputPanel();
            ShowInitialOptionsPanel();
        }

        //called when "Exit" button of ItemShop is pressed, AND when the scene changes
        public void OnExitBtnPressed()
        {
            ShowInitialOptionsPanel();
            HideBuyPanel();
            HideSellPanel();
            HideQtyInputPanel();
            HideItemShopPanel();

            if (OnExitReactions != null)
            {
                OnExitReactions.React();
                OnExitReactions = null;
            }
        }

        //buttons will represent items for us to buy... called by OnBuyBtnPressed()
        void StockTheShelves()
        {
            //Bad ... we have more items than buttons...
            if (AvailableItems.Count > ItemsForSale_Btns.Count)
            {
                Debug.LogWarning("Trying to add too many items to shop. Max is " + ItemsForSale_Btns.Count);
            }

            //sort shop inventory by rarity / alphabetical...
            AvailableItems.RemoveAll(n => n == null);
            AvailableItems.Sort(delegate (Item b, Item a)
            {
                int xdiff = a.Rarity.CompareTo(b.Rarity);
                if (xdiff != 0) return xdiff;
                else
                {
                    return b.ItemName.CompareTo(a.ItemName);
                }
            });

            //loop through each button in the ItemsForSale_Btns List...
            for (int i = 0; i < ItemsForSale_Btns.Count; i++)
            {
                if (i < AvailableItems.Count)
                {
                    //Establish the item's price based on shop mark up...
                    int price = (int)((float)AvailableItems[i].SellValue * priceMarkUp);
                    //"Attach" the item at index i to the button at index i...
                    ItemsForSale_Btns[i].gameObject.GetComponent<ItemShop_ItemForSale_Slot>().ItemAttachedToButton = AvailableItems[i];
                    //Make this button visible... 
                    ItemsForSale_Btns[i].gameObject.SetActive(true);
                    //Set the Name, Price, and Icon of the item in the UI...
                    ItemsForSale_Btns[i].gameObject.transform.Find("ItemName_Txt").GetComponent<TextMeshProUGUI>().text = AvailableItems[i].ItemName;
                    ItemsForSale_Btns[i].gameObject.transform.Find("ItemName_Txt").GetComponent<TextMeshProUGUI>().color = AvailableItems[i].GetItemTextColor();
                    ItemsForSale_Btns[i].gameObject.transform.Find("ItemPrice_Txt").GetComponent<TextMeshProUGUI>().text = price.ToString();
                    ItemsForSale_Btns[i].gameObject.transform.Find("ItemIcon_Img").GetComponent<Image>().sprite = AvailableItems[i].Icon;
                }
                else
                {
                    //Hide all empty buttons
                    ItemsForSale_Btns[i].gameObject.SetActive(false);
                    ItemsForSale_Btns[i].gameObject.GetComponent<ItemShop_ItemForSale_Slot>().ItemAttachedToButton = null;
                }
            }
        }

        //buttons will represent items we can sell... called by OnSellBtnPressed()
        void LoadInvIntoButtons()
        {
            //ensure no null entries in inventory list
            InventoryManager.Instance.ItemsInInventory.RemoveAll(n => n == null);

            //Sort the items based on current inventory sort method
            switch (Inventory_UI.Instance.CurrentSortMethod)
            {
                case InvSortMethod.Alphabetical: // a - z
                    InventoryManager.Instance.ItemsInInventory.Sort((a, b) => a.ItemName.CompareTo(b.ItemName));
                    break;
                case InvSortMethod.Qty: // lrgst qty - smallest qty
                    InventoryManager.Instance.ItemsInInventory.Sort((b, a) => a.ItemQuantity.CompareTo(b.ItemQuantity));
                    break;
                case InvSortMethod.SellValue: // highest value - lowest value
                    InventoryManager.Instance.ItemsInInventory.Sort((b, a) => a.SellValue.CompareTo(b.SellValue));
                    break;
                case InvSortMethod.Rarity: //rare - common
                    InventoryManager.Instance.ItemsInInventory.Sort(delegate (Item b, Item a)
                    {
                        int xdiff = a.Rarity.CompareTo(b.Rarity); //sort by rarity first
                        if (xdiff != 0) return xdiff;
                        else
                        {
                            return b.ItemName.CompareTo(a.ItemName); //sort each rarity group alphabetically
                        }
                    });
                    break;
            }

            //loop through each button in the ItemsInInventory_Btns List...
            for (int i = 0; i < ItemsInInventory_Btns.Count; i++)
            {
                //if we have an item in inventory at index i...
                if (i < InventoryManager.Instance.ItemsInInventory.Count && !InventoryManager.Instance.ItemsInInventory[i].IsQuestItem)
                {
                    //find the item's value...
                    int value = InventoryManager.Instance.ItemsInInventory[i].SellValue;
                    //assign the item to button[i]...
                    ItemsInInventory_Btns[i].gameObject.GetComponent<ItemShop_ItemForSale_Slot>().ItemAttachedToButton = InventoryManager.Instance.ItemsInInventory[i];
                    //Make btn visible...
                    ItemsInInventory_Btns[i].gameObject.SetActive(true);
                    //Set the Name, Value, and Icon of the item in the UI...
                    ItemsInInventory_Btns[i].gameObject.transform.Find("ItemName_Txt").GetComponent<TextMeshProUGUI>().text = InventoryManager.Instance.ItemsInInventory[i].ItemName;
                    ItemsInInventory_Btns[i].gameObject.transform.Find("ItemName_Txt").GetComponent<TextMeshProUGUI>().color = InventoryManager.Instance.ItemsInInventory[i].GetItemTextColor();
                    ItemsInInventory_Btns[i].gameObject.transform.Find("ItemPrice_Txt").GetComponent<TextMeshProUGUI>().text = value.ToString();
                    ItemsInInventory_Btns[i].gameObject.transform.Find("ItemIcon_Img").GetComponent<Image>().sprite = InventoryManager.Instance.ItemsInInventory[i].Icon;
                }
                else
                {
                    //no item, no need for a button to show...
                    ItemsInInventory_Btns[i].gameObject.SetActive(false);
                    ItemsInInventory_Btns[i].gameObject.GetComponent<ItemShop_ItemForSale_Slot>().ItemAttachedToButton = null;
                }
            }
        }

        //Set by ShowItemShopPanelReaction.cs
        public void SetOnExitReactions(ReactionCollection rc)
        {
            OnExitReactions = rc;
        }

        #region Methods to show / hide ItemShop UI
        void ShowItemShopPanel()
        {
            ItemShop_Panel.SetActive(true);
        }

        void HideItemShopPanel()
        {
            ItemShop_Panel.SetActive(false);
        }

        void ShowBuyPanel()
        {
            Buy_Panel.SetActive(true);
        }

        void HideBuyPanel()
        {
            Buy_Panel.SetActive(false);
        }

        void ShowSellPanel()
        {
            Sell_Panel.SetActive(true);
        }

        void HideSellPanel()
        {
            Sell_Panel.SetActive(false);
        }

        void ShowInitialOptionsPanel()
        {
            InitialOptions_Panel.SetActive(true);
        }

        void HideInitialoptionsPanel()
        {
            InitialOptions_Panel.SetActive(false);
        }

        void ShowQtyInputPanel()
        {
            QtyInput_Panel.SetActive(true);
        }

        void HideQtyInputPanel()
        {
            QtyInput_Panel.SetActive(false);
        }
        #endregion
    }
}
