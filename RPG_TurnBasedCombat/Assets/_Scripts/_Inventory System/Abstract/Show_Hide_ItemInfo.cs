using System.Collections.Generic;
using RPG.CombatSystem;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//Last Reviewed: 1/26/20
namespace RPG.Inventory
{
    //base class used to show tooltips for items when mouse hovers over a button
    //slightly different implementations for equipped, in-combat, and in-inventory buttons
    public abstract class Show_Hide_ItemInfo : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        protected Vector2 panelPosition;
        protected GameObject itemInfoPanel;
        protected TextMeshProUGUI itemName_Txt;
        protected TextMeshProUGUI itemDescription_Txt;
        protected TextMeshProUGUI useableBy_Txt;
        protected TextMeshProUGUI minLvl_Txt;
        protected Button btn;
        protected Item itemToShow;


        protected virtual void Start()
        {
            SetPanelPosition();
            InitializeFields();
            btn = GetComponent<Button>();
            if (btn != null && SceneManager.GetActiveScene().name != "CombatScene")
            {
                btn.onClick.AddListener(() => Invoke("OnBtnClick", 0.1f));
            }
        }

        protected virtual void OnDestroy()
        {
            if (btn)
            {
                btn.onClick.RemoveAllListeners();
            }
        }

        void OnDisable()
        {
            if (itemInfoPanel)
            {
                itemInfoPanel.SetActive(false);
            }
        }

        void InitializeFields()
        {
            ItemInfo_Panel_Fields fields = GameObject.FindObjectOfType<ItemInfo_Panel_Fields>();
            itemInfoPanel = fields.ItemInfoPanel;
            itemName_Txt = fields.ItemName_Txt;
            itemDescription_Txt = fields.ItemDescription_Txt;
            useableBy_Txt = fields.UseableBy_Txt;
            minLvl_Txt = fields.MinLvl_Txt;
        }

        //show the tooltip
        public void OnPointerEnter(PointerEventData eventData)
        {
            PositionPanel();
            Item itm = GetItemToShow();
            if (itm != null)
            {
                itemInfoPanel.SetActive(true);
                itemName_Txt.text = itm.ItemName;
                itemDescription_Txt.text = itm.ItemDescription;
                minLvl_Txt.text = itm.MinLvl.ToString();
                useableBy_Txt.text = itm.GetNamesOfPCsThatAreAvailableAndCanUseOrEquipItem();
            }
        }

        void PositionPanel()
        {
            itemInfoPanel.GetComponent<RectTransform>().anchoredPosition = new Vector3(panelPosition.x, panelPosition.y, 0);
        }

        //hide the tooltip
        public void OnPointerExit(PointerEventData eventData)
        {
            itemInfoPanel.SetActive(false);
        }

        //adjust the tooltip if the item in the slot changes after button is clicked
        protected virtual void OnBtnClick()
        {
            Item itm = GetItemToShow();
            if (itm != null)
            {
                itemInfoPanel.SetActive(true);
                itemName_Txt.text = itm.ItemName;
                itemDescription_Txt.text = itm.ItemDescription;
                useableBy_Txt.text = itm.GetNamesOfPCsThatAreAvailableAndCanUseOrEquipItem();
            }

            if (!gameObject.activeSelf || itm == null)
            {
                itemInfoPanel.SetActive(false);
            }
        }

        abstract protected Item GetItemToShow();
        abstract protected void SetPanelPosition();
    }
}
