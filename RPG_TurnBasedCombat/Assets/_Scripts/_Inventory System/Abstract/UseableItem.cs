using System.Collections.Generic;
using RPG.CombatSystem;
using UnityEngine;

//Last Reviewed: 1/26/20
namespace RPG.Inventory
{
    //base class for items that are useable
    public abstract class UseableItem : Item
    {
        public bool UseableInCombat = true;
        public bool CombatOnly = false;
        public AudioClip OnUseSoundEffect;
        public GameObject ParticleEffect;
        public TargetType TargetType;

        public abstract void UseItem(Base_Stats_Config caster, List<Base_Stats_Config> targets);
    }
}
