using RPG.CombatSystem;
using UnityEngine;

//Last Reviewed: 1/26/20
namespace RPG.Inventory
{
    public enum EquipmentSlot { Head, Chest, Waist, Legs, Feet, Adornment, Weapon }

    //base class for items that can be equipped by PCs
    [System.Serializable]
    public abstract class EquipableItem : Item
    {
        [SerializeField] protected EquipmentSlot slotToEquipIn;

        public EquipmentSlot SlotToEquipIn { get { return slotToEquipIn; } }


        public abstract void OnEquip(PC_Stats_Config PC);
        public abstract void OnUnequip(PC_Stats_Config PC);
    }
}
