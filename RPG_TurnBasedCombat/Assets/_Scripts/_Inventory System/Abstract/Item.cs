using System.Collections.Generic;
using RPG.CombatSystem;
using RPG.Messaging;
using UnityEngine;

//Last Reviewed: 1/26/20
namespace RPG.Inventory
{
    public enum ItemRarity { Common, Uncommon, Rare, UltraRare, Quest }
    public enum UseableBy { Any, Saul, Karl, Babz, Billy, Jimbo, Lance, Lisa, SirJohn, Ed }

    //base class for all items in the game
    [System.Serializable]
    public abstract class Item : ScriptableObject
    {
        public const int maxQty = 99; //max qty allowed of any particular item
        #region Item Rarity Colors
        Color32 common = new Color32(0, 255, 0, 255);
        public const string common_h = "#00FF00"; //green

        Color32 uncommon = new Color32(0, 146, 255, 255);
        public const string uncommon_h = "#0092FF"; //blue

        Color32 rare = new Color32(190, 85, 255, 255); //purple
        public const string rare_h = "#BE55FF";

        Color32 ultraRare = new Color32(255, 165, 0, 255); //orange
        public const string ultraRare_h = "#FFA500";

        Color32 quest = new Color32(255, 215, 0, 255); //gold
        public const string quest_h = "#FFD700";
        #endregion

        [SerializeField] Sprite icon = null;
        [SerializeField] string itemName = "";
        [Space(20)]
        [SerializeField] int minimumLevel = 1;
        [SerializeField] ItemRarity rarity = ItemRarity.Common;
        [SerializeField] List<UseableBy> pCsThatCanUse = new List<UseableBy> { UseableBy.Any };
        [SerializeField] int sellValue = 0; // between 0 and 9999, enforced by OnValidate()
        [Space(20)]
        [TextArea(3, 7)]
        [SerializeField] string itemDescription = "";

        int itemQuantity = 1; //the base qty of all items == 1, we only want to change this at runtime on instances of items, never on the item So in Resources

        public virtual ItemRarity Rarity { get { return rarity; } }
        public List<UseableBy> PCsThatCanUse { get { return pCsThatCanUse; } }
        public Sprite Icon { get { return icon; } }
        public string ItemName { get { return itemName; } }
        public int ItemQuantity { get { return itemQuantity; } }
        public int SellValue { get { return sellValue; } }
        public string ItemDescription { get { return itemDescription; } }
        public int MinLvl { get { return minimumLevel; } }
        public virtual bool IsQuestItem { get { return false; } }


        //Add or Subtract an item from inv ... Should ONLY be called from InventoryManager.cs
        public void ModifyItemQuantity(int amnt)
        {
            //TODO: check that this is an instance of the SO, and not the SO in Resources
            if (itemQuantity + amnt <= 0)
            {
                if (itemQuantity + amnt < 0)
                {
                    Debug.LogWarning(this.itemName + " had qty: " + itemQuantity + ". Tried to modify by " + amnt + ".");
                }

                itemQuantity = 1; //min qty of an item == 1 ...
                return;
            }

            if (itemQuantity + amnt > maxQty)
            {
                int wastedAmnt = (itemQuantity + amnt) - maxQty;
                SoundManager.Instance.PlayBuzzer();
                NotificationWindowManager.Instance.DisplayNotification("Max qty (" + maxQty + ") reached for " + this.itemName + $". {wastedAmnt} item(s) went to waste", Color.red);

                itemQuantity = maxQty;
                return;
            }

            itemQuantity += amnt;
        }

        //returns true if the PCName shows in the pCsThatCanUse List
        public bool PCCanUseItem(string PCName)
        {
            if (pCsThatCanUse.Contains(UseableBy.Any))
            {
                return true;
            }

            switch (PCName)
            {
                case "Saul":
                    return pCsThatCanUse.Contains(UseableBy.Saul);
                case "Karl":
                    return pCsThatCanUse.Contains(UseableBy.Karl);
                case "Babz":
                    return pCsThatCanUse.Contains(UseableBy.Babz);
                case "Billy":
                    return pCsThatCanUse.Contains(UseableBy.Billy);
                case "Jimbo":
                    return pCsThatCanUse.Contains(UseableBy.Jimbo);
                case "Lance":
                    return pCsThatCanUse.Contains(UseableBy.Lance);
                case "Lisa":
                    return pCsThatCanUse.Contains(UseableBy.Lisa);
                case "Sir John":
                    return pCsThatCanUse.Contains(UseableBy.SirJohn);
                case "Ed":
                    return pCsThatCanUse.Contains(UseableBy.Ed);
                default:
                    Debug.LogWarning("invalid PC name");
                    return false;
            }
        }

        //return the item's text color based on rarity
        public Color32 GetItemTextColor()
        {
            switch (rarity)
            {
                case ItemRarity.Common:
                    return common;
                case ItemRarity.Uncommon:
                    return uncommon;
                case ItemRarity.Rare:
                    return rare;
                case ItemRarity.UltraRare:
                    return ultraRare;
                case ItemRarity.Quest:
                    return quest;
                default:
                    return new Color32(255, 255, 255, 255); //white
            }
        }

        //return the item's text color based on rarity
        public string GetItemTextColorAsHexString()
        {
            switch (rarity)
            {
                case ItemRarity.Common:
                    return common_h;
                case ItemRarity.Uncommon:
                    return uncommon_h;
                case ItemRarity.Rare:
                    return rare_h;
                case ItemRarity.UltraRare:
                    return ultraRare_h;
                case ItemRarity.Quest:
                    return quest_h;
                default:
                    return "#FFFFFF";
            }
        }

        public string GetNamesOfPCsThatAreAvailableAndCanUseOrEquipItem()
        {
            if (PCsThatCanUse.Contains(UseableBy.Any))
            {
                return "Any";
            }

            string finalListOfNames = "";

            List<string> allAvailablePCNames = new List<string>();

            //add names of other PCs that are currently able to be put into the party to the List
            for (int i = 0; i < PCManager.Instance.AcquiredTeamMembers.Count; i++)
            {
                if (PCManager.Instance.AcquiredTeamMembers[i].IsAvailable)
                {
                    allAvailablePCNames.Add(PCManager.Instance.AcquiredTeamMembers[i].PC_Name);
                }
            }

            //loop through the item's list of PCs that can use it
            for (int i = 0; i < PCsThatCanUse.Count; i++)
            {
                //Saul
                if (PCsThatCanUse[i] == UseableBy.Saul)
                {
                    finalListOfNames += "Saul, ";
                }
                //Karl
                if (PCsThatCanUse[i] == UseableBy.Karl && allAvailablePCNames.Contains("Karl"))
                {
                    finalListOfNames += "Karl, ";
                }
                //Babz
                if (PCsThatCanUse[i] == UseableBy.Babz && allAvailablePCNames.Contains("Babz"))
                {
                    finalListOfNames += "Babz, ";
                }
                //Billy
                if (PCsThatCanUse[i] == UseableBy.Billy && allAvailablePCNames.Contains("Billy"))
                {
                    finalListOfNames += "Billy, ";
                }
                //Jimbo
                if (PCsThatCanUse[i] == UseableBy.Jimbo && allAvailablePCNames.Contains("Jimbo"))
                {
                    finalListOfNames += "Jimbo, ";
                }
                //Lance
                if (PCsThatCanUse[i] == UseableBy.Lance && allAvailablePCNames.Contains("Lance"))
                {
                    finalListOfNames += "Lance, ";
                }
                //Lisa
                if (PCsThatCanUse[i] == UseableBy.Lisa && allAvailablePCNames.Contains("Lisa"))
                {
                    finalListOfNames += "Lisa, ";
                }
                //Sir John
                if (PCsThatCanUse[i] == UseableBy.SirJohn && allAvailablePCNames.Contains("Sir John"))
                {
                    finalListOfNames += "Sir John, ";
                }
                //Ed
                if (PCsThatCanUse[i] == UseableBy.Ed && allAvailablePCNames.Contains("Ed"))
                {
                    finalListOfNames += "Ed, ";
                }
            }

            if (finalListOfNames == "")
            {
                finalListOfNames = "???";
            }
            else
            {
                //we remove two characters at the end of the string ... a comma and a space
                finalListOfNames = finalListOfNames.Remove(finalListOfNames.Length - 2);
            }

            return finalListOfNames;
        }

        //clamp value of itemCost in inspector...
        void OnValidate()
        {
            sellValue = Mathf.Clamp(sellValue, 0, 9999);
            if (IsQuestItem)
            {
                sellValue = 0;
                rarity = ItemRarity.Quest;
            }
        }
    }
}
