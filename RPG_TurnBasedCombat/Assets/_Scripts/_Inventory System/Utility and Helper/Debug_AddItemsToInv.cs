using System.Collections.Generic;
using UnityEngine;

//Last Reviewed: 1/20/22
namespace RPG.Inventory
{
    //temp class that allows me to add items into inv at runtime for testing
    public class Debug_AddItemsToInv : MonoBehaviour
    {
        [System.Serializable]
        public class ItemAndQty
        {
            public Item item;
            public int qty = 1;
        }

        [SerializeField] List<ItemAndQty> itemsToAdd = new List<ItemAndQty>();


        public void AddItems()
        {
            foreach (ItemAndQty itm in itemsToAdd)
            {
                if (itm.item == null || itm.qty < 1) continue;

                InventoryManager.Instance.AddItemToInventory(Instantiate(itm.item), itm.qty);
            }
            Inventory_UI.Instance.RefreshInventoryItemList(Inventory_UI.Instance.CurrentSortMethod);
        }
    }
}