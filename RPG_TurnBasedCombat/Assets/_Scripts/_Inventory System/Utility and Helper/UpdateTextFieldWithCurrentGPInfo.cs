using TMPro;
using UnityEngine;

//Last Reviewed: 1/30/20
namespace RPG.Inventory
{
    //Keeps any text field tracking GP up to date ... used in inventory and shop windows
    public class UpdateTextFieldWithCurrentGPInfo : MonoBehaviour
    {
        TextMeshProUGUI txtFieldforGPInfo;

        void Start()
        {
            InventoryManager.Instance.OnGPChange += OnGPChange;
            txtFieldforGPInfo = GetComponent<TextMeshProUGUI>();
            txtFieldforGPInfo.text = InventoryManager.Instance.CurrentGP.ToString();
        }

        void OnDestroy()
        {
            if (InventoryManager.InstanceExists)
            {
                InventoryManager.Instance.OnGPChange -= OnGPChange;
            }
        }

        void OnGPChange()
        {
            txtFieldforGPInfo.text = InventoryManager.Instance.CurrentGP.ToString();
        }
    }
}
