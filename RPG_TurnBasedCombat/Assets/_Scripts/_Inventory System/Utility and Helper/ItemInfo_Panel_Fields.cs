using TMPro;
using UnityEngine;

//Last Reviewed: 2/17/20
namespace RPG.Inventory
{
    //stores the panel and fields for the item tooltip.
    //attached to BootScene_Canvas
    public class ItemInfo_Panel_Fields : MonoBehaviour
    {
        [SerializeField] GameObject itemInfoPanel = null;
        [SerializeField] TextMeshProUGUI itemName_Txt = null;
        [SerializeField] TextMeshProUGUI itemDescription_Txt = null;
        [SerializeField] TextMeshProUGUI useableBy_Txt = null;
        [SerializeField] TextMeshProUGUI minLvl_Txt = null;

        public GameObject ItemInfoPanel { get { return itemInfoPanel; } }
        public TextMeshProUGUI ItemName_Txt { get { return itemName_Txt; } }
        public TextMeshProUGUI ItemDescription_Txt { get { return itemDescription_Txt; } }
        public TextMeshProUGUI UseableBy_Txt { get { return useableBy_Txt; } }
        public TextMeshProUGUI MinLvl_Txt { get { return minLvl_Txt; } }
    }
}
