using TMPro;
using UnityEngine;
using UnityEngine.UI;

//Last Reviewed: 1/30/20
namespace RPG.Inventory
{
    //Used for the ItemShop window that displays error messages
    public class HideMsgWindowWhenNoText : MonoBehaviour
    {
        TextMeshProUGUI TextToWatch;
        Image ImgToHide;

        void Start()
        {
            TextToWatch = GetComponentInChildren<TextMeshProUGUI>();
            ImgToHide = GetComponent<Image>();
        }

        void Update()
        {
            if (ImgToHide.enabled && TextToWatch.text == "")
            {
                ImgToHide.enabled = false;
            }
            else if (TextToWatch.text != "")
            {
                ImgToHide.enabled = true;
            }
        }
    }
}
